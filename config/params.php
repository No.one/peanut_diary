<?php
define('WEB_URL', 'http://'.@$_SERVER['HTTP_HOST']);
return [
    'title' => '优哩优哩',
    'domain' => [
        'www' => WEB_URL,
        'm' => WEB_URL.'/m',
        'manage' => WEB_URL.'/manage',
        'api' => WEB_URL.'/api',
    ],
    'web_menu'=>[
        'web_menu_info'=>[],
        'url' => []
    ],
    'upload' => [
        'avatar' => '/uploads/avatar',
        'advert' => '/uploads/advert',
        'cover' => '/uploads/cover',
        'recommend' => '/uploads/recommend',
        'goods_cate' => '/uploads/goods_cate',
        'goods' => '/uploads/goods',
        'integral' => '/uploads/integral',
        'integral_cate' => '/uploads/integral_cate',
        'shop_logo' => '/uploads/shop_logo',
        'shop_banner' => '/uploads/shop_banner',
        'shop_cate' => '/uploads/shop_cate',
        'suggest' => '/uploads/suggest',
        'customer' => '/uploads/customer',
        'propaganda' => '/uploads/propaganda',
        'hot' => '/uploads/hot',
    ],
    'api' => [
        'DATA_TRANSFER_KEY' => 'ead5de99e3dfe933ef56bd2ff6e08886', //和外部通信时用于加密参数的秘钥
    ],
    'sms' => [
        'common' => [
            'url' => 'http://114.55.25.138/msg/HttpBatchSendSM',
            'account' => 'Thxy2018_youliyouli',
            'pswd' => 'Youlitz2018@'
        ],
    ],
    'kuaidi100' => [
        'customer' => 'BFDE30582788B2BB712DEF1F858F9699',
        'key' => 'esrtvzei4023',
        'url' => 'http://poll.kuaidi100.com/poll/query.do',
    ], 
    'tbk' => [
        //优哩优利
//        'appkey' => '24894093',
//        'secretKey' => '933c880d4a8a161746b0c334370d0f69',
//        'zoneId' => '1675826392',
//        'pid' => 'mm_27498931_45370901_1675826392',
//        'appkey' => '25078628',
//        'secretKey' => 'd5dee076b8c96abde01bea49bf3748e6',
//        'zoneId' => '27346050461',
//        'pid' => 'mm_133089627_46626979_27346050461',
//        'token' => '70002100138174d226ccacba9e7b8a616880d140bb655f64471d52c47ecf41773390e723949552095',
        'appkey' => '24950323',
        'secretKey' => 'e82f58fc0feb39b5584162b5b4ab62f1',
        'zoneId' => '1379604562',
        'pid' => 'mm_132927835_46644979_1379604562',
        'token' => '700021003014d1b2079c40f39cea6b0caa67ce2c9d26d5036d938b31ed70742c56c161c2260577283',
    ],
    'wx'=>[
        //  公众号信息
        'mp'=>[
            //  账号基本信息
            'app_id'  => 'wxa3c080ba9e78a552', // 公众号的appid
            'secret'  => '085b299ae9d560b1d733426249e9688e', // 公众号的秘钥
            'token'   => 'weixin', // 接口的token
            'encodingAESKey'=>'',
            'safeMode'=>0,
            //  微信支付
            'payment'=>[
                'mch_id'        =>  '',// 商户ID
                'key'           =>  '',// 商户KEY
                'notify_url'    =>  '',// 支付通知地址
                'cert_path'     => '',// 证书
                'key_path'      => '',// 证书
            ],
            // web授权
            'oauth' => [
                'scopes'   => 'snsapi_userinfo',// 授权范围
                'callback' => '',// 授权回调
            ],
        ],

        //  小程序配置
        'mini'=>[
            //  基本配置
            'app_id'  => '', 
            'secret'  => '',
            //  微信支付
            'payment' => [
                'mch_id'        => '',
                'key'           => '',
            ],
        ]
    ],
    'app_download'=>'http://www.qq.com',
];
