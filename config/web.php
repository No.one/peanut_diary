<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    "timezone" => "Asia/Shanghai",
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'noone',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@runtime/cache',
        ],
        'queue' => require(__DIR__ . '/queue.php'),
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
         'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/common/mail',
            'useFileTransport' => FALSE,
            'transport' => [ 
                'class' => 'Swift_SmtpTransport', 
                'host' => 'smtp.qq.com', 
                'username' => '1399791106@qq.com', 
                'password' => 'hlznqjmnrofdjceh', 
                'port' => '465', 
                'encryption' => 'ssl', 
            ], 
            'messageConfig'=>[ 
                'charset'=>'UTF-8', 
                'from'=>['1399791106@qq.com'=>'优哩优利']
            ], 
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => require(__DIR__ . '/router.php')
    ],
    'modules' => [
        'manage' => [
            'class' => 'app\modules\manage\ManageModule',
        ],
        'm' => [
            'class' => 'app\modules\m\MModule',
        ],
        'weixin' => [
            'class' => 'app\modules\weixin\WeixinModule',
        ],
        'api' => [
            'class' => 'app\modules\api\apiModule',
        ],
        'web' => [
            'class' => 'app\modules\web\webModule',
        ]

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '192.168.*.*'],
    ];
}

return $config;
