var uploader = new plupload.Uploader({
    runtimes: 'gears,html5,html4,silverlight,flash',
    browse_button: 'cover_btn_big',
    url: common_ops.buildMUrl('/upload/pic'),
    flash_swf_url: 'plupload/Moxie.swf',
    silverlight_xap_url: 'plupload/Moxie.xap',
    filters: {
        max_file_size: '2mb',
        mime_types: [
            {title: "files", extensions: "jpg,png,gif,jpeg"}
        ]
    },
    multipart_params : {
        bucket : $('#photos_area input[name=bucket]').val(),
    },
    multi_selection: true,
    init: {
        FilesAdded: function(up, files) {
            var item = '';
            plupload.each(files, function(file) { //遍历文件
                item += "<span class='item' id='" + file['id'] + "'><span class='progress_bar'><span class='bar'></span><span class='percent'>0%</span></span></span>";
            });
            $("#photos_area").prepend(item);
            uploader.start();
        },
        UploadProgress: function(up, file) { //上传中，显示进度条 
            var percent = file.percent;
            $("#" + file.id).find('.bar').css({"width": percent + "%"});
            $("#" + file.id).find(".percent").text(percent + "%");
        },
        FileUploaded: function(up, file, info) {
            var data = eval("(" + info.response + ")");
            $("#" + file.id).html("<a class='picture_delete'>×</a><input type=hidden name='pics[]' value='" + data.name + "'><img src='" + data.src + "' alt='" + data.name + "'>")

            if (data.error != 0) {
                common_ops.alert(data.error);
            }
        },
        Error: function(up, err) {
            if (err.code == -600){
                common_ops.alert("上传图片大小不能超过2M！");
            }
            if (err.code == -601) {
                common_ops.alert("请上传jpg,png,gif,jpeg！");
            }
        }
    }
});
uploader.init();
//删除图片
$(function() {
    $("body").on("click", ".picture_delete", function() {
        $(this).parent(".item").remove();
    });
});

function upload_complete() {
    $("#photos_area").find(".item").remove();
}