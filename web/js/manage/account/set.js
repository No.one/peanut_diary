;

var account_set_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {

        $(".wrap_account_set .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var nickname_target = $(".wrap_account_set input[name=nickname]");
            var nickname = nickname_target.val();
            var mobile_target = $(".wrap_account_set input[name=mobile]");
            var mobile = mobile_target.val();
            var email_target = $(".wrap_account_set input[name=email]");
            var email = email_target.val();
            var login_name_target = $(".wrap_account_set input[name=login_name]");
            var login_name = login_name_target.val();
            var login_pwd_target = $(".wrap_account_set input[name=login_pwd]");
            var login_pwd = login_pwd_target.val();

            var role_id_target = $(".wrap_account_set select[name=role_id]");
            var role_id = role_id_target.val();


            if (nickname.length < 1) {
                common_ops.tip("请输入符合规范的姓名", nickname_target);
                return;
            }

            var mobile_retel = /^[1][3,4,5,7,8][0-9]{9}$/;
            if (mobile_retel.test(mobile) == false) {
                common_ops.tip('请输入正确的手机号码', mobile_target);
                return false;
            }

            var retel_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (retel_email.test(email) == false) {
                common_ops.tip("请输入正确的邮箱地址", email_target);
                return;
            }
            retel_login_name = /^[a-zA-Z][a-zA-Z0-9]{4,16}$/;
            if (retel_login_name.test(login_name) == false) {
                common_ops.tip("请输入正确的登录名", login_name_target);
                return;
            }

            if (login_pwd.length < 6) {
                common_ops.tip("请输入6-15位的登录密码", login_pwd_target);
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                nickname: nickname,
                mobile: mobile,
                email: email,
                login_name: login_name,
                login_pwd: login_pwd,
                id: $(".wrap_account_set input[name=id]").val(),
                role_id: role_id
            };

            $.ajax({
                url: common_ops.buildWebUrl("/account/set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/account/index");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });
        });
    }
};

$(document).ready(function () {
    account_set_ops.init();
});