;
var customer_user_question_set_ops = {
    init: function () {
        this.eventBind();
        common_ops.initpPluploadUploader(
            'cover_btn_big',
            common_ops.buildWebUrl('/upload/plupload'),
            {bucket: $('input[name=bucket]').val()},
            true
        );
    },
    eventBind: function () {
        var that = this;

        $('.wrap_book_set .save').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var id = $('input[name=id]').val();

            var summary = $('#summary').val();
            if (id <= 0){
                common_ops.alert("参数错误，请重试");
                return;
            }
            if (summary.length < 1){
                common_ops.alert("请输入你需要描述的问题");
                return;
            }
            var image = [];
            $(".item input").each(function (index, value) {
                image.push($(value).val());
            });
            console.log(image);
            btn_target.addClass("disabled");
            data = {
                id: id,
                summary: summary,
                image: image
            };
            $.ajax({
                url: common_ops.buildWebUrl("/customer/user_question_set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = window.location.href;
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });
        });

        $("body").on("click", ".picture_delete", function () {
            common_ops.removePluploadPic($(this));
        });

    },

};
$(document).ready(function () {
    customer_user_question_set_ops.init();
});

