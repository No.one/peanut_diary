;
var customer_notice_set_ops = {
    init: function () {
        this.eventBind();
        this.initEditor();
    },
    eventBind: function () {
        var that = this;

        $(".wrap_book_set .save").click(function () {
            var btn_target = $(this);

            var name_target = $('.wrap_book_set input[name=name]');
            var name = name_target.val();

            var summary = $.trim(that.ue.getContent());

            if (name.length <= 1){
                common_ops.tip('请填写标题',name_target);
                return
            }

            if (summary.length <= 10){
                common_ops.tip('请填写不少于10个字符的公告内容',summary_target);
                return
            }


            btn_target.addClass("disabled");

            var data = {
                name: name,
                summary: summary,
            };

            $.ajax({
                url: common_ops.buildWebUrl("/customer/notice_set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/customer/notice");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });



    },
    initEditor: function () {
        var that = this;
        that.ue = UE.getEditor('editor', {
            toolbars: [
                ['undo', 'redo', '|',
                    'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|', 'rowspacingtop', 'rowspacingbottom', 'lineheight'],
                ['customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                    'directionalityltr', 'directionalityrtl', 'indent', '|',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                    'link', 'unlink'],
                ['imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                    'horizontal', 'spechars', '|', 'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols']

            ],
            enableAutoSave: true,
            saveInterval: 60000,
            elementPathEnabled: false,
            zIndex: 4
        });
        that.ue.addListener('beforeInsertImage', function (t, arg) {
            console.log(t, arg);
            //alert('这是图片地址：'+arg[0].src);
            // that.ue.execCommand('insertimage', {
            //     src: arg[0].src,
            //     _src: arg[0].src,
            //     width: '250'
            // });
            return false;
        });
    },
};
$(document).ready(function () {
    customer_notice_set_ops.init();
});