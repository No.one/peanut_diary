;
var upload = {
    error: function (msg) {
        common_ops.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("advert", file_key) + '"/>'
                + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        image_set_ops.delete_img();
    }
};
var image_set_ops = {
    init:function(){
        this.eventBind();
        this.delete_img();
    },
    eventBind:function(){
        $(".wrap_image_set .upload_pic_wrap input[name=pic]").change(function () {
            $(".wrap_image_set .upload_pic_wrap").submit();
        });
        $(".wrap_image_set .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var pid = $(".wrap_image_set input[name=pid]").val();
            var name_target = $(".wrap_image_set input[name=name]");
            var name = name_target.val();
            
            var url_target = $(".wrap_image_set input[name=url]");
            var url = url_target.val();
            
            if( name.length < 1 ){
                common_ops.tip( "请输入符合规范的分类名称" ,name_target );
                return;
            }
            
            if ($(".wrap_image_set .pic-each").size() < 1) {
                common_ops.alert("请上传广告图");
                return;
            }
            
            if( url.length < 1 ){
                common_ops.tip( "请输入符合跳转地址" ,url_target );
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                name:name,
                pid:pid,
                url:url,
                weight:$(".wrap_image_set input[name=weight]").val(),
                main_image: $(".wrap_image_set .pic-each .del_image").attr("data"),
                id:$(".wrap_image_set input[name=id]").val()
            };

            $.ajax({
                url:common_ops.buildWebUrl("/advert/set-image") ,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(res){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/advert/images", {id:pid});
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    },
    delete_img: function () {
        $(".wrap_image_set .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },
};

$(document).ready( function(){
    image_set_ops.init();
});