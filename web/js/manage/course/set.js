;
var upload = {
    error: function (msg) {
        $.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("course", file_key) + '"/>'
            + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        course_set_ops.delete_img();
    }
};

var course_set_ops = {
    init: function () {
        this.ue = null;
        this.eventBind();
        this.initEditor();
        this.datetimepickerComponent();
        common_ops.initpPluploadUploader(
            'cover_btn_big',
            common_ops.buildWebUrl('/upload/plupload'),
            {bucket: $('input[name=bucket]').val()},
            true
        );
        course_set_ops.delete_img();
    },
    eventBind: function () {
        var that = this;

        $(".wrap_book_set input:radio[name=is_free]").change(function () {
            var is_free = $(this).val();
            that.free_ops(is_free);
        });

        $(".wrap_book_set input:radio[name=model]").change(function () {
            var model = $(this).val();
            that.model_ops(model);
        });

        $(".wrap_book_set .upload_pic_wrap input[name=pic]").change(function () {
            $(".wrap_book_set .upload_pic_wrap").submit();
        });

        $(".wrap_book_set input[name=tags]").tagsInput({
            width: 'auto',
            height: 40,
            onAddTag: function (tag) {
            },
            onRemoveTag: function (tag) {
            }
        });

        $(".wrap_book_set select[name=cat_id]").select2({
            language: "zh-CN",
            width: '100%'
        });

        $(".wrap_book_set .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var cat_id_target = $(".wrap_book_set select[name=cat_id]");
            var cat_id = cat_id_target.val();

            var name_target = $(".wrap_book_set input[name=name]");
            var name = name_target.val();

            var is_free = $('.wrap_book_set input[name=is_free]:checked').val();

            var price_target = $(".wrap_book_set input[name=price]");
            var price = price_target.val();

            var model = $('.wrap_book_set input[name=model]:checked').val();

            var date_from_target = $('.wrap_book_set input[name=date_from]');
            var date_from = date_from_target.val();
            var course_addr_target = $('.wrap_book_set input[name=course_addr]');
            var course_addr = course_addr_target.val();
            var contacts_target = $('.wrap_book_set input[name=contacts]');
            var contacts = contacts_target.val();

            var course_pic = [];
            $(".item input").each(function (index, value) {
                course_pic.push($(value).val());
            });

            var video_url_target = $('.wrap_book_set input[name=video_url]');
            var video_url = video_url_target.val();

            var summary = $.trim(that.ue.getContent());

            var stock_target = $(".wrap_book_set input[name=stock]");
            var stock = stock_target.val();

            var tags_target = $(".wrap_book_set input[name=tags]");
            var tags = $.trim(tags_target.val());

            if (parseInt(cat_id) < 1) {
                common_ops.tip("请输入分类", cat_id_target);
                return;
            }

            if (name.length < 1) {
                common_ops.tip("请输入符合规范的课程名称", name_target);
                return;
            }

            if (is_free == 0) {
                if (parseFloat(price) < 0) {
                    common_ops.tip("请输入符合规范的价格", price_target);
                    return;
                }
            }

            // if (model == 0){
            //     if (date_from.length < 1 )
            //     {
            //         common_ops.tip("请输入课程开始时间", date_from_target);
            //         return false;
            //     }
            //     if (date_to.length < 1)
            //     {
            //         common_ops.tip("请输入课程结束时间", date_to_target);
            //         return false;
            //     }
            //     if (course_addr.length < 1)
            //     {
            //         common_ops.tip("请输入课程地址", course_addr_target);
            //         return false;
            //     }
            //     if (contacts.length < 1)
            //     {
            //         common_ops.tip("请输入课程地址", contacts_target);
            //         return false;
            //     }
            // }

            if ($(".wrap_book_set .pic-each").size() < 1) {
                common_ops.alert("请上传封面图");
                return;
            }
            // if (video_url.length < 1) {
            //     common_ops.tip("请输入符合规范的课程视频链接", video_url_target);
            //     return;
            // }

            if (summary.length < 10) {
                common_ops.alert("请输入课程描述，并不能少于10个字符");
                return;
            }
            btn_target.addClass("disabled");
            var data = {
                cat_id: cat_id,
                name: name,
                is_free: is_free,
                price: price,
                model: model,
                date_from: date_from,
                course_addr: course_addr,
                contacts: contacts,
                main_image: $(".wrap_book_set .pic-each .del_image").attr("data"),
                course_pic: course_pic,
                video_url: video_url,
                summary: summary,
                tags: tags,
                id: $(".wrap_book_set input[name=id]").val()
            };

            $.ajax({
                url: common_ops.buildWebUrl("/course/set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/course/index");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });

        $("body").on("click", ".picture_delete", function () {
            common_ops.removePluploadPic($(this));
        });
    },
    delete_img: function () {
        $(".wrap_book_set .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },
    free_ops: function (is_free) {
        if (is_free == 1) {
            $('.wrap_book_set input[name=price]').val('');
            $('.wrap_book_set input[name=price]').attr("disabled", 'disabled');
        } else {
            $('.wrap_book_set input[name=price]').removeAttr('disabled');
        }
    },
    model_ops: function (model) {
        if (model == 1) {
            $('.wrap_book_set input[name=date_from]').val('');
            $('.wrap_book_set input[name=course_addr]').val('');
            $('.wrap_book_set input[name=contacts]').val('');
            $('.wrap_book_set input[name=date_from]').attr("disabled", 'disabled');
            $('.wrap_book_set input[name=course_addr]').attr("disabled", 'disabled');
            $('.wrap_book_set input[name=contacts]').attr("disabled", 'disabled');

            $('.wrap_book_set input[name=video_url]').removeAttr('disabled');
        } else {
            $('.wrap_book_set input[name=video_url]').val('');
            $('.wrap_book_set input[name=video_url]').attr("disabled", 'disabled');

            $('.wrap_book_set input[name=date_from]').removeAttr('disabled');
            $('.wrap_book_set input[name=course_addr]').removeAttr('disabled');
            $('.wrap_book_set input[name=contacts]').removeAttr('disabled');
        }
    },
    initEditor: function () {
        var that = this;
        that.ue = UE.getEditor('editor', {
            toolbars: [
                ['undo', 'redo', '|',
                    'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|', 'rowspacingtop', 'rowspacingbottom', 'lineheight'],
                ['customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                    'directionalityltr', 'directionalityrtl', 'indent', '|',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                    'link', 'unlink'],
                ['imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                    'horizontal', 'spechars', '|', 'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols']

            ],
            enableAutoSave: true,
            saveInterval: 60000,
            elementPathEnabled: false,
            zIndex: 4
        });
        that.ue.addListener('beforeInsertImage', function (t, arg) {
            console.log(t, arg);
            //alert('这是图片地址：'+arg[0].src);
            // that.ue.execCommand('insertimage', {
            //     src: arg[0].src,
            //     _src: arg[0].src,
            //     width: '250'
            // });
            return false;
        });
    },
    datetimepickerComponent: function () {
        var that = this;
        $.datetimepicker.setLocale('zh');
        params = {
            scrollInput: false,
            scrollMonth: false,
            scrollTime: true,
            dayOfWeekStart: 1,
            lang: 'zh',
            todayButton: true,//回到今天
            defaultSelect: true,
            defaultDate: new Date().Format('yyyy-MM-dd'),
            format: 'Y-m-d H:i',//格式化显示
            timepicker: true,
        };
        $('input[name=date_from]').datetimepicker(params);

    },
};
$(document).ready(function () {
    course_set_ops.init();
});