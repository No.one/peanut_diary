;
var upload = {
    error:function(msg){
        $.alert(msg);
    },
    success:function(file_key,type){
        if(!file_key){
            return;
        }
        var html = '<img src="'+common_ops.buildPicUrl("course",file_key)+'"/>'
            +'<span class="fa fa-times-circle del del_image" data="'+file_key+'"></span>';

        if( $(".upload_pic_wrap .pic-each").size() > 0 ){
            $(".upload_pic_wrap .pic-each").html( html );
        }else{
            $(".upload_pic_wrap").append('<span class="pic-each">'+ html + '</span>');
        }
        course_cat_set_ops.delete_img();
    }
};
var course_cat_set_ops = {
    init:function(){
        this.eventBind();
        course_cat_set_ops.delete_img();
    },

    eventBind:function(){
        var that = this;

        $(".wrap_cat_set .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var name_target = $(".wrap_cat_set input[name=name]");
            var name = name_target.val();

            var summary = $(".wrap_cat_set textarea[name=summary]").val();

            if( name.length < 1 ){
                common_ops.tip( "请输入符合规范的分类名称" ,name_target );
                return;
            }
            if( $(".pic-each").size() < 1 ){
                common_ops.alert( "请上传头像"  );
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                name : name,
                avatar : $(".pic-each .del_image").attr("data"),
                summary : summary,
                weight : $(".wrap_cat_set input[name=weight]").val(),
                id : $(".wrap_cat_set input[name=id]").val()
            };

            $.ajax({
                url:common_ops.buildWebUrl("/course/cat_set") ,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(res){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/course/cat");
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });


        $(".upload_pic_wrap input[name=pic]").change(function(){
            $(".form-group .upload_pic_wrap").submit();
        });
    },

    delete_img:function(){
        $(".del_image").unbind().click(function(){
            $(this).parent().remove();
        });
    },

};

$(document).ready( function(){
    course_cat_set_ops.init();
});