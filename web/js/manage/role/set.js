/**
 * Created by Administrator on 2018/2/2.
 */
;
var role_set_ops = {
    init:function () {
        this.eventBind();
    },
    eventBind:function () {
        $(".role_set_wrap .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                alert("正在处理，请不要重复提交");
                return false;
            }

            var name = $(".role_set_wrap input[name='name']").val();

            if( name.length < 1 ){
                alert("请输入合法的角色名称");
                return false;
            }


            btn_target.addClass("disabled");
            $.ajax({
                url:common_ops.buildWebUrl("/role/set"),
                type:'POST',
                data:{
                    id:$(".role_set_wrap input[name='id']").val(),
                    name:name,
                },
                dataType:'json',
                success:function( res ){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/role/index");
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    }
};
$(document).ready(function () {
    role_set_ops.init();
});
