;
var role_access_set_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        var that = this;
        $(".role_access_set_wrap .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                alert("正在处理，请不要重复提交");
                return false;
            }

            var access_ids = [];
            $(".role_access_set_wrap input[name='access_ids[]']").each(function () {
                if ($(this).prop("checked")) {
                    access_ids.push($(this).val());
                }
            });

            btn_target.addClass("disabled");
            $.ajax({
                url: common_ops.buildWebUrl('/role/access'),
                type: 'POST',
                data: {
                    id: $(".role_access_set_wrap input[name=id]").val(),
                    access_ids: access_ids
                },
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    alert(res.msg);
                    if (res.code == 200) {
                        window.location.href = common_ops.buildWebUrl("/role/index");
                    }
                }
            });
        });

        $("input").click(function(){
            var level = $(this).attr("level");
            if(level==0){
                var str = '_';
                var inputs = $("input[data*="+str+"]");
                if($(this).prop("checked")){
                    that.ops(inputs, true);
                }else{
                    that.ops($("input"), false);
                }
            }
            if(level==1){
                var id = $(this).attr("id");
                var inputs2 = $("input[fid="+id+"]");
                if($(this).prop("checked")){
                    that.ops(inputs2, true);
                }else{
                    that.ops(inputs2, false);
                }
            }
            if(level==2){
                var fid = $(this).attr('fid');
                var inputs3 = $("input[id="+fid+"]");
                var sibling_input_num = $("input[fid="+fid+"]:checked").length;
                if($(this).prop("checked")){
                    that.ops(inputs3, true);
                } else {
                    if (sibling_input_num <= 0){
                        that.ops(inputs3, false);
                    }
                }
            }
        })
    },
    ops:function (ops_this, bool) {
        ops_this.prop("checked", bool);
    }
};

$(document).ready(function () {
    role_access_set_ops.init();
});