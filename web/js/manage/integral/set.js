;
var upload = {
    error: function (msg) {
        $.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("integral", file_key) + '"/>'
                + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        goods_set_ops.delete_img();
    }
};
var goods_set_ops = {
    init: function () {
        this.ue = null;
        this.eventBind();
        this.initEditor();
        common_ops.initpPluploadUploader('cover_btn_big', common_ops.buildWebUrl('/upload/plupload'),{bucket: $('input[name=bucket]').val()},true);
        this.delete_img();
    },
    eventBind: function () {
        var that = this;
        $(".wrap_goods_set select[name=cate_id]").select2({
            language: "zh-CN",
            width: '100%'
        });

        $(".wrap_goods_set .upload_pic_wrap input[name=pic]").change(function () {
            $(".wrap_goods_set .upload_pic_wrap").submit();
        });

        $(".wrap_goods_set .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var cate_id_target = $(".wrap_goods_set select[name=cate_id]");
            var cate_id = cate_id_target.val();

            var goods_name_target = $(".wrap_goods_set input[name=goods_name]");
            var goods_name = goods_name_target.val();

            var pic = [];
            $(".item input").each(function (index, value) {
                pic.push($(value).val());
            });

            var price_target = $(".wrap_goods_set input[name=price]");
            var price = price_target.val();


            var summary = $.trim(that.ue.getContent());

            var stock_target = $(".wrap_goods_set input[name=stock]");
            var stock = stock_target.val();

            if (parseInt(cate_id) < 1) {
                common_ops.tip("请选择分类", cate_id_target);
                return;
            }


            if (goods_name.length < 1) {
                common_ops.alert("请输入商品名称", goods_name_target);
                return;
            }

            if ($(".wrap_goods_set .pic-each").size() < 1) {
                common_ops.alert("请上传封面图");
                return;
            }

            var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
            if (parseFloat(price) <= 0 && reg.test(price) == false) {
                common_ops.tip("请输入符合规范的售卖价格", price_target);
                return;
            }

            if (summary.length < 10) {
                common_ops.tip("请输入描述，并不能少于10个字符", $("#editor"));
                return;
            }

            if (parseInt(stock) < 1) {
                common_ops.tip("请输入符合规范的库存量", stock_target);
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                cate_id: cate_id,
                goods_name: goods_name,
                main_image: $(".wrap_goods_set .pic-each .del_image").attr("data"),
                pic: pic,
                price: price,
                summary: summary,
                stock: stock,
                id: $(".wrap_goods_set input[name=id]").val()
            };

            $.ajax({
                url: common_ops.buildWebUrl("/integral/set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/integral/index");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });



        $("body").on("click", ".picture_delete", function () {
            common_ops.removePluploadPic($(this));
        });

    },
    delete_img: function () {
        $(".wrap_goods_set .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },
    initEditor: function () {
        var that = this;
        that.ue = UE.getEditor('editor', {
            toolbars: [
                ['undo', 'redo', '|',
                    'bold', 'italic', 'underline', 'strikethrough', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|', 'rowspacingtop', 'rowspacingbottom', 'lineheight'],
                ['customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                    'directionalityltr', 'directionalityrtl', 'indent', '|',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                    'link', 'unlink'],
                ['insertimage', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                    'horizontal', 'spechars', '|', 'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols']

            ],
            enableAutoSave: true,
            saveInterval: 60000,
            elementPathEnabled: false,
            zIndex: 4
        });
        that.ue.addListener('beforeInsertImage', function (t, arg) {
            console.log(t, arg);
            //alert('这是图片地址：'+arg[0].src);
            // that.ue.execCommand('insertimage', {
            //     src: arg[0].src,
            //     _src: arg[0].src,
            //     width: '250'
            // });
            return false;
        });
    },
};

$(document).ready(function () {
    goods_set_ops.init();
});