;
var favorites_ops = {
    init: function () {
        this.eventBind();
       
    },
    eventBind: function () {
        var that = this;
        $(".wrap_favorites select[name=favorites_id]").select2({
            language: "zh-CN",
            width: '100%'
        });
        
        $(".wrap_favorites .reset").click(function(){
            $(".wrap_favorites select[name=favorites_id]").select2("val", "");
        })

        $(".wrap_favorites .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var favorites_id_target = $(".wrap_favorites select[name=favorites_id]");
            var favorites_id = favorites_id_target.val();

            if (parseInt(favorites_id) < 1) {
                common_ops.tip("请选择选品库", favorites_id_target);
                return;
            }
            btn_target.addClass("disabled");
            var data = {
                favorites_id: favorites_id
            };
            
            $.ajax({
                url: common_ops.buildWebUrl("/goods/execute-update"),
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend:function(){
                  common_ops.loading("采集中")  
                },
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = res.data.url;
                        }
                    }
                    common_ops.alert("采集成功", callback);
                }
            });

        });
        
    },
};

$(document).ready(function () {
    favorites_ops.init();
});