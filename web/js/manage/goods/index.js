;
var goods_index_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        var that = this;
        $(".remove").click(function () {
            that.ops("remove", $(this).attr("data"))
        });

        $(".down").click(function () {
            that.ops("down", $(this).attr("data"))
        });

        $(".up").click(function () {
            that.ops("up", $(this).attr("data"))
        });

        $(".wrap_search .search").click(function () {
            $(".wrap_search").submit();
        });

        $("#all").change(function () {
            if (this.checked) {
                $("input[name='goods_ids[]']:checkbox").each(function () {
                    $(this).prop("checked", true);
                });
            } else {
                $("input[name='goods_ids[]']:checkbox").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });

        $(".btn_delete").click(function () {
            var goods_ids = $("input[name='goods_ids[]']");
            var num = 0;
            var check_ids = "";
            if (!jQuery.isEmptyObject(goods_ids)) {
                $.each(goods_ids, function (i, item) {
                    if (item.checked) {
                        check_ids += $(this).val()+',';
                        num += 1;
                    }
                })
            }

            if (num == 0) {
                common_ops.alert("请先勾选记录");
                return;
            }
            
            common_ops.confirm("确定要删除吗?", 
                {
                    ok: function () {
                        $.ajax({
                            url: common_ops.buildWebUrl("/goods/del"),
                            type: 'POST',
                            data: {
                                goods_ids:check_ids.substr(0, check_ids.length-1)
                            },
                            dataType: 'json',
                            success: function (res) {
                                var callback = null;
                                if (res.code == 200) {
                                    callback = function () {
                                        window.location.href = window.location.href;
                                    }
                                }
                                common_ops.alert(res.msg, callback);
                            }
                        });
                    }
                }
            );
        });
        
//        $.ajax({
//                    url: common_ops.buildWebUrl("/goods/del"),
//                    type: 'POST',
//                    data: {
//                        goods_ids: goods_ids
//                    },
//                    dataType: 'json',
//                    success: function (res) {
//                        var callback = null;
//                        if (res.code == 200) {
//                            callback = function () {
//                                window.location.href = window.location.href;
//                            }
//                        }
//                        common_ops.alert(res.msg, callback);
//                    }
//                });
    },
    ops: function (act, id) {
        var callback = {
            'ok': function () {
                $.ajax({
                    url: common_ops.buildWebUrl("/goods/ops"),
                    type: 'POST',
                    data: {
                        act: act,
                        id: id
                    },
                    dataType: 'json',
                    success: function (res) {
                        var callback = null;
                        if (res.code == 200) {
                            callback = function () {
                                window.location.href = window.location.href;
                            }
                        }
                        common_ops.alert(res.msg, callback);
                    }
                });
            },
            'cancel': null
        };
        common_ops.confirm((act == "remove") ? "确定删除？" : "确定操作？", callback);
    }
};

$(document).ready(function () {
    goods_index_ops.init();
});