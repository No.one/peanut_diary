;
var list_ops = {
    init: function () {
        this.eventBind();

    },
    eventBind: function () {
        
        var page = $("table").data("page");
        var favorites_id = $("table").data("favorites");
        if(page == 2){
            requestAlert('第1页采集完成，即将采集下一页','success');
        }
        var data = {
            favorites_id: favorites_id,
            p:page
        };
        $.ajax({
            url: common_ops.buildWebUrl("/goods/execute-update"),
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (res) {
                if (res.code == 200) {
                    requestAlert(res.msg,'success');
                    if(res.data.url){
                        window.location.href = res.data.url;
                    }
                }else{
                    requestAlert('请求失败!','danger');
                }
            }
        });
    },
};

$(document).ready(function () {
    list_ops.init();
});

function requestAlert(title, type) {
    bootoast({
        message: title,
        type: type,
        position: 'right-top',
        icon: undefined,
        timeout: 5,
        animationDuration: 300,
        dismissable: true
    });
}