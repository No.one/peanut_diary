;
var upload = {
    error: function (msg) {
        $.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("customer", file_key) + '"/>'
                + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        setting_index_ops.delete_img();
    }
};
var setting_index_ops = {
    init: function () {
        this.eventBind();
        this.delete_img();
    },
    eventBind: function () {
        var that = this;
        $(".wrap_setting_index .upload_pic_wrap input[name=pic]").change(function () {
            $(".wrap_setting_index .upload_pic_wrap").submit();
        });

        $(".wrap_setting_index .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var wechat_target = $(".wrap_setting_index input[name=wechat]");
            var wechat = wechat_target.val();

            var work_time_target = $(".wrap_setting_index input[name=work_time]");
            var work_time = work_time_target.val();

            if (wechat.length < 1) {
                common_ops.tip("请输入客服微信", wechat_target);
                return;
            }

            if ($(".wrap_setting_index .pic-each").size() < 1) {
                common_ops.alert("请上传二维码");
                return;
            }

            if (work_time.length < 1) {
                common_ops.tip("请输入工作时间", work_time_target);
                return;
            }
            btn_target.addClass("disabled");
            var data = {
                wechat: wechat,
                work_time: work_time,
                customer_img: $(".wrap_setting_index .pic-each .del_image").attr("data"),
            };

            $.ajax({
                url: common_ops.buildWebUrl("/setting/index"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/setting/index");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });
        });
    },
    delete_img: function () {
        $(".wrap_setting_index .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },
};
$(document).ready(function () {
    setting_index_ops.init();
});
