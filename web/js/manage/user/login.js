;
var user_login_ops = {
    init: function () {
        this.eventBind();
        this.login_bg();
    },
    eventBind: function () {
        var that = this;
    },
    login_bg:function () {
        var pic = parseInt(Math.random() * (14 - 1 + 1) + 1) + ".jpg";
        var url = common_ops.buildWwwUrl('/images/manage/bg-img/' + pic);
        $(".login-bg").css('background-image', 'url("' + url + '")');
    }
};
$(document).ready(function () {
    user_login_ops.init();
});