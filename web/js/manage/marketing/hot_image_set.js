;
var upload = {
    error: function (msg) {
        $.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("hot", file_key) + '"/>'
                + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        hot_image_set_ops.delete_img();
    }
};
var hot_image_set_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        
        $("#hot_image_wrap select[name=goods_id]").select2({
            language: "zh-CN",
            width: '100%',
            dropdownParent:$("#hot_image_wrap")
        });
        
        $(".set_pic").click(function(){
            $('#hot_image_wrap').modal('show');
        });

        
        $("#hot_image_wrap .upload_pic_wrap input[name=pic]").change(function () {
            $("#hot_image_wrap .upload_pic_wrap").submit();
        });

         $("#hot_image_wrap .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var goods_id = $("#goods_id").val();

            if( goods_id < 1 ){
                common_ops.alert("请选择商品");
                return;
            }
        
            if ($("#hot_image_wrap .pic-each").size() < 1) {
                common_ops.alert("请上传封面图");
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                goods_id: goods_id,
                main_image: $("#hot_image_wrap .pic-each .del_image").attr("data"),
                hot_id: $("#hot_image_wrap input[name=hot_id]").val(),
            };

            $.ajax({
                url: common_ops.buildWebUrl("/marketing/hot-image"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                           window.location.href = window.location.href;
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });

        $(".remove").click( function(){
            var id = $(this).attr("data");
            var callback = {
                'ok':function(){
                    $.ajax({
                        url:common_ops.buildWebUrl("/marketing/hot-image-ops"),
                        type:'POST',
                        data:{
                            id:id
                        },
                        dataType:'json',
                        success:function( res ){
                            var callback = null;
                            if( res.code == 200 ){
                                callback = function(){
                                    window.location.href = window.location.href;
                                }
                            }
                            common_ops.alert( res.msg,callback );
                        }
                    });
                },
                'cancel':null
            };
            common_ops.confirm( "确定删除？",callback );
        });

    },
    delete_img: function () {
        $("#hot_image_wrap .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },

};
$(document).ready(function () {
    hot_image_set_ops.init();
});

