;
var hot_set_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        var that = this;

        $(".wrap_hot_set .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var name_target = $('.wrap_hot_set input[name=name]');
            var name = name_target.val();

            var summary_target = $(".wrap_hot_set textarea[name=summary]");
            var summary = $.trim(summary_target.val());

            var weight = $('.wrap_hot_set input[name=weight]').val();

            if (name.length < 1) {
                common_ops.tip("请输入文章名称", name_target);
                return;
            }

            if (summary.length < 10) {
                common_ops.alert("请输入文章内容，并不能少于10个字符");
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                id: $('.wrap_hot_set input[name=id]').val(),
                name: name,
                summary: summary,
                weight: weight,
            };

            $.ajax({
                url: common_ops.buildWebUrl("/marketing/hot-set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/marketing/hot");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });

    },
};
$(document).ready(function () {
    hot_set_ops.init();
});