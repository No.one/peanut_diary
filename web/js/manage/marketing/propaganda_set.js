;
var upload = {
    error: function (msg) {
        $.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("propaganda", file_key) + '"/>'
                + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        propaganda_set_ops.delete_img();
    }
};
var propaganda_set_ops = {
    init: function () {
        this.eventBind();
        common_ops.initpPluploadUploader(
            'cover_btn_big',
            common_ops.buildWebUrl('/upload/plupload'),
            {bucket: $('input[name=bucket]').val()},
            true
        );
    },
    eventBind: function () {
        var that = this;
        
        $(".wrap_propaganda_set .upload_pic_wrap input[name=pic]").change(function () {
            $(".wrap_propaganda_set .upload_pic_wrap").submit();
        });

         $(".wrap_propaganda_set .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var name_target = $(".wrap_propaganda_set input[name=name]");
            var name = name_target.val();

            var summary_target = $(".wrap_propaganda_set textarea[name=summary]");
            var summary = $.trim(summary_target.val());
            
            if (name.length < 1) {
                common_ops.alert("请输入商品名称", name_target);
                return;
            }

            if ($(".wrap_propaganda_set .pic-each").size() < 1) {
                common_ops.alert("请上传封面图");
                return;
            }

            if (summary.length < 10) {
                common_ops.tip("请输入描述，并不能少于10个字符", summary_target);
                return;
            }
            
            btn_target.addClass("disabled");

            var data = {
                name: name,
                main_image: $(".wrap_propaganda_set .pic-each .del_image").attr("data"),
                summary: summary,
                weight:$(".wrap_propaganda_set input[name=weight]").val(),
                id: $(".wrap_propaganda_set input[name=id]").val()
            };

            $.ajax({
                url: common_ops.buildWebUrl("/marketing/propaganda-set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildWebUrl("/marketing/propaganda");
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });

        $("body").on("click", ".picture_delete", function () {
            common_ops.removePluploadPic($(this));
        });

    },
    delete_img: function () {
        $(".wrap_propaganda_set .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },

};
$(document).ready(function () {
    propaganda_set_ops.init();
});

