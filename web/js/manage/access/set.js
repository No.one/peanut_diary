/**
 * Created by Administrator on 2018/2/2.
 */
;
var access_set_ops = {
    init:function () {
        this.eventBind();
    },
    eventBind:function () {
        $(".access_set_wrap .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                alert("正在处理，请不要重复提交");
                return false;
            }

            var fid = $(".access_set_wrap select[name=fid]").val();
            var title = $(".access_set_wrap input[name='title']").val();
            var urls = $(".access_set_wrap textarea[name='urls']").val();
            var icon = $(".access_set_wrap input[name='icon']").val();
            var weigh = $(".access_set_wrap input[name='weigh']").val();

            if( title.length < 1 ){
                alert("请输入合法的权限标题");
                return false;
            }

            if( urls.length < 1 ){
                alert("请输入合法的Urls");
                return false;
            }

            btn_target.addClass("disabled");
            $.ajax({
                url:common_ops.buildWebUrl("/access/set"),
                type:'POST',
                data:{
                    id:$(".access_set_wrap input[name='id']").val(),
                    fid:fid,
                    title:title,
                    urls:urls,
                    icon:icon,
                    weigh:weigh
                },
                dataType:'json',
                success:function( res ){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/access/index");
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    }
};
$(document).ready(function () {
    access_set_ops.init();
});
