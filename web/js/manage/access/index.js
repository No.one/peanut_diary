/**
 * Created by Administrator on 2018/2/2.
 */
;
var access_index_ops = {
    init:function () {
        this.eventBind();
    },
    eventBind:function () {
        var that = this;
        $(".access_set_wrap .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                alert("正在处理，请不要重复提交");
                return false;
            }

            var title = $(".access_set_wrap input[name='title']").val();
            var urls = $(".access_set_wrap textarea[name='urls']").val();

            if( title.length < 1 ){
                alert("请输入合法的权限标题");
                return false;
            }

            if( urls.length < 1 ){
                alert("请输入合法的Urls");
                return false;
            }

            btn_target.addClass("disabled");
            $.ajax({
                url:common_ops.buildWebUrl("/access/set"),
                type:'POST',
                data:{
                    id:$(".access_set_wrap input[name='id']").val(),
                    title:title,
                    urls:urls
                },
                dataType:'json',
                success:function( res ){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/access/index");
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });

        $(".remove").click(function () {
            that.ops( "remove",$(this).attr("data") )
        })
    },
    ops:function( act,id ){
        var callback = {
            'ok':function(){
                $.ajax({
                    url:common_ops.buildWebUrl("/access/ops"),
                    type:'POST',
                    data:{
                        act:act,
                        id:id
                    },
                    dataType:'json',
                    success:function( res ){
                        var callback = null;
                        if( res.code == 200 ){
                            callback = function(){
                                window.location.href = window.location.href;
                            }
                        }
                        common_ops.alert( res.msg,callback );
                    }
                });
            },
            'cancel':null
        };
        common_ops.confirm( ( act=="remove" )?"确定删除？":"确定恢复？",callback );
    }
};
$(document).ready(function () {
    access_index_ops.init();
});
