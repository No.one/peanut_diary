;
var shop_goods_set_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {

        $("#shop_goods_wrap select[name=goods_id]").select2({
            language: "zh-CN",
            width: '100%',
            dropdownParent: $("#shop_goods_wrap")
        });

        $(".set_pic").click(function () {
            $('#shop_goods_wrap').modal('show');
        });
        $("#shop_goods_wrap .save").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var goods_id = $("#goods_id").val();

            if (goods_id < 1) {
                common_ops.alert("请选择商品");
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                goods_id: goods_id,
                shop_id: $("#shop_goods_wrap input[name=shop_id]").val(),
            };

            $.ajax({
                url: common_ops.buildWebUrl("/shop/shop-goods"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = window.location.href;
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });

        });

        $(".remove").click(function () {
            var id = $(this).attr("data");
            var callback = {
                'ok': function () {
                    $.ajax({
                        url: common_ops.buildWebUrl("/shop/shop-goods-ops"),
                        type: 'POST',
                        data: {
                            id: id
                        },
                        dataType: 'json',
                        success: function (res) {
                            var callback = null;
                            if (res.code == 200) {
                                callback = function () {
                                    window.location.href = window.location.href;
                                }
                            }
                            common_ops.alert(res.msg, callback);
                        }
                    });
                },
                'cancel': null
            };
            common_ops.confirm("确定删除？", callback);
        });

    },
};
$(document).ready(function () {
    shop_goods_set_ops.init();
});

