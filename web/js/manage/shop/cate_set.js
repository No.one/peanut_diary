;
var upload = {
    error: function (msg) {
        common_ops.alert(msg);
    },
    success: function (file_key, type) {
        if (!file_key) {
            return;
        }
        var html = '<img src="' + common_ops.buildPicUrl("shop_cate", file_key) + '"/>'
                + '<span class="fa fa-times-circle del del_image" data="' + file_key + '"></span>';

        if ($(".upload_pic_wrap .pic-each").size() > 0) {
            $(".upload_pic_wrap .pic-each").html(html);
        } else {
            $(".upload_pic_wrap").append('<span class="pic-each">' + html + '</span>');
        }
        shop_cate_set_ops.delete_img();
    }
};
var shop_cate_set_ops = {
    init:function(){
        this.eventBind();
        this.delete_img();
    },
    eventBind:function(){
        $(".wrap_cate_set .upload_pic_wrap input[name=pic]").change(function () {
            $(".wrap_cate_set .upload_pic_wrap").submit();
        });
        $(".wrap_cate_set .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var name_target = $(".wrap_cate_set input[name=name]");
            var name = name_target.val();

            if( name.length < 1 ){
                common_ops.tip( "请输入符合规范的分类名称" ,name_target );
                return;
            }
            
            if ($(".wrap_cate_set .pic-each").size() < 1) {
                common_ops.alert("请上传图标");
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                name:name,
                weight:$(".wrap_cate_set input[name=weight]").val(),
                main_image: $(".wrap_cate_set .pic-each .del_image").attr("data"),
                id:$(".wrap_cate_set input[name=id]").val()
            };

            $.ajax({
                url:common_ops.buildWebUrl("/shop/cate-set") ,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(res){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/shop/cate");
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    },
    delete_img: function () {
        $(".wrap_cate_set .del_image").unbind().click(function () {
            $(this).parent().remove();
        });
    },
};

$(document).ready( function(){
    shop_cate_set_ops.init();
});