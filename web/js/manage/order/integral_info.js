;
var integral_info_ops = {
    init:function(){
        this.eventBind();
    },
    eventBind:function(){
        $("#express_wrap select[name=express_id]").select2({
            language: "zh-CN",
            width: '100%',
            dropdownParent:$("#express_wrap")
        });

        $(".express_send").click(function(){
            $('#express_wrap').modal('show');
        });


        $('#express_wrap .save').click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var express_id = $("#express_id").val();

            var express_number_target = $('#express_wrap input[name=express_number]');
            var express_number = express_number_target.val();

            if( express_id < 1 ){
                common_ops.alert("请选择快递");
                return;
            }

            if( express_number.length < 1 ){
                common_ops.tip( "请输入符合要求的快递信息" ,express_number_target );
                return;
            }

            btn_target.addClass("disabled");

            $.ajax({
                url:common_ops.buildWebUrl("/order/integral-info") ,
                type:'POST',
                data:{
                    id:$('#express_wrap input[name=order_id]').val(),
                    express_number:express_number,
                    express_id :express_id
                },
                dataType:'json',
                success:function(res){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = window.location.href;
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    }
};

$(document).ready( function(){
    integral_info_ops.init();
});

