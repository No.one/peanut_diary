;
var member_grade_ops = {
    init:function(){
        this.eventBind();
    },
    eventBind:function(){

        $(".wrap_grade_set .save").click( function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var name_target = $(".wrap_grade_set input[name=name]");
            var name = name_target.val();
            var ratio_target = $(".wrap_grade_set input[name=ratio]");
            var ratio = ratio_target.val();
            var sort_tarage = $(".wrap_grade_set input[name=sort]");
            var sort = sort_tarage.val();

            if( name.length < 1 ){
                common_ops.tip( "请输入符合规范的名称" ,name_target );
                return;
            }
       
            if(ratio.length > 0){
                var ratio_retel = /^(0.[\d+]{0,2}|1)$/;
                if (ratio_retel.test(ratio) == false) {
                    common_ops.tip('请取0.01~1直接的值', ratio_target);
                    return false;
                }
            }

            var sort_retel = /^[0-9]*[1-9][0-9]*$/;
            if(sort_retel.test(sort) == false){
                common_ops.tip('请输入整数', sort_tarage);
                return false;
            }

            btn_target.addClass("disabled");

            var data = {
                name:name,
                ratio:ratio,
                sort:sort,
                id:$(".wrap_grade_set input[name=id]").val()
            };

            $.ajax({
                url:common_ops.buildWebUrl("/member/grade_set") ,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(res){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/member/grade");
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    }
};

$(document).ready( function(){
    member_grade_ops.init();
});