;
var member_index_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        var that = this;
        $(".remove").click(function () {
            that.ops("remove", $(this).attr("data"))
        });

        $(".normal").click(function () {
            that.ops("normal", $(this).attr("data"))
        });

        $(".ban").click(function () {
            that.ops("ban", $(this).attr("data"))
        });

        $(".wrap_search .search").click(function () {
            $(".wrap_search").submit();
        });

    },
    ops: function (act, id) {
        var callback = {
            'ok': function () {
                $.ajax({
                    url: common_ops.buildWebUrl("/member/ops"),
                    type: 'POST',
                    data: {
                        act: act,
                        id: id
                    },
                    dataType: 'json',
                    success: function (res) {
                        var callback = null;
                        if (res.code == 200) {
                            callback = function () {
                                window.location.href = window.location.href;
                            }
                        }
                        common_ops.alert(res.msg, callback);
                    }
                });
            },
            'cancel': null
        };
        var msg = "";
        switch (act)
        {
            case "normal":
                msg = "确定禁止？";
                break;
            case "ban":
                msg = "确定恢复？";
                break;

            case "remove":
                msg = "确定删除？";
                break;
            default:
                msg ="确定操作？";
                break;
        }
        common_ops.confirm(msg, callback);
    },
    scan_QR_code_ops: function (act, id) {
        var callback = {
            'ok': function () {
                $.ajax({
                    url: common_ops.buildWebUrl("/member/scan_ops"),
                    type: 'POST',
                    data: {
                        act: act,
                        id: id
                    },
                    dataType: 'json',
                    success: function (res) {
                        var callback = null;
                        if (res.code == 200) {
                            callback = function () {
                                window.location.href = window.location.href;
                            }
                        }
                        common_ops.alert(res.msg, callback);
                    }
                });
            },
            'cancel': null
        };
       
        common_ops.confirm((act == "recover") ? "确定设置此用户为课程扫码人员？" : "取消此用户为课程扫码权限？", callback);
    }

};

$(document).ready(function () {
    member_index_ops.init();
});