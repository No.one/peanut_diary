;
var goods_index_ops = {
    init:function(){
        this.eventBind();
    },
    eventBind:function(){
        var that = this;
        $(".remove").click( function(){
            that.ops( "remove",$(this).attr("data") )
        });

        $("input[name=stock]").mouseleave(function () {
            var obj = $(this);
            // common_ops.msg( '操作成功', 5);
            var stock = obj.val();
            if(stock.length!=0){
                var data = {
                    member_id:$("input[name=id]").val(),
                    stock:stock,
                    goods_id:$(this).data('goods_id')
                };
                that.ops("stock",data)
            }
        });

        $(".wrap_search .search").click( function(){
            $(".wrap_search").submit();
        });
    },
    ops:function( act,data ){
        var callback = {
            'ok':function(){
                // console.log(data);
                $.ajax({
                    url:common_ops.buildWebUrl("/member/stock"),
                    type:'POST',
                    data:data,
                    dataType:'json',
                    success:function( res ){
                        var callback = null;
                        if( res.code == 200 ){
                            callback = function(){
                                window.location.href = window.location.href;
                            }
                        }
                        common_ops.alert( res.msg,callback );
                    }
                });
            },
            'cancel':null
        };
        common_ops.confirm( "确定设置？",callback );
    }
};

$(document).ready( function(){
    goods_index_ops.init();
});