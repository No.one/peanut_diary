;

var member_set_ops = {
    init:function(){
        this.eventBind();
    },
    eventBind:function(){

        $(".wrap_member_set .save").click( function(){
            
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var id = $(".wrap_member_set input[name=id]").val();
            var nickname_target = $(".wrap_member_set input[name=nickname]");
            var nickname = nickname_target.val();
            var truename_target = $(".wrap_member_set input[name=truename]");
            var truename = truename_target.val();
            var mobile_target = $(".wrap_member_set input[name=mobile]");
            var mobile = mobile_target.val();
            var password_target = $(".wrap_member_set input[name=password]");
            var password = password_target.val();
            var grade_id_target = $(".wrap_member_set select[name=grade_id]");
            var grade_id = grade_id_target.val();

            if( nickname.length < 1 ){
                common_ops.tip( "请输入昵称" ,nickname_target );
                return;
            }
            if(id.length < 1 && password.length < 6){
                common_ops.tip( "请输入6-15位111的登录密码" ,password_target );
                return;
            }

            if( truename.length < 1 ){
                common_ops.tip( "请输入符合规范的姓名" ,truename_target );
                return;
            }

            var mobile_retel = /^[1][3,4,5,7,8,9][0-9]{9}$/;
            if (mobile_retel.test(mobile) == false) {
                common_ops.tip('请输入正确的手机号码', mobile_target);
                return false;
            }

            if(grade_id == 0){
                common_ops.tip('请选择用户等级', grade_id_target);
                return false;
            }

             btn_target.addClass("disabled");

            var data = {
                nickname:nickname,
                truename:truename,
                mobile:mobile,
                password:password,
                grade_id:grade_id,
                id:$(".wrap_member_set input[name=id]").val()
            };
          
            $.ajax({
                url:common_ops.buildWebUrl("/member/set") ,
                type:'POST',
                data:data,
                dataType:'json',
                success:function(res){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = common_ops.buildWebUrl("/member/index");
                        }
                        common_ops.alert( res.msg,callback );
                    }else if(res.code == -2){
                        $.each(res.data,function(index,value) {
                            common_ops.tip(value, $(".wrap_member_set input[name="+index+"]"));
                            return false;
                        });
                    }else{
                        common_ops.alert( res.msg,callback );
                    }
                }
            });
        });
    }
};

$(document).ready( function(){
    member_set_ops.init();
});