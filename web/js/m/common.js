;
var common_ops = {
    init:function(){
        this.eventBind();
        this.setIconLight();
    },
    eventBind:function(){

    },
    setIconLight:function(){
        var pathname = window.location.pathname;
        var nav_name = null;

        if(  pathname.indexOf("/m/default") > -1 || pathname == "/m" || pathname == "/m/" ){
            nav_name = "default";
        }

        if(  pathname.indexOf("/m/product") > -1  ){
            nav_name = "product";
        }

        if(  pathname.indexOf("/m/user") > -1  ){
            nav_name = "user";
        }

        if( nav_name == null ){
            return;
        }

        $(".footer_fixed ."+nav_name).addClass("aon");
    },
    buildMUrl:function( path ,params){
        var url =   "/m" + path;
        var _paramUrl = '';
        if( params ){
            _paramUrl = Object.keys(params).map(function(k) {
                return [encodeURIComponent(k), encodeURIComponent(params[k])].join("=");
            }).join('&');
            _paramUrl = "?"+_paramUrl;
        }
        return url + _paramUrl

    },
    buildWwwUrl:function( path ,params){
        var url =    path;
        var _paramUrl = '';
        if( params ){
            _paramUrl = Object.keys(params).map(function(k) {
                return [encodeURIComponent(k), encodeURIComponent(params[k])].join("=");
            }).join('&');
            _paramUrl = "?"+_paramUrl;
        }
        return url + _paramUrl

    },
    buildPicUrl:function( bucket,img_key ){
        var upload_config = eval( '(' + $(".hidden_layout_warp input[name=upload_config]").val() +')' );
        var domain = "http://" + window.location.hostname;
        return domain + upload_config[ bucket ] + "/" + img_key;
    },
    notlogin:function( referer ){
        if ( ! referer) {
            common_ops.alert('授权过期,系统将引导您重新授权');
            referer = location.pathname + location.search;
        }
        window.location.href = this.buildMUrl("/user/login",{ referer:referer });
    },
    getCaptcha:function () {
        $(".login_form_wrap .get_captcha").click(function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
				common_ops.alert('正在处理!!请不要重复提交');
                return;
            }

            var mobile = $(".login_form_wrap input[name=mobile]").val();
            var img_captcha = $(".login_form_wrap input[name=img_captcha]").val();


            var retel=/^1[345678][0-9]{9}$/;
            if( retel.test(mobile) == false ){
				common_ops.alert('请输入正确的手机号码');
                return false;
            }

            btn_target.addClass("disabled");
			 //下面就是实现倒计时的效果代码
			function StepTimess(type){
				var id=type;
		 		$(id).attr("disabled",true);
				var d = new Date();
				 d.setSeconds(d.getSeconds() + 10);
				 var m = d.getMonth() + 1;
				 var time = d.getFullYear() + '-' + m + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
				 var end_time = new Date(Date.parse(time.replace(/-/g, "/"))).getTime(),
				 //月份是实际月份-1
				 sys_second = (end_time - new Date().getTime()) / 1000;
				 var timer = setInterval(function() {
					 if (sys_second > 2) {
						 sys_second -= 1;
						 var day = Math.floor((sys_second / 3600) / 24);
						 var hour = Math.floor((sys_second / 3600) % 24);
						 var minute = Math.floor((sys_second / 60) % 60);
						 var second = Math.floor(sys_second % 60);
						 var time_text = '';
						 if (day > 0) {
						 time_text += day + '天';
						 }
						 if (hour > 0) {
						 if (hour < 10) {
						 hour = '0' + hour;
						 }
						 time_text += hour + '小时';
						 }
						 if (minute > 0) {
						 if (minute < 10) {
						 minute = '0' + minute;
						 }
						 time_text += minute + '分';
						 }
						 if (second > 0) {
							 if (second < 10) {
								 second = '0' + second;
								 }
								 time_text += second + '秒';
							 }
						 	 $(id).text(time_text);
						 }else {
						 	 clearInterval(timer);
							 $(id).attr("disabled", false);
							 $(id).text('获取验证码');				 
						 }
					 },1000);
			}
			StepTimess(btn_target);
            $.ajax({
                url: "/default/get_captcha",
                type:'POST',
                data:{
                    'mobile':mobile,
                    'img_captcha':img_captcha,
                    'source':'wechat'
                },
                dataType:'json',
                success:function(res){
					common_ops.alert(res.msg);
                    btn_target.removeClass("disabled");
                    //由于是验证，没有短信通道，直接告知验证码多少了
                    if( res.code != 200 ){
                        $("#img_captcha").click();
                        return;
                    }
                }
            });
        });
   },
    showMorecont:function (click_obj, content_obj, icon_obj) {
        click_obj.click(function(){
            icon_obj.toggleClass('flipy');
            content_obj.toggleClass('showmorecont');
   		});
   },
    alert:function( msg ,cb ){
        layer.open({
            content: msg,
            btn: '确定',
            shadeClose: false,
            yes: function(index){
                if( typeof cb == "function" ){
                    cb();
                }
                layer.close(index);
            }
        });
    },
    confirm:function( msg,callback ){
        callback = ( callback != undefined )?callback: { 'ok':null, 'cancel':null };
        console.log(callback)
        layer.open({
            content:msg
            ,btn: ['确定', '取消']
            ,yes: function(index){
                //确定事件
                if( typeof callback.ok == "function" ){
                    callback.ok();
                }
                layer.close(index);
            },no:function( index ) {
                //取消事件
                if (typeof callback.cancel == "function") {
                    callback.cancel();
                }
                layer.close(index);
            }
        })
    },
    tip:function(msg, times){
        if (!times) times = 2;//2秒后自动关闭
        layer.open({
            content: msg
            ,skin: 'msg'
            ,time: times
        });
    },
};
$(document).ready( function() {
    common_ops.init();
});