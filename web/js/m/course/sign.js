;
var course_sign_set = {
    init:function () {
        this.eventBind();
    },
    eventBind:function () {
        var msg = $(".warp_sign_ops input[name=msg]").val();
        common_ops.alert(msg);
    }
};
$(document).ready(function () {
    course_sign_set.init();
});
