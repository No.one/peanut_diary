;
var course_info_ops = {
    init: function () {
        this.eventBind();
        this.updateViewCount();
    },
    eventBind: function () {
        var that = this;
        $(".pro_fixed").on('click','.add_fav',function () {
            if ($(this).hasClass("has_faved")) {
                return false;
            }
            var fav_this = $(this);
            $.ajax({
                url: common_ops.buildMUrl("/course/fav"),
                type: 'POST',
                data: {
                    book_id: $(this).attr("data"),
                    act: 'set',
                    type: 2
                },
                dataType: 'json',
                success: function (res) {
                    if (res.code == -302) {
                        common_ops.notlogin();
                        return;
                    }
                    fav_this.html('取消收藏');
                    fav_this.removeClass("add_fav");
                    fav_this.addClass("del_fav has_faved");
                    common_ops.tip(res.msg);
                }
            });
        });

        $(".pro_fixed").on('click','.del_fav',function () {
            var fav_this = $(this);
            $.ajax({
                url: common_ops.buildMUrl("/product/fav"),
                type: 'POST',
                data: {
                    id: $(this).attr("data"),
                    act: 'del',
                    type: 2
                },
                dataType: 'json',
                success: function (res) {
                    fav_this.html('加入收藏');
                    fav_this.addClass("add_fav");
                    fav_this.removeClass("del_fav has_faved");
                    common_ops.tip(res.msg);
                }
            });
        });

        $(".sign_now_btn").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert('正在处理!!请不要重复提交');
                return;
            }
            btn_target.addClass('btn_target');
            $.ajax({
                url: common_ops.buildMUrl("/course/course_order_sign"),
                type: 'POST',
                data: {
                    id: $(this).attr("data")
                },
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass('disabled');
                    common_ops.alert(res.msg);
                    window.location.href = window.location.href
                }
            });
        })

        $(".order_now_btn").click(function () {
            window.location.href = common_ops.buildMUrl("/course/order", {
                'id': $(this).attr("data"),
            })
        });

        TouchSlide({
            slideCell: "#slideBox",
            titCell: ".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
            mainCell: ".bd ul",
            effect: "leftLoop",
            autoPage: true,//自动分页
            autoPlay: true //自动播放
        });

        $('.proban').on('click', '.videobox', function () {
            if ($(this).hasClass('play')) {
                $(this).children('video').css({'position': 'absolute', 'z-index': '-1'});
                $(this).children('video').trigger("pause");
                $(this).removeClass('play');
                $(this).addClass('pause hidden');
                $('#slideBox').removeClass('hidden');
            } else {
                $(this).children('video').trigger("play");
                $(this).removeClass('pause');
                $(this).addClass('play');
                $(this).children('video').css({'position': 'relative', 'z-index': '1'});
            }
        })

        $('video').each(function () {
            var myVideo = $(this)[0];
            myVideo.addEventListener('pause', function () {
                $(this).parent().removeClass('play');
                $(this).parent().addClass('pause');
                $('#slideBox').removeClass('hidden');
                if (/(Android)/i.test(navigator.userAgent)) {
                    //alert(navigator.userAgent);
                    $(this).css({'display': 'none'});
                    $(this).parent().addClass('hidden');
                } else {
                    $(this).css({'position': 'absolute', 'z-index': '-1'});
                }
            });
            myVideo.addEventListener('play', function () {
                $(this).parent().removeClass('pause');
                $(this).parent().addClass('play');
                $('#slideBox').addClass('hidden');
                if (/(Android)/i.test(navigator.userAgent)) {
                    //alert(navigator.userAgent);
                    $(this).css({'display': 'block'});
                } else {
                    $(this).css({'position': 'relative', 'z-index': '1'});
                }
            });
            myVideo.addEventListener("webkitendfullscreen", function () {
                $(this).parent().removeClass('play');
                $(this).parent().addClass('pause hidden');
                $('#slideBox').removeClass('hidden');
                if (/(Android)/i.test(navigator.userAgent)) {
                    //alert(navigator.userAgent);
                    $(this).css({'display': 'none'});
                } else {
                    $(this).css({'position': 'absolute', 'z-index': '-1'});
                }
                myVideo.pause();
            }, false);

            // myVideo.addEventListener("mozfullscreenchange", function () {
            //     $(this).parent().removeClass('play');
            //     $(this).parent().addClass('pause');
            //     $('#slideBox').removeClass('hidden');
            //     myVideo.pause();
            //     if (/(Android)/i.test(navigator.userAgent)) {
            //         //alert(navigator.userAgent);
            //         $(this).css({'display': 'none'});
            //     } else {
            //         $(this).css({'position': 'absolute', 'z-index': '-1'});
            //     }
            // }, false);
            // myVideo.addEventListener("webkitfullscreenchange", function () {
            //     $(this).parent().removeClass('play');
            //     $(this).parent().addClass('pause');
            //     $('#slideBox').removeClass('hidden');
            //     myVideo.pause();
            //     if (/(Android)/i.test(navigator.userAgent)) {
            //         //alert(navigator.userAgent);
            //         $(this).css({'display': 'none'});
            //     } else {
            //         $(this).css({'position': 'absolute', 'z-index': '-1'});
            //     }
            // }, false);
        })

        $(".get_video_btn").click(function () {
            $('.videobox').removeClass('hidden');
            $('#slideBox').addClass('hidden');
            var myVideo = $('#videocont')[0];
            myVideo.play();
            var fullscreenvideo = that.fullscreen(myVideo);
            myVideo[fullscreenvideo]();
        });

    },
    updateViewCount:function(){
        $.ajax({
            url:common_ops.buildMUrl("/course/ops"),
            type:'POST',
            data:{
                act:'view_count',
                book_id:$(".pro_fixed input[name=id]").val()
            },
            dataType:'json',
            success:function( res ){
            }
        });
    },
    fullscreen:function(elem) {
        var prefix = 'webkit';
        if (elem[prefix + 'EnterFullScreen']) {
            return prefix + 'EnterFullScreen';
        } else if (elem[prefix + 'RequestFullScreen']) {
            return prefix + 'RequestFullScreen';
        };
        return false;
    }
};
$(document).ready(function () {
    course_info_ops.init();
});