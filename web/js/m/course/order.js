;
var course_order_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        $(".do_order").click(function () {
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert('正在处理!!请不要重复提交');
                return;
            }

            var nickname = $('.course_form_box input[name=nickname]').val();
            if (nickname.length < 1) {
				common_ops.alert('请填写姓名');
                return;
            }
            var mobile = $('.course_form_box input[name=mobile]').val();
            var retel=/^1[345678][0-9]{9}$/;
            if( retel.test(mobile) == false ){
				common_ops.alert('请输入符合要求的手机号码');
                return false;
            }
            var data = [];
            $(".order_list li").each(function () {
                var tmp_course_id = $(this).attr("data");
                var tmp_quantity = $(this).attr("data-quantity");
                data.push(tmp_course_id + "#" + tmp_quantity);
            });

            if (data.length < 1) {
				common_ops.alert('请选择了课程在提交');
                return;
            }
            btn_target.addClass("disabled");
            $.ajax({
                url: common_ops.buildMUrl("/course/order"),
                type: 'POST',
                data: {
                    course_items: data,
                    nickname: nickname,
                    mobile: mobile,
                    sc: $(".op_box input[name=sc]").val()
                },
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = res.data.url;
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
    }
};
$(document).ready(function () {
    course_order_ops.init();
});
