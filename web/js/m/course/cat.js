;
var course_cat_ops = {
    init:function () {
        this.eventBind();
        this.p = 1;
    },
    eventBind:function () {
        var that = this;
        process = true;
        $( window ).scroll( function() {
            if( ( ( $(window).height() + $(window).scrollTop() ) > $(document).height() - 20 ) && process ){
                process = false;
                that.p += 1;
                var data = {
                    p:that.p
                };
                $.ajax({
                    url:common_ops.buildMUrl( "/course/cat_search" ),
                    type:'GET',
                    dataType:'json',
                    data:data,
                    success:function( res ){
                        process = true;
                        if( res.code != 200 ){
                            return;
                        }
                        var html = "";
                        for( idx in res.data.data ){
                            var info = res.data.data[ idx ];
                            html += '<li>' +
                                '<a href="'+common_ops.buildMUrl('/course/cat_info', {'id':info.id})+'">' +
                            '<i class="pic"><img src="'+info.avatar+'"/></i>' +
                                '<h2>'+info.name+'</h2>' +
                                '<span class="fav_list_desc">'+info.summary+'</span>' +
                                '</li>';
                        }
                        $("ul.fav_list").append( html );
                        if( !res.data.has_next ){
                            process = false;
                        }
                    }
                });
            }
        });
    }
};
$(document).ready(function () {
    course_cat_ops.init();
});
