/**
 * Created by Administrator on 2018/1/22.
 */
;
var user_online_set_ops = {
    init: function () {
        this.eventBind();
        this.upload_num = 3; //图片上传数量
    },
    eventBind: function () {
        var that = this;
        $('.layoutbox .save').click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }
            var id = $('.layoutbox input[name=id]').val();

            var summary = $('#summary').val();
            if (summary.length < 1) {
                common_ops.alert("请输入你需要描述的问题");
                return;
            }
            var image = [];
            $(".item input").each(function (index, value) {
                image.push($(value).val());
            });
            if (image.length > that.upload_num) {
                common_ops.tip("上传图片不能多余" + that.upload_num + "张");
                return;
            }

            btn_target.addClass("disabled");
            data = {
                id: id,
                summary: summary,
                image: image
            };
            $.ajax({
                url: common_ops.buildMUrl("/user/online_set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = common_ops.buildMUrl('/user/question');
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });
        })
    },

};
$(document).ready(function () {
    user_online_set_ops.init();
});
