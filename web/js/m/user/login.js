/**
 * Created by Administrator on 2017/12/29.
 */
;
var user_login_ops = {
    init:function () {
        this.eventBind();
    },
    eventBind:function(){
        var than = this;
        $(".login_form_wrap .dologin").click(function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
				common_ops.alert('正在处理!!请不要重复提交');
                return;
            }

            var mobile = $(".login_form_wrap input[name=mobile]").val();
            var captcha_code = $(".login_form_wrap input[name=captcha_code]").val();
			var retel=/^0?(13|15|17|18|14)[0-9]{9}$/;
            if( retel.test(mobile) == false ){
				common_ops.alert('请输入正确的手机号码');
                return false;
            }

            if( captcha_code.length < 1){
				common_ops.alert('请输入手机验证码');
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                mobile:mobile,
                captcha_code:captcha_code,
                referer:$(".hide_wrap input[name=referer]").val()
            };

            $.ajax({
                url:common_ops.buildMUrl("/user/login"),
                type:'POST',
                data:data,
                dataType:'json',
                success:function( res ){
                    btn_target.removeClass("disabled");
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            window.location.href = res.data.url;
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            });
        });
       common_ops.getCaptcha();
    }
};
$(document).ready(function () {
    user_login_ops.init();
});