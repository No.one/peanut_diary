;
var user_address_ops = {
    init:function(){
        this.eventBind();
    },
    eventBind:function(){
        $(".del").click(function () {
            $(this).parent().parent().remove();

            $.ajax({
                url:common_ops.buildMUrl("/user/address_ops"),
                type:'POST',
                data:{
                    id:$(this).attr("data"),
                    act:'del'
                },
                dataType:'json',
                success:function( res ){
                }
            });
        });

        $(".set_default").click(function () {
            $.ajax({
                url:common_ops.buildMUrl("/user/address_ops"),
                type:'POST',
                data:{
                    id:$(this).attr("data"),
                    act:'set_default'
                },
                dataType:'json',
                success:function( res ){
                    var callback = null;
                    if( res.code == 200 ){
                        callback = function () {
                            window.location.href = window.location.href;
                        }
                    }
                    common_ops.alert( res.msg, callback);
                }
            });
        });
    }
};



$(document).ready( function(){
    user_address_ops.init();
});