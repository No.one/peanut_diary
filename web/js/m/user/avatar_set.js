/**
 * Created by Administrator on 2018/1/22.
 */
;
var user_avatar_set_op = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        var that = this;
        $('#submit_btn').click(function () {
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
                common_ops.alert('正在处理!!请不要重复提交');
                return;
            }

            btn_target.addClass("disabled");

            $("#save").trigger('click');
            var image_result = $('#image_base64').val();
            if (image_result != '') {
                $.ajax({
                    url: common_ops.buildMUrl("/user/avatar_set"),
                    type: 'POST',
                    data: {
                        image_result: image_result
                    },
                    dataType: 'json',
                    success: function (res) {
                        btn_target.removeClass("disabled");
                        var callback = null;
                        if (res.code == 200){
                            callback = function () {
                                window.location.href = common_ops.buildMUrl('/user/set_up');
                            }
                        }
                        common_ops.alert(res.msg, callback);
                    }
                });
            }
        })
    },
};
$(document).ready(function () {
    user_avatar_set_op.init();
});
