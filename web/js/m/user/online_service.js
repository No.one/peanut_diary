/**
 * Created by Administrator on 2018/1/22.
 */
;
var user_online_service_ops = {
    init:function () {
        this.eventBind();
    },
    eventBind:function () {
        var that = this;
        $('.online-title').click(function(){
        	$(this).find('.title-moreicon').toggleClass('flipy');
        	$(this).siblings().slideToggle();
        });

        $('.pro_fixed .order_now_btn').click(function () {
            window.location.href = common_ops.buildMUrl('/user/online_set');
        })
    }
};
$(document).ready(function () {
    user_online_service_ops.init();
});
