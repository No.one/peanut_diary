;
var user_order_ops = {
    init: function () {
        this.eventBind();
    },
    eventBind: function () {
        $(".order_header").click(function () {
            $(this).next().toggle();
        });

        $(".close").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var callback = {
                'ok': function () {
                    $.ajax({
                        url: common_ops.buildMUrl("/order/ops"),
                        type: 'POST',
                        data: {
                            act: 'close',
                            id: btn_target.attr("data")
                        },
                        dataType: 'json',
                        success: function (res) {
                            btn_target.removeClass("disabled");
                            var callback = null;
                            if (res.code == 200) {
                                callback = function () {
                                    window.location.href = window.location.href;
                                }
                            }
                            common_ops.alert(res.msg, callback);
                        }
                    });
                },
                'cancel': null
            };
            common_ops.confirm("确认取消订单？", callback);

        });

        $(".confirm_express").click(function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var callback = {
                'ok': function () {
                    $.ajax({
                        url: common_ops.buildMUrl("/order/ops"),
                        type: 'POST',
                        data: {
                            act: 'confirm_express',
                            id: btn_target.attr("data")
                        },
                        dataType: 'json',
                        success: function (res) {
                            var callback = null;
                            if (res.code == 200) {
                                callback = function () {
                                    window.location.href = window.location.href;
                                }
                            }
                            common_ops.alert(res.msg, callback);
                        }
                    });
                },
                'cancel': null
            };
            common_ops.confirm("确认收货？", callback);
        });
    }
};

$(document).ready(function () {
    user_order_ops.init();
});
