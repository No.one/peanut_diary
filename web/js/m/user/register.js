;
var user_register_ops = {
    init:function () {
        this.eventBind();
    },
    eventBind:function(){
        var than = this;
        $(".login_form_wrap .dologin").click(function(){
            var btn_target = $(this);
            if( btn_target.hasClass("disabled") ){
				common_ops.alert('正在处理!!请不要重复提交');
                return;
            }

            var nickname = $(".login_form_wrap input[name=nickname]").val();
            var mobile = $(".login_form_wrap input[name=mobile]").val();
            var captcha_code = $(".login_form_wrap input[name=captcha_code]").val();

			if(nickname.length<1){
				common_ops.alert('请输入昵称');
                return false;
            }
            if(!/^[\u4E00-\u9FA5A-Za-z0-9]{2,20}$/.test(nickname) ){
				common_ops.alert('昵称只支持中文、字母、数字的组合，2-20个字符');
                return false;
            }
            var retel=/^1[345678][0-9]{9}$/;
            if( retel.test(mobile) == false ){
				common_ops.alert('请输入符合要求的手机号码');
                return false;
            }

            if( captcha_code.length < 1){
				common_ops.alert('请输入手机验证码');
                return;
            }

            btn_target.addClass("disabled");

            var data = {
                mobile:mobile,
                nickname:nickname,
                captcha_code:captcha_code,
                referer:$(".hide_wrap input[name=referer]").val()
            };

            $.ajax({
                url:common_ops.buildMUrl("/user/register"),
                type:'POST',
                data:data,
                dataType:'json',
                success:function( res ){
					common_ops.alert(res.msg);
                    btn_target.removeClass("disabled");
                    if( res.code != 200 ){
                        $("#img_captcha").click();
                        return;
                    }
                    window.location.href = res.data.url ;
                }
            });

        });

        common_ops.getCaptcha();
    }
};
$(document).ready(function () {
    user_register_ops.init();
});
