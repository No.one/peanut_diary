;
var user_fav_list = {
    init:function(){
        this.eventBind();
    },
    eventBind:function(){
        $(".fav_list li .del_fav").click(function () {
            $(this).parent().remove();
            $.ajax({
                url:common_ops.buildMUrl("/product/fav"),
                type:'POST',
                data:{
                    id:$(this).attr("data"),
                    act:'del',
                    type: 1
                },
                dataType:'json',
                success:function( res ){
                }
            });
        });

        $(".order_now_btn").click(function () {
            window.location.href = common_ops.buildMUrl("/product/order",{ 'id':$(this).attr("data"), quantity:1 } )
        })
    }
};
$(document).ready( function(){
    user_fav_list.init();
} );
