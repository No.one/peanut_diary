/**
 * Created by Administrator on 2018/1/22.
 */
;
var user_notice_ops = {
    init:function () {
        this.eventBind();
        this.clickRead();
    },
    eventBind:function () {
        var that = this;
    },
    clickRead:function () {
        $('.notice-box li').click(function () {
            $(this).children('a').toggleClass('flipy');
            $(this).find('.hidden').slideToggle('normal');
        });
    }
};
$(document).ready(function () {
    user_notice_ops.init();
});
