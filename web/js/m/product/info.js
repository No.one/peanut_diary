;
var product_info_ops = {
    init:function(){
        this.eventBind();
        this.updateViewCount();
    },
    eventBind:function(){
        $(".pro_fixed").on('click','.add_fav',function () {
            if ($(this).hasClass("has_faved")) {
                return false;
            }
            var fav_this = $(this);
            $.ajax({
                url: common_ops.buildMUrl("/product/fav"),
                type: 'POST',
                data: {
                    book_id: $(this).attr("data"),
                    act: 'set',
                    type: 1
                },
                dataType: 'json',
                success: function (res) {
                    if (res.code == -302) {
                        common_ops.notlogin();
                        return;
                    }
                    fav_this.html('取消收藏');
                    fav_this.removeClass("add_fav");
                    fav_this.addClass("del_fav has_faved");
                    common_ops.tip(res.msg);
                }
            });
        });

        $(".pro_fixed").on('click','.del_fav',function () {
            var fav_this = $(this);
            $.ajax({
                url: common_ops.buildMUrl("/product/fav"),
                type: 'POST',
                data: {
                    id: $(this).attr("data"),
                    act: 'del',
                    type: 1
                },
                dataType: 'json',
                success: function (res) {
                    fav_this.html('加入收藏');
                    fav_this.addClass("add_fav");
                    fav_this.removeClass("del_fav has_faved");
                    common_ops.tip(res.msg);
                }
            });
        });
        
        TouchSlide({ 
			slideCell:"#slideBox",
			titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
			mainCell:".bd ul", 
			effect:"leftLoop", 
			autoPage:true,//自动分页
			autoPlay:true //自动播放
		});
		
		
		$('.proban').on('click','.videobox',function(){
			if ($(this).hasClass('play')) {
				$(this).children('video').css({'position':'absolute','z-index':'-1'});
				$(this).children('video').trigger("pause");
				$(this).removeClass('play');
				$(this).addClass('pause');
			} else {
				$(this).children('video').trigger("play");
				$(this).removeClass('pause');
				$(this).addClass('play');
				$(this).children('video').css({'position':'relative','z-index':'1'});
			}
		})
		
		$('video').each(function(){
            var myVideo=$(this)[0];
            myVideo.addEventListener('pause',function(){
                $(this).parent().removeClass('play');
                $(this).parent().addClass('pause');
				if (/(Android)/i.test(navigator.userAgent)) {
				    //alert(navigator.userAgent);
				    $(this).css({'display':'none'});
				}else{
					 $(this).css({'position':'absolute','z-index':'-1'});
				}
            })
            myVideo.addEventListener('play',function(){
            	$(this).parent().removeClass('pause');
                $(this).parent().addClass('play');
                if (/(Android)/i.test(navigator.userAgent)) {
				    //alert(navigator.userAgent);
				    $(this).css({'display':'block'});
				}else{
					$(this).css({'position':'relative','z-index':'1'});
				}
            })
       })

        $(".add_cart_btn").click( function() {
            $.ajax({
                url:common_ops.buildMUrl("/product/cart"),
                type:'POST',
                data:{
                    act:'set',
                    book_id:$(this).attr("data"),
                    quantity:$(".quantity-form input[name=quantity]").val()
                },
                dataType:'json',
                success:function( res ){
                    if( res.code == -302 ){
                        common_ops.notlogin( );
                        return;
                    }
                    common_ops.alert( res.msg );
                }
            });
        });

        $(".order_now_btn").click( function(){
            window.location.href = common_ops.buildMUrl("/product/order",{ 'id':$(this).attr("data"), quantity:1 } )
        });

        //加减效果 start
        $(".quantity-form .icon_lower").click(function () {
            var num = parseInt($(this).next(".input_quantity").val());
            if (num > 1) {
                $(this).next(".input_quantity").val(num - 1);
            }
        });
        $(".quantity-form .icon_plus").click(function () {
            var num = parseInt($(this).prev(".input_quantity").val());
            var max = parseInt($(this).prev(".input_quantity").attr("max"));
            if (num < max) {
                $(this).prev(".input_quantity").val(num + 1);
            }
        });
        //加减效果 end

    },
    updateViewCount:function(){
        $.ajax({
            url:common_ops.buildMUrl("/product/ops"),
            type:'POST',
            data:{
                act:'view_count',
                book_id:$(".pro_fixed input[name=id]").val()
            },
            dataType:'json',
            success:function( res ){
            }
        });
    }
};

$(document).ready( function(){
    product_info_ops.init();
});