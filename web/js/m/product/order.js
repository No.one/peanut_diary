;
var product_order_ops = {
    init: function () {
        this.province_infos = {};
        this.eventBind();
        this.updateViewCount();
    },
    eventBind: function () {
        var that = this;
        //加减效果 start
        $(".quantity-form .icon_lower").click(function () {
            var id = $(this).attr("data");
            var num = parseInt($(this).next(".input_quantity").val());
            if (num > 1) {
                num = num - 1;
                //$(this).next(".input_quantity").val(num);
            }
            window.location.href = common_ops.buildMUrl("/product/order", {'id': id, quantity: num})
        });
        $(".quantity-form .icon_plus").click(function () {
            var id = $(this).attr("data");
            var num = parseInt($(this).prev(".input_quantity").val());
            var max = parseInt($(this).prev(".input_quantity").attr("max"));
            if (num < max) {
                num = num + 1;
                //$(this).prev(".input_quantity").val(num);
                window.location.href = common_ops.buildMUrl("/product/order", {'id': id, quantity: num})
            } else {
                common_ops.alert('已达库存最大值');
                return false;
            }


        });
        //加减效果 end

        $(".do_order").click(function () {
            var address_id = $(".select_address input[name=address_id]").val();
            if (address_id == undefined) {
                common_ops.alert("请选择收货地址");
                return;
            }

            var data = [];
            $(".order-list li").each(function () {
                var tmp_book_id = $(this).attr("data");
                var tmp_quantity = $(this).attr("data-quantity");
                data.push(tmp_book_id + "#" + tmp_quantity);
            });

            if (data.length < 1) {
                common_ops.alert("请选择了商品在提交");
                return;
            }

            $.ajax({
                url: common_ops.buildMUrl("/product/order"),
                type: 'POST',
                data: {
                    product_items: data,
                    address_id: address_id,
                    sc: $(".op_box input[name=sc]").val()
                },
                dataType: 'json',
                success: function (res) {
                    callback = null;
                    if (res.code == 200) {
                        callback = function () {
                            window.location.href = res.data.url;
                        }
                    }
                    common_ops.alert(res.msg, callback);
                }
            });
        });

        //保存
        $(".address").on('click', '.set_save', function () {
            var btn_target = $(this);
            if (btn_target.hasClass("disabled")) {
                common_ops.alert("正在处理!!请不要重复提交");
                return;
            }

            var nickname = $(".add_address_box input[name=nickname]").val();
            var mobile = $(".add_address_box input[name=mobile]").val();
            var province_id = $(".add_address_box #province_id").val();
            var city_id = $(".add_address_box #city_id").val();
            var area_id = $(".add_address_box #area_id").val();
            var address = $(".add_address_box input[name=address]").val();

            if (!nickname || nickname.length < 1) {
                common_ops.alert("请输入符合规范的收货人姓名");
                return;
            }

            if (!/^[1-9]\d{10}$/.test(mobile)) {
                common_ops.alert("请输入符合规范的收货人手机号码");
                return;
            }

            if (province_id < 1) {
                common_ops.alert("请选择省");
                return;
            }

            if (city_id < 1) {
                common_ops.alert("请选择市");
                return;
            }

            if (area_id < 1) {
                common_ops.alert("请选择区");
                return;
            }

            if (!address || address.length < 3) {
                common_ops.alert("请输入符合规范的收货人详细地址");
                return;
            }

            btn_target.addClass("disabled");
            var data = {
                id: $(".hide_wrap input[name=id]").val(),
                nickname: nickname,
                mobile: mobile,
                province_id: province_id,
                city_id: city_id,
                area_id: area_id,
                address: address
            };
            $.ajax({
                url: common_ops.buildMUrl("/product/address_set"),
                type: 'POST',
                data: data,
                dataType: 'json',
                async: false,
                success: function (res) {
                    btn_target.removeClass("disabled");
                    callback = null;
                    if( res.code == 200 ){
                        callback = function(){
                            $(".alert-box").removeClass('address_box_show');
                            window.location.href = window.location.href;
                        }
                    }
                    common_ops.alert( res.msg,callback );
                }
            })
        });

        //增加地址
        $("body").on('click', '.add_address,.add_address_btn', function () {
            html_str = that.getAddressProvince([]);
            var html = '<div class="cont-title">新建收货地址<a class="alert-close" href="javascript:;"></a></div>' +
                '<div class="hide_wrap"><input type="hidden" name="name" value=""></div>' +
                '<div class="cont-list"><ul><li><span class="li-title">收货人</span>' +
                '<div class="li-cont"><input class="alert-input" type="text" placeholder="名字" name="nickname" value="" /></div>' +
                '</li><li><span class="li-title">联系电话</span>' +
                '<div class="li-cont"><input class="alert-input" type="text" placeholder="手机或固定电话" name="mobile" id="" value="" /></div></li>' +

                '<div class="addr_input_box" style="padding-left: 7.5rem;">' +
                '<span style="left: 8px; font-size: 1.6rem;">选择地区</span>' +
                '<div class="select_box">' +
                '<select id="province_id">' +
                '<option value="0">请选择省</option>' +
                html_str +
                '</select>' +
                '</div>' +
                '<div class="select_box">' +
                '<select id="city_id">' +
                '<option value="0">请选择市</option>' +
                '</select></div>' +
                '<div class="select_box">' +
                '<select id="area_id">' +
                '<option value="0">请选择区</option>' +
                '</select>' +
                '</div>' +
                '</div>' +

                '<li><span class="li-title">地址详情</span>' +
                '<div class="li-cont"><input class="alert-input" type="text" placeholder="如街道，楼层，门牌号等" name="address" id="" value="" /></div>' +
                '</li></ul></div>		<a class="alert-btn-edit set_save" href="javascript:;">确定</a>';
            $(".add_address_box .cont-box").html(html);
            $(".alert-box").removeClass('address_box_show');
            $("body .add_address_box").addClass('address_box_show');

        });

        //选择地址
        $(".address_box").on('click', '.select_address', function () {
            $(".select_address_box").addClass('address_box_show');
            $.ajax({
                url: common_ops.buildMUrl("/product/address"),
                type: 'POST',
                dataType: 'json',
                success: function (res) {
                    var html = "";
                    $.each(res.data, function (index, value) {
                        if (value.is_default == 1) {
                            html += '<li class="active">';
                        } else {
                            html += '<li >';
                        }
                        html += '<dl><dt><span>' +
                            value.nickname + '</span>， <a href="javascript:;">' +
                            value.mobile + '</a><i class="edit_address_icon" data="' +
                            value.id + '"></i></dt><dd>收货地址：' +
                            value.address + '</dd></dl></li>';
                    });
                    $(".select_address_box .cont-list ul").html(html)
                }
            });
        });

        //编辑地址
        $("body").on('click', '.select_address_box .edit_address_icon', function () {
            var id = $(this).attr('data');
            $.ajax({
                url: common_ops.buildMUrl("/product/address_set"),
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (res) {
                    address_info = {
                        province_id: res['data']['province_id'],
                    };
                    html_str = that.getAddressProvince(address_info);
                    var html = '<div class="hidden hide_wrap">' +
                        '<input type="hidden" id="area_id_before" value="'+res['data']['area_id']+'">' +
                        '<input type="hidden" id="city_id_before" value="'+res['data']['city_id']+'">' +
                        '</div>'+
                        '<div class="cont-title">编辑收货地址<a class="alert-close" href="javascript:;"></a></div>' +
                        '<div class="hide_wrap"><input type="hidden" name="id" value="' + res['data']['id'] + '"></div>' +
                        '<div class="cont-list"><ul><li><span class="li-title">收货人</span>' +
                        '<div class="li-cont"><input class="alert-input" type="text" placeholder="名字" name="nickname" value="' + res['data']['nickname'] + '" /></div>' +
                        '</li><li><span class="li-title">联系电话</span>' +
                        '<div class="li-cont"><input class="alert-input" type="text" placeholder="手机或固定电话" name="mobile" id="" value="' + res['data']['mobile'] + '" /></div>' +
                        '</li>' +
                        '<div class="addr_input_box" style="padding-left: 7.5rem;">' +
                        '<span style="left: 8px; font-size: 1.6rem;">选择地区</span>' +
                        '<div class="select_box">' +
                        '<select id="province_id">' +
                        '<option value="0">请选择省</option>' +
                        html_str +
                        '</select>' +
                        '</div>' +
                        '<div class="select_box">' +
                        '<select id="city_id">' +
                        '<option value="0">请选择市</option>' +
                        '</select></div>' +
                        '<div class="select_box">' +
                        '<select id="area_id">' +
                        '<option value="0">请选择区</option>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +

                        '<li><span class="li-title">地址详情</span>' +
                        '<div class="li-cont"><input class="alert-input" type="text" placeholder="如街道，楼层，门牌号等" name="address" id="" value="' + res['data']['address'] + '" /></div>' +
                        '</li></ul></div>		<a class="alert-btn-edit set_save" href="javascript:;">确定</a>' +
                        '<a class="alert-btn-del" href="javascript:;" data="' + res['data']['id'] + '">删除地址</a>';
                    $(".add_address_box .cont-box").html(html);
                    $(".alert-box").removeClass('address_box_show');
                    $("body .add_address_box").addClass('address_box_show');
                    that.init_select();
                }
            });
            return false;
        });

        //关闭按钮
        $("body").on('click', '.alert-close', function () {
            $(".alert-box").removeClass('address_box_show');
        });

        //按钮选中
        $("body").on('click', '.select_address_box li', function () {
            var select_address_box_that = $(this);
            var address_id = $(this).find('.edit_address_icon').attr('data');
            $.ajax({
                url: common_ops.buildMUrl("/product/address_ops"),
                type: 'POST',
                data: {
                    id: address_id,
                    act: 'set_default'
                },
                dataType: 'json',
                success: function (res) {
                    if (res.code == 200) {
                        //插入
                        that.getTmpAddress(res.data);
                        select_address_box_that.addClass('active').siblings().removeClass('active');
                    }
                }
            });
        });

        //删除地址
        $("body").on('click', '.alert-btn-del', function () {
            $.ajax({
                url: common_ops.buildMUrl("/product/address_ops"),
                type: 'POST',
                data: {
                    id: $(this).attr("data"),
                    act: 'del'
                },
                dataType: 'json',
                success: function (res) {
                    window.location.href = window.location.href;
                }
            });
        });

        $(".add_address_box").on('change', '#province_id', function () {
            var id = $(this).val();
            if (id <= 0) {
                return;
            }
            for (var key in that.province_infos) {
                if (key == id) {
                    that.province_cascade();
                    return;
                }
            }
            $.ajax({
                url: common_ops.buildWwwUrl("/default/cascade"),
                data: {'id': id},
                dataType: 'json',
                async: false,
                success: function (res) {
                    if (res.code == 200) {
                        that.province_infos[id] = res.data;
                        that.province_cascade();
                    } else {
                        common_ops.alert(res.msg);
                    }
                }
            })
        });

        $(".add_address_box").on('change', "#city_id", function () {
            that.city_cascade();
        });
    },
    getAddressProvince: function (data) {
        var html_str = ''
        $.ajax({
            url: common_ops.buildMUrl("/product/get_address_province"),
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function (res) {
                $.each(res.data, function (index, value) {
                    html_str += '<option ';
                    if (index == data.province_id){
                        html_str += 'selected';
                    }
                    html_str += ' value="' + index + '">' + value + '</option>';
                });
            }
        });
        return html_str;
    },
    getTmpAddress: function (data) {
        html = '<li class="select_address"><dl>' +
            '<input type="hidden" name="address_id" value="' + data.id + '">' +
            '<dt>收货人：' + data.nickname + '<a href="javascript:;">' + data.mobile + '</a></dt>' +
            '<dd>收货地址：' + data.tmp_area_info + '</dd>' +
            '</dl></li>';
        $('.set_address').html(html);
    },
    updateViewCount: function () {
        $.ajax({
            url: common_ops.buildMUrl("/product/ops"),
            type: 'POST',
            data: {
                act: 'view_count',
                book_id: $(".pro_fixed input[name=id]").val()
            },
            dataType: 'json',
            success: function (res) {
            }
        });
    },
    province_cascade: function () {
        var id = $("#province_id").val();
        var province_info = this.province_infos[id];
        var city_info = province_info.city;
        if (id <= 0) {
            return;
        }
        $("#city_id").html("");
        $("#city_id").append("<option value='0'>请选择市</option>");
        for (var idx in city_info) {
            if (parseInt($("#city_id_before").val()) == city_info[idx]['id']) {
                $("#city_id").append("<option value='" + city_info[idx]['id'] + "' selected='select'>" + city_info[idx]['name'] + "</option>");
                continue;
            }
            $("#city_id").append("<option value='" + city_info[idx]['id'] + "'>" + city_info[idx]['name'] + "</option>");
        }
    },
    city_cascade: function () {
        var id = $("#province_id").val();
        var province_info = this.province_infos[id];
        var city_id = $("#city_id").val();
        var district_info = province_info.district[city_id];
        if (id <= 0 || city_id <= 0) {
            return;
        }
        $("#area_id").html("");
        $("#area_id").append("<option value='0'>请选择区</option>");
        for (var idx in district_info) {
            if (parseInt($("#area_id_before").val()) == district_info[idx]['id']) {
                $("#area_id").append("<option value='" + district_info[idx]['id'] + "' selected='select'>" + district_info[idx]['name'] + "</option>");
                continue;
            }
            $("#area_id").append("<option value='" + district_info[idx]['id'] + "'>" + district_info[idx]['name'] + "</option>");
        }
    },
    init_select: function () {
        if ($("#province_id").val() > 0) {
            $("#province_id").change();
        }
        if ($("#city_id").val() > 0) {
            $("#city_id").change();
        }
    }
};
$(document).ready(function () {
    product_order_ops.init();
});
