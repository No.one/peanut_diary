<?php

namespace app\controllers;


use app\common\components\BaseWebController;
use app\common\components\HttpClient;
use app\common\services\captcha\ValidateCode;
use app\common\services\ConstantMapService;
use app\common\services\PayOrderService;
use app\common\services\sms\SmsService;
use app\common\services\UtilService;
use app\common\services\weixin\TemplateService;
use app\models\course\Course;
use app\models\course\CourseMarketQrcode;
use app\models\member\Member;
use app\models\notice\Notice;
use app\models\pay\PayOrder;
use app\models\pay\PayOrderItem;
use app\models\sms\SmsCaptcha;
use app\common\services\AreaService;
use dosamigos\qrcode\lib\Enum;
use dosamigos\qrcode\QrCode;

class DefaultController extends BaseWebController
{
    public function actionIndex()
    {
        return "hello world!";
    }

    private $captcha_cookie_name = "validate_code";

    public function actionImg_captcha()
    {
        $font_path = \Yii::$app->getBasePath() . "/web/fonts/captcha.ttf";
        $captcha_handle = new ValidateCode($font_path);
        $captcha_handle->doimg();
        $this->setSession($this->captcha_cookie_name, $captcha_handle->getCode());
    }

    public function actionGet_captcha()
    {
        $mobile = $this->post("mobile", "");
        if (!$mobile || !preg_match('/^1[0-9]{10}$/', $mobile)) {
            $this->removeSession($this->captcha_cookie_name);
            return $this->renderJson([], "请输入符合要求的手机号码", -1);
        }

        $member_ip = UtilService::getIP();

        $before_date = date('Y-m-d H:i:s', strtotime('-1 hour'));
        $member_get_sms_captcha_num = SmsCaptcha::find()->where(['ip' => $member_ip])
            ->andWhere(['>=', "created_time", $before_date])->count();
        if ($member_get_sms_captcha_num >= 6)
        {
            return $this->renderJSON([],"一个小时内最多发送6条手机短信验证码",-1);
        }


        //发送手机验证码，能发验证码，能验证
        $model_sms = new SmsCaptcha();
        $model_sms->geneCustomCaptcha($mobile, $member_ip);
        if ($model_sms) {
            return $this->renderJson([], "发送成功".$model_sms->captcha);
        }

        return $this->renderJson([], ConstantMapService::$default_syserror, -1);
    }

    public function actionQrcode()
    {
        $qr_code_url = $this->get("qr_code_url", "");
        header('Content-type: image/png');
        QrCode::png($qr_code_url, false, Enum::QR_ECLEVEL_H, 5, 0, false);
        exit();
    }

    public function actionCascade()
    {
        $province_id = $this->get('id', 0);
        $tree_info = AreaService::getProvinceCityTree($province_id);
        return $this->renderJSON($tree_info);
    }

    /**
     * 接收curl  web/user/notice_set 请求
     * 接收值为  id=1
     * 发送微信信息服务
     */
    public function actionSend_wx_msg(){
        $xml_data = file_get_contents("php://input");
        parse_str( $xml_data);

        if (!$id){
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $notice_info = Notice::find()->where(['id'=>$id])->one();

        $msg = [
            'name' => $notice_info['name'],
            'summary' => $notice_info['summary'],
            'created_time'=> $notice_info['created_time']
        ];
        if ($notice_info){
            if ($notice_info['type'] == 2){
                $member_list = Member::find()->select('id')->where(['status'=>1])->indexBy('id')->all();
                $ids = array_keys($member_list);
                foreach ($ids as $_item_id){
                    TemplateService::bulletinNotice($_item_id, $msg);
                }
            } else {

            }
        } else {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
    }

}