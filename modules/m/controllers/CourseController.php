<?php
namespace app\modules\m\controllers;

use app\common\services\book\BookService;
use app\common\services\ConstantMapService;
use app\common\services\course\CourseService;
use app\common\services\CoursePayOrderService;
use app\common\services\DataHelper;
use app\common\services\PayOrderService;
use app\common\services\QueueListService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\book\Book;
use app\models\course\Course;
use app\models\course\CourseCat;
use app\models\course\CourseMarketQrcode;
use app\models\member\MemberCart;
use app\models\member\MemberFav;
use app\models\member\OauthMemberBind;
use app\models\pay\PayOrder;
use app\models\pay\PayOrderItem;
use app\modules\m\controllers\common\BaseController;

class CourseController extends BaseController
{

    public function actionIndex()
    {
        return false;
        $kw = trim($this->get("kw", ""));
        $sort_field = trim($this->get("sort_field", "default"));
        $sort = trim($this->get("sort", ""));
        $sort = in_array($sort, ['asc', 'desc']) ? $sort : 'desc';

        $list = $this->getSearchData();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'is_free' => $_item['is_free'],
                    'price' => UtilService::encode($_item['price']),
                    'model' => $_item['model'],
                    'main_image_url' => UrlService::buildPicUrl("course", $_item['main_image']),
                    'month_count' => $_item['month_count']
                ];
            }
        }

        $search_conditions = [
            'kw' => $kw,
            'sort_field' => $sort_field,
            'sort' => $sort
        ];

        return $this->render("index", [
            'list' => $data,
            'search_conditions' => $search_conditions
        ]);
    }

    public function actionInfo()
    {
        $id = intval($this->get("id", 0));
        $reback_url = UrlService::buildMUrl("/course/index");
        if (!$id) {
            return $this->redirect($reback_url);
        }

        $info = Course::findOne(['id' => $id]);
        $info['course_pic'] = json_decode($info['course_pic']);
        $pic = $info['course_pic'];
//        if ($info['course_pic']) {
//            array_unshift($pic, $info['main_image']);
//        }

        if (!$info) {
            return $this->redirect($reback_url);
        }
        $has_faved = false;
        if ($this->current_user) {
            $has_faved = MemberFav::find()->where(['member_id' => $this->current_user['id'], 'book_id' => $id, 'type' => 2])->count();
        }
        $course_ops = CourseService::course_has_buyed($info, $this->current_user);
        $has_buyed = $course_ops['state'];

        return $this->render("info", [
            'info' => $info,
            'pic' => $pic,
            'has_faved' => $has_faved,
            'has_buyed' => $has_buyed,
            'qrcode_image' => $course_ops['qrcode_image']
        ]);
    }

    /**
     * 课程报名
     */
    public function actionCourse_order_sign()
    {
        $course_id = $this->post('id', 0);
        $quantity = 1;
        if ($course_id) {
            $course_info = Course::find()->where(['id' => $course_id, 'is_free' => 1])->one();
        }
        $reback_uri = UrlService::buildMUrl('/course/info', ['id' => $course_id]);
        $ret = $this->course_has_buyed($course_info);
        switch ($ret['statue']) {
            case 1:
            case 2:
            case 4:
                return $this->redirect($reback_uri);
                break;
        }
        if ($course_info['price'] != 0) {
            return $this->redirect($reback_uri);
        }

        $discount = isset($params['discount']) ? $params['discount'] : 0;
        $total_price = sprintf("%.2f", $course_info['price']);
        $discount = sprintf("%.2f", $discount);
        $pay_price = $total_price - $discount;
        $pay_price = sprintf("%.2f", $pay_price);
        $date_now = date("Y-m-d H:i:s");
        $connection = PayOrder::getDb();
        $transaction = $connection->beginTransaction();
        try {
            $model_pay_order = new PayOrder();
            $model_pay_order->order_sn = self::generate_order_sn();
            $model_pay_order->member_id = $this->current_user['id'];
            $model_pay_order->pay_type = 0;
            $model_pay_order->pay_source = 0;
            $model_pay_order->target_type = 2;
            $model_pay_order->total_price = $total_price;
            $model_pay_order->discount = $discount;
            $model_pay_order->pay_price = $pay_price;
            $model_pay_order->note = '';
            $model_pay_order->status = 1;
            $model_pay_order->express_status = 1;
            $model_pay_order->express_address_id = 0;
            $model_pay_order->pay_time = $date_now;
            $model_pay_order->updated_time = $date_now;
            $model_pay_order->created_time = $date_now;

            if (!$model_pay_order->save(0)) {
                throw new Exception("创建订单失败");
            }

            $new_item = new PayOrderItem();
            $new_item->pay_order_id = $model_pay_order->id;
            $new_item->member_id = $this->current_user['id'];
            $new_item->quantity = $quantity;
            $new_item->price = $course_info['price'];
            $new_item->target_type = $model_pay_order->target_type;
            $new_item->target_id = $course_info['id'];
            $new_item->status = isset($course_info['status']) ? $course_info['status'] : 1;

            $new_item->name = $course_info['name'];
            $new_item->model = $course_info['model'];
            $new_item->is_free = $course_info['is_free'];
            $new_item->main_image = $course_info['main_image'];
            $new_item->note = isset($_item['note']) ? $_item['note'] : "";
            $new_item->updated_time = $date_now;
            $new_item->created_time = $date_now;

            if (!$new_item->save(0)) {
                throw new Exception("创建订单失败");
            }

            BookService::confirmOrderItem($new_item->id);
            CourseService::createCourseQrcode($model_pay_order, $new_item);

            $transaction->commit();

            //需要做一个队列数据库了,用队列处理销售月统计
            QueueListService::addQueue("pay", [
                'member_id' => $model_pay_order->member_id,
                'target_type' => $model_pay_order->target_type,
                'pay_order_id' => $model_pay_order->id,
            ]);

            return $this->renderJSON([], "报名成功");
        } catch (Exception $e) {
            $transaction->rollBack();
            return self::_err($e->getMessage());
        }
    }

    public static function generate_order_sn()
    {
        do {
            $sn = md5(microtime(1) . rand(0, 9999999) . '!@%egg#$');

        } while (PayOrder::findOne(['order_sn' => $sn]));

        return $sn;
    }

    public function actionCat()
    {
        $data = [];
        $list = $this->actionGetCatData();
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'avatar' => UrlService::buildPicUrl('course', $_item['avatar']),
                    'summary' => $_item['summary']
                ];
            }
        }
        return $this->render('cat', [
            'list' => $data
        ]);
    }

    public function actionCat_search(){
        $list = $this->actionGetCatData();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'avatar' => UrlService::buildPicUrl('course', $_item['avatar']),
                    'summary' => $_item['summary']
                ];
            }
        }
        return $this->renderJson(['data' => $data, 'has_next' => (count($data) == 5) ? 1 : 0]);
    }

    public function actionGetCatData($page_size = 5){
        $p = intval($this->get("p", 1));
        if ($p < 1) {
            $p = 1;
        }
        $query = CourseCat::find()->where(['status' => 1])->orderBy(['weight' => SORT_DESC, 'id' => SORT_DESC]);

        return $query->offset(($p - 1) * $page_size)
            ->limit($page_size)
            ->all();
    }

    public function actionCat_info()
    {
        $id = intval($this->get('id', 0));
        $reback_url = UrlService::buildMUrl("/course/cat");
        if (!$id) {
            return $this->redirect($reback_url);
        }

        $info = CourseCat::findOne(['id' => $id]);
        if (!$info) {
            return $this->redirect($reback_url);
        }
        $data = [];
        $list = Course::find()->where(['cat_id' => $id])->andWhere(['status' => 1])->all();
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'main_image' => UrlService::buildPicUrl('course', $_item['main_image']),
                    'summary' => $_item['summary'],
                    'is_free' => intval($_item['is_free']),
                    'price' => UtilService::encode($_item['price']),
                    'model' => intval($_item['model']),
                ];
            }
        }

        return $this->render('cat_info', [
            'info' => $info,
            'list' => $data
        ]);
    }

    public function actionOps()
    {
        $act = trim($this->post("act", ""));
        $course_id = intval($this->post("book_id", 0));
        $course_info = Course::findOne(['id' => $course_id]);
        if (!$course_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $course_info->view_count += 1;
        $course_info->update(0);
        return $this->renderJson([]);
    }

    public function actionCart()
    {
        if (\Yii::$app->request->isGet) {
            $list = MemberCart::find()->where(['member_id' => $this->current_user['id']])->orderBy(['id' => SORT_DESC])->all();
            $data = [];
            if ($list) {
                $book_mapping = DataHelper::getDicByRelateID($list, Book::className(), "book_id", "id", ['name', 'price', 'main_image', 'stock']);
                foreach ($list as $_item) {
                    $tmp_book_info = $book_mapping[$_item['book_id']];
                    $data[] = [
                        'id' => $_item['id'],
                        'quantity' => $_item['quantity'],
                        'book_id' => $_item['book_id'],
                        'book_price' => $tmp_book_info['price'],
                        'book_stock' => $tmp_book_info['stock'],
                        'book_name' => UtilService::encode($tmp_book_info['name']),
                        'book_main_image' => UrlService::buildPicUrl("book", $tmp_book_info['main_image'])
                    ];
                }
            }
            return $this->render("cart", [
                'list' => $data
            ]);
        }

        $act = trim($this->post("act", ""));
        $id = intval($this->post("id", 0));
        $book_id = intval($this->post("book_id", 0));
        $quantity = intval($this->post("quantity", 0));
        $date_now = date("Y-m-d H:i:s");

        if (!in_array($act, ["del", "set"])) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if ($act == "del") {
            $cart_info = MemberCart::find()->where(['member_id' => $this->current_user['id'], 'id' => $id])->one();
            if ($cart_info) {
                $cart_info->delete();
            }
            return $this->renderJSON([], "操作成功");
        }


        if (!$book_id || !$quantity) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $book_info = Book::findOne(['id' => $book_id]);
        if (!$book_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $cart_info = MemberCart::find()->where(['member_id' => $this->current_user['id'], 'book_id' => $book_id])->one();
        if ($cart_info) {
            $model_cart = $cart_info;
        } else {
            $model_cart = new MemberCart();
            $model_cart->member_id = $this->current_user['id'];
            $model_cart->created_time = $date_now;
        }

        $model_cart->book_id = $book_id;
        $model_cart->quantity = $quantity;
        $model_cart->updated_time = $date_now;
        $model_cart->save(0);

        return $this->renderJSON([], "操作成功");
    }

    public function actionFav()
    {
        $act = trim($this->post("act", ""));
        $type = trim($this->post('type', ''));
        $id = intval($this->post("id", 0));
        $book_id = intval($this->post("book_id", 0));

        if (!in_array($act, ["del", "set"])) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if (!in_array($type, [1, 2])) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if ($act == "del") {
            if (!$id) {
                return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
            }
            $fav_info = MemberFav::find()->where(['member_id' => $this->current_user['id'], 'id' => $id, 'type' => $type])->one();
            if ($fav_info) {
                $fav_info->delete();
            }

            return $this->renderJSON([], "取消收藏");
        }

        if (!$book_id) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $has_faved = MemberFav::find()->where(['member_id' => $this->current_user['id'], 'book_id' => $book_id, 'type' => $type])->count();
        if ($has_faved) {
            return $this->renderJSON([], "已收藏", -1);
        }

        $model_fav = new MemberFav();
        $model_fav->member_id = $this->current_user['id'];
        $model_fav->book_id = $book_id;
        $model_fav->type = $type;
        $model_fav->created_time = date("Y-m-d H:i:s");
        $model_fav->save(0);
        return $this->renderJSON([], "收藏成功");
    }

    public function actionOrder()
    {
        if (\Yii::$app->request->isGet) {
            $course_id = intval($this->get("id", 0));
            $quantity = 1;
            $sc = $this->get("sc", "course");
            $product_list = [];
            $total_pay_money = 0;
            if ($course_id) {
                $course_info = Course::find()->where(['id' => $course_id])->one();
                if ($course_info) {
                    $product_list[] = [
                        'id' => $course_info['id'],
                        'name' => UtilService::encode($course_info['name']),
                        'quantity' => $quantity,
                        'is_free' => $course_info['is_free'],
                        'price' => $course_info['price'],
                        'model' => $course_info['model'],
                        'main_image' => UrlService::buildPicUrl("course", $course_info['main_image'])
                    ];
                    $total_pay_money += $course_info['price'] * $quantity;
                }
            }
            $reback_uri = UrlService::buildMUrl('/course/info', ['id' => $course_id]);
            if ($total_pay_money <= 0) {
                return $this->redirect($reback_uri);
            }

            return $this->render("order", [
                'product_list' => $product_list,
                'total_pay_money' => sprintf("%.2f", $total_pay_money),
                'sc' => $sc
            ]);
        }

        $sc = trim($this->post("sc", ""));
        $course_items = $this->post("course_items", []);
        $nickname = trim($this->post("nickname", ''));
        $mobile = trim($this->post("mobile", ''));

        if (!$course_items) {
            return $this->renderJSON([], "请选择课程之后在提交", -1);
        }

        if (mb_strlen($nickname, 'utf-8') < 1) {
            return $this->renderJSON([], "请填写姓名", -1);
        }

        if (!preg_match("/^1[34578]\d{9}$/", $mobile)) {
            return $this->renderJSON([], "请填写联系电话", -1);
        }

        $course_ids = [];
        foreach ($course_items as $_item) {
            $tmp_item_info = explode("#", $_item);
            $course_ids[] = $tmp_item_info[0];
        }
        $course_mapping = Course::find()->where(['id' => $course_ids])->indexBy("id")->all();
        if (!$course_mapping) {
            return $this->renderJSON([], "请选择课程之后在提交", -1);
        }

        $target_type = 2; //课程
        $items = [];
        foreach ($course_items as $_item) {
            $tmp_item_info = explode("#", $_item);
            $tmp_course_info = $course_mapping[$tmp_item_info[0]];
            $items[] = [
                'price' => $tmp_course_info['price'] * $tmp_item_info[1],
                'quantity' => $tmp_item_info[1],
                'is_free' => $tmp_course_info['is_free'],
                'target_type' => $target_type,
                'name' => $tmp_course_info['name'],
                'model' => $tmp_course_info['model'],
                'main_image' => $tmp_course_info['main_image'],
                'target_id' => $tmp_item_info[0]
            ];
        }

        $params = [
            'pay_type' => 1,
            'pay_source' => 2,
            'target_type' => $target_type,
            'note' => '姓名：' . $nickname . " 电话：" . $mobile . ' 购买课程',
            'status' => -8,
            'express_address_id' => 0,//快递地址
        ];


        $ret = CoursePayOrderService::createPayOrder($this->current_user['id'], $items, $params);

        if (!$ret) {
            return $this->renderJSON([], "提交失败，失败原因：" . PayOrderService::getLastErrorMsg(), -1);
        }

        return $this->renderJSON(['url' => UrlService::buildMUrl("/pay/buy/?pay_order_id={$ret['id']}")], '下单成功,前去支付');
    }

    public function actionSearch()
    {
        $list = $this->getSearchData();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'price' => UtilService::encode($_item['price']),
                    'main_image_url' => UrlService::buildPicUrl("book", $_item['main_image']),
                    'month_count' => $_item['month_count']
                ];
            }
        }
        return $this->renderJson(['data' => $data, 'has_next' => (count($data) == 4) ? 1 : 0]);
    }

    /**
     * 课程签到
     */
    public function actionSign()
    {
        //判断扫码人 信息
        $openid = $this->getSession($this->auth_cookie_current_openid);
        if (!$openid) {
            echo $this->getHtmlScript(ConstantMapService::$default_syserror);
            exit();
        }

        $oauth_member_bind_info = OauthMemberBind::find()->joinWith('member_info')
            ->where([OauthMemberBind::tableName() . '.openid' => $openid])->one();

        if (!$oauth_member_bind_info['member_info']['scan_QR_code']) {
            echo $this->getHtmlScript(ConstantMapService::$default_syserror);
            exit();
        }

        $member_id = $this->get('member_id', 0);
        $order_id = $this->get('order_id', 0);
        $order_sn = $this->get('order_sn', '');
        $order_item_id = $this->get('order_item_id', 0);

        if (!$member_id || !$order_sn || !$order_id || !$order_item_id) {
            echo $this->getHtmlScript('二维码不清楚请重新扫码');
            exit();
        }
        $pay_order_info = PayOrder::find()->where(['id' => $order_id])
            ->andWhere(['member_id' => $member_id])
            ->andWhere(['order_sn' => $order_sn])->one();
        if (!$pay_order_info) {
            echo $this->getHtmlScript("订单不存在");
            exit();
        }

        $pay_order_items = PayOrderItem::find()->where(['pay_order_id' => $order_id])
            ->andWhere(['member_id' => $member_id])->one();
        if (!$pay_order_items) {
            echo $this->getHtmlScript("订单不存在");
            exit();
        }

        $course_market_qrcode_info = CourseMarketQrcode::find()->where(['order_id' => $order_id])
            ->andWhere(['member_id' => $member_id])->andWhere(['order_item_id' => $order_item_id])->one();

        if (!$course_market_qrcode_info) {
            echo $this->getHtmlScript("订单不存在");
        }
        if ($course_market_qrcode_info['status']) {
            echo $this->getHtmlScript("二维码已经使用于时间" . $course_market_qrcode_info['updated_time']);
            exit();
        }
        $course_market_qrcode_info->status = 1;
        $course_market_qrcode_info->updated_time = date('Y-m-d H:i:s');
        $course_market_qrcode_info->save(0);

        echo $this->getHtmlScript("扫码成功");
        exit();
    }

    public function getHtmlScript($msg)
    {
        return $this->render("sign", [
            'msg' => $msg,
        ]);
    }

    private function getSearchData($page_size = 4)
    {
        $kw = trim($this->get("kw", ""));
        $sort_field = trim($this->get("sort_field", "default"));
        $sort = trim($this->get("sort", ""));
        $sort = in_array($sort, ['asc', 'desc']) ? $sort : 'desc';
        $p = intval($this->get("p", 1));
        if ($p < 1) {
            $p = 1;
        }

        $query = Course::find()->where(['status' => 1]);
        if ($kw) {
            $where_name = ['LIKE', 'name', '%' . strtr($kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $where_tags = ['LIKE', 'tags', '%' . strtr($kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $query->andWhere(['OR', $where_name, $where_tags]);
        }

        switch ($sort_field) {
            case "view_count":
            case "month_count":
            case "price":
                $query->orderBy([$sort_field => ($sort == "asc") ? SORT_ASC : SORT_DESC, 'id' => SORT_DESC]);
                break;
            default:
                $query->orderBy(['id' => SORT_DESC]);
                break;
        }

        return $query->offset(($p - 1) * $page_size)
            ->limit($page_size)
            ->all();
    }
}