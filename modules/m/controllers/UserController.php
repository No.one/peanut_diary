<?php

namespace app\modules\m\controllers;

use app\common\services\ConstantMapService;
use app\common\services\DataHelper;
use app\common\services\Express;
use app\common\services\QueueListService;
use app\common\services\UploadService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\common\services\weixin\TemplateService;
use app\models\article\Article;
use app\models\book\Book;
use app\models\City;
use app\models\course\Course;
use app\models\course\CourseMarketQrcode;
use app\models\Express as ModelExpress;
use app\models\market\MarketQrcode;
use app\models\market\QrcodeScanHistory;
use app\models\member\Member;
use app\models\member\MemberAddress;
use app\models\member\MemberComments;
use app\models\member\MemberFav;
use app\models\member\OauthMemberBind;
use app\models\notice\Notice;
use app\models\pay\PayOrder;
use app\models\pay\PayOrderItem;
use app\models\question\QuestionImages;
use app\models\question\UserQuestion;
use app\models\QueueList;
use app\models\sms\SmsCaptcha;
use app\modules\m\controllers\common\BaseController;
use app\common\services\AreaService;
use Codeception\Lib\Generator\Helper;
use yii\base\Exception;


class UserController extends BaseController
{

    public function actionIndex()
    {
        return $this->render('index', [
            'current_user' => $this->current_user
        ]);
    }

    /**
     * 用户注册
     */
    public function actionRegister()
    {
        if (\Yii::$app->request->isGet) {
            return $this->render("register");
        }

        $mobile = trim($this->post("mobile"));
        $nickname = trim($this->post("nickname"));
        $captcha_code = trim($this->post("captcha_code"));
        $date_now = date("Y-m-d H:i:s");

        if (!preg_match('/^[a-zA-Z0-9\x{4e00}-\x{9fa5}]{2,20}$/u', $nickname)) {
            return $this->renderJSON([], "请输入正确的昵称", -1);
        }

        if (mb_strlen($mobile, "utf-8") != 11 || !preg_match("/^[1-9]\d{10}$/", $mobile)) {
            return $this->renderJSON([], "请输入正确的手机号码", -1);
        }

        if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code)) {
            return $this->renderJSON([], "请输入正确的短信验证码", -1);
        }

        $member_info = Member::find()->where(['mobile' => $mobile])->one();
        if ($member_info) {
            $this->renderJSON([], "手机号码已注册，请直接登录", -1);
        }
        $model_member = new Member();
        $model_member->nickname = $nickname;
        $model_member->mobile = $mobile;
        $model_member->setSalt();
        $model_member->avatar = ConstantMapService::$default_avatar;
        $model_member->reg_ip = sprintf("%u", ip2long(UtilService::getIP()));
        $model_member->status = 1;
        $model_member->created_time = $model_member->updated_time = $date_now;
        $model_member->save(0);
        $member_info = $model_member;
        //todo设置登录态
        $this->setLoginStatus($member_info);
        return $this->renderJSON(['url' => UrlService::buildMUrl("/user/index")], "注册成功");
    }

    /**
     * 登录页面
     */
    public function actionLogin()
    {
        if (\Yii::$app->request->isGet) {
            return $this->render("login");
        }

        $mobile = trim($this->post("mobile"));
        $captcha_code = trim($this->post("captcha_code"));
        $date_now = time("Y-m-d H:i:s");
        if (mb_strlen($mobile, "utf-8") != 11 || !preg_match("/^[1][3,4,5,7,8][0-9]{9}$/", $mobile)) {
            return $this->renderJSON([], "请输入正确的手机号码", -1);
        }

        if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code)) {
            return $this->renderJSON([], "请输入正确的短信验证码", -1);
        }

        $member_info = Member::find()->where(["mobile" => $mobile])->one();
        if (!$member_info) {
            $this->renderJSON([], "手机号码未注册", -1);
        }

        if (!$member_info['status']) {
            return $this->renderJSON([], "您的账号已被禁止，请联系客服解决", -1);
        }

        $model_member = new Member();
        $model_member->updated_time = $date_now;
        $model_member->update(0);

        //todo设置登录态
        $this->setLoginStatus($member_info);
        return $this->renderJSON(['url' => UrlService::buildMUrl("/user/index")], "登录成功");
    }

    /**
     * 退出登录
     */
    public function actionLogin_out()
    {
        $this->removeSession($this->auth_cookie_current_openid);
        $this->removeSession($this->auth_cookie_name);
        return $this->redirect(UrlService::buildMUrl("/user/login"));
    }

    /**
     * 帐号绑定之前
     */
    public function actionBind_before()
    {
        return $this->render("bind_before");
    }

    public function actionBind_step1()
    {
        return $this->render("bind_before");
    }

    /**
     * 账号绑定
     * @return string|void
     */
    public function actionBind()
    {
        if (\Yii::$app->request->isGet) {
            $info = $this->current_user;
            return $this->render("bind", [
                'referer' => trim($this->get("referer")),
                'info' => $info
            ]);
        }

        $mobile = trim($this->post("mobile"));
        $nickname = trim($this->post("nickname"));
        $captcha_code = trim($this->post("captcha_code"));
        $referer = trim($this->post("referer", ""));
        $openid = $this->getSession($this->auth_cookie_current_openid);
//        $unionid   = $this->getCookie($this->auth_cookie_current_unionid);
        $unionid = "";
        $date_now = date("Y-m-d H:i:s");

        if (!preg_match('/^[a-zA-Z0-9\x{4e00}-\x{9fa5}]{2,20}$/u', $nickname)) {
            return $this->renderJSON([], "请输入正确的昵称", -1);
        }

        if (mb_strlen($mobile, "utf-8") != 11 || !preg_match("/^[1-9]\d{10}$/", $mobile)) {
            return $this->renderJSON([], "请输入正确的手机号码", -1);
        }

        if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code)) {
            return $this->renderJSON([], "请输入正确的短信验证码", -1);
        }

        $member_info = Member::find()->where(['mobile' => $mobile])->one();
        if (!$member_info) {
            $model_member = new Member();
            $model_member->nickname = $nickname;
            $model_member->mobile = $mobile;
            $model_member->setSalt();
            $model_member->avatar = ConstantMapService::$default_avatar;
            $model_member->reg_ip = sprintf("%u", ip2long(UtilService::getIP()));
            $model_member->status = 1;
            $model_member->created_time = $model_member->updated_time = $date_now;
            $ret = $model_member->save(0);
//            $ret = MemberService::set( [ 'mobile' => $mobile,'passwd' => '' ] );
            if (!$ret) {
                return $this->renderJSON([], '绑定失败', -1);
            }
            $member_info = $model_member;
        }

        if (!$member_info || !$member_info['status']) {
            return $this->renderJSON([], "您的账号已被禁止，请联系客服解决", -1);
        }

        if ($openid) {
            //检查该手机号是否绑定过其他微信（一个手机号只能绑定一个微信,也只能绑定一个支付宝）
            $client_type = ConstantMapService::$client_type_wechat;
            $bind_info = OauthMemberBind::findOne(['member_id' => $member_info['id'], "openid" => $openid, 'type' => $client_type]);

            if (!$bind_info) {
                $model_bind = new OauthMemberBind();
                $model_bind->member_id = $member_info['id'];
                $model_bind->type = $client_type;
                $model_bind->client_type = ConstantMapService::$client_type_mapping[$client_type];
                $model_bind->openid = $openid ?: '';
                $model_bind->unionid = $unionid ?: '';
                $model_bind->extra = '';
                $model_bind->updated_time = $date_now;
                $model_bind->created_time = $date_now;
                $model_bind->save(0);

                //绑定之后要做的事情
                QueueListService::addQueue("bind", [
                    'member_id' => $member_info['id'],
                    'type' => 1,
                    'openid' => $model_bind->openid
                ]);
            }
        }

        //如果用户头像或者unionid没有，就获取//这个时候做登录特殊处理，例如更新用户名和头像等等新
        if (UtilService::isWechat() && ($member_info->avatar == ConstantMapService::$default_avatar || $member_info->nickname == $member_info->mobile)) {
            return $this->renderJSON(['url' => UrlService::buildMUrl("/oauth/login", ['scope' => 'snsapi_userinfo'])], "绑定成功");
        }

        $this->setLoginStatus($member_info);
        return $this->renderJSON(['url' => UrlService::buildMUrl("/user/index")], "绑定成功");
    }



    public function actionFav()
    {
        $type = $this->get('type', 0);
        $type = $type ? $type : 2;

        $list = MemberFav::find()->where(['member_id' => $this->current_user['id']])
            ->andWhere(['type' => $type])->orderBy(['id' => SORT_DESC])->all();

        $data = [];
        if ($list) {
            $book_mapping = DataHelper::getDicByRelateID($list, Book::className(), "book_id", "id", ['name', 'price', 'main_image', 'stock']);
            $course_mapping = DataHelper::getDicByRelateID($list, Course::className(), "book_id", "id",
                ['name', 'price', 'main_image', 'is_free', 'model', 'summary']);

            foreach ($list as $_item) {
                if ($_item['type'] == 1) {
                    $tmp_book_info = $book_mapping[$_item['book_id']];
                    $data[] = [
                        'id' => $_item['id'],
                        'book_id' => $_item['book_id'],
                        'book_price' => $tmp_book_info['price'],
                        'book_name' => UtilService::encode($tmp_book_info['name']),
                        'book_main_image' => UrlService::buildPicUrl("book", $tmp_book_info['main_image']),
                        'url' => UrlService::buildMUrl("/product/info", ['id' => $_item['book_id']]),
                    ];

                } else {
                    $tmp_book_info = $course_mapping[$_item['book_id']];
                    $data[] = [
                        'id' => $_item['id'],
                        'book_id' => $_item['book_id'],
                        'book_price' => $tmp_book_info['price'],
                        'book_name' => UtilService::encode($tmp_book_info['name']),
                        'book_main_image' => UrlService::buildPicUrl("course", $tmp_book_info['main_image']),
                        'is_free' => $tmp_book_info['is_free'],
                        'model' => $tmp_book_info['model'],
                        'summary' => $tmp_book_info['summary'],
                        'url' => UrlService::buildMUrl("/course/info", ['id' => $_item['book_id']]),
                    ];
                }

            }
        }

        return $this->render("fav", [
            'list' => $data,
            'type' => $type,
        ]);
    }

    public function actionAddress()
    {
        $list = MemberAddress::find()->where(['member_id' => $this->current_user['id'], 'status' => 1])
            ->orderBy(['is_default' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all();
        $data = [];
        if ($list) {
            $area_mapping = DataHelper::getDicByRelateID($list, City::className(), "area_id", "id", ['province', 'city', 'area']);
            foreach ($list as $_item) {
                $tmp_area_info = $area_mapping[$_item['area_id']];
                $tmp_area = $tmp_area_info['province'] . $tmp_area_info['city'];
                if ($_item['province_id'] != $_item['city_id']) {
                    $tmp_area .= $tmp_area_info['area'];
                }

                $data[] = [
                    'id' => $_item['id'],
                    'is_default' => $_item['is_default'],
                    'nickname' => UtilService::encode($_item['nickname']),
                    'mobile' => UtilService::encode($_item['mobile']),
                    'address' => $tmp_area . UtilService::encode($_item['address']),
                ];
            }
        }
        return $this->render('address', [
            'list' => $data
        ]);
    }

    public function actionAddress_set()
    {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = MemberAddress::find()->where(['id' => $id, 'member_id' => $this->current_user['id']])->one();
            }
            return $this->render('address_set', [
                "province_mapping" => AreaService::getProvinceMapping(),
                'info' => $info
            ]);
        }

        $id = intval($this->post("id", 0));
        $nickname = trim($this->post("nickname", ""));
        $mobile = trim($this->post("mobile", ""));
        $province_id = intval($this->post("province_id", 0));
        $city_id = intval($this->post("city_id", 0));
        $area_id = intval($this->post("area_id", 0));
        $address = trim($this->post("address", ""));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($nickname, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的收货人姓名", -1);
        }

        if (!preg_match("/^[1-9]\d{10}$/", $mobile)) {
            return $this->renderJSON([], "请输入符合规范的收货人手机号码", -1);
        }

        if ($province_id < 1) {
            return $this->renderJSON([], "请选择省", -1);
        }

        if ($city_id < 1) {
            return $this->renderJSON([], "请选择市", -1);
        }

        if ($area_id < 1) {
            return $this->renderJSON([], "请选择区", -1);
        }

        if (mb_strlen($address, "utf-8") < 3) {
            return $this->renderJSON([], "请输入符合规范的收货人详细地址", -1);
        }

        $info = [];
        if ($id) {
            $info = MemberAddress::find()->where(['id' => $id, 'member_id' => $this->current_user['id']])->one();
        }

        if ($info) {
            $model_address = $info;
        } else {
            $model_address = new MemberAddress();
            $model_address->member_id = $this->current_user['id'];
            $model_address->status = 1;
            $model_address->created_time = $date_now;
        }

        $model_address->nickname = $nickname;
        $model_address->mobile = $mobile;
        $model_address->province_id = $province_id;
        $model_address->city_id = $city_id;
        $model_address->area_id = $area_id;
        $model_address->address = $address;
        $model_address->updated_time = $date_now;
        $model_address->save(0);

        return $this->renderJSON([], "操作成功");
    }

    public function actionAddress_ops()
    {
        $act = trim($this->post("act", ""));
        $id = intval($this->post("id", 0));

        if (!in_array($act, ["del", "set_default"])) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if (!$id) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $info = MemberAddress::find()->where(['member_id' => $this->current_user['id'], 'id' => $id])->one();
        switch ($act) {
            case "del":
                $info->is_default = 0;
                $info->status = 0;
                break;
            case "set_default":
                $info->is_default = 1;
                break;
        }

        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);

        if ($act == "set_default") {
            MemberAddress::updateAll(
                ['is_default' => 0],
                ['AND', ['member_id' => $this->current_user['id'], 'status' => 1], ['!=', 'id', $id]]
            );
        }
        return $this->renderJSON([], "操作成功");
    }

    public function actionComment()
    {
        $list = MemberComments::find()->where(['member_id' => $this->current_user['id']])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();

        return $this->render('comment', [
            'list' => $list
        ]);
    }

    public function actionComment_set()
    {
        if (\Yii::$app->request->isGet) {
            $pay_order_id = intval($this->get("pay_order_id", 0));
            $book_id = intval($this->get("book_id", 0));
            $pay_order_info = PayOrder::findOne(['id' => $pay_order_id, 'status' => 1, 'express_status' => 1]);
            $reback_url = UrlService::buildMUrl("/user/index");
            if (!$pay_order_info) {
                return $this->redirect($reback_url);
            }

            $pay_order_item_info = PayOrderItem::findOne(['pay_order_id' => $pay_order_id, 'target_id' => $book_id]);
            if (!$pay_order_item_info) {
                return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
            }

            if ($pay_order_item_info['comment_status']) {
                return $this->renderJS("您已经评论过啦，不能重复评论", $reback_url);
            }


            return $this->render('comment_set', [
                'pay_order_info' => $pay_order_info,
                'book_id' => $book_id
            ]);
        }

        $pay_order_id = intval($this->post("pay_order_id", 0));
        $book_id = intval($this->post("book_id", 0));
        $score = intval($this->post("score", 0));
        $content = trim($this->post('content', ''));
        $date_now = date("Y-m-d H:i:s");

        if ($score <= 0) {
            return $this->renderJSON([], "请打分", -1);
        }

        if (mb_strlen($content, "utf-8") < 3) {
            return $this->renderJSON([], "请输入符合要求的评论内容", -1);
        }

        $pay_order_info = PayOrder::findOne(['id' => $pay_order_id, 'status' => 1, 'express_status' => 1]);
        if (!$pay_order_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $pay_order_item_info = PayOrderItem::findOne(['pay_order_id' => $pay_order_id, 'target_id' => $book_id]);
        if (!$pay_order_item_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if ($pay_order_item_info['comment_status']) {
            return $this->renderJSON([], "您已经评论过啦，不能重复评论", -1);
        }

        $book_info = Book::findOne(['id' => $book_id]);
        if (!$book_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $model_comment = new MemberComments();
        $model_comment->member_id = $this->current_user['id'];
        $model_comment->book_id = $book_id;
        $model_comment->pay_order_id = $pay_order_id;
        $model_comment->score = $score * 2;
        $model_comment->content = $content;
        $model_comment->created_time = $date_now;
        $model_comment->save(0);

        $pay_order_item_info->comment_status = 1;
        $pay_order_item_info->update(0);

        $book_info->comment_count += 1;
        $book_info->update(0);


        return $this->renderJSON([], "评论成功");
    }

    public function actionExpress_info()
    {
        $order_id = intval($this->get('order_id', 0));
        if (!$order_id) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $pay_order_info = PayOrder::find()->where(['id' => $order_id])->one();
        $express_info = ModelExpress::find()->where(['id' => $pay_order_info['express_id']])->one();
        $express_list = Express::get_express($express_info['e_code'], $pay_order_info['express_info']);
        return $this->render('express_info', [
            'express_list' => $express_list
        ]);
    }

    //设置
    public function actionSet_up()
    {
        $has_in = OauthMemberBind::find()->where(['member_id' => $this->current_user['id'], 'type' => 1])->count();
        $data = [
            'nickname' => $this->current_user['nickname'],
            'mobile' => $this->current_user['mobile'],
            'avatar_url' => UrlService::buildPicUrl('avatar', $this->current_user['avatar']),
            'has_in' => $has_in ? $has_in : 0,
        ];

        return $this->render('set_up', [
            'info' => $data
        ]);
    }

    public function actionAvatar_set()
    {
        if (\Yii::$app->request->isGet) {
            $info = Member::find()->where(['id' => $this->current_user['id']])->one();
            $avatar = UrlService::buildPicUrl('avatar', $info['avatar']);

            return $this->render('avatar_set', [
                'avatar' => $avatar,
            ]);
        }

        $image_result = $this->post('image_result', '');
        $image = UploadService::uploadByUrl($image_result, 'avatar');
        if ($image['code'] != 200) {
            return $this->renderJSON([], "上传失败");
        }
        $member_info = $this->current_user;
        $member_info->avatar = $image['path'];
        $member_info->updated_time = date('Y-m-d H:i:s', time());
        $member_info->save(0);
        return $this->renderJSON([], "上传成功");
    }

    //公告
    public function actionNotice()
    {
        $query = Notice::find()->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC]);
        $where_like = ['LIKE', 'to_member_id', '%,' . strtr($this->current_user['id'], ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . ',%', false];
        $where_eq = ['to_member_id' => 'all'];
        $where_eq_id = ['to_member_id' => $this->current_user['id']];
        $query->andWhere(['OR', $where_like, $where_eq, $where_eq_id]);

        $notice_list = $query->all();

        return $this->render('notice', [
            'list' => $notice_list
        ]);
    }

    //在线客服
    public function actionOnline_service()
    {
        $list = Article::find()->where(['status' => 1])->orderBy(['weight' => SORT_ASC, 'id' => SORT_DESC])->all();
        return $this->render('online_service', [
            'list' => $list
        ]);

    }

    //在线问答
    public function actionOnline_set()
    {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = UserQuestion::find()->where(['id' => $id])->one();
            if (!$info) {
                $id = 0;
            }
            return $this->render('online_set', [
                'id' => $id
            ]);
        }

        $id = intval($this->post('id', 0));
        $summary = trim($this->post('summary', ''));
        $image = $this->post('image', []);
        $now_date = date('Y-m-d H:i:s');

        if (mb_strlen($summary, 'utf-8') < 1) {
            return $this->renderJSON([], '请输入你需要描述的问题');
        }
        $user_question_info = UserQuestion::find()->where([])->count();
        if (!$user_question_info) {
            $id = 0;
        }
        $connection = UserQuestion::getDb();
        $transaction = $connection->beginTransaction();

        $user_question_model = new UserQuestion();
        $user_question_model->p_id = $id;
        $user_question_model->member_id = $this->current_user['id'];
        $user_question_model->member_name = $this->current_user['mobile'] . '<br>' . $this->current_user['nickname'];
        $user_question_model->role = 2;
        $user_question_model->name = mb_substr($summary, 0, 20);
        $user_question_model->summary = $summary;
        $user_question_model->status = 1;
        $user_question_model->updated_time = $now_date;
        $user_question_model->created_time = $now_date;
        $ret = $user_question_model->save(0);
        if ($ret) {
            if ($image) {
                foreach ($image as $_item) {
                    $question_images_model = new QuestionImages();
                    $question_images_model->question_id = $user_question_model->id;
                    $question_images_model->image_key = $_item;
                    $question_images_model->created_time = $now_date;
                    if (!$question_images_model->save(0)) {
                        return $this->renderJSON([], '操作失败', -1);
                    }
                }
            }
        } else {
            return $this->renderJSON([], '操作失败', -1);
        }

        $transaction->commit();
        return $this->renderJSON([], '操作成功');

    }

    //我的答疑
    public function actionQuestion()
    {
        $list = UserQuestion::find()->joinWith('image')->where(['user_question.p_id' => 0, 'member_id' => $this->current_user['id']])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();
        return $this->render('question', [
            'list' => $list,
        ]);
    }

    //我的答疑
    public function actionQuestion_info()
    {
        $id = intval($this->get('id', 0));
        $info = [];
        if ($id) {
            $info = UserQuestion::find()->joinWith('image')->where(['user_question.id' => $id])->asArray()->one();
            $info['message'] = UserQuestion::find()->joinWith('image')->where(['p_id' => $info['id']])
                ->orderBy(['created_time' => SORT_ASC, 'id' => SORT_ASC])->asArray()->all();
        }
        return $this->render('question_info', [
            'info' => $info
        ]);
    }

    public function actionBuilding()
    {
        return $this->render('building');
    }
}
