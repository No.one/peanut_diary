<?php
namespace app\modules\m\controllers;


use app\common\services\UploadService;
use app\common\services\UrlService;
use app\modules\m\controllers\common\BaseController;

class UploadController extends BaseController
{
    protected $allow_file = ["gif", "jpg", "png", "jpeg"];

    /**
     * 上传接口
     * bucket: avatar/brand/book
     */
    public function actionPic()
    {
        $bucket = $this->post('bucket');
        $type = $this->post('type');
        $call_back_target = 'window.parent.upload';

        if (!$_FILES || !isset($_FILES['file'])) {
            return "<script type='text/javascript'>{$call_back_target}.error('没有选择文件');</script>";
        }

        $file_name = $_FILES['file']['name'];

        $tmp_file_extend = explode(".", $file_name);
        if (!in_array(strtolower(end($tmp_file_extend)), $this->allow_file)) {
            return "<script type='text/javascript'>{$call_back_target}.error('请上传图片文件,jpg,png,jpeg,gif');</script>";
        }

        $ret = UploadService::uploadByFile($_FILES['file']['name'], $_FILES['file']['tmp_name'], $bucket);

        if ($ret['code'] == 200) {
            $path = UrlService::buildPicUrl($bucket, $ret['path']);
            echo json_encode(array("error" => "0", "src" => $path, "name" => $ret['path']));
        } else {
            echo json_encode(array("error" => "上传有误，清检查服务器配置！"));
        }
    }
}