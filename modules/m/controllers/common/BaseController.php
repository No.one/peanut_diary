<?php

namespace app\modules\m\controllers\common;
use app\common\components\BaseWebController;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\member\Member;

class BaseController extends BaseWebController {

	protected  $auth_cookie_current_openid = "taichi_m_openid";
	protected  $auth_cookie_name = "taichi_member";
	protected  $salt = "dm3HsNYz3Uyddd46Rjg";
	protected  $current_user = null;

	/*这部分永远不用登录*/
	protected $allowAllAction = [
		'm/oauth/login',
		'm/oauth/logout',
		'm/oauth/callback',

		'm/user/bind',
//        'm/user/bind_before',
        'm/user/bind_step1',
		'm/user/register',
		'm/user/login',
        'm/user/login_out',

		'm/pay/callback',

		'm/product/ops',
		'm/product/search',
        'm/product/index',

        'm/course/index',
        'm/course/ops',
        'm/course/info',
        'm/course/cat',
        'm/course/cat_info',
        'm/course/cat_search',

        'm/user/building'
	];

	/**
	 * 以下特殊url
	 * 如果在微信中,可以不用登录(但是必须要有openid)
	 * 如果在H5浏览器,可以不用登录
	 */
	public $special_AllowAction = [
		'm/default/index',

		'm/product/index',
		'm/product/info',

        'm/user/bind',
//        'm/user/bind_before',

        'm/course/index',
        'm/course/info',
        'm/course/cat',
        'm/course/cat_info',
	];

	public function __construct($id, $module, $config = []){
		parent::__construct($id, $module, $config = []);
		$this->layout = "main";

		\Yii::$app->view->params['share_info'] = json_encode( [
			'title' => \Yii::$app->params['title'],
			'desc' => \Yii::$app->params['title'],
			'img_url' => UrlService::buildWwwUrl("/images/common/qrcode.jpg"),
		] );
	}

	public function beforeAction( $action ){
		$login_status = $this->checkLoginStatus();
		$this->setMenu();

        //设置头部title
        $this->getTitle($action->getUniqueId());

        if( UtilService::isWechat() ){
            $openid = $this->getSession( $this->auth_cookie_current_openid );
            if( !$openid ){
                if ($action->getUniqueId() == 'm/user/bind'){
                    //登录后判断的页面
                    $redirect_url = UrlService::buildMUrl( "/oauth/login" , ['type'=>2]);
                    $this->redirect( $redirect_url );
                }
            }
        }

		if ( in_array($action->getUniqueId(), $this->allowAllAction ) ) {
			return true;
		}
		if( !$login_status ){
			if( \Yii::$app->request->isAjax ){
				$this->renderJSON([],"未登录,系统将引导您重新登录",-302);
			}else{
				$redirect_url = UrlService::buildMUrl( "/user/login" );

				if( UtilService::isWechat() ){
					$openid = $this->getSession( $this->auth_cookie_current_openid );
					if( $openid ){
						if( in_array( $action->getUniqueId() ,$this->special_AllowAction ) ){
							return true;
						}
					}else{
						$redirect_url = UrlService::buildMUrl( "/oauth/login" );
					}
				}else{
					if( in_array( $action->getUniqueId() ,$this->special_AllowAction ) ){
						return true;
					}
				}
				if ($action->getUniqueId() == 'm/user/bind_before'){
				    //绑定页面 判断
                    $redirect_url = UrlService::buildMUrl( "/oauth/login" , ['type'=>1]);
                }
				$this->redirect( $redirect_url );
			}
			return false;
		}
		return true;
	}

	protected function checkLoginStatus(){

		$auth_cookie = $this->getSession( $this->auth_cookie_name );

		if( !$auth_cookie ){
			return false;
		}
		list($auth_token,$member_id) = explode("#",$auth_cookie);
		if( !$auth_token || !$member_id ){
			return false;
		}
		if( $member_id && preg_match("/^\d+$/",$member_id) ){
			$member_info = Member::findOne([ 'id' => $member_id,'status' => 1 ]);
			if( !$member_info ){
				$this->removeAuthToken();
				return false;
			}
			if( $auth_token != $this->geneAuthToken( $member_info ) ){
				$this->removeAuthToken();
				return false;
			}
			$this->current_user = $member_info;
			\Yii::$app->view->params['current_user'] = $member_info;
			return true;
		}
		return false;
	}

	public function setLoginStatus( $user_info ){
		$auth_token = $this->geneAuthToken( $user_info );
		$this->setSession($this->auth_cookie_name,$auth_token."#".$user_info['id']);
	}

	protected  function removeLoginStatus(){
		$this->removeSession($this->auth_cookie_name);
	}

	public function geneAuthToken( $member_info ){
		return md5( $this->salt."-{$member_info['id']}-{$member_info['mobile']}-{$member_info['salt']}");
	}

	protected function setMenu(){

		$menu_hide = false;
		$url = \Yii::$app->request->getPathInfo();
		if( stripos($url,"product/info") !== false ){
			$menu_hide = true;
		}
        $menu_hide = true;  // 屏蔽菜单栏
		\Yii::$app->view->params['menu_hide'] = $menu_hide;
	}

	public function goHome(){
		return $this->redirect( UrlService::buildMUrl( "/user/index" ) );
	}

    public function removeAuthToken(){
        $this->removeLoginStatus();
        $this->removeSession( $this->auth_cookie_current_openid );
    }

    /**
     * 设置title
     * @param $action
     */
    public function getTitle($action){
	    $title = $this->headTitle();
        if(array_key_exists($action,$title)){
            \Yii::$app->params['title'] = $title[$action];
        }
    }

    private function headTitle(){
        return [
            'm/course/cat' => '五季名家',
            'm/course/cat_info' => '名家详情',
            'm/course/info' => '课程详情',
            'm/course/order' => '确认订单',
            'm/pay/buy' => '在线支付',
            'm/product/index' => '周边产品',
            'm/product/info' => '产品详情',
            'm/product/order' => '确认订单',
            'm/user/course_order' => '我的课程',
            'm/user/online_set' => '在线问答',
            'm/user/fav' => '我的收藏',
            'm/user/question' => '我的答疑',
            'm/user/product_order' => '已购订单',
            'm/user/notice' => '新闻动态',
            'm/user/index' => '个人中心',
            'm/user/avatar_set' => '更换头像',
            'm/user/set_up' => '个人资料',
            'm/user/address' => '地址管理',
            'm/user/address_set' => '地址管理',
            'm/user/login' => '登录',
            'm/user/bind_before' => '选择绑定',
            'm/user/register' => '注册',
            'm/user/bind' => '帐号绑定',
            'm/user/express_info' => "快递信息",
            'm/user/online_service' => '在线客服'
        ];
    }

}