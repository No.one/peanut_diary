<?php
use app\common\services\StaticService;
use app\common\services\UrlService;
use app\common\services\UtilService;

StaticService::includeAppCssStatic("/plugins/swiperbox/css/swipebox.css", \app\assets\MAsset::className());
StaticService::includeAppJsStatic("/plugins/swiperbox/jquery.swipebox.js", \app\assets\MAsset::className());
StaticService::includeAppJsStatic("/js/m/user/question_info.js", \app\assets\MAsset::className());
?>
<div class="myquestion-box" style="margin-top: 0;">
    <dl>
        <dt style="display: block;"><?= $info['summary'] ?></dt>
        <dd class="questionpic clearfix">
            <?php if ($info['image']): ?>
                <?php foreach ($info['image'] as $_info_image): ?>
                    <div>
                        <a href="<?= UrlService::buildPicUrl('question', $_info_image['image_key']) ?>"
                           class="swipebox">
                            <img width="100%"
                                 src="<?= UrlService::buildPicUrl('question', $_info_image['image_key']) ?>"/>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </dd>
        <dd class="questiontime">2017-12-26 16:58</dd>
    </dl>
</div>
<?php if ($info['message']): ?>
    <div class="myquestion-box questioninfo">
        <?php foreach ($info['message'] as $_item): ?>
            <dl>
                <dt style="display: block;"><?= $_item['role'] == 2 ? "我" : "客服" ?>:</dt>
                <dd class="questiontime"><?=$_item['summary'] ?></dd>
                <dd class="questionpic clearfix">
                    <?php if ($_item['image']):?>
                        <?php foreach ($_item['image'] as $_item_image): ?>
                            <div>
                                <a href="<?= UrlService::buildPicUrl('question',$_item_image['image_key']) ?>" class="swipebox">
                                    <img width="100%" src="<?= UrlService::buildPicUrl('question',$_item_image['image_key']) ?>"/>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif;?>
                </dd>
            </dl>
            <dd><?=$_item['created_time']?></dd><br>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="bottom-empty">&nbsp;</div>
<div class="pro_fixed clearfix">
    <input style="width: 96%;" type="button" value="继续提问" class="order_now_btn" data="<?= $info['id'] ?>">
</div>