<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/m/user/order.js",\app\assets\MAsset::className() );
?>
<?php if( $list ):?>
<?php foreach( $list as $_item ):?>
<div class="order-box" style="margin-top: 0.6rem;">
	<p class="order-box-title">订单编号 <?=$_item['sn'];?><i class="status-1"><?=$_item['status_desc'];?></i></p><!--状态class区分 status-1 待付款 status-2 运输中 status-3 已完成-->
    <?php if( $_item['status'] == 1 ):?>
        <p>快递状态：<?=$_item['express_status_desc'];?></p>
        <?php if( $_item['express_info'] ):?>
            <a href="<?=UrlService::buildMUrl('/user/express_info',['order_id'=>$_item['id']])?>"><p>快递公司：<?=$_item['express_name'];?>&nbsp; 单号：<?=$_item['express_info'];?></p></a>
        <?php endif;?>
    <?php endif;?>
    <ul class="order-list">
		<?php foreach( $_item['items'] as $_item_info ):?>
		<li class="clearfix">
			<div class="order-cont">
                <a href="<?=UrlService::buildMUrl('/product/info',['id'=>$_item_info['book_id']]);?>">
				<div class="order-pic"><img src="<?=$_item_info['book_main_image'];?>"/></div>
				</a>
				<dl class="order-des">
                    <a href="<?=UrlService::buildMUrl('/product/info',['id'=>$_item_info['book_id']]);?>">
					<dt><?=$_item_info['book_name'];?></dt>
					</a>
					<dd style="font-size: 1.4rem; color: #86908f; margin-top: 0.6rem;">¥<?=$_item_info['pay_price'];?> x 1</dd>
					<!--<dd class="order-des-pirce">实付：¥<?=$_item_info['pay_price'];?></dd>-->
				</dl>
				
			</div>
		</li>
		<?php endforeach;?>
		
		<div class="order-btn">
			<span style="float: left;">实付：¥<?=$_item_info['pay_price'];?></span>
			<?php if( $_item['status'] == -8 ):?>
        	<a class="close" data="<?=$_item['id'];?>" href="<?=UrlService::buildNullUrl();?>">取消订单</a>
        	<a class="green-btn" href="<?=$_item["pay_url"];?>">微信支付</a>
        	<?php elseif( $_item['status'] == 1 && $_item['express_status'] == -6):?>
        	<a class="green-btn confirm_express" data="<?=$_item['id'];?>" href="<?=UrlService::buildNullUrl();?>">确认收货</a>
        	<?php endif;?>
        </div>
	</ul>
</div>
<?php endforeach;?>
<?php else:?>
    <section class="layout-nodata">
		<img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
	</section>
<?php endif;?>