<?php
use app\common\services\StaticService;
use app\common\services\UrlService;

StaticService::includeAppJsStatic( "/js/m/user/notice.js",\app\assets\MAsset::className() );
?>
<div class="notice-box">
    <?php if ($list):?>
	<ul>
        <?php foreach ($list as $_item):?>
            <li data="<?=$_item['id']?>">
                <a class="moreicon" href="javascript:;"><img width="100%" src="<?=UrlService::buildImageUrl("/xiajiantou-icon.png")?>"/></a>
                <dl>
                    <dt><?=$_item['name']?></dt>
                    <dd class="hidden"><?=$_item['summary']?></dd>
                    <dd><?=$_item['created_time']?></dd>
                </dl>
            </li>
        <?php endforeach;?>
	</ul>
    <?php else:?>
        <section class="layout-nodata">
            <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
        </section>
    <?php endif;?>

</div>