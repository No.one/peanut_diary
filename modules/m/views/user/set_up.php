<?php
use app\common\services\StaticService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\common\services\ConstantMapService;

StaticService::includeAppJsStatic( "/js/m/user/set_up.js",\app\assets\MAsset::className() );

?>
<div class="setup-box">
	<ul>
        <li>
            <a href="<?=UrlService::buildMUrl('/user/avatar_set')?>">
            <span>头像</span>
            <div class="box-right box-jt">
                <i class="user-pic"><img src="<?=$info['avatar_url']?>"/></i>
            </div>
            </a>
        </li>
		<li>
			<span>姓名</span>
			<div class="box-right"><?=$info['nickname']?></div>
		</li>
		<li>
			<span>联系方式</span>
			<div class="box-right"><?=$info['mobile']?></div>
		</li>
        <li>
            <?php if ($info['has_in']):?>
                <span>微信</span>
                <div class="box-right box-jt">
                    <?=ConstantMapService::$bind_state[$info['has_in']]?>
                </div>
            <?php else:?>
                <a href="<?=UrlService::buildMUrl('/user/bind')?>">
                    <span>微信</span>
                    <div class="box-right box-jt">
                        <?=ConstantMapService::$bind_state[$info['has_in']]?>
                    </div>
                </a>
            <?php endif;?>
        </li>
	</ul>
</div>