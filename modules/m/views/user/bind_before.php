<?php

use \app\common\services\StaticService;
use \app\common\services\UrlService;
?>
<!--<div class="page_title clearfix">
    <span>账号绑定之前</span>
</div>-->
<div class="login_form_wrap">
    <div class="form_box">
        <div class="form_input_box">
            <span>已有帐号？</span>
            <a class="form_input" href="<?=UrlService::buildMUrl('/user/bind')?>">点击绑定</a>
        </div>
        <div class="form_input_box">
            <span>没有帐号？</span>
            <a class="form_input" href="<?=UrlService::buildMUrl('/user/bind')?>">点击注册</a>
        </div>
    </div>
</div>