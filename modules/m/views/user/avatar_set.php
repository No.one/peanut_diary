<?php
use app\common\services\StaticService;
use app\common\services\UrlService;

StaticService::includeAppCssStatic( "/plugins/cropper/cropper.css",\app\assets\MAsset::className() );
StaticService::includeAppJsStatic( "/plugins/cropper/cropper.js",\app\assets\MAsset::className() );
StaticService::includeAppJsStatic( "/plugins/cropper/main.js",\app\assets\MAsset::className() );

StaticService::includeAppJsStatic( "/js/m/user/avatar_set.js",\app\assets\MAsset::className() );
?>
<div class="img-container" style="max-height: 300px;">
    <img id="image" src="<?=$avatar?>" alt="Picture">
</div>
<div class="op_box">
    <label class="red_btn" for="inputImage" title="Upload image file" style="width: 100%;">
        <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
        上传
    </label>
</div>
<div class="op_box">
    <a id="submit_btn" class="red_btn reg_btn" href="javascript:void(0);" style="width: 100%;" >保存</a>
    <input type="hidden" name='image_base64' value="" id='image_base64'>
    <a id="download" href="javascript:void(0);" download="cropped.jpg" style="display: none;">download</a>
</div>
<div class="docs-buttons" style="display: none;">
    <div class="btn-group btn-group-crop">
        <button id='save' type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { maxWidth: 4096, maxHeight: 4096 })">
          Get Cropped Canvas
        </span>
        </button>
    </div>
</div>
