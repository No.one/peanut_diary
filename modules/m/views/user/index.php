<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
?>
<div class="mem_info user-center">
	<span class="m_pic"><img src="<?=UrlService::buildPicUrl( "avatar",$current_user['avatar'] );?>" /></span>
    <p><?=UtilService::encode( $current_user['nickname'] );?><!--<a href="<?=UrlService::buildMUrl('/user/login_out')?>">退出登录</a>--></p>
</div>
<div class="user-center-box">
	<a href="<?=UrlService::buildMUrl("/user/course_order");?>"><i class="wdkc-icon"></i>我的课程</a>
	<a href="<?=UrlService::buildMUrl("/user/fav");?>"><i class="wdsc-icon"></i>我的收藏</li></a>
	<a href="<?=UrlService::buildMUrl("/user/question");?>"><i class="wddy-icon"></i>我的答疑</li></a>
	<a href="<?=UrlService::buildMUrl("/user/product_order");?>"><i class="ygcp-icon"></i>已购产品</li></a>
	<a href="<?=UrlService::buildMUrl("/user/address");?>"><i class="wdshdz-icon"></i>我的收货地址</li></a>
</div>
<div class="user-center-box" style="margin-top: 0.6rem;">
	<a href="<?=UrlService::buildMUrl("/user/notice");?>"><i class="wdtz-icon"></i>通知
<!--        <span class="messages">32</span>-->
        </li></a>
</div>
<div class="user-center-box" style="margin-top: 0.6rem;">
	<a href="<?=UrlService::buildMUrl("/user/set_up");?>"><i class="wdsz-icon"></i>设置</li></a>
</div>
<!--<div class="footer_fixed clearfix">
    <span><a href="<?=UrlService::buildMUrl("/default/index");?>"><i class="home_icon"></i><b>首页</b></a></span>
    <span><a href="<?=UrlService::buildMUrl("/product/index");?>"><i class="store_icon"></i><b></b></a></span>
    <span><a href="<?=UrlService::buildMUrl("/user/index");?>" class="aon"><i class="member_icon"></i><b>我的</b></a></span>
</div>-->