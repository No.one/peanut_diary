<?php
use \app\common\services\StaticService;
use \app\common\services\UrlService;
StaticService::includeAppJsStatic( "/js/m/user/login.js",\app\assets\MAsset::className() );
?>
<!--<div class="page_title clearfix">
    <span>登录</span>
</div>-->
<div class="login_form_wrap">
    <div class="form_box">
        <div class="form_input_box">
            <span>手机号</span>
            <input name="mobile" type="text" placeholder="请输入手机号" class="form_input" value="" />
        </div>

        <div class="form_input_box phone_code">
            <span>验证码</span>
            <input name="captcha_code" type="text" placeholder="请输入验证码" class="form_input" />
            <button type="button" class="get_captcha">获取验证码</button>
        </div>
    </div>
    <div class="op_box">
        <input style="width: 100%;" type="button" value="登录" class="red_btn dologin"  />
    </div>
    <div class="op_box">
        <a style="width: 100%;" href="<?=UrlService::buildMUrl('/user/register')?>" class="red_btn reg_btn">注册</a>
    </div>
   <div class="wx-box"><span>微信快速登录</span>
   		<a href="<?=UrlService::buildMUrl('/user/bind_before')?>"><img width="100%" src="<?=UrlService::buildImageUrl("/wx-icon.png")?>"/></a>
   </div>
</div>
