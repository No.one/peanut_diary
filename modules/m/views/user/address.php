<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/m/user/address.js",\app\assets\MAsset::className() );
?>
<?php if( $list ):?>
<ul class="address_list">
    <?php foreach( $list as $_item ):?>
	<li>
		<p><span><?=$_item['nickname'];?></span><?=$_item['mobile'];?></p>
		<p><?=$_item['address'];?></p>
		<div class="addr_op">
			<em class="del" data="<?=$_item['id'];?>">删除</em>
			<a href="<?=UrlService::buildMUrl("/user/address_set",[ 'id' => $_item['id'] ]);?>">编辑</a>
            <?php if( $_item['is_default'] ):?>
			<span class="default_set aon"><i class="check_icon"></i>默认地址</span>
            <?php else:?>
                <span class="set_default" data="<?=$_item['id'];?>"><i class="check_icon"></i>设为默认 </span>
            <?php endif;?>
		</div>
	</li>
    <?php endforeach;?>
</ul>

<?php else:?>
    <section class="layout-nodata">
        <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
    </section>
<?php endif;?>

<div class="op_box">
    <a href="<?=UrlService::buildMUrl("/user/address_set");?>" class="red_btn reg_btn" style="width: 100%;">添加新地址</a>
</div>
