<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use app\common\services\ConstantMapService;
StaticService::includeAppJsStatic( "/js/m/user/fav.js",\app\assets\MAsset::className() );
?>
<div class="fav_head">
	<a class="<?=$type==2?'active':''?>" href="<?=UrlService::buildMUrl('/user/fav',['type'=>2])?>">课程</a>
	<a class="<?=$type==1?'active':''?>" href="<?=UrlService::buildMUrl('/user/fav',['type'=>1])?>">产品</a>
</div>
<div class="fav_cont">
<?php if( $list ):?>
<ul class="fav_list fav_list2">
	<?php foreach( $list as $_item ):?>
	<li>
		<a href="<?=$_item['url']?>">
			<i class="pic"><img src="<?=$_item["book_main_image"];?>"/></i>
			<h2><?=$_item["book_name"];?></h2>
            <?php if ($type==2):?>
			<span class="fav_list_desc"><?=strip_tags($_item["summary"]);?></span>
            <?php endif;?>
		</a>
        <?php if ($type==2):?>
            <div class="tag_box">
                <?=ConstantMapService::$course_free_mapping[$_item['is_free']]?>
                <?=ConstantMapService::$course_model_mapping[$_item['model']]?>
                <label class="tag-price fr"><?php if($_item['is_free'] == 0){echo '￥'.$_item['book_price'];}?></label>
            </div>
            <?php else:?>
            <div class="fav_product">
                <span>¥ <em><?=$_item['book_price']?></em></span>
            </div>
<!--            <input type="button" class="fav_product_buy" value="购买" data="--><?//= $_item['book_id']; ?><!--"/>-->
        <?php endif;?>
		<!--<span class="del_fav" data="<?=$_item["id"];?>"><i class="del_fav_icon"></i></span>-->
	</li>
	<?php endforeach;?>
</ul>
</div>
<?php else:?>
    <section class="layout-nodata">
        <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
    </section>
<?php endif;?>
