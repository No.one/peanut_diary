<?php
use app\common\services\StaticService;
use app\common\services\UrlService;
use app\common\services\UtilService;

StaticService::includeAppJsStatic("/js/m/user/online_service.js", \app\assets\MAsset::className());
?>
<?php if ($list): ?>
    <?php foreach ($list as $_item):?>
        <div class="online-box">
            <div class="online-title"><span><?=$_item['name']?></span><a
                        class="title-moreicon" href="javascript:;"><img width="100%"src="<?= UrlService::buildImageUrl("/xiajiantou-icon.png") ?>"/></a>
            </div>
            <div class="online-cont">
                <?=$_item['summary']?>
            </div>
        </div>
    <?php endforeach;?>
<?php else: ?>
    <section class="layout-nodata">
        <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
    </section>
<?php endif; ?>
<div class="bottom-empty">&nbsp;</div>
<div class="pro_fixed clearfix">
    <input style="width: 96%;" type="button" value="未解决问题" class="order_now_btn" data="3">
</div>