<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use app\common\services\ConstantMapService;
StaticService::includeAppJsStatic( "/js/m/user/order.js",\app\assets\MAsset::className() );
?>
<?php if( $list ):?>
<?php foreach( $list as $_item ):?>
<div class="order-box" style="margin-top: 0.6rem;">
	<p class="order-box-title">订单编号 <?=$_item['sn'];?><?=$_item['status_desc'];?></p><!--状态class区分 status-1 待付款 status-2 运输中 status-3 已完成-->
	<ul class="order-list">
		<?php foreach( $_item['items'] as $_item_info ):?>
		<li class="clearfix">
            <div class="order-cont">
				<a href="<?=UrlService::buildMUrl('/course/info',['id'=>$_item_info['book_id']]);?>">
				<div class="order-pic"><img src="<?=$_item_info['book_main_image'];?>"/></div>
				</a>
				<dl class="order-des">
                    <a href="<?=UrlService::buildMUrl('/course/info',['id'=>$_item_info['book_id']]);?>">
					<dt><?=$_item_info['book_name'];?></dt>
					</a>
					<div class="tag_box">
                        <?=ConstantMapService::$course_free_mapping[$_item_info['is_free']]?>
                        <?=ConstantMapService::$course_model_mapping[$_item_info['model']]?>
                        <label class="tag-price"><?php if($_item_info['is_free'] == 0){echo '￥'.$_item_info['pay_price'];}?></label>
                    </div>
                    <?php if ($_item_info['model'] == 0):?>
                        <dd class="classTime">
                            上课时间：<?=$_item_info['course_date']?>
                        </dd>
                    <?php endif;?>


                    <?php if ($_item_info['model'] == 0 && $_item['status'] == 1):?>
                        <dd class="classTime">二维码：
                            <?=ConstantMapService::$course_qrcode_status[$_item_info['qrcode_status']];?>
                        </dd>
                    <?php endif;?>
				</dl>
			</div>
		</li>
		<?php endforeach;?>
		<?php if( $_item['status'] == -8 ):?>
		<div class="order-btn">
        	<a class="close" data="<?=$_item['id'];?>" href="<?=UrlService::buildNullUrl();?>">取消订单</a>
        	<a class="green-btn" href="<?=$_item["pay_url"];?>">微信支付</a>
        </div>
       <?php elseif( $_item['status'] == 1 && $_item['express_status'] == -6):?>
       	<div class="order-btn">
            <a class="green-btn" data="<?=$_item['id'];?>" href="<?=UrlService::buildNullUrl();?>">确认收货</a>
        </div>
        <?php elseif( $_item['status'] == 1 && $_item['express_status'] == 1):?>
<!--        <div class="order-btn">-->
<!--            <a class="green-btn" data="--><?//=$_item['id'];?><!--" href="--><?//=UrlService::buildMUrl('/course/info');?><!--">订单详情</a>-->
<!--        </div>-->
		<?php endif;?>
	</ul>
</div>
<?php endforeach;?>
<?php else:?>
    <section class="layout-nodata">
		<img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
	</section>
<?php endif;?>