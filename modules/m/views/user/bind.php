<?php
use \app\common\services\StaticService;
use \app\common\services\UrlService;
StaticService::includeAppJsStatic( "/js/m/user/bind.js",\app\assets\MAsset::className() );
?>
<!--<div class="page_title clearfix">
	<span>账号绑定</span>
</div>-->
<div class="login_form_wrap">
    <div class="form_box">
        <div class="form_input_box">
            <span>昵称</span>
            <input name="nickname" type="text"  <?=$info?"disabled":""?> placeholder="请输入昵称" class="form_input" value="<?=$info?$info['nickname']:''?>" />
        </div>
        <div class="form_input_box">
            <span>手机号</span>
            <input name="mobile" type="text" <?=$info?"disabled":""?> placeholder="请输入手机号" class="form_input" value="<?=$info?$info['mobile']:''?>" />
        </div>
        <div class="form_input_box phone_code">
            <span>验证码</span>
            <input name="captcha_code" type="text" placeholder="请输入验证码" class="form_input" />
            <button type="button" class="get_captcha">获取验证码</button>
        </div>
    </div>
    <div class="op_box">
        <input style="width: 100%;" type="button" value="绑定" class="red_btn dologin"  />
    </div>
</div>
