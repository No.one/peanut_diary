<?php
use app\common\services\StaticService;
use app\common\services\UrlService;
use app\common\services\UtilService;

StaticService::includeAppJsStatic("/plugins/plupload/plupload/plupload.full.min.js", \app\assets\MAsset::className());
StaticService::includeAppJsStatic("/plugins/plupload/uploads.js", \app\assets\MAsset::className());
StaticService::includeAppJsStatic("/js/m/user/online_set.js", \app\assets\MAsset::className());
?>
<section class="layoutbox">
    <form id="ajsddd">
        <input type="hidden" name="id" value="<?= $id ? $id : 0 ?>">
        <textarea class="form-control" id="summary" rows="5" name="summary" placeholder="请描述问题，我们会尽快回复您..."></textarea>
        <div class="photo_box_cont">
            <p class="shelves_box_title" style="font-size: 1.6rem;">上传照片</p>
            <div id="photos_area" class="photos_area">
                <a class="cover_btn" id="cover_btn_big"><span>+</span></a>
                <input type="hidden" name="bucket" value="question">
            </div>
        </div>
    </form>
    <a class="submit-btn save">提交</a>
</section>