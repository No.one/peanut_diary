<?php
use app\common\services\UrlService;
?>
<?php if ($express_list): ?>
    <?php foreach ($express_list as $_express_item): ?>
        <?= $_express_item ?><br>
    <?php endforeach; ?>
<?php else: ?>
    <section class="layout-nodata">
        <img src="<?= UrlService::buildImageUrl("/nodata.png") ?>" width="100%"/>
    </section>
<?php endif; ?>
