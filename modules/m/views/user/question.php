<?php
use app\common\services\StaticService;
use app\common\services\UrlService;
use app\common\services\UtilService;

StaticService::includeAppJsStatic("/js/m/user/question.js", \app\assets\MAsset::className());
?>
<?php if ($list): ?>
    <?php foreach ($list as $_info): ?>
        <a href="<?= UrlService::buildMUrl("/user/question_info", ['id' => $_info['id']]); ?>">
            <div class="myquestion-box">
                <dl>
                    <dt><?= $_info['summary'] ?></dt>
                    <dd class="questionpic clearfix">
                        <?php if ($_info['image']): ?>
                            <?php foreach ($_info['image'] as $_item_image): ?>
                                <div><img width="100%"
                                          src="<?= UrlService::buildPicUrl('question', $_item_image['image_key']) ?>"/>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </dd>
                    <dd class="questiontime"><?= $_info['created_time'] ?></dd>
                </dl>
            </div>
        </a>
    <?php endforeach; ?>
<?php else:?>
    <section class="layout-nodata">
        <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
    </section>
<?php endif; ?>