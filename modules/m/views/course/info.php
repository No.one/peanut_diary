<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use app\common\services\ConstantMapService;

StaticService::includeAppJsStatic("/js/m/course/info.js", \app\assets\MAsset::className());
?>
<div class="proban">
    <div id="slideBox" class="slideBox">
        <div class="bd">
            <?php if (!$pic):?>
            <ul>
                <li><img src="<?= UrlService::buildPicUrl("course", $info['main_image']); ?>"/></li>
            </ul>
            <?php else:?>
                <ul>
                    <?php foreach ($pic as $_item_pic):?>
                        <li><img src="<?= UrlService::buildPicUrl("course", $_item_pic); ?>"/></li>
                    <?php endforeach;?>
                </ul>
            <?php endif;?>
        </div>
        <div class="hd">
            <ul>
                <?php if (!$pic):?>
                    <li>1</li>
                <?php else:?>
                    <ul>
                        <?php foreach ($pic as $key=>$_item_pic):?>
                            <li>$key</li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </ul>
        </div>
    </div>
    <!--视频-->
    <div class="videobox hidden">
        <video id="videocont" width="100%" preload="auto" >
            <source src="<?=$info['video_url']?>" type="video/mp4"></source>
        </video>
    </div>
</div>

<div class="pro_header">
        <h2><?=UtilService::encode($info['name']); ?></h2>
        <div class="tag_box">
        	<?=ConstantMapService::$course_model_mapping[$info['model']]?>
            <?=ConstantMapService::$course_free_mapping[$info['is_free']]?>
            <label class="tag-price"><?php if($info['is_free'] == 0){echo '¥'.UtilService::encode($info['price']);}?></label>
        </div>
</div>
<div class="pro_warp">
    <p class="shelves_box_title">课程介绍
<!--        <a class="moreicon" href="javascript:;"><img width="100%" src="--><?//=UrlService::buildImageUrl("/xiajiantou-icon.png")?><!--"/></a>-->
    </p>
    <div class="pro_warp_cont moreicon-cont"><?= nl2br($info['summary']); ?></div>
    <?php if($info['model'] == 0):?>
    <p>时间：<?=$info['course_date']?></p>
    <p>地点：<?=$info['course_addr']?></p>
    <p>联系方式：<?=$info['contacts']?></p>
    <?php endif;?>
</div>
<?php if ($has_buyed == 4): ?>
<div class="pro_warp">
    <p class="shelves_box_title">课程二维码</p>
    <div class="QR-code clearfix">
    	<i><img src="<?=UrlService::buildPicUrl('qrcode',$qrcode_image)?>"/></i>
    	<span>用于线下参加课程通过名师验证</span>
    </div>
</div>
<?php endif;?>
<div class="bottom-empty">&nbsp;</div>
<div class="pro_fixed clearfix">
    <?php if ($has_faved): ?>
        <a class="fav has_faved del_fav" href="<?= UrlService::buildNullUrl(); ?>" data="<?=$info['id']; ?>">取消收藏</a>
    <?php else: ?>
        <a class="fav add_fav" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $info['id']; ?>">加入收藏</a>
    <?php endif; ?>
    <?php if ($has_buyed == 1): ?>
        <input type="button" value="查看视频" class="get_video_btn" data="<?= $info['id']; ?>"/>
    <?php elseif ($has_buyed == 2 || $has_buyed == 5): ?>
        <input type="button" value="立即购买" class="order_now_btn" data="<?= $info['id']; ?>"/>
    <?php elseif ($has_buyed == 3): ?>
        <input type="button" value="立即参加" class="sign_now_btn" data="<?= $info['id']; ?>"/>
    <?php elseif ($has_buyed == 4): ?>
<!--        <input type="button" value="二维码" class="get_qrcode_btn" data="--><?//= $info['id']; ?><!--"/>-->
    <?php endif; ?>
    <input type="hidden" name="id" value="<?= $info['id']; ?>">
</div>
