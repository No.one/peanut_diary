<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use app\common\services\ConstantMapService;

StaticService::includeAppJsStatic("/js/m/course/order.js", \app\assets\MAsset::className());
?>
<div class="order_box">
    <p class="shelves_box_title pt10 pb_10">购买课程</p>
    <?php if ($product_list): ?>
        <ul class="order_list">
            <?php foreach ($product_list as $_item): ?>
                <li data="<?= $_item["id"]; ?>" data-quantity="<?= $_item['quantity']; ?>">
                    <a href="<?= UrlService::buildMUrl("/course/info", ["id" => $_item['id']]); ?>">
                        <!--<i class="pic">
                            <img src="<?= $_item["main_image"]; ?>" style="width: 100px;height: 100px;"/>
                        </i>-->
                        <h2><?= $_item['name']; ?> <!--x <?= $_item['quantity']; ?>--></h2>
                        <div class="tag_box">
                        	<?= ConstantMapService::$course_model_mapping[$_item['model']] ?>
                        	<?= ConstantMapService::$course_free_mapping[$_item['is_free']] ?>
                           	<label class="tag-price"><?php if($_item['is_free'] == 0){echo '￥'.$_item['price'];}?></label>
                        </div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
<div class="addr_form_box_title">请确认个人信息</div>
<div class="addr_form_box">
    <div class="course_form_box">
        <div class="course_input_box">
            <span>姓名</span>
            <input name="nickname" type="text" placeholder="请输入姓名" class="course_input" value=""/>
        </div>
        <div class="course_input_box">
            <span>手机号</span>
            <input name="mobile" type="text" placeholder="请输入手机号" class="course_input" value=""/>
        </div>
    </div>
</div>

<div class="op_box">
    <input type="hidden" name="sc" value="<?= $sc; ?>">
    <input style="width: 100%;" type="button" value="确认订单" class="red_btn do_order"/>
</div>
