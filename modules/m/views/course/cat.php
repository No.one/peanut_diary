<?php
use app\common\services\ConstantMapService;
use app\common\services\UrlService;
use app\common\services\StaticService;

StaticService::includeAppJsStatic( "/js/m/course/cat.js",\app\assets\MAsset::className() );
?>
<?php if( $list ):?>
<ul class="fav_list">
	<?php foreach( $list as $_item ):?>
	<li>
		<a href="<?=UrlService::buildMUrl("/course/cat_info",[ 'id' => $_item['id'] ]);?>">
			<i class="pic"><img src="<?=$_item["avatar"];?>"/></i>
			<h2><?=$_item["name"];?></h2>
            <span class="fav_list_desc"><?=$_item["summary"];?></span>
		</a>
	</li>
	<?php endforeach;?>
</ul>
<?php else:?>
    <section class="layout-nodata">
        <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
    </section>
<?php endif;?>
