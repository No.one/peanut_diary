<?php
use app\common\services\ConstantMapService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\common\services\StaticService;
use app\assets\WebAsset;
StaticService::includeAppJsStatic('./js/m/course/cat_info.js', WebAsset::className());
?>
<div class="mem_info">
    <span class="m_pic"><img src="<?=UrlService::buildPicUrl( "course",$info['avatar'] );?>" /></span>
    <p><?=UtilService::encode( $info['name'] );?></p>
</div>
<div class="shelves_box">
    <p class="shelves_box_title">名师介绍 <a class="moreicon" href="javascript:;"><img width="100%" src="<?=UrlService::buildImageUrl("/xiajiantou-icon.png")?>"/></a></p>
    <div class="shelves_box_cont moreicon-cont"><?=UtilService::encode( $info['summary'] );?></div>
</div>
<?php if( $list ):?>
<div class="shelves_box">
	<p class="shelves_box_title">代教课程</p>
    <ul class="fav_list fav_list2">
        <?php foreach( $list as $_item ):?>
            <li>
                <a href="<?=UrlService::buildMUrl("/course/info",[ 'id' => $_item['id'] ]);?>">
                    <i class="pic"><img src="<?=$_item["main_image"];?>"/></i>
                    <h2><?=$_item["name"];?></h2>
                    <span class="fav_list_desc"><?=strip_tags($_item["summary"]);?></span>
                    <div class="tag_box">
                        <?=ConstantMapService::$course_free_mapping[$_item['is_free']]?>
                        <?=ConstantMapService::$course_model_mapping[$_item['model']]?>
                        <label class="tag-price fr"><?php if($_item['is_free'] == 0){echo '￥'.$_item['price'];}?></label>
                    </div>
                </a>
            </li>
        <?php endforeach;?>
    </ul>
</div>
<?php else:?>
<li>
    <div class="no-data">
        <span class="fav_list_desc"><?=ConstantMapService::$default_nodata;?></span>
    </div>
</li>
<?php endif;?>
