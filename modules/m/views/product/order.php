<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use app\common\services\ConstantMapService;
StaticService::includeAppJsStatic( "/js/m/product/order.js",\app\assets\MAsset::className() );
?>
<div class="address_box">
	<ul class="set_address">
        <?php if( $address_list ):?>
            <?php foreach( $address_list as $_idx => $_address_info  ):?>
                <li class="select_address">
                    <dl>
                        <input type="hidden" name="address_id" value="<?=$_address_info['id'];?>">
                        <dt>收货人：<?=$_address_info['nickname'];?><a href="javascript:;"><?=$_address_info['mobile'];?></a></dt>
                        <dd>收货地址：<?=$_address_info['address'];?></dd>
                    </dl>
                </li>
            <?php endforeach;?>
        <?php else:?>
            <li class="add_address">
                <i class="address_icon"></i><span>新建收货地址</span>
            </li>
        <?php endif;?>
	</ul>
</div>
<div class="order-box">
	<p class="shelves_box_title">购买产品</p>
	<?php if( $product_list ):?>
	<ul class="order-list">
		<?php foreach( $product_list as $_item ):?>
		<li class="clearfix"   data="<?=$_item["id"];?>" data-quantity="<?=$_item['quantity'];?>">
			<div class="order-cont">
				<a href="<?=UrlService::buildMUrl("/product/info",[ "id" => $_item['id'] ]);?>">
				<div class="order-pic"><img src="<?=$_item["main_image"];?>"/></div>
				</a>
				<dl class="order-des">
					<a href="<?=UrlService::buildMUrl("/product/info",[ "id" => $_item['id'] ]);?>">
					<dt><?=$_item['name'];?></dt>
					</a>
					<dd class="order-des-pirce">¥ <em><?=$_item['price'];?></em></dd>
					<div class="quantity-form">
			            <a href="javascript:;" class="icon_lower" data="<?=$_item['id']?>"></a>
			            <input type="text" name="quantity" class="input_quantity" max="<?=$_item["stock"];?>" value="<?=$_item['quantity'];?>" readonly="readonly" max="10"/>
			            <a href="javascript:;" class="icon_plus" data="<?=$_item['id']?>"></a>
			        </div>
				</dl>
			</div>
		</li>
		<?php endforeach;?>
	</ul>
	<?php endif;?>
</div>

<div class="order-box">
	<p class="shelves_box_title">购买信息</p>
	<ul>
		<li class="clearfix">
			<span>配送方式</span>
			<span class="express-box"><?=ConstantMapService::$product_who_pay_ship[$ship['who_pay_ship']]?>
                <?php if ($ship['who_pay_ship']==2){echo $ship['ship_price'];}?></span>
		</li>
		<li class="clearfix">
			<span>合计</span>
			<span class="order-totalprice">¥ <em><?=$total_pay_money;?></em></span>
		</li>
	</ul>
</div>
<div class="bottom-empty">&nbsp;</div>
<div class="pro_fixed order-pro-fixed clearfix">
    <input type="hidden" name="sc" value="<?=$sc;?>">
    <input type="button" value="提交订单" class="order-sub do_order"  />
    <div class="order-payment">实付款：¥ <em><?=$total_pay_money;?></em></div>
</div>


<div class="alert-box add_address_box address">
	<div class="cont-box"></div>
</div>

<div class="alert-box select_address_box">
	<div class="cont-box">
		<div class="cont-title">选择收货地址 <a class="alert-close" href="javascript:;"></a></div>
		<div class="cont-list">
			<ul></ul>
			<div class="add_address_btn">添加地址</div>
		</div>
	</div>
</div>

