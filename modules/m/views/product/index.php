<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/m/product/index.js",\app\assets\MAsset::className() );
?>
<div class="search_header">
    <input name="kw" type="text" class="search_input" placeholder="请输入搜索的产品" value="<?=$search_conditions['kw'];?>" />
    <i class="search_icon"></i>
</div>
<div class="probox">
    <?php if( $list ):?>
        <ul class="prolist">
            <?php foreach( $list as $_item ):?>
            <li>
                <a href="<?=UrlService::buildMUrl("/product/info",[ 'id' => $_item['id'] ]);?>">
                    <i><img src="<?=$_item['main_image_url'];?>"/></i>
                    <span><?=$_item['name'];?></span>
                    <b><!--<label>月销量<?=$_item['month_count'];?></label>--><em>¥</em><?=$_item['price'];?></b>
                </a>
            </li>
            <?php endforeach;?>
    </ul>
    <?php else:?>
        <section class="layout-nodata">
            <img src="<?=UrlService::buildImageUrl("/nodata.png")?>" width="100%"/>
        </section>
    <?php endif;?>
</div>