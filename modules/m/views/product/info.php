<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/m/product/info.js",\app\assets\MAsset::className() );
?>
<div class="proban">
    <!--banner切换-->
    <div id="slideBox" class="slideBox">
        <div class="bd">
            <?php if (!$pic):?>
                <ul>
                    <li><img src="<?= UrlService::buildPicUrl("book", $info['main_image']); ?>"/></li>
                </ul>
            <?php else:?>
                <ul>
                    <?php foreach ($pic as $_item_pic):?>
                        <li><img src="<?= UrlService::buildPicUrl("book", $_item_pic); ?>"/></li>
                    <?php endforeach;?>
                </ul>
            <?php endif;?>
        </div>
        <div class="hd">
            <ul>
                <?php if (!$pic):?>
                    <li>1</li>
                <?php else:?>
                    <ul>
                        <?php foreach ($pic as $key=>$_item_pic):?>
                            <li>$key</li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </ul>
        </div>
    </div>

</div>
<div class="pro_header">
    <div class="pro_tips">
        <h2><?=UtilService::encode( $info['name'] );?></h2>
        <h3><em>¥</em><?=UtilService::encode( $info['price'] );?></h3>
    </div>
</div>

<div class="pro_warp">
	<p class="shelves_box_title">产品介绍</p>
	<div class="pro_warp_cont"><?=nl2br($info['summary']);?></div>
</div>
<div class="bottom-empty"></div>
<div class="pro_fixed clearfix">
    <?php if ($has_faved): ?>
        <a class="fav has_faved del_fav" href="<?= UrlService::buildNullUrl(); ?>" data="<?=$info['id']; ?>">取消收藏</a>
    <?php else: ?>
        <a class="fav add_fav" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $info['id']; ?>">加入收藏</a>
    <?php endif; ?>
    <input type="button" value="立即购买" class="order_now_btn" data="<?=$info['id'];?>"/>
    <input type="hidden" name="id" value="<?=$info['id'];?>">
</div>
