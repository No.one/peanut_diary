<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/m/pay/buy.js",\app\assets\MAsset::className() );
?>
<div class="addr_form_box_title">请确认支付信息</div>
<div class="addr_form_box">
    <div class="course_form_box">
        <div class="course_input_box">
            <span>支付金额</span>
            <input name="" type="text" placeholder="" disabled="disabled" class="course_input" value="<?=$pay_order_info['pay_price'];?>"/>
        </div>
        <div class="course_input_box">
            <span>支付备注</span>
            <input name="" type="text" placeholder="" disabled="disabled" class="course_input" value="<?=$pay_order_info['note'];?>"/>
        </div>
    </div>
</div>
<div class="op_box">
	<input style="width: 100%;" type="button" value="微信支付" class="red_btn do_pay"  />
</div>
<div class="op_box">
    <input style="width: 100%;" type="button" value="模拟支付" class="red_btn do_now"  />
</div>

<div class="hide_wrap hidden">
	<input type="hidden" name="pay_order_id" value="<?=$pay_order_info['id'];?>">
</div>