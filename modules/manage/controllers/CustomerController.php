<?php
namespace app\modules\manage\controllers;


use app\common\components\HttpClient;
use app\common\services\ConstantMapService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\article\Article;
use app\models\notice\Notice;
use app\models\question\QuestionImages;
use app\models\question\UserQuestion;
use app\modules\manage\controllers\common\BaseController;

class CustomerController extends BaseController
{
    public function actionIndex()
    {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $cat_id = intval($this->get("cat_id", 0));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = Article::find();

        if ($mix_kw) {
            $where_name = ['LIKE', 'name', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $query->andWhere($where_name);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }

        if ($cat_id) {
            $query->andWhere(['cat_id' => $cat_id]);
        }

        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);


        $list = $query->orderBy(['id' => SORT_DESC])
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)
            ->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'status' => $_item['status'],
                    'weight' => $_item['weight'],
                    'created_time' => $_item['created_time'],
                ];
            }
        }
        return $this->render('index', [
            'list' => $data,
            'search_conditions' => [
                'mix_kw' => $mix_kw,
                'p' => $p,
                'status' => $status,
            ],
            'status_mapping' => ConstantMapService::$status_mapping,
            'pages' => [
                'total_count' => $total_res_count,
                'page_size' => $this->page_size,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    public function actionSet()
    {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = [];
            if ($id) {
                $info = Article::find()->where(['id' => $id])->one();
            }

            return $this->render('set', [
                'info' => $info
            ]);
        }
        $id = intval($this->post('id', 0));
        $name = trim($this->post('name', ''));
        $summary = trim($this->post('summary', ''));
        $weight = intval($this->post('weight', 1));
        $now_date = date('Y-m-d H:i:s');
        if (mb_strlen($name, 'utf-8') < 1) {
            return $this->renderJSON([], '请输入文章名称');
        }

        if (mb_strlen($summary, 'utf-8') < 10) {
            return $this->renderJSON([], '请输入文章内容，并不能少于10个字符');
        }
        if ($id) {
            $info = Article::find()->where(['id' => $id])->one();
        } else {
            $info = new Article();
            $info->created_time = $now_date;
            $info->status = 1;
        }
        $info->name = $name;
        $info->summary = $summary;
        $info->weight = $weight;
        $info->updated_time = $now_date;
        $info->save(0);
        return $this->renderJSON([], '添加成功');
    }

    public function actionOps()
    {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的文章", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Article::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定文章不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->status = 0;
                break;
            case "recover":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionUser_question()
    {
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = UserQuestion::find()->where(['p_id' => 0, 'role' => 2]);
        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy(['id' => SORT_DESC])
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)
            ->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'summary' => $_item['summary'],
                    'member_name' => $_item['member_name'],
                    'created_time' => $_item['created_time'],
                ];
            }
        }
        return $this->render('user_question', [
            'list' => $data,
            'status_mapping' => ConstantMapService::$status_mapping,
            'pages' => [
                'total_count' => $total_res_count,
                'page_size' => $this->page_size,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    public function actionUser_question_set()
    {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = [];
            if ($id) {
                $info = UserQuestion::find()->joinWith('image')->where(['user_question.id' => $id])->asArray()->one();
                $info['message'] = UserQuestion::find()->joinWith('image')->where(['p_id' => $info['id']])
                    ->orderBy(['created_time' => SORT_ASC, 'id' => SORT_ASC])->asArray()->all();
            }

            return $this->render('user_question_set', [
                'info' => $info
            ]);
        }

        $id = intval($this->post('id', 0));
        $summary = trim($this->post('summary', ''));
        $image = $this->post('image', []);
        $now_date = date('Y-m-d H:i:s');

        if ($id <= 0) {
            return $this->renderJSON([], '参数错误，请重试');
        }
        if (mb_strlen($summary, 'utf-8') < 1) {
            return $this->renderJSON([], '请输入你需要描述的问题');
        }
        $user_question_info = UserQuestion::find()->where([])->count();
        if (!$user_question_info) {
            $id = 0;
        }
        $connection = UserQuestion::getDb();
        $transaction = $connection->beginTransaction();
        $user_question_model = new UserQuestion();
        $user_question_model->p_id = $id;
        $user_question_model->member_id = $this->current_user['uid'];
        $user_question_model->member_name = $this->current_user['nickname'];
        $user_question_model->role = 1;
        $user_question_model->name = mb_substr($summary, 0, 20);
        $user_question_model->summary = $summary;
        $user_question_model->status = 1;
        $user_question_model->updated_time = $now_date;
        $user_question_model->created_time = $now_date;
        $ret = $user_question_model->save(0);
        if ($ret) {
            if ($image) {
                foreach ($image as $_item) {
                    $question_images_model = new QuestionImages();
                    $question_images_model->question_id = $user_question_model->id;
                    $question_images_model->image_key = $_item;
                    $question_images_model->created_time = $now_date;
                    if (!$question_images_model->save(0)) {
                        return $this->renderJSON([], '操作失败', -1);
                    }
                }
            }
        } else {
            return $this->renderJSON([], '操作失败', -1);
        }

        $transaction->commit();
        return $this->renderJSON([], '操作成功');
    }

    public function actionNotice(){
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));

        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = Notice::find();

        if ($mix_kw) {
            $where_name = ['LIKE', 'name', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $query->andWhere($where_name);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }

        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);


        $list = $query->orderBy(['id' => SORT_DESC])
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)
            ->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'status' => $_item['status'],
                    'created_time' => $_item['created_time'],
                ];
            }
        }

        return $this->render('notice', [
            'list' => $data,
            'search_conditions' => [
                'mix_kw' => $mix_kw,
                'p' => $p,
                'status' => $status,
            ],
            'status_mapping' => ConstantMapService::$status_mapping,
            'pages' => [
                'total_count' => $total_res_count,
                'page_size' => $this->page_size,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    public function actionNotice_info()
    {
        $id = intval($this->get("id", 0));
        $reback_url = UrlService::buildWebUrl("/customer/notice");
        if (!$id) {
            return $this->redirect($reback_url);
        }

        $info = Notice::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->redirect($reback_url);
        }
        return $this->render('notice_info',[
            'info' => $info
        ]);
    }

    public function actionNotice_set(){
        if (\Yii::$app->request->isGet){
            return $this->render('notice_set');
        }

        $name = $this->post('name','');
        $summary = $this->post('summary','');

        if (mb_strlen($name,'utf-8') < 1){
            return $this->renderJSON([],'请填写标题', -1);
        }

        if (mb_strlen($summary,'utf-8') < 10){
            return $this->renderJSON([],'请填写不少于10个字符的公告内容', -1);
        }
        $notice_model= new Notice();
        $notice_model->from_member_id = $this->current_user['uid'];
        $notice_model->from_name = $this->current_user['nickname'];
        $notice_model->to_member_id = 'all';
        $notice_model->type = 2;
        $notice_model->send_type = 1;
        $notice_model->name = $name;
        $notice_model->summary = $summary;
        $notice_model->status = 1;
        $notice_model->created_time = date("Y-m-d H:i:s",time());
        $notice_model->save(0);

        $config = \Yii::$app->params['domain'];
        HttpClient::post($config['www']. "/default/send_wx_msg",['id'=>$notice_model->id]);
        return $this->renderJSON([],'操作成功');
    }

    public function actionNotice_ops(){
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的文章", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Notice::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定文章不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->status = 0;
                break;
            case "recover":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }
}