<?php

namespace app\modules\manage\controllers;


use app\common\services\ConstantMapService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\log\AppAccessLog;
use app\models\rbac\Access;
use app\models\User;
use app\modules\manage\controllers\common\BaseController;
use app\common\services\TreeService;

class AccessController extends BaseController
{

    public function actionIndex()
    {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = Access::find();

        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->where(['status' => 1])->orderBy(['weigh' => SORT_DESC, 'id' => SORT_DESC])
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)->asArray()
            ->all();

        $list = TreeService::subtree($list);

        return $this->render("index", [
            'list' => $list,
            'search_conditions' => [
                'mix_kw' => $mix_kw,
                'p' => $p,
                'status' => $status
            ],
            'status_mapping' => ConstantMapService::$status_mapping,
            'pages' => [
                'total_count' => $total_res_count,
                'page_size' => $this->page_size,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    public function actionSet()
    {
        //如果是get请求则演示页面
        if (\Yii::$app->request->isGet) {
            $id = $this->get("id", 0);
            $info = [];
            if ($id) {
                $info = Access::find()->where(['status' => 1, 'id' => $id])->one();
            }
            $access_list = Access::find()->where(['fid' => 0])->andWhere(['!=', 'id', $id])
                ->orderBy(['weigh' => SORT_DESC, 'id' => SORT_DESC])->all();

            return $this->render('set', [
                'info' => $info,
                'access_list' => $access_list
            ]);
        }

        $id = intval($this->post("id", 0));
        $fid = intval($this->post('fid', 0));
        $title = trim($this->post("title", ""));
        $urls = trim($this->post("urls", ""));
        $icon = trim($this->post('icon', ''));
        $weigh = intval($this->post('weigh', 0));
        $date_now = date("Y-m-d H:i:s");
        if ($weigh < 1) {
            $weigh = 1;
        }
        if ($weigh > 255) {
            $weigh = 255;
        }

        if (mb_strlen($title, "utf-8") < 1 || mb_strlen($title, "utf-8") > 20) {
            return $this->renderJSON([], '请输入合法的权限标题', -1);
        }

        if (!$urls) {
            return $this->renderJSON([], '请输入合法的Urls', -1);
        }

        $urls = explode("\n", $urls);
        if (!$urls) {
            return $this->renderJSON([], '请输入合法的Urls', -1);
        }
        $url = mb_substr($urls[0], 7);

        //查询同一标题的是否存在
        $has_in = Access::find()->where(['title' => $title])->andWhere(['!=', 'id', $id])->count();
        if ($has_in) {
            return $this->renderJSON([], '该权限标题已存在', -1);
        }

        $has_child = Access::find()->where(['fid' => $id])->andWhere(['!=', 'fid', $id])->count();
        if ($has_child) {
            return $this->renderJSON([], '该权限为父级权限无法在设置他为子权限', -1);
        }

        //查询指定id的权限
        $info = Access::find()->where(['id' => $id])->one();
        if ($info) {//如果存在则是编辑
            $model_access = $info;
        } else {//不存在就是添加
            $model_access = new Access();
            $model_access->status = 1;
            $model_access->created_time = $date_now;
        }
        $model_access->fid = $fid;
        $model_access->title = $title;
        $model_access->url = $url;
        $model_access->icon = $icon;
        $model_access->weigh = $weigh;
        $model_access->urls = json_encode($urls);//json格式保存的
        $model_access->updated_time = $date_now;
        $model_access->save(0);

        return $this->renderJSON([], '操作成功');
    }

    public function actionOps()
    {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的权限角色", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Access::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定权限角色不存在", -1);
        }

        $has_child = Access::find()->where(['fid' => $id])->count();
        if ($has_child) {
            return $this->renderJSON([], '该权限为父级权限，请清空子级权限后在进行删除', -1);
        }

        switch ($act) {
            case "remove":
                $info->delete();
                break;
        }
        return $this->renderJSON([], "操作成功");
    }

}