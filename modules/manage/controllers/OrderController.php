<?php

namespace app\modules\manage\controllers;

use app\common\services\ConstantMapService;
use app\common\services\DataHelper;
use app\common\services\QueueListService;
use app\common\services\UrlService;
use app\models\member\Member;
use app\models\order\TaobaoOrder;
use app\models\integral\IntegralGoods;
use app\models\integral\IntegralOrder;
use app\models\integral\IntegralOrderGoods;
use app\models\order\ExpressOrder;
use app\modules\manage\controllers\common\BaseController;
use app\models\Express as ModelExpress;

class OrderController extends BaseController {
    
    /**
     * 淘宝订单
     * @return type
     */
    public function actionIndex() {
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;
        $query = TaobaoOrder::find()->alias('o');
        $query->joinWith(['member'=>function($query){
            $query->select('adzoneid, nickname');
        }], TRUE, 'INNER JOIN');
        if ($status != ConstantMapService::$status_default) {
            $query->andWhere(['o.order_status' => TaobaoOrder::$orderStatus[$status]]);
        }
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('o.id, o.adv_id, o.order_sn, o.goods_name, o.order_status, o.balance_amount, o.commission_ratio, o.commission_amount, o.create_time')
            ->orderBy(['o.id' => SORT_DESC])
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)
            ->asArray()
            ->all();
        return $this->render('index', [
                    'list' => $list,
                    'search_conditions' => [
                        'p' => $p,
                        'status' => $status
                    ],
                    'status_mapping' => TaobaoOrder::$orderStatus,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }
    
    public function actionInfo(){
        $id = intval($this->get("id", 0));
        $reback_url = UrlService::buildWebUrl("/order/index");
        $query = TaobaoOrder::find();
        $query->with(['commision'=>  function($query){
            $query->with(['selfCommision'=>function($query){
                $query->select('id, nickname, mobile');
            }])->with(['higherCommision'=>function($query){
                $query->select('id, nickname, mobile');
            }])->with(['operatorCommision'=>function($query){
                $query->select('id, nickname, mobile');
            }])->select('order_id, member_id, self_commision, higher_id, higher_commision, operate_id, operator_commision');
        }]);
        $info = $query->where(['id'=>$id])->select('id, adv_id, order_sn, goods_name, order_status, balance_amount, commission_ratio, commission_amount, create_time')->asArray()->one();
//        echo "<pre>";
//        print_r($info);
//        exit();
        if(empty($info)){
            return $this->redirect($reback_url);
        }
        return $this->render("info", ['order_info' => $info]);
        
    }

    /**
     * 积分订单列表
     * @return type
     */
    public function actionIntegralList() {
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = IntegralOrder::find();

        if ($status != ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }
        $query->andWhere(['delete_at'=>0]);

        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->orderBy(['id' => SORT_DESC])
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)
                ->asArray()
                ->all();

        $integral_status_mapping = ConstantMapService::$integral_status_mapping;
        if ($list) {
            $order_item_list = IntegralOrderGoods::find()->where(['order_id' => array_column($list, "id"), 'delete_at'=>0])->asArray()->all();
            $goods_mapping = IntegralGoods::find()->select(["id", 'goods_name'])->where(['id' => array_column($order_item_list, "goods_id")])->indexBy("id")->all();
            $order_goods_mapping = [];
            foreach ($order_item_list as $_order_item_info) {
                $tmp_goods_info = $goods_mapping[$_order_item_info['goods_id']];
                if (!isset($order_goods_mapping[$_order_item_info['order_id']])) {
                    $order_goods_mapping[$_order_item_info['order_id']] = [];
                }
                $order_goods_mapping[$_order_item_info['order_id']][] = [
                    'goods_name' => $tmp_goods_info['goods_name'],
                    'num' => $_order_item_info['num']
                ];
            }

            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'sn' => $_item['order_number'],
                    'total_price' => $_item['total_price'],
                    'status_desc' => $integral_status_mapping[$_item['status']],
                    'status' => $_item['status'],
                    'created_time' => date("Y-m-d H:i", strtotime($_item['created_time'])),
                    'items' => isset($order_goods_mapping[$_item['id']]) ? $order_goods_mapping[$_item['id']] : []
                ];
            }
        }

        return $this->render('integral_list', [
                    'list' => $data,
                    'search_conditions' => [
                        'p' => $p,
                        'status' => $status
                    ],
                    'status_mapping' => $integral_status_mapping,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    /**
     * 积分订单详情
     * @return type
     */
    public function actionIntegralInfo() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $reback_url = UrlService::buildWebUrl("/order/integral-list");
            if (!$id) {
                return $this->redirect($reback_url);
            }

            $order_info = IntegralOrder::find()
                    ->alias('io')
                    ->innerJoin("{{%express_order}} eo", "io.id=eo.order_id and eo.type=2")
                    ->where(['io.id' => $id, 'io.delete_at'=>0])
                    ->select('io.*, eo.status as express_status, eo.consignee, eo.consignee_tel, eo.province, eo.city, eo.district, eo.address')
                    ->asArray()
                    ->one();
            if (!$order_info) {
                return $this->redirect($reback_url);
            }

            $order_item_list = IntegralOrderGoods::find()->where(['order_id' => $id, 'delete_at'=>0])->asArray()->all();
            $goods_mapping = IntegralGoods::find()->select(["id", 'goods_name'])
                            ->where(['id' => array_column($order_item_list, "goods_id")])
                            ->indexBy("id")->all();

            $order_goods_items = [];
            foreach ($order_item_list as $_order_item_info) {
                $tmp_goods_info = $goods_mapping[$_order_item_info['goods_id']];
                $order_goods_items[] = [
                    'goods_name' => $tmp_goods_info['goods_name'],
                    'num' => $_order_item_info['num'],
                    'price' => $_order_item_info['price'],
                ];
            }

            $data_order_info = [
                'id' => $order_info['id'],
                'sn' => $order_info['order_number'],
                'total_price' => $order_info['total_price'],
                'status_desc' => ConstantMapService::$integral_status_mapping[$order_info['status']],
                'status' => $order_info['status'],
                'express_status_desc' => ConstantMapService::$express_status_mapping[$order_info['express_status']],
                'express_status' => $order_info['express_status'],
                'created_time' => $order_info['created_time'],
            ];

            $member_info = Member::findOne(['id' => $order_info['member_id']]);
            $data_member_info = [
                'nickname' => $member_info['nickname'],
                'mobile' => $member_info['mobile'],
            ];

            $data_address_info = [
                'consignee' => $order_info['consignee'],
                'consignee_tel' => $order_info['consignee_tel'],
                'address' => $order_info['province'] . $order_info['city'] . $order_info['district'] . $order_info['address'],
            ];
            //快递列表
            $express_list = ModelExpress::find()->where(['e_state' => '1'])->all();
            return $this->render("integral_info", [
                        'order_info' => $data_order_info,
                        'order_goods_items' => $order_goods_items,
                        'member_info' => $data_member_info,
                        'address_info' => $data_address_info,
                        'express_list' => $express_list
            ]);
        }
        
        $id = intval($this->post("id", 0));
        $express_id = intval($this->post('express_id'), 0);
        $express_number = trim($this->post("express_number", 0));
        if (!$id) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if ($express_id < 0) {
            return $this->renderJSON([], '请选择快递');
        }

        if (mb_strlen($express_number, "utf-8") < 3) {
            return $this->renderJSON([], '请输入符合要求的快递信息', -1);
        }

        $order_info = IntegralOrder::find()->where(['id' => $id,  'delete_at'=>0])->one();
        if (!$order_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $express_order = ExpressOrder::find()->where(['order_id'=>$id])->one();
        if (!$express_order) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $order_info->status = 1;
        $express_order->express_id = $express_id;
        $express_order->express_number = $express_number;
        $express_order->status = 1;
        $express_order->updated_time = date("Y-m-d H:i:s");
        if (($order_info->update(0)) && ($express_order->update(0))) {
            //发货之后要发通知
            QueueListService::addQueue("express", [
                'member_id' => $order_info['member_id'],
                'order_id' => $id
            ]);
        }
        return $this->renderJSON([], "操作成功");
    }

    
    public function actionExpress() {
        $id = intval($this->post("id", 0));
        $express_id = intval($this->post('express_id'), 0);
        $express_info = trim($this->post("express_info", 0));
        if (!$id) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        if ($express_id < 0) {
            return $this->renderJSON([], '请选择快递');
        }

        if (mb_strlen($express_info, "utf-8") < 3) {
            return $this->renderJSON([], '请输入符合要求的快递信息', -1);
        }

        $pay_order_info = PayOrder::find()->where(['id' => $id])->one();
        if (!$pay_order_info) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $pay_order_info->express_id = $express_id;
        $pay_order_info->express_info = $express_info;
        $pay_order_info->express_status = -6;
        $pay_order_info->updated_time = date("Y-m-d H:i:s");
        if ($pay_order_info->update(0)) {
            //发货之后要发通知
            QueueListService::addQueue("express", [
                'member_id' => $pay_order_info['member_id'],
                'pay_order_id' => $id
            ]);
        }
        return $this->renderJSON([], "操作成功");
    }

}
