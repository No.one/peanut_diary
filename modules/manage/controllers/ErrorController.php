<?php

namespace app\modules\manage\controllers;


use app\modules\manage\controllers\common\BaseController;

class ErrorController extends BaseController
{
    public function actionForbidden(){
        return $this->render('forbidden');
    }
}