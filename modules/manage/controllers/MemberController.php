<?php

namespace app\modules\manage\controllers;

use app\common\services\ConstantMapService;
use app\common\services\DataHelper;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\member\MemberStock;
use app\models\member\Member;
use app\models\member\MemberGrade;
use app\models\member\MemberComments;
use app\models\member\OauthMemberBind;
use app\models\goods\Goods;
use app\models\goods\Brand;
use app\models\pay\PayOrder;
use app\modules\manage\controllers\common\BaseController;

class MemberController extends BaseController {

    public function actionIndex() {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ( $p > 0 ) ? $p : 1;

        $query = Member::find();

        if ($mix_kw) {
            $where_nickname = [ 'LIKE', 'nickname', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $where_truename = [ 'LIKE', 'truename', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $where_mobile = [ 'LIKE', 'mobile', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $query->andWhere([ 'OR', $where_nickname, $where_mobile, $where_truename, $where_email]);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere([ 'status' => $status]);
        }
        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy([ 'id' => SORT_DESC])
                ->offset(( $p - 1 ) * $this->page_size)
                ->limit($this->page_size)
                ->all();

        $data = [];
        $grade_list = \Yii::$app->cache->get('memberGrade');
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'nickname' => UtilService::encode($_item['nickname']),
                    'mobile' => UtilService::encode($_item['mobile']),
                    'truename' => UtilService::encode($_item['truename']),
                    'invite_code' => UtilService::encode($_item['invite_code']),
                    'grade' => $grade_list[$_item['grade_id']]['name'],
                    'status_desc' => ConstantMapService::$status_mapping[$_item['status']],
                    'status' => $_item['status'],
                ];
            }
        }

        return $this->render('index', [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status
                    ],
                    'status_mapping' => ConstantMapService::$status_mapping,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    public function actionInfo() {
        $id = intval($this->get("id", 0));
        $reback_url = UrlService::buildWebUrl("/member/index");
        if (!$id) {
            return $this->redirect($reback_url);
        }

        $info = Member::find()->where([ 'id' => $id])->one();
        if (!$info) {
            return $this->redirect($reback_url);
        }

        $pay_order_list = PayOrder::find()->where([ 'member_id' => $id, 'status' => [ -8, 1]])->orderBy([ 'id' => SORT_DESC])->all();
        $comments_list = MemberComments::find()->where([ 'member_id' => $id])->orderBy([ 'id' => SORT_DESC])->all();


        return $this->render("info", [
                    "info" => $info,
                    "pay_order_list" => $pay_order_list,
                    'comments_list' => $comments_list
        ]);
    }

    public function actionSet() {

        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = Member::find()->where([ 'id' => $id])->one();
            }
            //取出所有的等级
            $grade_list = MemberGrade::find()->where(['status' => 1])->orderBy([ 'id' => SORT_DESC])->all();
            return $this->render('set', [
                        'info' => $info,
                        'grade_list' => $grade_list,
            ]);
        }
        $id = intval($this->post("id", 0));
        $nickname = trim($this->post("nickname", ""));
        $truename = trim($this->post("truename", ""));
        $mobile = trim($this->post("mobile", 0));
        $password = trim($this->post("password", 0));
        $grade_id = intval($this->post("grade_id", 1));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($nickname, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的姓名", -1);
        }

        if (!$id && mb_strlen($password, "utf-8") < 6) {
            return $this->renderJSON([], "请输入6-15位的登录密码", -1);
        }

        if (mb_strlen($mobile, "utf-8") != 11 || !preg_match("/^[1][3,4,5,7,8][0-9]{9}$/", $mobile)) {
            return $this->renderJSON([], "请输入正确的手机号码", -1);
        }


        $info = [];
        if ($id) {
            $info = Member::findOne(['id' => $id]);
        }
        if ($info) {
            $model_member = $info;
            $model_member->nickname = $nickname;
            $model_member->truename = $truename;
            $model_member->mobile = $mobile;
            $model_member->grade_id = $grade_id;
            if (!empty($password)) {
                $model_member->setPassword($password);
            }
            $model_member->updated_time = $date_now;
        } else {
            $model_member = new Member();
            $model_member->status = 1;
            $model_member->nickname = $nickname;
            $model_member->truename = $truename;
            $model_member->mobile = $mobile;
            $model_member->setSalt(32);
            $model_member->setPassword($password);
            $model_member->grade_id = $grade_id;
            $model_member->created_time = $date_now;
        }

        $model_member->save();
        if (!empty($model_member->errors)) {
            return $this->renderJSON(array_reverse($model_member->errors), "", -2);
        } else {
            return $this->renderJSON([], "操作成功");
        }
    }

    public function actionOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的会员账号号", -1);
        }

        if (!in_array($act, ['remove', 'normal', 'ban'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Member::find()->where([ 'id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定会员账号不存在", -1);
        }


        switch ($act) {
            case "remove":
                $info->delete();
                break;
            case "normal":
                $info->status = 0;
                break;
            case "ban":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionComment() {
        $p = intval($this->get("p", 1));
        $p = ( $p > 0 ) ? $p : 1;

        $query = MemberComments::find();

        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy([ 'id' => SORT_DESC])
                ->offset(( $p - 1 ) * $this->page_size)
                ->limit($this->page_size)
                ->all();

        $data = [];
        if ($list) {
            $member_mapping = DataHelper::getDicByRelateID($list, Member::className(), "member_id", "id", [ 'nickname', 'avatar', 'mobile']);
            $book_mapping = DataHelper::getDicByRelateID($list, Book::className(), "book_id", "id", [ 'name']);
            foreach ($list as $_item) {
                $tmp_member_info = isset($member_mapping[$_item['member_id']]) ? $member_mapping[$_item['member_id']] : [];
                $tmp_book_info = isset($book_mapping[$_item['book_id']]) ? $book_mapping[$_item['book_id']] : [];
                $data[] = [
                    'content' => UtilService::encode($_item['content']),
                    'score' => UtilService::encode($_item['score']),
                    'member_info' => $tmp_member_info,
                    'book_name' => $tmp_book_info ? UtilService::encode($tmp_book_info['name']) : ''
                ];
            }
        }
        return $this->render('comment', [
                    'list' => $data,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    /**
     * 会员等级列表
     * @return string
     */
    public function actionGrade() {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ( $p > 0 ) ? $p : 1;

        $query = MemberGrade::find();

        if ($mix_kw) {
            $query->andWhere(['LIKE', 'name', strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\'])]);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere([ 'status' => $status]);
        }


        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy([ 'sort' => SORT_ASC, 'id' => SORT_DESC])
                ->offset(( $p - 1 ) * $this->page_size)
                ->limit($this->page_size)
                ->all();

        $data = [];

        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'ratio' => UtilService::encode($_item['ratio']),
                    'sort' => UtilService::encode($_item['sort']),
                    'status' => UtilService::encode($_item['status']),
                    'status_desc' => ConstantMapService::$status_grade[$_item['status']],
                ];
            }
        }

        return $this->render('grade', [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status
                    ],
                    'status_mapping' => ConstantMapService::$status_grade,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    /**
     * 会员详情 新增 编辑
     * @return string|void
     */
    public function actionGrade_set() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = MemberGrade::find()->where([ 'id' => $id])->one();
            }

            return $this->render('grade_set', [
                        'info' => $info,
                        'status_mapping' => ConstantMapService::$status_grade,
            ]);
        }

        $id = intval($this->post("id", 0));
        $name = trim($this->post("name", ""));
        $ratio = floatval($this->post("ratio", 0));
        $sort = floatval($this->post("sort", 50));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的名称", -1);
        }
        
        if ($ratio >0 && mb_strlen($ratio, "utf-8") > 0 && !preg_match('/^(0.[\d+]{0,2}|1)$/', $ratio)) {
            return $this->renderJSON([], "比例的取值范围是0.01~1", -1);
        }

        if (mb_strlen($sort, "utf-8") > 0 && !preg_match('/^[0-9]*[1-9][0-9]*$/', $sort)) {
            return $this->renderJSON([], "排序请输入整数", -1);
        }


        $info = [];
        if ($id) {
            $info = MemberGrade::findOne(['id' => $id]);
        }
        if ($info) {
            $model_grade = $info;
            $model_grade->name = $name;
            $model_grade->ratio = $ratio;
            $model_grade->sort = $sort;
            $model_grade->updated_time = $date_now;
        } else {
            $model_grade = new MemberGrade();
            $model_grade->status = 1;
            $model_grade->name = $name;
            $model_grade->ratio = $ratio;
            $model_grade->created_time = $date_now;
        }

        $model_grade->save(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionGrade_ops() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的记录", -1);
        }

        if (!in_array($act, ['remove', 'normal', 'ban'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = MemberGrade::find()->where([ 'id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "记录不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->delete();
                return $this->renderJSON([], "操作成功");
                break;
            case "normal":
                $info->status = 0;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                return $this->renderJSON([], "操作成功");
                break;
            case "ban":
                $info->status = 1;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                return $this->renderJSON([], "操作成功");
                break;
        }
    }

    public function actionStock() {
        if (!\Yii::$app->request->isPost) {
            $id = intval($this->get("id", 0));
            $mix_kw = trim($this->get("mix_kw", ""));
            $brand_id = intval($this->get("brand_id", 0));
            $p = intval($this->get("p", 1));
            $p = ($p > 0) ? $p : 1;

            $info = Member::find()->where([ 'id' => $id])->one();

            if (!$info) {
                $this->redirect(UrlService::buildWebUrl("/member/index"));
                return false;
            }

            $stockQuery = (new \yii\db\Query())
                    ->select(['goods_id', 'sum_stock' => 'SUM(stock)'])
                    ->from('{{%member_stock}}')
                    ->groupBy('goods_id');

            $query = Goods::find()->alias('g');
            $query->leftJoin(['gs' => $stockQuery], 'g.id = gs.goods_id');
            $query->leftJoin("{{%member_stock}} ms", "g.id = ms.goods_id");
            $query->andWhere(['=', 'g.deleted_time', '0000-00-00 00:00:00']);
            if ($mix_kw) {
                $query->andWhere(['LIKE', 'g.goods_name', strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\'])]);
            }

            $query->andWhere(['g.status' => 1]);

            if ($brand_id) {
                $query->andWhere(['g.brand_id' => $brand_id]);
            }


            //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
            //60,60 ~ 11,10 - 1
            $total_res_count = $query->count();
            $total_page = ceil($total_res_count / $this->page_size);

            $list = $query
                    ->select('g.*, ms.stock as member_stock, gs.sum_stock')
                    ->orderBy(['g.id' => SORT_DESC])
                    ->offset(($p - 1) * $this->page_size)
                    ->limit($this->page_size)
                    ->asArray()
                    ->all();

            $brand_mapping = Brand::find()->orderBy(['id' => SORT_DESC])->indexBy("id")->all();
            $data = [];

            if ($list) {
                foreach ($list as $_item) {
                    $tmp_brand_info = isset($brand_mapping[$_item['brand_id']]) ? $brand_mapping[$_item['brand_id']] : [];
                    $data[] = [
                        'id' => $_item['id'],
                        'goods_name' => UtilService::encode($_item['goods_name']),
                        'price' => UtilService::encode($_item['price']),
                        'stock' => ($_item['stock'] - $_item['sum_stock']) > 0 ? UtilService::encode($_item['stock'] - $_item['sum_stock']) : 0,
                        'member_stock' => $_item['member_stock'] ? UtilService::encode($_item['member_stock']) : 0,
                        'status' => UtilService::encode($_item['status']),
                        'status_desc' => ConstantMapService::$status_goods[$_item['status']],
                        'brand_name' => $tmp_brand_info ? UtilService::encode($tmp_brand_info['name']) : '',
                    ];
                }
            }

            return $this->render('stock', [
                        'list' => $data,
                        'search_conditions' => [
                            'mix_kw' => $mix_kw,
                            'p' => $p,
                            'brand_id' => $brand_id,
                        ],
                        'brand_mapping' => $brand_mapping,
                        'pages' => [
                            'total_count' => $total_res_count,
                            'page_size' => $this->page_size,
                            'total_page' => $total_page,
                            'p' => $p,
                            'uid' => $id,
                        ]
            ]);
        } else {
            $member_id = intval($this->post('member_id'));
            $stock = intval($this->post('stock'));
            $goods_id = intval($this->post('goods_id'));

            $stockQuery = (new \yii\db\Query())
                    ->select(['goods_id', 'sum_stock' => 'SUM(stock)'])
                    ->from('{{%member_stock}}')
                    ->andWhere('goods_id=' . $goods_id)
                    ->andWhere(['!=', 'member_id', $member_id])
                    ->groupBy('goods_id');

            $query = Goods::find()->alias('g');
            $query->leftJoin(['s' => $stockQuery], 'g.id=s.goods_id');
            $query->where('g.id=' . $goods_id);
            $goods_info = $query->select('stock, s.sum_stock')->asArray()->one();

            if (($goods_info['stock'] - $goods_info['sum_stock'] - $stock) >= 0) {
                $info = MemberStock::find()->where([ 'member_id' => $member_id, 'goods_id' => $goods_id])->one();
                if ($info) {
                    $res = MemberStock::updateAll(['stock' => $stock], ['member_id' => $info['member_id'], 'goods_id' => $info['goods_id']]);
                } else {
                    $model_stock = new MemberStock();
                    $model_stock->member_id = $member_id;
                    $model_stock->goods_id = $goods_id;
                    $model_stock->stock = $stock;
                    $res = $model_stock->save(0);
                }
                if ($res) {
                    return $this->renderJSON([], "操作成功");
                } else {
                    return $this->renderJSON([], "操作失败", -1);
                }
            } else {
                return $this->renderJSON([], "可用库存不足", -1);
            }
        }
    }

}
