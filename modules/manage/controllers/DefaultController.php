<?php
namespace app\modules\manage\controllers;

use app\common\services\UrlService;
use app\modules\manage\controllers\common\BaseController;

class DefaultController extends BaseController{

    public function actionIndex(){
        return $this->redirect( UrlService::buildWebUrl("/dashboard/index") );
    }
}
