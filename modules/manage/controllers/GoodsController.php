<?php

namespace app\modules\manage\controllers;

use app\common\services\goods\GoodsService;
use app\common\services\ConstantMapService;
use app\common\services\DataHelper;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\common\services\TaoBaoService;
use app\models\goods\Goods;
use app\models\goods\GoodsCate;
use app\models\goods\Channels;
use app\modules\manage\controllers\common\BaseController;

class GoodsController extends BaseController {

    public function actionIndex() {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $cate_id = intval($this->get("cate_id", 0));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = Goods::find();
        if ($mix_kw) {
            $query->andWhere(['LIKE', 'name', strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\'])]);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }

        if ($cate_id) {
            $query->andWhere(['category_id' => $cate_id]);
        }

        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy(['id' => SORT_DESC])
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)
                ->all();
        $cate_mapping = GoodsCate::find()->orderBy(['id' => SORT_DESC])->indexBy("id")->all();
        $data = [];

        if ($list) {
            foreach ($list as $_item) {
                $tmp_cate_info = isset($cate_mapping[$_item['category_id']]) ? $cate_mapping[$_item['category_id']] : [];
                $data[] = [
                    'id' => $_item['id'],
                    'goods_name' => UtilService::encode($_item['name']),
                    'price' => UtilService::encode($_item['price']),
                    'tk_rate' => UtilService::encode($_item['tk_rate']),
                    'status' => UtilService::encode($_item['status']),
                    'status_desc' => ConstantMapService::$status_goods[$_item['status']],
                    'coupon_info' => ($_item['coupon_status'] == 1) ? UtilService::encode($_item['coupon_info']) : '暂无',
                    'cate_name' => $tmp_cate_info ? UtilService::encode($tmp_cate_info['name']) : '',
                ];
            }
        }

        return $this->render('index', [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status,
                        'cate_id' => $cate_id,
                    ],
                    'status_mapping' => ConstantMapService::$status_goods,
                    'cate_mapping' => $cate_mapping,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    public function actionInfo() {
        $id = intval($this->get("id", 0));
        $reback_url = UrlService::buildWebUrl("/goods/index");
        if (!$id) {
            return $this->redirect($reback_url);
        }

        $info = Goods::find()
                        ->from(['g' => Goods::tableName()])
                        ->joinWith('brand')->where(['g.id' => $id])->one();
        if (!$info) {
            return $this->redirect($reback_url);
        }

        //销售历史
        $sale_change_log_list = GoodsSaleChangeLog::find()->where(['goods_id' => $id])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()->all();
        $data_sale_change_log = [];
        if ($sale_change_log_list) {
            $member_mapping = DataHelper::getDicByRelateID($sale_change_log_list, Member::className(), "member_id", "id", ["nickname"]);
            foreach ($sale_change_log_list as $_sale_item) {
                $tmp_member_info = isset($member_mapping[$_sale_item['member_id']]) ? $member_mapping[$_sale_item['member_id']] : [];
                $data_sale_change_log[] = [
                    'quantity' => $_sale_item['quantity'],
                    'price' => $_sale_item['price'],
                    'member_info' => $tmp_member_info,
                    'created_time' => $_sale_item['created_time']
                ];
            }
        }

        //库存变更历史
        $stock_change_list = GoodsStockChangeLog::find()->where(['goods_id' => $id])
                        ->orderBy(['id' => SORT_DESC])->asArray()->all();

        return $this->render("info", [
                    "info" => $info,
                    "type_mapping" => ConstantMapService::$goods_type,
                    'stock_change_list' => $stock_change_list,
                    'sale_change_log_list' => $data_sale_change_log
        ]);
    }

    public function actionSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = Goods::find()->where(['id' => $id])->one();
            }

            $brand_list = Brand::find()->orderBy(['id' => SORT_DESC])->all();
            return $this->render('set', [
                        'brand_list' => $brand_list,
                        'goods_type' => ConstantMapService::$goods_type,
                        'info' => $info
            ]);
        }

        $id = intval($this->post("id", 0));
        $brand_id = intval($this->post("brand_id", 0));
        $goods_name = trim($this->post("goods_name", ""));
        $alias_name = trim($this->post("alias_name", ""));
        $price = floatval($this->post("price", 0));
        $summary = trim($this->post("summary", ""));
        $stock = intval($this->post("stock", 0));
        $shelf_life = trim($this->post("shelf_life", ""));
        $date_now = date("Y-m-d H:i:s");

        if (!$brand_id) {
            return $this->renderJSON([], "请选择品牌", -1);
        }

        if (mb_strlen($goods_name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的商品名称", -1);
        }

        if (mb_strlen($alias_name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的商品别名", -1);
        }

        if ($price < 0) {
            return $this->renderJSON([], "请输入符合规范的商品售卖价格", -1);
        }


        if (mb_strlen($summary, "utf-8") < 10) {
            return $this->renderJSON([], "请输入商品描述，并不能少于10个字符", -1);
        }

        if ($stock < 1) {
            return $this->renderJSON([], "请输入符合规范的库存量", -1);
        }

        if (mb_strlen($shelf_life, "utf-8") < 1) {
            return $this->renderJSON([], "请输入商品保质期", -1);
        }

        $info = [];
        if ($id) {
            $info = Goods::findOne(['id' => $id]);
        }
        if ($info) {
            $model_goods = $info;
        } else {
            $model_goods = new Goods();
            $model_goods->status = 1;
            $model_goods->created_time = $date_now;
        }

        $before_stock = $model_goods->stock;
        $model_goods->goods_name = $goods_name;
        $model_goods->alias_name = $alias_name;
        $model_goods->brand_id = $brand_id;
        $model_goods->price = $price;
        $model_goods->summary = $summary;
        $model_goods->stock = $stock;
        $model_goods->shelf_life = $shelf_life;
        $model_goods->updated_time = $date_now;
        if ($model_goods->save(0)) {
            GoodsService::setStockChangeLog($model_goods->id, ($model_goods->stock - $before_stock));
        }
        return $this->renderJSON([], "操作成功");
    }

    public function actionOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的账号", -1);
        }

        if (!in_array($act, ['remove', 'down', 'up'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Goods::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定商品不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->deleted_time = date("Y-m-d H:i:s");
                break;
            case "down":
                $info->status = 0;
                break;
            case "up":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }
    
    public function actionDel(){
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }
        $goods_ids = trim($this->post('goods_ids'));
        if(Goods::deleteAll(['in','id', explode(',', $goods_ids)])){
            return $this->renderJSON([], "操作成功");
        }else{
            return $this->renderJSON([], "操作失败", -1);
        }
        
    }

    public function actionCate() {
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $query = GoodsCate::find();

        if ($status > ConstantMapService::$status_default) {
            $query->where(['status' => $status]);
        }

        $list = $query->orderBy(['weight' => SORT_DESC, 'id' => SORT_DESC])->all();
        return $this->render('cate', [
                    'list' => $list,
                    'status_mapping' => ConstantMapService::$status_mapping,
                    'search_conditions' => [
                        'status' => $status
                    ]
        ]);
    }

    public function actionCate_set() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = GoodsCate::find()->where(['id' => $id])->one();
            }
            return $this->render("cate_set", [
                        'info' => $info
            ]);
        }

        $id = intval($this->post("id", 0));
        $weight = intval($this->post("weight", 1));
        $name = trim($this->post("name", ""));
        $main_image = trim($this->post("main_image", ""));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的品牌名称", -1);
        }

        $has_in = GoodsCate::find()->where(['name' => $name])->andWhere(['!=', 'id', $id])->count();
        if ($has_in) {
            return $this->renderJSON([], "该品牌名称已存在，请换一个试试", -1);
        }
        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }
        $cate_info = GoodsCate::find()->where(['id' => $id])->one();
        if ($cate_info) {
            $model_cate = $cate_info;
        } else {
            $model_cate = new GoodsCate();
            $model_cate->created_time = $date_now;
        }

        $model_cate->name = $name;
        $model_cate->weight = $weight;
        $model_cate->main_image = $main_image;
        $model_cate->updated_time = $date_now;
        $model_cate->save(0);

        return $this->renderJSON([], "操作成功");
    }

    public function actionCate_ops() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的数据", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = GoodsCate::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定数据不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->status = 0;
                break;
            case "recover":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }

    /**
     * 选品库
     */
    public function actionFavorites() {
        if (!\Yii::$app->request->isPost) {
            $taobao = new TaoBaoService();
            $taobao->getFavorites();
            $favorites = \Yii::$app->session->get("favorites");
            return $this->render("favorites", ['list' => $favorites]);
        }
    }

    public function actionExecuteUpdate() {
        if (\Yii::$app->request->isPost) {
            $favorites_id = trim($this->post('favorites_id'));
            $favorites = \Yii::$app->session->get("favorites");
            $pageNo = trim($this->post('p', 1));
            if (!array_key_exists($favorites_id, $favorites)) {
                return $this->renderJSON([], "选品库错误", -1);
            }
            $res = $this->updateTaobao($favorites_id, $pageNo);
            if($res['next_page'] == 0){
                return $this->renderJSON(['url'=> UrlService::buildWebUrl("/goods/index")], "更新成功");
            }else{
                return $this->renderJSON(['url' => UrlService::buildWebUrl("/goods/execute-update", ['favorites_id' => $favorites_id, 'p' => $res['next_page']])], "第".$pageNo."页采集完成，即将采集下一页");
            }
        } else {
            $p = intval($this->get('p', 1));
            $favorites_id = trim($this->get('favorites_id'));
            $list = Goods::find()->offset(0)->limit(20)->orderBy(['id'=>SORT_DESC])->all();
            return $this->render("goods_list", ['list' => $list, 'p'=>$p, 'n'=>(($p-2)>1?($p-2):0), 'favorites_id'=>$favorites_id]);
        }
    }

    private function updateTaobao($favorites_id, $pageNo) {
        $taobao = new TaoBaoService();
        $list = $taobao->getItemFavorites($favorites_id, $pageNo);
        if ($list) {
            $total = $taobao->getTotal();
            $pageTotal = $taobao->getPages();
            $favorites = \Yii::$app->session->get("favorites");
            $category = null;
            if ($favorites) {
                $categoryName = $favorites[$favorites_id];
                $array = explode('-', $categoryName);
                //是否推荐商品
                if (strpos($categoryName, '推荐') || strpos($categoryName, '频道')) {
                    $channel = Channels::getByName(['name' => $array[0]]);
                }
                $parent = GoodsCate::getByName($array[0]);
                if ($parent) {
                    if (!isset($array[2])) {
                        $category = $parent;
                    } else {
                        $category = GoodsCate::getByParentIdAndName($parent->id, $array[1]);
                    }
                }
            }
            foreach ($list as $k => $v) {
                if ($category) {
                    $list[$k]->category_id = $category->id;
                }
                //是否推荐商品
                if (isset($channel) && !empty($channel)) {
                    $list[$k]->channel_id = $channel->id;
                }
                $v->save(0);
            }
            $data['page_no'] = $pageNo;
            $data['page_total'] = ceil($pageTotal);
            $data['total'] = $total;
            $data['next_page'] = 0;
            if ($data['page_total'] != $pageNo) {
                $data['next_page'] = $pageNo + 1;
            }
            return $data;
        }
    }

}
