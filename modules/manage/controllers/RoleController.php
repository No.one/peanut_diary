<?php

namespace app\modules\manage\controllers;


use app\common\services\ConstantMapService;
use app\common\services\TreeService;
use app\common\services\UrlService;
use app\models\rbac\Access;
use app\models\rbac\Role;
use app\models\rbac\RoleAccess;
use app\modules\manage\controllers\common\BaseController;

class RoleController extends BaseController
{
    public function actionIndex()
    {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = Role::find();


        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->where(['status' => 1])->orderBy(['id' => SORT_DESC])
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)
            ->all();

        return $this->render("index", [
            'list' => $list,
            'search_conditions' => [
                'mix_kw' => $mix_kw,
                'p' => $p,
                'status' => $status
            ],
            'status_mapping' => ConstantMapService::$status_mapping,
            'pages' => [
                'total_count' => $total_res_count,
                'page_size' => $this->page_size,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    public function actionSet()
    {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = [];
            if ($id) {
                $info = Role::find()->where(['id' => $id])->one();
            }
            return $this->render('set', [
                'info' => $info
            ]);
        }
        $id = $this->post("id", 0);
        $name = $this->post("name", "");
        $date_now = date("Y-m-d H:i:s");
        if (!$name) {
            return $this->renderJSON([], "请输入合法的角色名称", -1);
        }
        //查询是否存在角色名相等的记录
        $role_info = Role::find()
            ->where(['name' => $name])->andWhere(['!=', 'id', $id])
            ->one();
        if ($role_info) {
            return $this->renderJSON([], "该角色名称已存在，请输入其他的角色名称", -1);
        }

        $info = Role::find()->where(['id' => $id])->one();
        if ($info) {//编辑动作
            $role_model = $info;
        } else {//添加动作
            $role_model = new Role();
            $role_model->created_time = $date_now;
        }
        $role_model->name = $name;
        $role_model->updated_time = $date_now;

        $role_model->save(0);
        return $this->renderJSON([], "操作成功", 200);


    }

    //设置角色和权限的关系逻辑
    public function actionAccess(){
        //http get 请求 展示页面
        if( \Yii::$app->request->isGet ){
            $id = $this->get("id",0);
            $reback_url =  UrlService::buildWebUrl("/role/index");
            if( !$id ){
                return $this->redirect( $reback_url );
            }
            $info = Role::find()->where([ 'id' => $id ])->one();
            if( !$info ){
                return $this->redirect( $reback_url );
            }

            //取出所有的权限
            $access_list = Access::find()->where([ 'status' => 1 ])->orderBy(['weigh' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all();
            $access_list = TreeService::subtree($access_list);

            //取出所有已分配的权限
            $role_access_list = RoleAccess::find()->where([ 'role_id' => $id ])->asArray()->all();
            $access_ids = array_column( $role_access_list,"access_id" );
            return $this->render("access",[
                "info" => $info,
                'access_list' => $access_list,
                "access_ids" => $access_ids
            ]);
        }
        //实现保存选中权限的逻辑
        $id = $this->post("id",0);
        $access_ids = $this->post("access_ids",[]);

        if( !$id ){
            return $this->renderJSON([],"您指定的角色不存在",-1);
        }

        $info = Role::find()->where([ 'id' => $id ])->one();
        if( !$info ){
            return $this->renderJSON([],"您指定的角色不存在",-1);
        }

        //取出所有已分配给指定角色的权限
        $role_access_list = RoleAccess::find()->where([ 'role_id' => $id ])->asArray()->all();
        $assign_access_ids = array_column( $role_access_list,'access_id' );

        //找出删除的权限
        $delete_access_ids = array_diff( $assign_access_ids,$access_ids );
        if( $delete_access_ids ){
            RoleAccess::deleteAll([ 'role_id' => $id,'access_id' => $delete_access_ids ]);
        }

        //找出添加的权限
        $new_access_ids = array_diff( $access_ids,$assign_access_ids );
        if( $new_access_ids ){
            foreach( $new_access_ids as $_access_id  ){
                $tmp_model_role_access = new RoleAccess();
                $tmp_model_role_access->role_id = $id;
                $tmp_model_role_access->access_id = $_access_id;
                $tmp_model_role_access->created_time = date("Y-m-d H:i:s");
                $tmp_model_role_access->save( 0 );
            }
        }
        return $this->renderJSON([],"操作成功~~",200 );
    }
}