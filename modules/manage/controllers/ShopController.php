<?php

namespace app\modules\manage\controllers;

use app\models\shop\Shop;
use app\models\shop\ShopCate;
use app\models\shop\ShopGoods;
use app\models\goods\Goods;
use app\common\services\ConstantMapService;
use app\common\services\UtilService;
use app\common\services\UrlService;
use app\modules\manage\controllers\common\BaseController;

class ShopController extends BaseController {

    public function actionIndex() {
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $mix_kw = trim($this->get("mix_kw", ""));
        $cate_id = intval($this->get("cate_id", 0));
        $p = intval($this->get("p", 1));
        $p = ( $p > 0 ) ? $p : 1;

        $query = Shop::find();
        $query->with('cate');
        if ($mix_kw) {
            $query->andWhere(['LIKE', 'name', strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\'])]);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }

        if ($cate_id) {
            $query->andWhere(['cate_id' => $cate_id]);
        }

        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $cate_mapping = ShopCate::find()->orderBy(['id' => SORT_DESC])->indexBy("id")->all();

        $list = $query->orderBy([ 'id' => SORT_DESC])
                ->offset(( $p - 1 ) * $this->page_size)
                ->limit($this->page_size)
                ->asArray()
                ->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'status' => UtilService::encode($_item['status']),
                    'status_desc' => ConstantMapService::$status_goods[$_item['status']],
                    'cate_name' => UtilService::encode($_item['cate']['name']),
                ];
            }
        }

        return $this->render("index", [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status,
                        'cate_id' => $cate_id,
                    ],
                    'status_mapping' => ConstantMapService::$status_goods,
                    'cate_mapping' => $cate_mapping,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    public function actionSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = Shop::find()->where(['id' => $id])->one();
                $info['pic'] = json_decode($info['pic'], TRUE);
            }
            $cate_list = ShopCate::find()->orderBy(['id' => SORT_DESC])->all();
            return $this->render('set', [
                        'cate_list' => $cate_list,
                        'info' => $info
            ]);
        }

        $id = intval($this->post("id", 0));
        $weight = intval($this->post("weight", 1));
        $cate_id = intval($this->post("cate_id", 0));
        $name = trim($this->post("name", ""));
        $main_image = trim($this->post("main_image", ""));
        $pic = $this->post('pic', '');
        $pic = json_encode($pic);
        if (!$cate_id) {
            return $this->renderJSON([], "请选择分类", -1);
        }

        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的商品名称", -1);
        }

        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }

        $info = [];
        if ($id) {
            $info = Shop::findOne(['id' => $id]);
        }
        if ($info) {
            $model_shop = $info;
        } else {
            $model_shop = new Shop();
        }

        $model_shop->name = $name;
        $model_shop->cate_id = $cate_id;
        $model_shop->main_image = $main_image;
        $model_shop->pic = $pic;
        $model_shop->weight = $weight;
        $model_shop->save(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的记录", -1);
        }

        if (!in_array($act, ['remove', 'down', 'up'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Shop::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定商品不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->delete();
                break;
            case "down":
                $info->status = 0;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
            case "up":
                $info->status = 1;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
        }
        return $this->renderJSON([], "操作成功");
    }

    public function actionShopGoods() {
        if (\Yii::$app->request->isGet) {
            $shop_id = intval($this->get("id"));
            $info = Shop::findOne($shop_id);
            if (empty($info)) {
                return $this->redirect(UrlService::buildWebUrl('/shop/index'));
            }
            $query = ShopGoods::find()->with(['goods' => function($query) {
                            $query->select('id,name');
                        }])->where(['shop_id' => $shop_id]);
            $list = $query->orderBy(['created_time' => SORT_DESC])->all();
            $goods_list = Goods::find()->where(['status' => 1])->select('id, name')->all();
            return $this->render('shop_goods', [
                        'info' => $info,
                        'goods_list' => $goods_list,
                        'list' => $list
            ]);
        }
        $goods_id = intval($this->post('goods_id'));
        $shop_id = intval($this->post('shop_id'));
        $model = new ShopGoods();
        $model->goods_id = $goods_id;
        $model->shop_id = $shop_id;
        $model->save(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionShopGoodsOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        if (!$id) {
            return $this->renderJSON([], "请选择要删除的商品", -1);
        }

        $info = ShopGoods::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定商品不存在", -1);
        }
        $info->delete();
        return $this->renderJSON([], "操作成功");
    }

    public function actionCate() {
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $query = ShopCate::find();

        if ($status > ConstantMapService::$status_default) {
            $query->where(['status' => $status]);
        }
        $list = $query->orderBy(['weight' => SORT_DESC, 'id' => SORT_DESC])->all();
        return $this->render('cate', [
                    'list' => $list,
                    'status_mapping' => ConstantMapService::$status_mapping,
                    'search_conditions' => [
                        'status' => $status
                    ]
        ]);
    }

    public function actionCateSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = ShopCate::find()->where(['id' => $id])->one();
            }
            return $this->render("cate_set", ['info' => $info]);
        }

        $id = intval($this->post("id", 0));
        $weight = intval($this->post("weight", 1));
        $name = trim($this->post("name", ""));
        $main_image = trim($this->post("main_image", ""));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的分类名称", -1);
        }

        $has_in = ShopCate::find()->where(['name' => $name])->andWhere(['<>', 'id', $id])->count();
        if ($has_in) {
            return $this->renderJSON([], "该分类名称已存在，请换一个试试", -1);
        }

        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }

        $cate_info = ShopCate::find()->where(['id' => $id])->one();
        if ($cate_info) {
            $model_cate = $cate_info;
        } else {
            $model_cate = new ShopCate();
            $model_cate->created_time = $date_now;
        }
        $model_cate->name = $name;
        $model_cate->weight = $weight;
        $model_cate->main_image = $main_image;
        $model_cate->updated_time = $date_now;
        $model_cate->save(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionCateOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的数据", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = ShopCate::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定数据不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->status = 0;
                break;
            case "recover":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }

}
