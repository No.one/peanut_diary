<?php

namespace app\modules\manage\controllers;

use app\modules\manage\controllers\common\BaseController;
use app\models\CustomerSetting;

class SettingController extends BaseController {

    public function actionIndex() {
        if (\Yii::$app->request->isGet) {
            $info = CustomerSetting::find()->where(['member_id' => 1])->one();
            return $this->render('index', [
                        'info' => $info
            ]);
        } else {
            $wechat = trim($this->post("wechat"));
            $work_time = trim($this->post("work_time"));
            $customer_img = trim($this->post("customer_img", ""));
            if (mb_strlen($wechat, "utf-8") < 1) {
                return $this->renderJSON([], "请输入微信号", -1);
            }
            if (!$customer_img) {
                return $this->renderJSON([], "请上传图片之后在提交", -1);
            }
            if (mb_strlen($work_time, "utf-8") < 1) {
                return $this->renderJSON([], "请输入工作时间", -1);
            }
            $customer = CustomerSetting::find()->where(['member_id' => 1])->one();
            if ($customer) {
                $model = $customer;
            } else {
                $model = new CustomerSetting();
            }
            $model->wechat = $wechat;
            $model->customer_img = $customer_img;
            $model->work_time = $work_time;
            if ($model->save(0)) {
                return $this->renderJSON([], "操作成功");
            } else {
                return $this->renderJSON([], "操作失败", -1);
            }
        }
    }

}
