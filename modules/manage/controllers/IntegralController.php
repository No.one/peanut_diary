<?php

namespace app\modules\manage\controllers;

use app\common\services\goods\GoodsService;
use app\common\services\ConstantMapService;
use app\common\services\DataHelper;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\integral\IntegralGoods;
use app\models\integral\IntegralCate;
use app\common\services\integral\IntegralGoodsService;
use app\models\member\Member;
use app\modules\manage\controllers\common\BaseController;

class IntegralController extends BaseController {

    public function actionIndex() {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $cate_id = intval($this->get("cate_id", 0));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = IntegralGoods::find();
        if ($mix_kw) {
            $query->andWhere(['LIKE', 'goods_name', strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\'])]);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }

        if ($cate_id) {
            $query->andWhere(['cate_id' => $cate_id]);
        }

        //分页功能,需要两个参数，1：符合条件的总记录数量  2：每页展示的数量
        //60,60 ~ 11,10 - 1
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy(['id' => SORT_DESC])
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)
                ->all();
        $cate_mapping = IntegralCate::find()->orderBy(['id' => SORT_DESC])->indexBy("id")->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $tmp_cate_info = isset($cate_mapping[$_item['cate_id']]) ? $cate_mapping[$_item['cate_id']] : [];
                $data[] = [
                    'id' => $_item['id'],
                    'goods_name' => UtilService::encode($_item['goods_name']),
                    'price' => UtilService::encode($_item['price']),
                    'stock' => UtilService::encode($_item['stock']),
                    'status' => UtilService::encode($_item['status']),
                    'status_desc' => ConstantMapService::$status_goods[$_item['status']],
                    'cate_name' => $tmp_cate_info ? UtilService::encode($tmp_cate_info['name']) : '',
                ];
            }
        }
        return $this->render('index', [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status,
                        'cate_id' => $cate_id,
                    ],
                    'status_mapping' => ConstantMapService::$status_goods,
                    'cate_mapping' => $cate_mapping,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    public function actionInfo() {
        $id = intval($this->get("id", 0));
        $reback_url = UrlService::buildWebUrl("/integral/index");
        if (!$id) {
            return $this->redirect($reback_url);
        }

        $info = IntegralGoods::find()
                        ->alias('ig')
                        ->innerJoin("{{%integral_cate}} ic", 'ig.cate_id=ic.id')
                        ->where(['ig.id' => $id])->one();
        if (!$info) {
            return $this->redirect($reback_url);
        }

        //销售历史
        $sale_change_log_list = GoodsSaleChangeLog::find()->where(['goods_id' => $id])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()->all();
        $data_sale_change_log = [];
        if ($sale_change_log_list) {
            $member_mapping = DataHelper::getDicByRelateID($sale_change_log_list, Member::className(), "member_id", "id", ["nickname"]);
            foreach ($sale_change_log_list as $_sale_item) {
                $tmp_member_info = isset($member_mapping[$_sale_item['member_id']]) ? $member_mapping[$_sale_item['member_id']] : [];
                $data_sale_change_log[] = [
                    'quantity' => $_sale_item['quantity'],
                    'price' => $_sale_item['price'],
                    'member_info' => $tmp_member_info,
                    'created_time' => $_sale_item['created_time']
                ];
            }
        }

        //库存变更历史
        $stock_change_list = GoodsStockChangeLog::find()->where(['goods_id' => $id])
                        ->orderBy(['id' => SORT_DESC])->asArray()->all();

        return $this->render("info", [
                    "info" => $info,
                    "type_mapping" => ConstantMapService::$goods_type,
                    'stock_change_list' => $stock_change_list,
                    'sale_change_log_list' => $data_sale_change_log
        ]);
    }

    public function actionSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = IntegralGoods::find()->where(['id' => $id])->one();
                $info['pic'] = json_decode($info['pic'], TRUE);
            }

            $cate_list = IntegralCate::find()->orderBy(['id' => SORT_DESC])->all();
            return $this->render('set', [
                        'cate_list' => $cate_list,
                        'info' => $info
            ]);
        }

        $id = intval($this->post("id", 0));
        $cate_id = intval($this->post("cate_id", 0));
        $goods_name = trim($this->post("goods_name", ""));
        $main_image = trim($this->post("main_image", ""));
        $pic = $this->post('pic', '');
        $pic = json_encode($pic);
        $price = floatval($this->post("price", 0));
        $summary = trim($this->post("summary", ""));
        $stock = intval($this->post("stock", 0));
        $date_now = date("Y-m-d H:i:s");

        if (!$cate_id) {
            return $this->renderJSON([], "请选择分类", -1);
        }

        if (mb_strlen($goods_name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的商品名称", -1);
        }
        
        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }

        if ($price < 0) {
            return $this->renderJSON([], "请输入符合规范的商品兑换积分", -1);
        }

        if (mb_strlen($summary, "utf-8") < 10) {
            return $this->renderJSON([], "请输入商品描述，并不能少于10个字符", -1);
        }

        if ($stock < 1) {
            return $this->renderJSON([], "请输入符合规范的库存量", -1);
        }

        $info = [];
        if ($id) {
            $info = IntegralGoods::findOne(['id' => $id]);
        }
        if ($info) {
            $model_goods = $info;
        } else {
            $model_goods = new IntegralGoods();
            $model_goods->status = 1;
            $model_goods->created_time = $date_now;
        }

        $before_stock = $model_goods->stock;
        $model_goods->goods_name = $goods_name;
        $model_goods->cate_id = $cate_id;
        $model_goods->main_image = $main_image;
        $model_goods->pic = $pic;
        $model_goods->price = $price;
        $model_goods->summary = $summary;
        $model_goods->stock = $stock;
        $model_goods->updated_time = $date_now;
        if ($model_goods->save(0)) {
            IntegralGoodsService::setStockChangeLog($model_goods->id, ($model_goods->stock - $before_stock));
        }
        return $this->renderJSON([], "操作成功");
    }

    public function actionOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的记录", -1);
        }

        if (!in_array($act, ['remove', 'down', 'up'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = IntegralGoods::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定商品不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->delete();
                break;
            case "down":
                $info->status = 0;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
            case "up":
                $info->status = 1;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
        }
        
        return $this->renderJSON([], "操作成功");
    }

    public function actionCate() {
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $query = IntegralCate::find();

        if ($status > ConstantMapService::$status_default) {
            $query->where(['status' => $status]);
        }
        $list = $query->orderBy(['weight' => SORT_DESC, 'id' => SORT_DESC])->all();
        return $this->render('cate', [
                    'list' => $list,
                    'status_mapping' => ConstantMapService::$status_mapping,
                    'search_conditions' => [
                        'status' => $status
                    ]
        ]);
    }

    public function actionCate_set() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = [];
            if ($id) {
                $info = IntegralCate::find()->where(['id' => $id])->one();
            }
            return $this->render("cate_set", ['info' => $info]);
        }

        $id = intval($this->post("id", 0));
        $weight = intval($this->post("weight", 1));
        $name = trim($this->post("name", ""));
        $main_image = trim($this->post("main_image", ""));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的分类名称", -1);
        }

        $has_in = IntegralCate::find()->where(['name' => $name])->andWhere(['<>', 'id', $id])->count();
        if ($has_in) {
            return $this->renderJSON([], "该分类名称已存在，请换一个试试", -1);
        }
        
        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }
        
        $cate_info = IntegralCate::find()->where(['id' => $id])->one();
        if ($cate_info) {
            $model_cate = $cate_info;
        } else {
            $model_cate = new IntegralCate();
            $model_cate->created_time = $date_now;
        }

        $model_cate->name = $name;
        $model_cate->weight = $weight;
        $model_cate->main_image = $main_image;
        $model_cate->updated_time = $date_now;
        $model_cate->save(0);

        return $this->renderJSON([], "操作成功");
    }

    public function actionCate_ops() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的数据", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = IntegralCate::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定数据不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->status = 0;
                break;
            case "recover":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }

}
