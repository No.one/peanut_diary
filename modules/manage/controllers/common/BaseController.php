<?php

namespace app\modules\manage\controllers\common;

use app\common\components\BaseWebController;
use app\common\services\TreeService;
use app\common\services\UrlService;
use app\models\rbac\Access;
use app\models\rbac\RoleAccess;
use app\models\rbac\UserRole;
use app\models\User;
use app\common\services\applog\ApplogService;

class BaseController extends BaseWebController
{
    protected $page_size = 50;
    protected $auth_cookie_name = "manage_auth";
    protected $current_user = null;
    protected $web_menu_info_cookie = 'manage_menu_cookie';
    //二级菜单
    protected $web_menu_url_cookie = [];


    public $allowAllAction = [
        'manage/user/login',
        'manage/user/logout',
        'manage/dashboard/index',
        'manage/default/index'
    ];

    /**
     * 不需要权限的链接
     * @var array
     */
    public $ignore_url = [
        'manage/user/login',
        'manage/user/logout',
        'manage/error/forbidden',
        'manage/dashboard/index'
    ];

    /**
     * 保存去的权限链接
     * @var array
     */
    public $privilege_urls = [];

    /**
     * 超级管理员帐号
     * @var array
     */
    protected $is_super_admin = [
        'admin',
    ];

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config = []);
        $this->layout = "main";
    }

    public function beforeAction($action)
    {
        $is_login = $this->checkLoginStatus();
        //获取菜单
        $this->get_web_menu();

        if (in_array($action->getUniqueId(), $this->allowAllAction)) {
            return true;
        }

        if (!$is_login) {
            if (\Yii::$app->request->isAjax) {
                $this->renderJSON([], "未登录,请返回用户中心", -302);
            } else {
                $this->redirect(UrlService::buildWebUrl("/user/login"));
            }
            return false;
        }

        ApplogService::addAppLog($this->current_user['uid']);

        //判断当前访问的链接 是否在 所拥有的权限列表中
//        if (!$this->checkPrivilege($action->getUniqueId())) {
//            $this->redirect(UrlService::buildWebUrl("/error/forbidden"));
//            return false;
//        }
        return true;
    }

    /**
     * 获取菜单
     */
    public function get_web_menu($uid = 0)
    {
        if (!$uid && $this->current_user) {
            $uid = $this->current_user->uid;
        }
        $list = $this->getCookie($this->web_menu_info_cookie);

        if (!$list){
            if (in_array($this->current_user['login_name'], $this->is_super_admin)) {
                $list = Access::find()->orderBy(['weigh' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all();
            } else {
                $role_ids = UserRole::find()->where(['uid' => $uid])->select('role_id')->asArray()->column();
                if ($role_ids) {
                    $access_ids = RoleAccess::find()->where(['role_id' => $role_ids])->select('access_id')->asArray()->column();
                    $list = Access::find()->where(['id' => $access_ids])
                        ->orderBy(['weigh' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all();
                    $this->setCookie($this->web_menu_info_cookie, $list);
                }
            }
        }
        if ($list) {
            \Yii::$app->params['web_menu']['web_menu_info'] = TreeService::subtreechild($list);
            foreach ($list as $_item) {
                if ($_item['fid'] != 0){
                    array_push($this->web_menu_url_cookie, $_item['url']);
                }
            }
        }
        \Yii::$app->params['web_menu']['url'] = $this->web_menu_url_cookie;
    }


    //检查是否有访问指定链接的权限
    public function checkPrivilege($url)
    {
        //如果是超级管理员 也不需要权限判断
        if (in_array($this->current_user['login_name'], $this->is_super_admin)) {
            return true;
        }

        //有一些页面是不需要进行权限判断的
        if (in_array($url, $this->ignore_url)) {
            return true;
        }

        return in_array($url, $this->getRolePrivilege());
    }

    /**
     * 获取某用户的所有权限
     * 取出指定用户的所属角色，
     * 在通过角色 取出 所属 权限关系
     * 在权限表中取出所有的权限链接
     */
    public function getRolePrivilege($uid = 0)
    {
        if (!$uid && $this->current_user) {
            $uid = $this->current_user->uid;
        }

        if (!$this->privilege_urls) {
            $role_ids = UserRole::find()->where(['uid' => $uid])->select('role_id')->asArray()->column();
            if ($role_ids) {
                $list = $this->getCookie($this->web_menu_info_cookie);

                if (!$list) {
                    //在通过角色 取出 所属 权限关系
                    $access_ids = RoleAccess::find()->where(['role_id' => $role_ids])->select('access_id')->asArray()->column();
                    //在权限表中取出所有的权限链接
                    $list = Access::find()->where(['id' => $access_ids])
                        ->orderBy(['weigh' => SORT_DESC, 'id' => SORT_DESC])->asArray()->all();
                    $this->setCookie($this->web_menu_info_cookie, $list);
                }

                if ($list) {
                    foreach ($list as $_item) {
                        $tmp_urls = @json_decode($_item['urls'], true);
                        $this->privilege_urls = array_merge($this->privilege_urls, $tmp_urls);
                    }
                }

            }
        }
        return $this->privilege_urls;
    }

    /**
     * 目的：验证是否当前登录态有效 true or  false
     */
    protected function checkLoginStatus()
    {

        $auth_cookie = $this->getSession($this->auth_cookie_name);

        if (!$auth_cookie) {
            return false;
        }
        list($auth_token, $uid) = explode("#", $auth_cookie);
        if (!$auth_token || !$uid) {
            return false;
        }
        if ($uid && preg_match("/^\d+$/", $uid)) {
            $user_info = User::findOne(['uid' => $uid, 'status' => 1]);
            if (!$user_info) {
                $this->removeAuthToken();
                return false;
            }
            if ($auth_token != $this->geneAuthToken($user_info)) {
                $this->removeAuthToken();
                return false;
            }
            $this->current_user = $user_info;
            \Yii::$app->view->params['current_user'] = $user_info;
            return true;
        }
        return false;
    }

    public function setLoginStatus($user_info)
    {
        $auth_token = $this->geneAuthToken($user_info);
        $this->setSession($this->auth_cookie_name, $auth_token . "#" . $user_info['uid']);
    }

    protected function removeAuthToken()
    {
        $this->removeCookie($this->web_menu_info_cookie);
        $this->removeSession($this->auth_cookie_name);
    }

    //统一生成加密字段 ,加密字符串 = md5( login_name + login_pwd + login_salt )
    public function geneAuthToken($user_info)
    {
        return md5("{$user_info['login_name']}-{$user_info['login_pwd']}-{$user_info['login_salt']}");
    }

    public function getUid()
    {
        return $this->current_user ? $this->current_user['uid'] : 0;
    }

}