<?php

namespace app\modules\manage\controllers;

use app\common\components\HttpClient;
use app\common\services\ConstantMapService;
use app\common\services\UrlService;
use app\common\services\UtilService;
use app\models\marketing\Propaganda;
use app\models\marketing\Hot;
use app\models\marketing\HotImage;
use app\models\goods\Goods;
use app\modules\manage\controllers\common\BaseController;

class MarketingController extends BaseController {

    public function actionHot() {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;

        $query = Hot::find();

        if ($mix_kw) {
            $where_name = ['LIKE', 'name', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $query->andWhere($where_name);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);


        $list = $query->orderBy(['id' => SORT_DESC])
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)
                ->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'status' => $_item['status'],
                    'weight' => $_item['weight'],
                    'created_time' => $_item['created_time'],
                ];
            }
        }
        return $this->render('hot', [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status,
                    ],
                    'status_mapping' => ConstantMapService::$status_mapping,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    public function actionHotSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = [];
            if ($id) {
                $info = Hot::find()->where(['id' => $id])->one();
            }
            return $this->render('set', [
                        'info' => $info
            ]);
        }
        $id = intval($this->post('id', 0));
        $name = trim($this->post('name', ''));
        $summary = trim($this->post('summary', ''));
        $weight = intval($this->post('weight', 1));
        if (mb_strlen($name, 'utf-8') < 1) {
            return $this->renderJSON([], '请输入名称');
        }
        if (mb_strlen($summary, 'utf-8') < 10) {
            return $this->renderJSON([], '请输入内容，并不能少于10个字符');
        }
        if ($id) {
            $info = Hot::find()->where(['id' => $id])->one();
        } else {
            $info = new Hot();
        }
        $info->name = $name;
        $info->summary = $summary;
        $info->weight = $weight;
        $info->save(0);
        return $this->renderJSON([], '添加成功');
    }

    public function actionHotOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的记录", -1);
        }

        if (!in_array($act, ['remove', 'down', 'up'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Hot::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定数据不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->delete();
                break;
            case "down":
                $info->status = 0;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
            case "up":
                $info->status = 1;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
        }
        return $this->renderJSON([], "操作成功");
    }

    public function actionHotImage() {
        if (\Yii::$app->request->isGet) {
            $hot_id = intval($this->get("id"));
            $info = Hot::findOne($hot_id);
            if (empty($info)) {
                return $this->redirect(UrlService::buildWebUrl('/marketing/hot'));
            }
            $query = HotImage::find()->where(['hot_id' => $hot_id]);
            $list = $query->orderBy(['created_time' => SORT_DESC])->all();
            $data = [];
            if ($list) {
                foreach ($list as $_item) {
                    $data[] = [
                        'id' => $_item['id'],
                        'file_key' => $_item['file_key'],
                    ];
                }
            }
            $goods_list = Goods::find()->where(['status' => 1])->select('id, name')->all();
            return $this->render('hot_images', [
                        'info' => $info,
                        'goods_list' => $goods_list,
                        'list' => $data
            ]);
        }
        $image_key = trim($this->post("main_image", ""));
        $goods_id = intval($this->post('goods_id'));
        $hot_id = intval($this->post('hot_id'));
        if (!$image_key) {
            return $this->renderJSON([], "请上传图片之后在提交", -1);
        }
        $model = new HotImage();
        $model->file_key = $image_key;
        $model->goods_id = $goods_id;
        $model->hot_id = $hot_id;
        $model->save(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionHotImageSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = [];
            if ($id) {
                $info = HotImage::find()->where(['id' => $id])->asArray()->one();
            }
            return $this->render('hot_image_set', [
                        'info' => $info
            ]);
        }

        $id = intval($this->post('id', 0));
        $name = trim($this->post('name'));
        $summary = trim($this->post('summary', ''));
        $main_image = trim($this->post("main_image", ""));
        $weight = intval($this->post('weight', 1));
        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的标题", -1);
        }

        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }

        if (mb_strlen($summary, "utf-8") < 10) {
            return $this->renderJSON([], "请输入描述，并不能少于10个字符", -1);
        }

        $info = [];
        if ($id) {
            $info = Propaganda::findOne(['id' => $id]);
        }
        if ($info) {
            $model = $info;
        } else {
            $model = new Propaganda();
        }
        $model->name = $name;
        $model->main_image = $main_image;
        $model->summary = $summary;
        $model->weight = $weight;
        $model->save(0);
        return $this->renderJSON([], "操作成功");
    }
    
    public function actionHotImageOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        if (!$id) {
            return $this->renderJSON([], "请选择要删除的图片", -1);
        }

        $info = HotImage::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定图片不存在", -1);
        }
        $info->delete();
        return $this->renderJSON([], "操作成功");
    }

    /**
     * 宣传素材
     * @return type
     */
    public function actionPropaganda() {
        $mix_kw = trim($this->get("mix_kw", ""));
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $p = intval($this->get("p", 1));
        $p = ($p > 0) ? $p : 1;
        $query = Propaganda::find();
        if ($mix_kw) {
            $where_name = ['LIKE', 'name', '%' . strtr($mix_kw, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%', false];
            $query->andWhere($where_name);
        }

        if ($status > ConstantMapService::$status_default) {
            $query->andWhere(['status' => $status]);
        }
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->orderBy(['id' => SORT_DESC])
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)
                ->all();
        $data = [];
        if ($list) {
            foreach ($list as $_item) {
                $data[] = [
                    'id' => $_item['id'],
                    'name' => UtilService::encode($_item['name']),
                    'weight' => $_item['weight'],
                    'share_count' => $_item['share_count'],
                    'status' => $_item['status'],
                    'created_time' => $_item['created_time'],
                ];
            }
        }
        return $this->render('propaganda', [
                    'list' => $data,
                    'search_conditions' => [
                        'mix_kw' => $mix_kw,
                        'p' => $p,
                        'status' => $status,
                    ],
                    'status_mapping' => ConstantMapService::$status_marketing,
                    'pages' => [
                        'total_count' => $total_res_count,
                        'page_size' => $this->page_size,
                        'total_page' => $total_page,
                        'p' => $p
                    ]
        ]);
    }

    public function actionPropagandaSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get('id', 0));
            $info = [];
            if ($id) {
                $info = Propaganda::find()->where(['id' => $id])->asArray()->one();
            }
            return $this->render('propaganda_set', [
                        'info' => $info
            ]);
        }

        $id = intval($this->post('id', 0));
        $name = trim($this->post('name'));
        $summary = trim($this->post('summary', ''));
        $main_image = trim($this->post("main_image", ""));
        $weight = intval($this->post('weight', 1));
        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的标题", -1);
        }

        if (mb_strlen($main_image, "utf-8") < 3) {
            return $this->renderJSON([], "请上传封面图", -1);
        }

        if (mb_strlen($summary, "utf-8") < 10) {
            return $this->renderJSON([], "请输入描述，并不能少于10个字符", -1);
        }

        $info = [];
        if ($id) {
            $info = Propaganda::findOne(['id' => $id]);
        }
        if ($info) {
            $model = $info;
        } else {
            $model = new Propaganda();
        }
        $model->name = $name;
        $model->main_image = $main_image;
        $model->summary = $summary;
        $model->weight = $weight;
        $model->save(0);
        return $this->renderJSON([], "操作成功");
    }

    public function actionPropagandaOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的记录", -1);
        }

        if (!in_array($act, ['remove', 'down', 'up'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Propaganda::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定数据不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->delete();
                break;
            case "down":
                $info->status = 0;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
            case "up":
                $info->status = 1;
                $info->updated_time = date("Y-m-d H:i:s");
                $info->update(0);
                break;
        }
        return $this->renderJSON([], "操作成功");
    }

}
