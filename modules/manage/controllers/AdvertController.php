<?php

namespace app\modules\manage\controllers;

use app\common\services\ConstantMapService;
use app\common\services\UrlService;
use app\models\advert\Advert;
use app\models\advert\AdvertImages;
use app\models\member\Cover;
use app\modules\manage\controllers\common\BaseController;

class AdvertController extends BaseController {
    
    public function actionIndex(){
        $status = intval($this->get("status", ConstantMapService::$status_default));
        $query = Advert::find();

        if ($status > ConstantMapService::$status_default) {
            $query->where(['status' => $status]);
        }
        $list = $query->orderBy(['weight' => SORT_DESC, 'id' => SORT_DESC])->all();
        return $this->render('index', [
                    'list' => $list,
                    'status_mapping' => ConstantMapService::$status_mapping,
                    'search_conditions' => [
                        'status' => $status
                    ]
        ]);
    }

    public function actionSet() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $info = Advert::findOne($id);
            return $this->render("set", [
                        'info' => $info
            ]);
        }

        $id = intval($this->post("id", 0));
        $weight = intval($this->post("weight", 1));
        $name = trim($this->post("name", ""));
        $date_now = date("Y-m-d H:i:s");

        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的广告位名称", -1);
        }

        $has_in = Advert::find()->where(['name' => $name])->andWhere(['<>', 'id', $id])->count();
        if ($has_in) {
            return $this->renderJSON([], "该广告位名称已存在，请换一个试试", -1);
        }

        $cate_info = Advert::find()->where(['id' => $id])->one();
        if ($cate_info) {
            $model_cate = $cate_info;
        } else {
            $model_cate = new Advert();
            $model_cate->created_time = $date_now;
        }

        $model_cate->name = $name;
        $model_cate->weight = $weight;
        $model_cate->updated_time = $date_now;
        $model_cate->save(0);

        return $this->renderJSON([], "操作成功");
    }
    
    public function actionOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        $act = trim($this->post('act', ''));
        if (!$id) {
            return $this->renderJSON([], "请选择要操作的数据", -1);
        }

        if (!in_array($act, ['remove', 'recover'])) {
            return $this->renderJSON([], "操作有误，请重试", -1);
        }

        $info = Advert::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定数据不存在", -1);
        }

        switch ($act) {
            case "remove":
                $info->status = 0;
                break;
            case "recover":
                $info->status = 1;
                break;
        }
        $info->updated_time = date("Y-m-d H:i:s");
        $info->update(0);
        return $this->renderJSON([], "操作成功");
    }
    

    public function actionImages() {
        $id = intval($this->get("id", 0));
        $check = Advert::findOne($id);
        if (!$check) {
            $this->redirect('/advert/index');
        }
        $list = AdvertImages::find()->where(['pid'=>$id])->orderBy(['id' => SORT_DESC])->all();
        return $this->render("images", [
            'list' => $list,
            'pid'=>$id,
        ]);
    }

    public function actionSetImage() {
        if (\Yii::$app->request->isGet) {
            $id = intval($this->get("id", 0));
            $pid = intval($this->get("pid", 0));
            $check = Advert::findOne($pid);
            if (!$check) {
                $this->redirect('/advert/index');
            }
            $info = AdvertImages::findOne($id);
            if($info){
               $pid = $info->pid;
            }
            return $this->render("set_image", [
                'info' => $info,
                'pid' => $pid
            ]);
        }
        
        $id = intval($this->post("id", 0));
        $weight = intval($this->post("weight", 1));
        $pid = intval($this->post("pid", 0));
        $name = trim($this->post("name", ""));
        $url = trim($this->post("url", ""));
        $main_image = trim($this->post("main_image", ""));
        $date_now = date("Y-m-d H:i:s");
        
        if (mb_strlen($name, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的分类名称", -1);
        }

        if (!$main_image) {
            return $this->renderJSON([], "请上传图片之后在提交", -1);
        }
        
        if (mb_strlen($url, "utf-8") < 1) {
            return $this->renderJSON([], "请输入符合规范的跳转地址", -1);
        }

        $total_count = AdvertImages::find()->where(['pid'=>$pid])->count();
        if ($total_count >= 5) {
            return $this->renderJSON([], "最多上传五张", -1);
        }
        
        $image_info = AdvertImages::find()->where(['id' => $id])->one();
        if ($image_info) {
            $model_image = $image_info;
        } else {
            $model_image = new AdvertImages();
            $model_image->created_time = $date_now;
        }

        $model_image->name = $name;
        $model_image->url = $url;
        $model_image->pid = $pid;
        $model_image->weight = $weight;
        $model_image->main_image = $main_image;
        $model_image->updated_time = $date_now;
        $model_image->save(0);

        return $this->renderJSON([], "操作成功");
    }

    public function actionImageOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        if (!$id) {
            return $this->renderJSON([], "请选择要删除的图片", -1);
        }

        $info = AdvertImages::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定图片不存在", -1);
        }
        $info->delete();
        return $this->renderJSON([], "操作成功");
    }
    
    public function actionCover(){
        if (\Yii::$app->request->isGet) {
            $list = Cover::find()->orderBy(['id' => SORT_DESC])->all();
            return $this->render("cover", [
                        'list' => $list
            ]);
        }
        $image_key = trim($this->post("image_key", ""));
        if (!$image_key) {
            return $this->renderJSON([], "请上传图片之后在提交", -1);
        }
        $model = new Cover();
        $model->image_key = $image_key;
        $model->created_time = date("Y-m-d H:i:s");
        $model->save(0);
        return $this->renderJSON([], "操作成功");
    }
    
    public function actionCoverOps() {
        if (!\Yii::$app->request->isPost) {
            return $this->renderJSON([], ConstantMapService::$default_syserror, -1);
        }

        $id = $this->post('id', []);
        if (!$id) {
            return $this->renderJSON([], "请选择要删除的图片", -1);
        }

        $info = Cover::find()->where(['id' => $id])->one();
        if (!$info) {
            return $this->renderJSON([], "指定图片不存在", -1);
        }
        $info->delete();
        return $this->renderJSON([], "操作成功");
    }
    

}
