<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;

StaticService::includeAppJsStatic("/plugins/plupload/plupload/plupload.full.min.js", \app\assets\WebAsset::className());

StaticService::includeAppJsStatic("/js/manage/customer/user_question_set.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_customer.php", ['current' => 'user_question']); ?>
<div class="row mg-t20 wrap_book_set">
    <div class="col-lg-12">
        <h2 class="text-center">用户答疑</h2>
        <?php if ($info): ?>
            <div class="form-horizontal m-t">
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">用户 <?= $info ? $info['member_name'] : "" ?>:</label>
                    <div class="col-lg-10">
                        <?= $info ? $info['summary'] : "" ?>
                    </div>
                    <div class="col-lg-10">
                        <?php foreach ($info['image'] as $_image): ?>
                            <a href="<?= UrlService::buildPicUrl('question', $_image['image_key']) ?>" class="swipebox"
                               target="_blank">
                                <img style="width: 90px;height: 90px;"
                                     src="<?= UrlService::buildPicUrl('question', $_image['image_key']) ?>">
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>

                <?php foreach ($info['message'] as $_item_msg): ?>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"><?= $_item_msg['role'] == "1" ? "管理 " : "用户 " ?><?= $_item_msg['member_name'] ?>
                            :</label>
                        <div class="col-lg-10">
                            <?= $_item_msg['summary'] ?>
                        </div>
                        <div class="col-lg-10">
                            <?php foreach ($_item_msg['image'] as $_item_image): ?>
                                <a href="<?= UrlService::buildPicUrl('question', $_item_image['image_key']) ?>"
                                   class="swipebox" target="_blank">
                                    <img style="width: 90px;height: 90px;"
                                         src="<?= UrlService::buildPicUrl('question', $_item_image['image_key']) ?>">
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-lg-offset-2"><?=$_item_msg['created_time']?></div>
                <?php endforeach; ?>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">我:</label>
                    <div class="col-lg-8">
                        <textarea id="summary" name="summary" style="height: 200px;width: 70%"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">上传照片:</label>
                    <div class="col-lg-8">
                        <div id="photos_area" class="photos_area">
                            <a class="cover_btn" id="cover_btn_big"><span>+</span></a>
                            <input type="hidden" name="bucket" value="question">
                        </div>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-lg-4 col-lg-offset-2">
                        <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                        <button class="btn btn-w-m btn-outline btn-primary save">发送</button>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<iframe name="upload_file" class="hide"></iframe>
