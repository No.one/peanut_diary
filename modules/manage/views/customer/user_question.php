<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;

StaticService::includeAppJsStatic("/js/manage/customer/index.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_customer.php", ['current' => 'user_question']); ?>
<div class="row">
    <div class="col-lg-12">

        <table class="table table-bordered m-t">
            <thead>
            <tr>
                <th>问题</th>
                <th>用户</th>
                <th width="15%">提问时间</th>
                <th width="10%">操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($list): ?>
                <?php foreach ($list as $_item): ?>
                    <tr>
                        <td><?= $_item['name']; ?></td>
                        <td><?= $_item['member_name'] ?></td>
                        <td><?= $_item['created_time']; ?></td>
                        <td>
                            <a class="m-l" href="<?= UrlService::buildWebUrl("/customer/user_question_set", ['id' => $_item['id']]); ?>">
                                <i class="fa fa-edit fa-lg"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6">暂无数据</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/pagination.php", [
            'pages' => $pages,
            'url' => '/user_question/index'
        ]); ?>

    </div>
</div>
