<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;
use app\assets\WebAsset;

StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.config.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.all.min.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/lang/zh-cn/zh-cn.js",\app\assets\WebAsset::className() );

StaticService::includeAppJsStatic("/js/manage/customer/notice_set.js", WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_customer.php", ['current' => 'notice']); ?>
<div class="row mg-t20 wrap_book_set">
    <div class="col-lg-12">
        <h2 class="text-center">通知公告</h2>
        <div class="form-horizontal m-t">

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">标题:</label>
                <div class="col-lg-8">
                    <input type="text" placeholder="请输入标题" name="name" class="form-control"  value="" >
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">内容:</label>
                <div class="col-lg-8">
                    <textarea id="editor" name="summary" style="height: 250px;width: 700px"><?= ''?></textarea>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= '' ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">发送</button>
                </div>
            </div>
        </div>
    </div>
</div>

<iframe name="upload_file" class="hide"></iframe>
