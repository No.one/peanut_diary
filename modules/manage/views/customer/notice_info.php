<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
?>

<?php echo Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_customer.php",[ 'current' => 'notice' ]);?>
<div class="row m-t">
	<div class="col-lg-9 col-lg-offset-2 m-t">
        <h2 class="text-center">公告详情</h2>
		<dl class="dl-horizontal">
			<dt>标题</dt>
			<dd><?=$info?UtilService::encode( $info['name'] ):'';?></dd>
			<dt>内容</dt>
			<dd><?=$info?nl2br( UtilService::encode(  $info['summary'] ) ):'';?></dd>
		</dl>
	</div>
</div>
