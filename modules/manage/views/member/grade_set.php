<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;


StaticService::includeAppJsStatic( "/js/manage/member/grade_set.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_member.php", ['current' => 'grade']); ?>

<div class="row mg-t20 wrap_grade_set">
	<div class="col-lg-12">
		<h2 class="text-center">等级设置</h2>
		<div class="form-horizontal m-t">
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-lg-2 control-label">名称:</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" placeholder="请输入名称" name="name" value="<?=$info?$info['name']:'';?>">
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-lg-2 control-label">比例:</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" placeholder="请输入比例（选填）" name="ratio" value="<?=$info?$info['ratio']:'';?>">
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-lg-2 control-label">排序:</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" placeholder="请输入排序" name="sort" value="<?=$info?$info['sort']:'50';?>">
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-lg-4 col-lg-offset-2">
					<input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
					<button class="btn btn-w-m btn-outline btn-primary save">保存</button>
				</div>
			</div>
		</div>
	</div>
</div>
