<?php

use \app\common\services\StaticService;
use app\common\services\UtilService;

StaticService::includeAppJsStatic("/js/manage/member/set.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_member.php", ['current' => 'index']); ?>

<div class="row mg-t20 wrap_member_set">
    <div class="col-lg-12">
        <h2 class="text-center">会员设置</h2>
        <div class="form-horizontal m-t">
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">昵称:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入昵称" name="nickname" value="<?= $info ? $info['nickname'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">密码:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="输入密码则修改" name="password" value="">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">会员姓名:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入会员名称" name="truename" value="<?= $info ? $info['truename'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">手机号码:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入手机号码" name="mobile" value="<?= $info ? $info['mobile'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">用户等级:</label>
                <div class="col-lg-10">
                    <select name="grade_id" class="form-control">
                        <option value="0">请选择等级</option>
                        <?php foreach ($grade_list as $_grade_info): ?>
                            <option value="<?= $_grade_info['id']; ?>" <?php if ($info && $_grade_info['id'] == $info['grade_id']): ?> selected <?php endif; ?>><?= UtilService::encode($_grade_info['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
