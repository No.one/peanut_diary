<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;
StaticService::includeAppJsStatic( "/js/manage/member/stock.js",\app\assets\WebAsset::className() );
?>

<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_member.php", ['current' => 'index']); ?>

<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-b-md">
                    <h2>会员库存</h2>
                </div>
            </div>
        </div>
        <form class="form-inline wrap_search">
            <div class="row  m-t p-w-m">
                <input type="hidden" name="id" value="<?=$pages['uid'];?>">
                <div class="form-group">
                    <select name="brand_id" class="form-control inline">
                        <option value="0">请选择品牌</option>
						<?php foreach( $brand_mapping as $_brand_info ):?>
                            <option value="<?=$_brand_info['id'];?>" <?php if( $search_conditions['brand_id']  == $_brand_info['id']):?> selected <?php endif;?> ><?=UtilService::encode( $_brand_info['name'] );?></option>
						<?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="mix_kw" placeholder="请输入关键字" class="form-control" value="<?=$search_conditions['mix_kw'];?>">
                        <span class="input-group-btn">
                            <button type="button" class="btn  btn-primary search">
                                <i class="fa fa-search"></i>搜索
                            </button>
                        </span>
                    </div>
                </div>
            </div>

        </form>
        <table class="table table-bordered m-t">
            <thead>
            <tr>
                <th>商品名</th>
                <th>品牌</th>
                <th>可分配库存</th>
                <th>占用库存</th>
            </tr>
            </thead>
            <tbody>
			<?php if( $list ):?>
				<?php foreach( $list as $_item ):?>
                    <tr>
                        <td><?= $_item['goods_name'];?></td>
                        <td><?= $_item['brand_name'] ;?></td>
                        <td><?= $_item['stock'] ;?></td>
                        <td>
                            <div class="input-group">
                                <input type="text" name="stock" placeholder="请输入库存" class="form-control" value="<?=$_item['member_stock'];?>" data-goods_id="<?= $_item['id'];?>">
                            </div>
                        </td>
                    </tr>
				<?php endforeach;?>
			<?php else:?>
                <tr><td colspan="4">暂无数据</td></tr>
			<?php endif;?>
            </tbody>
        </table>
		<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/stock_pagination.php", [
			'pages' => $pages,
			'url' => '/member/stock',
		]); ?>

    </div>
</div>
