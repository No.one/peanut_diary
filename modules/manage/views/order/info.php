<?php

use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_order.php", ['current' => 'index']); ?>
<div class="row m-t wrap_info">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-b-md">
                    <h2>订单信息</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="m-t">订单编号：<?= UtilService::encode($order_info['order_sn']); ?></p>
                <p>创建时间：<?= UtilService::encode($order_info['create_time']); ?></p>
                <p>订单状态：<?= UtilService::encode($order_info['order_status']); ?></p>
                <p>商品名称：<?= UtilService::encode($order_info['goods_name']); ?></p>
                <p>价格：<?= UtilService::encode($order_info['balance_amount']); ?></p>
                <p>佣金比例：<?= UtilService::encode($order_info['commission_ratio']); ?></p>
                <p>已获佣金：<?= UtilService::encode($order_info['commission_amount']); ?></p>
            </div>
        </div>
    </div>
    <?php if(!empty($order_info['commision'])):?>
     <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-b-md">
                    <h2>分润明细</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="m-t">推荐人：<?= UtilService::encode($order_info['commision']['selfCommision']['nickname']); ?></p>
                <p>电话：<?= UtilService::encode($order_info['commision']['selfCommision']['mobile']); ?></p>
                <p>佣金：<?= UtilService::encode($order_info['commision']['self_commision']); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="m-t">推荐人上一级：<?= UtilService::encode($order_info['commision']['higherCommision']['nickname']); ?></p>
                <p>电话：<?= UtilService::encode($order_info['commision']['higherCommision']['mobile']); ?></p>
                <p>佣金：<?= UtilService::encode($order_info['commision']['higher_commision']); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="m-t">运营商：<?= UtilService::encode($order_info['commision']['operatorCommision']['nickname']); ?></p>
                <p>电话：<?= UtilService::encode($order_info['commision']['operatorCommision']['mobile']); ?></p>
                <p>佣金：<?= UtilService::encode($order_info['commision']['operator_commision']); ?></p>
            </div>
        </div>
         <div class="row">
            <div class="col-lg-12">
                <p class="m-t">平台佣金：<?= UtilService::encode($order_info['commission_amount'] - $order_info['commision']['self_commision'] - $order_info['commision']['higher_commision'] - $order_info['commision']['operator_commision']); ?></p>
            </div>
        </div>
    </div>
    <?php endif;?>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <div class="col-lg-4 col-lg-offset-2">
            <button class="btn btn-w-m btn-outline btn-primary" onclick="history.go(-1);">返回</button>
        </div>
    </div>
</div>
<br/>
