<?php

use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;

StaticService::includeAppJsStatic("/js/manage/order/index.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_order.php", ['current' => 'index']); ?>

<div class="row">
    <div class="col-lg-12">
        <form class="form-inline wrap_search">
            <div class="row  m-t p-w-m">
                <div class="form-group">
                    <select name="status" class="form-control inline">
                        <option value="<?= ConstantMapService::$status_default; ?>">请选择状态</option>
                        <?php foreach ($status_mapping as $_status => $_title): ?>
                            <option value="<?= $_status; ?>" <?php if ($search_conditions['status'] == $_status): ?> selected <?php endif; ?> ><?= $_title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </form>
        <hr/>
        <table class="table table-bordered m-t">
            <thead>
                <tr>
                    <th>订单编号</th>
                    <th>推广人</th>
                    <th>名称</th>
                    <th>价格</th>
                    <th>佣金比例</th>
                    <th>已获佣金</th>
                    <th>状态</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($list): ?>
                    <?php foreach ($list as $_item): ?>
                        <tr>
                            <td><?= $_item['order_sn']; ?></td>
                            <td><?= $_item['member']['nickname'] ?></td>
                            <td><?= $_item['goods_name']; ?></td>
                            <td><?= $_item['balance_amount']; ?></td>
                            <td><?= $_item['commission_ratio']; ?></td>
                            <td><?= $_item['commission_amount']; ?></td>
                            <td><?= $_item['order_status']; ?></td>
                            <td><?= $_item['create_time']; ?></td>
                            <td>
                                <a  href="<?= UrlService::buildWebUrl("/order/info", [ 'id' => $_item['id']]); ?>">
                                    <i class="fa fa-eye fa-lg"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td colspan="9">暂无数据</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?php
        echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/pagination.php", [
            'pages' => $pages,
            'url' => '/order/index',
        ]);
        ?>

    </div>
</div>
