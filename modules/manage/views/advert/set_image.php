<?php

use \app\common\services\UrlService;
use \app\common\services\StaticService;

StaticService::includeAppJsStatic("/js/manage/advert/set_image.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_advert.php", ['current' => 'advert']); ?>

<div class="row m-t  wrap_image_set">
    <div class="col-lg-12">
        <h2 class="text-center">广告设置</h2>
        <div class="form-horizontal m-t m-b">
            <div class="form-group">
                <label class="col-lg-2 control-label">广告名称:</label>
                <div class="col-lg-10">
                    <input type="text" name="name" class="form-control" placeholder="请输入广告名称" value="<?= $info ? $info['name'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                 <label class="col-lg-2 control-label">主图:</label>
                 <div class="col-lg-10">
                     <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?=UrlService::buildWebUrl("/upload/pic");?>">
                         <div class="upload_wrap pull-left">
                             <i class="fa fa-upload fa-2x"></i>
                             <input type="hidden" name="bucket" value="advert" />
                             <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                         </div>
                         <?php if( $info && $info['main_image'] ):?>
                             <span class="pic-each">
                                <img src="<?=UrlService::buildPicUrl("advert",$info['main_image']);?>">
                                <span class="fa fa-times-circle del del_image" data="<?=$info['main_image'];?>"><i></i></span>
                            </span>
                         <?php endif;?>
                     </form>
                 </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">权重:</label>
                <div class="col-lg-10">
                    <input type="text" name="weight" class="form-control" placeholder="请输入权重" value="<?= $info ? $info['weight'] : '1'; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">跳转地址:</label>
                <div class="col-lg-10">
                    <input type="text" name="url" class="form-control" placeholder="请输入跳转地址" value="<?= $info ? $info['url'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                    <input type="hidden" name="pid" value="<?= $pid; ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe name="upload_file" class="hide"></iframe>
