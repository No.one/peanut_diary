<?php

use \app\common\services\ConstantMapService;
use \app\common\services\UrlService;
use \app\common\services\StaticService;

StaticService::includeAppJsStatic("/js/manage/advert/index.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_advert.php", ['current' => 'advert']); ?>

<div class="row">
    <div class="col-lg-12">
        <form class="form-inline wrap_search">
            <div class="row  m-t p-w-m">
                <div class="form-group">
                    <select name="status" class="form-control inline">
                        <option value="<?= ConstantMapService::$status_default; ?>">请选择状态</option>
                        <?php foreach ($status_mapping as $_status => $_title): ?>
                            <option value="<?= $_status; ?>" <?php if ($search_conditions['status'] == $_status): ?> selected <?php endif; ?> ><?= $_title; ?></option>
<?php endforeach; ?>
                    </select>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-lg-12">
                    <a class="btn btn-w-m btn-outline btn-primary pull-right" href="<?= UrlService::buildWebUrl("/advert/set"); ?>">
                        <i class="fa fa-plus"></i>广告位
                    </a>
                </div>
            </div>
        </form>
        <table class="table table-bordered m-t">
            <thead>
                <tr>
                    <th>序号</th>
                    <th>广告位名称</th>
                    <th>状态</th>
                    <th>权重</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
            <?php if ($list): ?>
                <?php foreach ($list as $_item): ?>
                        <tr>
                            <td><?= $_item['id']; ?></td>
                            <td><?= $_item['name']; ?></td>
                            <td><?= $status_mapping[$_item['status']]; ?></td>
                            <td><?= $_item['weight']; ?></td>
                            <td>
                                <?php if ($_item['status']): ?>
                                    <a  href="<?= UrlService::buildWebUrl("/advert/images", [ 'id' => $_item['id']]); ?>">
                                        <i class="fa fa-eye fa-lg"></i>
                                    </a>
                                    <a class="m-l" href="<?= UrlService::buildWebUrl("/advert/set", [ 'id' => $_item['id']]); ?>">
                                        <i class="fa fa-edit fa-lg"></i>
                                    </a>
                                    <a class="m-l remove" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $_item['id']; ?>">
                                        <i class="fa fa-trash fa-lg"></i>
                                    </a>
                                <?php else: ?>
                                    <a class="m-l recover" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $_item['id']; ?>">
                                        <i class="fa fa-rotate-left fa-lg"></i>
                                    </a>
                        <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td colspan="5">暂无数据</td></tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
