<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;

StaticService::includeAppJsStatic("/js/manage/advert/image.js", \app\assets\WebAsset::className());
?>

<?php echo Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_advert.php", [ 'current' => 'advert']); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="row m-t">
            <div class="col-lg-12">
                <a class="btn btn-w-m btn-outline btn-primary pull-right" href="<?= UrlService::buildWebUrl("/advert/set-image", ['pid'=>$pid]); ?>">
                    <i class="fa fa-plus"></i>图片
                </a>
            </div>
        </div>
        <table class="table table-bordered m-t">
            <thead>
                <tr>
                    <th>名称</th>
                    <th>图片（16:9）</th>
                    <th>大图地址</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($list): ?>
                    <?php foreach ($list as $_item): ?>
                        <tr>
                            <td>
                                <?= $_item['name']; ?>
                            </td>
                            <td>
                                <img src="<?= UrlService::buildPicUrl("advert", $_item['main_image']); ?>" style="width: 100px;height: 100px;"/>
                            </td>
                            <td>
                                <a target="_blank" href="<?= UrlService::buildPicUrl("advert", $_item['main_image']); ?>">查看大图</a>
                            </td>
                            <td>
                                <a class="m-l" href="<?= UrlService::buildWebUrl("/advert/set-image", [ 'id' => $_item['id']]); ?>">
                                    <i class="fa fa-edit fa-lg"></i>
                                </a>
                                <a class="m-l remove" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $_item['id']; ?>">
                                    <i class="fa fa-trash fa-lg"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td colspan="4">暂无数据</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

