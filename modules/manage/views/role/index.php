<?php
use \app\common\services\ConstantMapService;
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/manage/role/index.js",\app\assets\WebAsset::className() );
?>

<?php echo Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_account.php",[ 'current' => 'role' ]);?>

<div class="row">
	<div class="col-lg-12">
        <form class="form-inline wrap_search">
            <div class="row m-t p-w-m">
                <div class="row">
                    <div class="col-lg-12">
                        <a class="btn btn-w-m btn-outline btn-primary pull-right" href="<?=UrlService::buildWebUrl("/role/set");?>">
                            <i class="fa fa-plus"></i>角色
                        </a>
                    </div>
                </div>
        </form>
        <table class="table table-bordered m-t">
            <thead>
            <tr>
                <th>角色</th>
                <th width="15%">时间</th>
                <th width="15%">操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if( $list ):?>
                <?php foreach( $list as $_key => $_access_info ):?>
                    <tr>
                        <td><?=$_access_info['name'];?></td>
                        <td><?=$_access_info['created_time'];?></td>
                        <td>
                            <?php if( $_access_info['status'] ):?>
                                <a class="m-l" href="<?=UrlService::buildWebUrl("/role/set",[ 'id' => $_access_info['id'] ]);?>">
                                    <i class="fa fa-edit fa-lg"></i>
                                </a>

                                <a class="m-l" href="<?=UrlService::buildWebUrl("/role/access",[ 'id' => $_access_info['id'] ]);?>">
                                    <i class="fa fa-cog fa-lg"></i>
                                </a>

<!--                                <a class="m-l remove" href="--><?//=UrlService::buildNullUrl();?><!--" data="--><?//=$_access_info['id'];?><!--">-->
<!--                                    <i class="fa fa-trash fa-lg"></i>-->
<!--                                </a>-->
                            <?php else:?>
                                <a class="m-l recover" href="<?=UrlService::buildNullUrl();?>" data="<?=$_access_info['id'];?>">
                                    <i class="fa fa-rotate-left fa-lg"></i>
                                </a>
                            <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else:?>
                <tr> <td colspan="3">暂无数据</td> </tr>
            <?php endif;?>
            </tbody>
        </table>

        <!--分页代码已被封装到统一模板文件中-->
		<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/pagination.php", [
			'pages' => $pages,
			'url' => "/role/index"
		]); ?>
	</div>
</div>

