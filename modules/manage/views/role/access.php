<?php
use app\common\services\StaticService;

StaticService::includeAppJsStatic("/js/manage/role/access.js", \app\assets\WebAsset::className());
?>

<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_account.php", ['current' => 'role']); ?>

<div class="row m-t  role_access_set_wrap">
    <div class="col-lg-12">
        <h2 class="text-center">权限设置</h2>
        <div class="form-horizontal m-t m-b">
            <div class="form-group">
                <label class="col-lg-2 control-label">权限:</label>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-top: 5px">
                    <?php if ($access_list): ?>
                    <label><input type="checkbox"  level="0">全选</label>

                    <?php foreach ($access_list as $_item): ?>
                    <p style="text-indent: <?= $_item['lev'] * 20 ?>px;<?= $_item['lev'] == 2 ? "float:left;" : "clear:both;" ?> ">
                        <label>
                            <input type="checkbox" name="access_ids[]" value="<?= $_item['id']; ?>"
                                   data="<?= $_item['id'] . "_" . $_item['lev'] ?>" level="<?= $_item['lev'] ?>"
                                   id="<?=$_item['id']?>" fid="<?=$_item['fid']?>"
                                <?php if (in_array($_item['id'], $access_ids)): ?> checked <?php endif; ?>
                            >
                            <?= $_item['title']; ?>
                        </label>

                        <?php endforeach; ?>

                        <?php endif; ?>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>

