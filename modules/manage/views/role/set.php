<?php
use app\common\services\StaticService;

StaticService::includeAppJsStatic( "/js/manage/role/set.js",\app\assets\WebAsset::className() );
?>

<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_account.php", ['current' => 'role']); ?>

<div class="row m-t  role_set_wrap">
	<div class="col-lg-12">
		<h2 class="text-center">角色设置</h2>
		<div class="form-horizontal m-t m-b">
			<div class="form-group">
				<label class="col-lg-2 control-label">角色名称:</label>
				<div class="col-lg-10">
                    <input type="text" class="form-control" name="name" placeholder="请输入角色名称" value="<?=$info?$info['name']:'';?>">
				</div>
			</div>

			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
					<button class="btn btn-w-m btn-outline btn-primary save">保存</button>
				</div>
			</div>
		</div>
	</div>
</div>

