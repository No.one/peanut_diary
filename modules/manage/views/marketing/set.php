<?php

use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;
StaticService::includeAppJsStatic( "/js/manage/marketing/set.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_marketing.php", ['current' => 'hot']); ?>
<div class="row mg-t20 wrap_hot_set">
    <div class="col-lg-12">
        <h2 class="text-center">文章添加</h2>
        <div class="form-horizontal m-t">

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">文章名称:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入文章名称" name="name" value="<?=$info?$info['name']:'';?>">
                </div>
            </div>
             <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">内容:</label>
                <div class="col-lg-8">
                    <textarea name="summary" style="height: 150px;width: 300px"><?= $info ? $info['summary'] : ''; ?></textarea>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">权重:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control"  name="weight" value="<?=$info?$info['weight']:1;?>">
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>

<iframe name="upload_file" class="hide"></iframe>
