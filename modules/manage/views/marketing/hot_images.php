<?php

use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;

StaticService::includeAppCssStatic("/plugins/select2/select2.min.css", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/select2.pinyin.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/zh-CN.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/pinyin.core.js", \app\assets\WebAsset::className());

StaticService::includeAppJsStatic("/js/manage/marketing/hot_image_set.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_marketing.php", ['current' => 'hot']); ?>
<style>
    .select2-drop {
        z-index: 10050 !important;
    }

    .select2-search-choice-close {
        margin-top: 0 !important;
        right: 2px !important;
        min-height: 10px;
    }

    .select2-search-choice-close:before {
        color: black !important;
    }

    /*防止select2不会自动失去焦点*/
    .select2-container {
        z-index: 16000 !important;
    }

    .select2-drop-mask {
        z-index: 15990 !important;
    }

    .select2-drop-active {
        z-index: 15995 !important;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center">每日爆款-<?=$info['name'];?>-商品管理</h2>
                <br/>
                <a class="btn btn-w-m btn-outline btn-primary pull-right set_pic" href="<?= UrlService::buildNullUrl(); ?>">
                    <i class="fa fa-plus"></i>素材
                </a>
            </div>
        </div>
        <table class="table table-bordered m-t">
            <thead>
                <tr>
                    <th>图片（16:9）</th>
                    <th>大图地址</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
               <?php if ($list): ?>
        <?php foreach ($list as $_item): ?>
                            <tr>
                                <td>
                                    <img src="<?= UrlService::buildPicUrl("hot", $_item['file_key']); ?>" style="width: 100px;height: 100px;"/>
                                </td>
                                <td>
                                    <a target="_blank" href="<?= UrlService::buildPicUrl("hot", $_item['file_key']); ?>">查看大图</a>
                                </td>
                                <td>
                                    <a class="m-l remove" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $_item['id']; ?>">
                                        <i class="fa fa-trash fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="3">暂无数据</td></tr>
    <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="hot_image_wrap" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">选择商品</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">商品:</label>
                            <div class="col-lg-10">
                                <select name="goods_id" id="goods_id" class="form-control">
                                    <option value="0">请选择商品</option>
                                    <?php foreach ($goods_list as $_goods): ?>
                                        <option value="<?= $_goods['id']; ?>"><?= UtilService::encode($_goods['name']); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top:50px">
                            <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?= UrlService::buildWebUrl("/upload/pic"); ?>">
                                <label class="col-lg-2 control-label">封面图:</label>
                                <div class="upload_wrap pull-left">
                                    <i class="fa fa-upload fa-2x"></i>
                                    <input type="hidden" name="bucket" value="hot" />
                                    <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input name="hot_id" type="hidden" value="<?=$info['id']?>"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary save">保存</button>
            </div>
        </div>
    </div>
</div>

<iframe name="upload_file" class="hide"></iframe>
