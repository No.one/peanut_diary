<?php

use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;

StaticService::includeAppJsStatic("/plugins/plupload/plupload/plupload.full.min.js", \app\assets\WebAsset::className());

StaticService::includeAppJsStatic("/js/manage/marketing/propaganda_set.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_marketing.php", ['current' => 'propaganda']); ?>
<div class="row mg-t20 wrap_propaganda_set">
    <div class="col-lg-12">
        <h2 class="text-center">宣传素材</h2>
        <div class="form-horizontal m-t">
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">标题:</label>
                <div class="col-lg-8">
                    <input type="text" placeholder="请输入标题" name="name" class="form-control"  value="<?= $info ? $info['name'] : ''; ?>" >
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">主图:</label>
                <div class="col-lg-10">
                    <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?= UrlService::buildWebUrl("/upload/pic"); ?>">
                        <div class="upload_wrap pull-left">
                            <i class="fa fa-upload fa-2x"></i>
                            <input type="hidden" name="bucket" value="propaganda" />
                            <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                        </div>
                        <?php if ($info && $info['main_image']): ?>
                            <span class="pic-each">
                                <img src="<?= UrlService::buildPicUrl("propaganda", $info['main_image']); ?>">
                                <span class="fa fa-times-circle del del_image" data="<?= $info['main_image']; ?>"><i></i></span>
                            </span>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">内容:</label>
                <div class="col-lg-8">
                    <textarea name="summary" style="height: 150px;width: 300px"><?= $info ? $info['summary'] : ''; ?></textarea>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">权重:</label>
                <div class="col-lg-10">
                    <input type="text" name="weight" class="form-control" placeholder="请输入权重" value="<?= $info ? $info['weight'] : '1'; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe name="upload_file" class="hide"></iframe>
