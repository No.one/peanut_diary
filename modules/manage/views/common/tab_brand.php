<?php
use \app\common\services\UrlService;
$tab_list = [
	'info' => [
		'title' => '品牌信息',
		'url' => '/brand/info'
	],
	'images' => [
		'title' => '品牌相册',
		'url' => '/brand/images'
	]
];
?>
<div class="row  border-bottom">
    <div class="col-lg-12">
        <div class="tab_title">
            <ul class="nav nav-pills">
                <?php foreach( $tab_list as  $_current => $_item ):?>
                    <?php if (in_array(substr($_item['url'], 1), Yii::$app->params['web_menu']['url'])):?>
                        <li <?php if( $current == $_current ):?> class="current" <?php endif;?> >
                            <a href="<?=UrlService::buildWebUrl( $_item['url'] );?>"><?=$_item['title'];?></a>
                        </li>
                    <?php endif;?>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>