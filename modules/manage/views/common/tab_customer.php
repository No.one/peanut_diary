<?php
use \app\common\services\UrlService;
$tab_list = [
	'index' => [
		'title' => '文章列表',
		'url' => '/customer/index'
	],
    'user_question' => [
        'title' => '用户答疑',
        'url' => '/customer/user_question',
    ],
    'notice' => [
        'title' => '通知公告',
        'url' => '/customer/notice',
    ],
];
?>
<div class="row  border-bottom">
    <div class="col-lg-12">
        <div class="tab_title">
            <ul class="nav nav-pills">
                <?php foreach( $tab_list as  $_current => $_item ):?>
                    <?php if (in_array(substr($_item['url'], 1), Yii::$app->params['web_menu']['url'])):?>
                        <li <?php if( $current == $_current ):?> class="current" <?php endif;?> >
                            <a href="<?=UrlService::buildWebUrl( $_item['url'] );?>"><?=$_item['title'];?></a>
                        </li>
                    <?php endif;?>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>