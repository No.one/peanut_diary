<?php

use \app\common\services\UrlService;

$tab_list = [
    'setting' => [
        'title' => '系统设置',
        'url' => '/setting/index'
    ],
    'advert' => [
        'title' => '广告位列表',
        'url' => '/advert/index'
    ],
    'cover' => [
        'title' => '推荐封面图列表',
        'url' => '/advert/cover'
    ],
];
?>
<div class="row border-bottom">
    <div class="col-lg-12">
        <div class="tab_title">
            <ul class="nav nav-pills">
            <?php foreach ($tab_list as $_current => $_item): ?>
                <?php if (in_array(substr($_item['url'], 1), Yii::$app->params['web_menu']['url'])): ?>
                <li <?php if ($current == $_current): ?> class="current" <?php endif; ?> >
                    <a href="<?= UrlService::buildWebUrl($_item['url']); ?>"><?= $_item['title']; ?></a>
                </li>
                <?php endif; ?>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>