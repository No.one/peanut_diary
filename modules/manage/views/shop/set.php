<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;

StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.config.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.all.min.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/lang/zh-cn/zh-cn.js",\app\assets\WebAsset::className() );


StaticService::includeAppCssStatic( "/plugins/select2/select2.min.css",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/select2.pinyin.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/zh-CN.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/pinyin.core.js",\app\assets\WebAsset::className() );

StaticService::includeAppJsStatic("/plugins/plupload/plupload/plupload.full.min.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic( "/js/manage/shop/set.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_shop.php", ['current' => 'shop']); ?>
<div class="row mg-t20 wrap_shop_set">
    <div class="col-lg-12">
        <h2 class="text-center">店铺设置</h2>
        <div class="form-horizontal m-t">
            <div class="form-group">
                <label class="col-lg-2 control-label">店铺分类:</label>
                <div class="col-lg-10">
                    <select name="cate_id" class="form-control">
                        <option value="0">请选择分类</option>
                        <?php foreach( $cate_list as $_cate_info ):?>
                            <option value="<?=$_cate_info['id'];?>" <?php if( $info &&  $_cate_info['id'] == $info['cate_id'] ):?> selected <?php endif;?> ><?=UtilService::encode( $_cate_info['name'] );?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">店铺名称:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入店铺名" name="name" value="<?=$info?$info['name']:'';?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                 <label class="col-lg-2 control-label">店铺logo:</label>
                 <div class="col-lg-10">
                     <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?=UrlService::buildWebUrl("/upload/pic");?>">
                         <div class="upload_wrap pull-left">
                             <i class="fa fa-upload fa-2x"></i>
                             <input type="hidden" name="bucket" value="shop_logo" />
                             <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                         </div>
                         <?php if( $info && $info['main_image'] ):?>
                             <span class="pic-each">
                                <img src="<?=UrlService::buildPicUrl("shop_logo",$info['main_image']);?>">
                                <span class="fa fa-times-circle del del_image" data="<?=$info['main_image'];?>"><i></i></span>
                            </span>
                         <?php endif;?>
                     </form>
                 </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">banner:</label>
                <div class="col-lg-8">
                    <span>图片尺寸最小：420*200</span>
                    <div id="photos_area" class="photos_area">
                        <?php if ($info):?>
                            <?php if($info['pic']):?>
                                <?php foreach ($info['pic'] as $_item_pic):?>
                                    <span class='item' id=''>
                                        <a class='picture_delete'>×</a>
                                        <input type=hidden name='pics[]' value='<?=$_item_pic?>'><img src='<?=UrlService::buildPicUrl('shop_banner',$_item_pic)?>' alt='' />
                                    </span>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endif;?>
                        <a class="cover_btn" id="cover_btn_big"><span>+</span></a>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">权重:</label>
                <div class="col-lg-10">
                    <input type="text" name="weight" class="form-control" placeholder="请输入权重" value="<?= $info ? $info['weight'] : '1'; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe name="upload_file" class="hide"></iframe>

