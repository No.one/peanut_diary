<?php
use app\common\services\StaticService;
use app\common\services\UtilService;

StaticService::includeAppJsStatic("/js/manage/access/set.js", \app\assets\WebAsset::className());
?>

<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_account.php", ['current' => 'access']); ?>

<div class="row m-t  access_set_wrap">
    <div class="col-lg-12">
        <h2 class="text-center">权限设置</h2>

        <div class="form-horizontal m-t m-b">
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">父级权限:</label>
                <div class="col-lg-10">
                    <select name="fid" class="form-control">
                        <option value="0">请选择父级权限</option>
                        <?php foreach ($access_list as $_access_info): ?>
                            <option value="<?= $_access_info['id']; ?>" <?php if( $info &&  $_access_info['id'] == $info['fid'] ):?> selected <?php endif;?> ><?= UtilService::encode($_access_info['title']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">权限标题:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="title" placeholder="请输入权限标题"
                           value="<?= $info ? $info['title'] : ''; ?>">
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">RULE_VAL:</label>
                <div class="col-lg-10">
                    <?php
                    $urls = $info ? @json_decode($info['urls'], true) : [];
                    $urls = $urls ? $urls : [];
                    ?>
                    <textarea class="form-control" rows="5" placeholder="权限规则值（module/controller/action）"
                              name="urls"><?= implode("\r\n", $urls); ?></textarea>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">图标:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="icon" placeholder="请输入图标"
                           value="<?= $info ? $info['icon'] : ''; ?>">
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">权重:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="weigh" placeholder="请输入权重"
                           value="<?= $info ? $info['weigh'] : 1; ?>">
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>

