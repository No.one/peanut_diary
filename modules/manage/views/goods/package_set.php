<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/manage/goods/package_set.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_goods.php", ['current' => 'goods']); ?>

<div class="row m-t  wrap_package_set">
	<div class="col-lg-12">
		<h2 class="text-center">包装设置</h2>
		<div class="form-horizontal m-t m-b">
			<div class="form-group">
				<label class="col-lg-2 control-label">包装:</label>
                <div class="col-lg-10">
                    <select name="type" class="form-control">
                        <?php foreach( $goods_type as $_status => $_title):?>
                            <option value="<?=$_status;?>" <?php if( $info &&  $_status == $info['type'] ):?> selected <?php endif;?> ><?=$_title?></option>
                        <?php endforeach;?>
                    </select>
                </div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-lg-4 col-lg-offset-2">
					<input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
                    <input type="hidden" name="goods_id" value="<?=$goods_id;?>">
					<button class="btn btn-w-m btn-outline btn-primary save">保存</button>
				</div>
			</div>
		</div>
	</div>
</div>
