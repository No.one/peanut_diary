<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppCssStatic("/plugins/select2/select2.min.css", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/select2.pinyin.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/zh-CN.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/pinyin.core.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/js/manage/goods/favorites.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_goods.php", ['current' => 'favorites']); ?>

<div class="row m-t wrap_favorites">
    <div class="col-lg-12">
        <h2 class="text-center">商品更新</h2>
        <div class="form-horizontal m-t m-b">
            <div class="form-group">
                <label class="col-lg-2 control-label">选品库:</label>
                <div class="col-lg-10">
                    <select name="favorites_id" class="form-control">
                        <?php foreach ($list as $_favorites => $_title): ?>
                            <option value="<?= $_favorites; ?>" ><?= $_title ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <button class="btn btn-w-m btn-outline btn-primary reset">重置</button>&nbsp;&nbsp;
                    <button class="btn btn-w-m btn-outline btn-primary save">一键更新</button>
                </div>
            </div>
        </div>
    </div>
</div>
