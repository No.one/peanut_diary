<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;

StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.config.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.all.min.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/lang/zh-cn/zh-cn.js",\app\assets\WebAsset::className() );


StaticService::includeAppCssStatic( "/plugins/select2/select2.min.css",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/select2.pinyin.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/zh-CN.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/pinyin.core.js",\app\assets\WebAsset::className() );


StaticService::includeAppJsStatic( "/js/manage/goods/set.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_goods.php", ['current' => 'goods']); ?>
<div class="row mg-t20 wrap_goods_set">
    <div class="col-lg-12">
        <h2 class="text-center">商品设置</h2>
        <div class="form-horizontal m-t">
            <div class="form-group">
                <label class="col-lg-2 control-label">商品品牌:</label>
                <div class="col-lg-10">
                    <select name="brand_id" class="form-control">
                        <option value="0">请选择分类</option>
                        <?php foreach( $brand_list as $_brand_info ):?>
                            <option value="<?=$_brand_info['id'];?>" <?php if( $info &&  $_brand_info['id'] == $info['brand_id'] ):?> selected <?php endif;?> ><?=UtilService::encode( $_brand_info['name'] );?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">商品名称:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入商品名" name="goods_name" value="<?=$info?$info['goods_name']:'';?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">商品别名:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入商品别名" name="alias_name" value="<?=$info?$info['alias_name']:'';?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">商品价格:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入商品售价" name="price" value="<?=$info?$info['price']:'';?>">
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">商品描述:</label>
                <div class="col-lg-8">
                    <textarea id="editor"  name="summary" style="height: 300px;"><?=$info?$info['summary']:'';?></textarea>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">库存:</label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="text" name="stock" class="form-control" value="<?=$info?$info['stock']:'';?>">
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">保质期:</label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="text" name="shelf_life" class="form-control" value="<?=$info?$info['shelf_life']:'';?>">
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>

