<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;

StaticService::includeAppCssStatic("/css/manage/bootoast.css", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/js/manage/bootoast.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic("/js/manage/goods/update.js", \app\assets\WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_goods.php", ['current' => 'favorites']); ?>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered m-t" data-page="<?= $p;?>" data-favorites="<?= $favorites_id;?>">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>名称</th>
                    <th>价格</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($list): ?>
                    <?php foreach ($list as $key => $_item): ?>
                        <tr>
                            <td><?= $n*10+$key+1; ?></td>
                            <td><?= $_item['name']; ?></td>
                            <td><?= $_item['price']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td colspan="3">暂无数据</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
