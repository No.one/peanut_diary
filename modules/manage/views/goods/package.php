<?php
use \app\common\services\ConstantMapService;
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/manage/goods/package.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_goods.php", ['current' => 'goods']); ?>

<div class="row">
	<div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-b-md">
                    <h2>商品包装</h2>
                </div>
            </div>
            <div class="col-lg-12">
                <a class="btn btn-w-m btn-outline btn-primary pull-right" href="<?=UrlService::buildWebUrl("/goods/package_set", ['goods_id'=>$goods_id]);?>">
                    <i class="fa fa-plus"></i>包装
                </a>
            </div>
        </div>
		<table class="table table-bordered m-t">
			<thead>
			<tr>
				<th>序号</th>
				<th>包装</th>
				<th>操作</th>
			</tr>
			</thead>
			<tbody>
            <?php if( $list ):?>
                <?php foreach( $list as $_item ):?>
                <tr>
                    <td><?=$_item['id'];?></td>
                    <td><?=$goods_type[ $_item['type'] ];?></td>
                    <td>
                        <a class="m-l" href="<?=UrlService::buildWebUrl("/goods/package_set",[ 'id' => $_item['id'], 'goods_id'=>$_item['goods_id']]);?>">
                            <i class="fa fa-edit fa-lg"></i>
                        </a>
                        <a class="m-l remove" href="<?=UrlService::buildNullUrl();?>" data="<?=$_item['id'];?>">
                            <i class="fa fa-trash fa-lg"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            <?php else:?>
                <tr><td colspan="3">暂无数据</td></tr>
            <?php endif;?>
			</tbody>
		</table>
	</div>
</div>
