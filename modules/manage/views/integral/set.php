<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;

StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.config.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.all.min.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/lang/zh-cn/zh-cn.js",\app\assets\WebAsset::className() );


StaticService::includeAppCssStatic( "/plugins/select2/select2.min.css",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/select2.pinyin.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/zh-CN.js",\app\assets\WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/select2/pinyin.core.js",\app\assets\WebAsset::className() );

StaticService::includeAppJsStatic("/plugins/plupload/plupload/plupload.full.min.js", \app\assets\WebAsset::className());
StaticService::includeAppJsStatic( "/js/manage/integral/set.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_integral.php", ['current' => 'integral']); ?>
<div class="row mg-t20 wrap_goods_set">
    <div class="col-lg-12">
        <h2 class="text-center">商品设置</h2>
        <div class="form-horizontal m-t">
            <div class="form-group">
                <label class="col-lg-2 control-label">商品分类:</label>
                <div class="col-lg-10">
                    <select name="cate_id" class="form-control">
                        <option value="0">请选择分类</option>
                        <?php foreach( $cate_list as $_cate_info ):?>
                            <option value="<?=$_cate_info['id'];?>" <?php if( $info &&  $_cate_info['id'] == $info['cate_id'] ):?> selected <?php endif;?> ><?=UtilService::encode( $_cate_info['name'] );?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">商品名称:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入商品名" name="goods_name" value="<?=$info?$info['goods_name']:'';?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                 <label class="col-lg-2 control-label">主图:</label>
                 <div class="col-lg-10">
                     <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?=UrlService::buildWebUrl("/upload/pic");?>">
                         <div class="upload_wrap pull-left">
                             <i class="fa fa-upload fa-2x"></i>
                             <input type="hidden" name="bucket" value="integral" />
                             <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                         </div>
                         <?php if( $info && $info['main_image'] ):?>
                             <span class="pic-each">
                                <img src="<?=UrlService::buildPicUrl("integral",$info['main_image']);?>">
                                <span class="fa fa-times-circle del del_image" data="<?=$info['main_image'];?>"><i></i></span>
                            </span>
                         <?php endif;?>
                     </form>
                 </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">轮播图:</label>
                <div class="col-lg-8">
                    <span>图片尺寸最小：420*200</span>
                    <div id="photos_area" class="photos_area">
                        <?php if ($info):?>
                            <?php if($info['pic']):?>
                                <?php foreach ($info['pic'] as $_item_pic):?>
                                    <span class='item' id=''>
                                        <a class='picture_delete'>×</a>
                                        <input type=hidden name='pics[]' value='<?=$_item_pic?>'><img src='<?=UrlService::buildPicUrl('integral',$_item_pic)?>' alt='' />
                                    </span>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endif;?>
                        <a class="cover_btn" id="cover_btn_big"><span>+</span></a>
                    </div>
                </div>
            </div>
            
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">兑换积分:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入商品兑换积分" name="price" value="<?=$info?$info['price']:'';?>">
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">商品描述:</label>
                <div class="col-lg-8">
                    <textarea id="editor"  name="summary" style="height: 300px;"><?=$info?$info['summary']:'';?></textarea>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">库存:</label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="text" name="stock" class="form-control" value="<?=$info?$info['stock']:'';?>">
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe name="upload_file" class="hide"></iframe>

