<?php
use \app\common\services\UrlService;
use \app\common\services\UtilService;
use \app\common\services\StaticService;
use \app\common\services\ConstantMapService;

StaticService::includeAppJsStatic("/js/manage/integral/index.js", \app\assets\WebAsset::className());
?>

<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_integral.php", ['current' => 'integral']); ?>

<div class="row">
    <div class="col-lg-12">
        <form class="form-inline wrap_search">
            <div class="row  m-t p-w-m">
                <div class="form-group">
                    <select name="status" class="form-control inline">
                        <option value="<?= ConstantMapService::$status_default; ?>">请选择状态</option>
                        <?php foreach ($status_mapping as $_status => $_title): ?>
                            <option value="<?= $_status; ?>" <?php if ($search_conditions['status'] == $_status): ?> selected <?php endif; ?> ><?= $_title; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <select name="cate_id" class="form-control inline">
                        <option value="0">请选择分类</option>
                        <?php foreach ($cate_mapping as $_cate_info): ?>
                            <option value="<?= $_cate_info['id']; ?>" <?php if ($search_conditions['cate_id'] == $_cate_info['id']): ?> selected <?php endif; ?> ><?= UtilService::encode($_cate_info['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="mix_kw" placeholder="请输入关键字" class="form-control" value="<?= $search_conditions['mix_kw']; ?>">
                        <span class="input-group-btn">
                            <button type="button" class="btn  btn-primary search">
                                <i class="fa fa-search"></i>搜索
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-lg-12">
                    <a class="btn btn-w-m btn-outline btn-primary pull-right" href="<?= UrlService::buildWebUrl("/integral/set"); ?>">
                        <i class="fa fa-plus"></i>商品
                    </a>
                </div>
            </div>
        </form>
        <table class="table table-bordered m-t">
            <thead>
                <tr>
                    <th>商品名</th>
                    <th>分类</th>
                    <th>积分</th>
                    <th>库存</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <?php if ($list): ?>
                    <?php foreach ($list as $_item): ?>
                        <tr>
                            <td><?= $_item['goods_name']; ?></td>
                            <td><?= $_item['cate_name']; ?></td>
                            <td><?= $_item['price']; ?></td>
                            <td><?= $_item['stock']; ?></td>
                            <td>
                                <a class="<?php if ($_item['status'] == 1): ?>down<?php else: ?>up<?php endif; ?>" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $_item['id']; ?>">
                                    <?= $_item['status_desc']; ?>
                                </a>
                            </td>
                            <td>
                                <a  href="<?= UrlService::buildWebUrl("/integral/info", [ 'id' => $_item['id']]); ?>">
                                    <i class="fa fa-eye fa-lg"></i>
                                </a>
                                <a class="m-l" href="<?= UrlService::buildWebUrl("/integral/set", [ 'id' => $_item['id']]); ?>">
                                    <i class="fa fa-edit fa-lg"></i>
                                </a>
                                <a class="m-l remove" href="<?= UrlService::buildNullUrl(); ?>" data="<?= $_item['id']; ?>">
                                    <i class="fa fa-trash fa-lg"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr><td colspan="6">暂无数据</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?php
        echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/pagination.php", [
            'pages' => $pages,
            'url' => '/integral/index'
        ]);
        ?>

    </div>
</div>
