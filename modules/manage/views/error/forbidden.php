<?php
use app\common\services\UrlService;
?>
<style>
    .vertical-center{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<div class="row">
	<div class="col-lg-12 vertical-center">
        <p>
            <span class="vertical-center">
                <img src="<?=UrlService::buildWwwUrl('/images/manage/no-access.png')?>" />
                <br><br>
                <span style="text-align: center">您没有足够的权限访问该页面，请联系管理员!!!</span>
            </span>
        </p>
	</div>
</div>
