<?php
use \app\common\services\UrlService;

\app\assets\WebAsset::register($this);
$upload_config = Yii::$app->params['upload'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>管理后台</title>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="profile-element text-center">
                        <img alt="image" class="img-circle" style="width: 78px;height: 66px"
                             src="<?= UrlService::buildWwwUrl("/images/manage/logo.png"); ?>"/>
                        <p class="text-muted">总后台</p>
                    </div>
                </li>
                <?php if (Yii::$app->params['web_menu']['web_menu_info']): ?>
                    <?php foreach (Yii::$app->params['web_menu']['web_menu_info'] as $_menu_info): ?>
                        <li class="<?= $_menu_info['class'] ?>">
                            <?php if (count($_menu_info['child']) > 0):
                            $url = reset($_menu_info['child']);?>
                            <a href="<?= UrlService::buildWwwUrl($url['url']); ?>">
                                <?php else:?>
                                <a href="<?= UrlService::buildWwwUrl($_menu_info['url']); ?>">
                            <?php endif;?>
                                <i class="<?= $_menu_info['icon'] ?>"></i>
                                <span class="nav-label"><?= $_menu_info['title'] ?></span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg" style="background-color: #ffffff;">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
                       href="<?= UrlService::buildNullUrl(); ?>"><i class="fa fa-bars"></i> </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
						<span class="m-r-sm text-muted welcome-message">
                            欢迎使用
                        </span>
                    </li>
                    <li class="hidden">
                        <a class="count-info" href="<?= UrlService::buildNullUrl(); ?>">
                            <i class="fa fa-bell"></i>
                            <span class="label label-primary">8</span>
                        </a>
                    </li>
                    <li class="dropdown user_info">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?= UrlService::buildNullUrl(); ?>">
                            <img alt="image" class="img-circle"
                                 src="<?= UrlService::buildWwwUrl("/images/manage/avatar.png"); ?>"/>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box">
                                    姓名：<?= $this->params['current_user']["nickname"]; ?>
                                    <a href="<?= UrlService::buildWebUrl("/user/edit"); ?>" class="pull-right">编辑</a>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box">
                                    手机号码：<?= $this->params['current_user']["mobile"]; ?>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="link-block text-center">
                                    <a class="pull-left" href="<?= UrlService::buildWebUrl("/user/reset-pwd"); ?>">
                                        <i class="fa fa-lock"></i> 修改密码
                                    </a>
                                    <a class="pull-right" href="<?= UrlService::buildWebUrl("/user/logout"); ?>">
                                        <i class="fa fa-sign-out"></i> 退出
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>

            </nav>
        </div>
        <?= $content; ?>

    </div>
</div>
<div class="hidden_layout_warp hide">
    <input type="hidden" name="upload_config" value='<?= json_encode($upload_config); ?>'/>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


