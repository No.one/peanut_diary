<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
StaticService::includeAppJsStatic( "/js/manage/setting/index.js",\app\assets\WebAsset::className() );
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_advert.php", ['current' => 'setting']); ?>
<div class="row m-t  wrap_setting_index">
    <div class="col-lg-12">
        <h2 class="text-center">设置</h2>
        <div class="form-horizontal m-t m-b">
            <div class="form-group">
                <label class="col-lg-2 control-label">客服微信:</label>
                <div class="col-lg-10">
                    <input type="text" name="wechat" class="form-control" placeholder="请输入客服微信" value="<?=$info?$info['wechat']:'';?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">客服二维码:</label>
                <div class="col-lg-10">
                    <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?=UrlService::buildWebUrl("/upload/pic");?>">
                        <div class="upload_wrap pull-left">
                            <i class="fa fa-upload fa-2x"></i>
                            <input type="hidden" name="bucket" value="customer" />
                            <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                        </div>
                        <?php if( $info && $info['customer_img'] ):?>
                            <span class="pic-each">
                                    <img src="<?=UrlService::buildPicUrl("customer",$info['customer_img']);?>">
                                    <span class="fa fa-times-circle del del_image" data="<?=$info['customer_img'];?>"><i></i></span>
                            </span>
                        <?php endif;?>
                    </form>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">工作时间:</label>
                <div class="col-lg-10">
                    <input type="text" name="work_time" class="form-control" placeholder="请输入工作时间" value="<?=$info?$info['work_time']:'';?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe name="upload_file" class="hide"></iframe>