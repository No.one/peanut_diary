<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use \app\common\services\UtilService;
use app\assets\WebAsset;

StaticService::includeAppJsStatic("/plugins/ueditor/ueditor.config.js", WebAsset::className());
StaticService::includeAppJsStatic("/plugins/ueditor/ueditor.all.min.js", WebAsset::className());
StaticService::includeAppJsStatic("/plugins/ueditor/lang/zh-cn/zh-cn.js", WebAsset::className());

StaticService::includeAppCssStatic("/plugins/tagsinput/jquery.tagsinput.min.css", WebAsset::className());
StaticService::includeAppJsStatic("/plugins/tagsinput/jquery.tagsinput.min.js", WebAsset::className());

StaticService::includeAppCssStatic("/plugins/select2/select2.min.css", WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/select2.pinyin.js", WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/zh-CN.js", WebAsset::className());
StaticService::includeAppJsStatic("/plugins/select2/pinyin.core.js", WebAsset::className());

StaticService::includeAppCssStatic( "/plugins/datetimepicker/jquery.datetimepicker.min.css",WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/datetimepicker/jquery.datetimepicker.full.min.js",WebAsset::className() );

StaticService::includeAppJsStatic("/plugins/plupload/plupload/plupload.full.min.js", \app\assets\WebAsset::className());

StaticService::includeAppJsStatic("/js/manage/course/set.js", WebAsset::className());
?>
<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_book.php", ['current' => 'course']); ?>
<div class="row mg-t20 wrap_book_set">
    <div class="col-lg-12">
        <h2 class="text-center">课程设置</h2>
        <div class="form-horizontal m-t">
            <div class="form-group">
                <label class="col-lg-2 control-label">五季名家:</label>
                <div class="col-lg-10">
                    <select name="cat_id" class="form-control">
                        <option value="0">请选择名家</option>
                        <?php foreach ($cat_list as $_cat_info): ?>
                            <option value="<?= $_cat_info['id']; ?>" <?php if ($info && $_cat_info['id'] == $info['cat_id']): ?> selected <?php endif; ?> ><?= UtilService::encode($_cat_info['name']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程名称:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入课程名" name="name"
                           value="<?= $info ? $info['name'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程价格:</label>
                <div class="col-lg-10">
                    <div>
                        <label><input type="radio" name="is_free" value="0" <?php if($info){if ($info['is_free'] == 0){echo 'checked';}} else {echo 'checked';}?>>收费</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label><input type="radio" name="is_free" value="1" <?php if($info){if ($info['is_free'] == 1){echo 'checked';}}?>>免费</label>
                    </div>
                    <div>
                        <input type="text" class="form-control" <?php if($info){if ($info['is_free'] == 1){echo 'disabled';}}?> placeholder="请输入课程售价" name="price" value="<?= $info ? $info['price'] : ''; ?>">
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程方式:</label>
                <div class="col-lg-10">
                    <label><input type="radio" name="model" value="1" <?php if($info){if ($info['model'] == 1){echo 'checked';}} else {echo 'checked';}?>>线上</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="model" value="0" <?php if($info){if ($info['model'] == 0){echo 'checked';}}?> >线下</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程时间:</label>
                <div class="col-lg-10">
                    <div class="input-group">
                        <input type="text" placeholder="请选择开始时间" name="date_from" class="form-control"  value="<?= $info ? $info['course_date'] : ''; ?>" <?php if($info){if ($info['model'] == 1){echo 'disabled';}}else{echo 'disabled';}?>>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程地址:</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="请输入课程地址" name="course_addr" class="form-control"  value="<?= $info ? $info['course_addr'] : ''; ?>" <?php if($info){if ($info['model'] == 1){echo 'disabled';}}else{echo 'disabled';}?>>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">联系方式:</label>
                <div class="col-lg-10">
                    <input type="text" placeholder="请输入联系方式" name="contacts" class="form-control"  value="<?= $info ? $info['contacts'] : ''; ?>" <?php if($info){if ($info['model'] == 1){echo 'disabled';}}else{echo 'disabled';}?>>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">封面图:</label>
                <div class="col-lg-10">
                    <span>图片尺寸最小：150*150</span>
                    <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST"
                          action="<?= UrlService::buildWebUrl("/upload/pic"); ?>">
                        <div class="upload_wrap pull-left">
                            <i class="fa fa-upload fa-2x"></i>
                            <input type="hidden" name="bucket" value="course"/>
                            <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                        </div>
                        <?php if ($info && $info['main_image']): ?>
                            <span class="pic-each">
							<img src="<?= UrlService::buildPicUrl("course", $info['main_image']); ?>" alt="150*150">
							<span class="fa fa-times-circle del del_image"
                                  data="<?= $info['main_image']; ?>"><i></i></span>
						</span>
                        <?php endif; ?>
                    </form>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">轮播图:</label>
                <div class="col-lg-8">
                    <span>图片尺寸最小：420*200</span>
                    <div id="photos_area" class="photos_area">
                        <?php if ($info):?>
                            <?php if($info['course_pic']):?>
                                <?php foreach ($info['course_pic'] as $_item_pic):?>
                                    <span class='item' id=''>
                                        <a class='picture_delete'>×</a>
                                        <input type=hidden name='pics[]' value='<?=$_item_pic?>'><img src='<?=UrlService::buildPicUrl('course',$_item_pic)?>' alt='' />
                                    </span>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endif;?>
                        <a class="cover_btn" id="cover_btn_big"><span>+</span></a>
                    </div>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程视频:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" placeholder="请输入课程视频链接" name="video_url"
                           value="<?= $info ? $info['video_url'] : ''; ?>" <?php if($info){if ($info['model'] == 0){echo 'disabled';}}?>>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">课程描述:</label>
                <div class="col-lg-8">
                    <textarea id="editor" name="summary"
                              style="height: 300px;"><?= $info ? $info['summary'] : ''; ?></textarea>
                </div>
            </div>

            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-lg-2 control-label">搜索标签:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="tags" value="<?= $info ? $info['tags'] : ''; ?>">
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-2">
                    <input type="hidden" name="id" value="<?= $info ? $info['id'] : 0; ?>">
                    <button class="btn btn-w-m btn-outline btn-primary save">保存</button>
                </div>
            </div>
        </div>
    </div>
</div>

<iframe name="upload_file" class="hide"></iframe>
