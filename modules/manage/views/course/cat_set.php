<?php
use \app\common\services\UrlService;
use \app\common\services\StaticService;
use app\assets\WebAsset;
StaticService::includeAppJsStatic( "/js/manage/course/cat_set.js",WebAsset::className() );

//ueditor
StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.config.js",WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/ueditor.all.min.js",WebAsset::className() );
StaticService::includeAppJsStatic( "/plugins/ueditor/lang/zh-cn/zh-cn.js",WebAsset::className() );

?>

<?php echo \Yii::$app->view->renderFile("@app/modules/manage/views/common/tab_book.php", ['current' => 'famous']); ?>

<div class="row m-t  wrap_cat_set">
	<div class="col-lg-12">
		<h2 class="text-center">五季名家</h2>
		<div class="form-horizontal m-t m-b">
            <div class="hr-line-dashed"></div>
			<div class="form-group">
				<label class="col-lg-2 control-label">名称:</label>
				<div class="col-lg-10">
					<input type="text" name="name" class="form-control" placeholder="请输入名家名称" value="<?=$info?$info['name']:'';?>">
				</div>
			</div>
            <div class="form-group">
                <label class="col-lg-2 control-label">头像:</label>
                <div class="col-lg-10">
                    <form class="upload_pic_wrap" target="upload_file" enctype="multipart/form-data" method="POST" action="<?=UrlService::buildWebUrl("/upload/pic");?>">
                        <div class="upload_wrap pull-left">
                            <i class="fa fa-upload fa-2x"></i>
                            <input type="hidden" name="bucket" value="course" />
                            <input type="file" name="pic" accept="image/png, image/jpeg, image/jpg,image/gif">
                        </div>
                        <?php if( $info && $info['avatar'] ):?>
                            <span class="pic-each">
							<img src="<?=UrlService::buildPicUrl("course",$info['avatar']);?>" alt="120*200">
							<span class="fa fa-times-circle del del_image" data="<?=$info['avatar'];?>"><i></i></span>
						</span>
                        <?php endif;?>
                    </form>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">描述:</label>
                <div class="col-lg-8">
                    <textarea   id="editor"  name="summary" style="height: 130px;width: 600px"><?=$info?$info['summary']:'';?></textarea>
                </div>
            </div>
			<div class="form-group">
				<label class="col-lg-2 control-label">权重:</label>
				<div class="col-lg-10">
					<input type="text" name="weight" class="form-control" placeholder="请输入分类名称" value="<?=$info?$info['weight']:'1';?>">
				</div>
			</div>
			<div class="hr-line-dashed"></div>
			<div class="form-group">
				<div class="col-lg-4 col-lg-offset-2">
					<input type="hidden" name="id" value="<?=$info?$info['id']:0;?>">
					<button class="btn btn-w-m btn-outline btn-primary save">保存</button>
				</div>
			</div>
		</div>
	</div>
</div>

<iframe name="upload_file" class="hide"></iframe>