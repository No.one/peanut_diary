<?php

namespace app\modules\weixin\controllers;

use app\common\components\BaseWebController;
use app\common\services\UrlService;
use app\common\services\weixin\RequestService;

class MenuController extends BaseWebController {

    public function actionSet() {
        $menu = [
            "button" => [
                [
                    "name" => "五季运动",
                    "sub_button" => [
                        [
                            "type" => "view",
                            "name" => "关于我们",
                            "url" => UrlService::buildMUrl("/user/building")
                        ],
                        [
                            "type" => "view",
                            "name" => "新闻动态",
                            "url" => UrlService::buildMUrl("/user/building")
                        ],
                        [
                            "type" => "view",
                            "name" => "联系我们",
                            "url" => UrlService::buildMUrl("/user/building")
                        ]
                    ],
                ],
                [
                    "name" => "五季商城",
                    "sub_button" => [
                        [
                            "type" => "view",
                            "name" => "五季名家",
                            "url" => UrlService::buildMUrl("/course/cat")
                        ],
                        [
                            "type" => "view",
                            "name" => "周边产品",
                            "url" => UrlService::buildMUrl("/product/index")
                        ],
                        [
                            "type" => "view",
                            "name" => "在线客服",
                            "url" => UrlService::buildMUrl("/user/online_service")
                        ]
                    ],
                ],
                [
                    "name" => "个人中心",
                    "sub_button" => [
                        [
                            "type" => "view",
                            "name" => "已购产品",
                            "url" => UrlService::buildMUrl("/user/product_order")
                        ],
                        [
                            "type" => "view",
                            "name" => "我的收藏",
                            "url" => UrlService::buildMUrl("/user/fav")
                        ],
                        [
                            "type" => "view",
                            "name" => "个人信息",
                            "url" => UrlService::buildMUrl("/user/index")
                        ]
                    ],
                ],
            ]
        ];
        $config = \Yii::$app->params['weixin'];
        RequestService::setConfig($config['appid'], $config['token'], $config['sk']);
        $access_token = RequestService::getAccessToken();
        if ($access_token) {
            $url = "menu/create?access_token={$access_token}";
            $ret = RequestService::send($url, json_encode($menu, JSON_UNESCAPED_UNICODE), 'POST');
            var_dump($ret);
        }
    }

}
