<?php

namespace app\modules\api\controllers;

use app\common\services\UtilService;
use app\common\services\UrlService;
use app\models\member\Member;
use app\models\member\MemberGrade;
use app\models\member\MemberInvite;
use app\models\member\Address;
use app\models\member\Cover;
use app\models\member\Suggest;
use app\models\member\Withdraw;
use app\models\sms\SmsCaptcha;
use app\models\order\TaobaoOrder;
use app\models\order\TaobaoCommision;
use app\models\CustomerSetting;
use app\models\UploadForm;
use app\common\handler\MemberHandler;
use app\common\components\ApiController;
use app\common\helpers\Err;
use yii\base\DynamicModel;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\db\Expression;
use dosamigos\qrcode\QrCode;

class MemberController extends ApiController {

    public function init() {
        $this->config['handle_login'] = ['login', 'register', 'sms-login', 'three-login', 'update-password', 'check-invite'];  //todo 小写 按yii路由请求规则 无需加前缀action
        parent::init();
    }

    /**
     * 检测邀请码
     */
    public function actionCheckInvite() {
        if (!verify_request_sign(['invite_code'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $invite_code = trim(post('invite_code'));
        $invite_info = Member::checkInviteCode($invite_code);
        if (mb_strlen($invite_code, "utf-8") != 6 || empty($invite_info)) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请填写正确的邀请码");
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
    }

    /**
     * 用户注册
     */
    public function actionRegister() {
        if (!verify_request_sign(['mobile', 'captcha_code', 'invite_code'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = trim(post("mobile"));
        $captcha_code = trim(post("captcha_code"));
        $invite_code = trim(post('invite_code'));
        $wechat = trim(post('wechat'));
        $avatar = trim(post('avatar'));
        $nickname = trim(post('nickname'));
        if ($wechat) {
            $check = Member::findOne(['wechat' => $wechat]);
            if ($check) {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "该微信已绑定，请更换");
            }
        }

        if (mb_strlen($mobile, "utf-8") != 11 || !preg_match("/^[1-9]\d{10}$/", $mobile)) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的手机号码");
        }

        if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code, 'register')) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的短信验证码");
        }

        $invite_info = Member::checkInviteCode($invite_code);
        if (mb_strlen($invite_code, "utf-8") != 6 || empty($invite_info)) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请填写正确的邀请码");
        }

        $member_info = Member::find()->where(['mobile' => $mobile])->one();
        if ($member_info) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "手机号码已注册，请直接登录");
        }
        $model_member = new Member();
        $model_member->mobile = $mobile;
        $model_member->reg_ip = UtilService::getIP();
        $model_member->status = 1;
        if ($nickname) {
            $model_member->nickname = $nickname;
        }
        if ($model_member->save(0)) {
            SmsCaptcha::flush($mobile, 'register');
            MemberInvite::createInvite($model_member->id, $invite_info->id);
            if ($wechat) {
                $model_member->wechat = $wechat;
                if ($avatar) {
                    $model_member->avatar = self::downloadImage($avatar, $model_member->id);
                }
                $model_member->save();
            }
            //绑定用户id
            \Yii::$app->queue->pushOn(new MemberHandler(), ['event' => 'pid', 'data' => ['member_id' => $model_member->id]], 'pid');
            $token = $model_member->getToken();
            $data = [
                'nickname' => $model_member['nickname'],
                'avatar' => (empty($model_member['avatar'])) ? "" : UrlService::buildPicUrl('avatar', $model_member['avatar']),
                'invite_code' => $model_member['invite_code'],
                'mobile' => $model_member['mobile'],
                'created_time' => $model_member->created_time,
                'token' => $token
            ];
            $this->setApiResult(Err::SUCCESS, "注册成功", $data);
        } else {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, $model_member::getModelError($model_member));
        }
    }

    /**
     * 会员登录接口
     */
    public function actionLogin() {
        if (!verify_request_sign(['mobile', 'password'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        $password = post('password');

        //参数验证
        $compact = ['mobile', 'password'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['mobile', 'password'], 'required'],
                    [['mobile', 'password'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $member = Member::findOne(['mobile' => $mobile]);
            if (empty($member)) {
                $this->setApiResult(Err::ERR_DATA_NOT_EXIST, '用户不存在');
            }
            if ($member['status'] == Member::STATUS_DISABLE) {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, '账户已禁用');
            }
            $result = $member->checkPassword($password);
            if ($result) {
                //没有淘宝pid 则绑定
                if (empty($member['tb_pid'])) {
                    \Yii::$app->queue->pushOn(new MemberHandler(), ['event' => 'pid', 'data' => ['member_id' => $member['id']]], 'pid');
                }
                $token = $member->getToken();
                $data = [
                    'nickname' => $member['nickname'],
                    'avatar' => (empty($member['avatar'])) ? "" : UrlService::buildPicUrl('avatar', $member['avatar']),
                    'invite_code' => $member['invite_code'],
                    'mobile' => $member['mobile'],
                    'score' => $member['score'],
                    'created_time' => $member['created_time'],
                    'token' => $token
                ];
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $data);
            } else {
                $this->setApiResult(Err::ERR_MEMBER_PWD_ERROR, Err::getMessage(Err::ERR_MEMBER_PWD_ERROR));
            }
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 会员短信登录
     */
    public function actionSmsLogin() {
        if (!verify_request_sign(['mobile', 'captcha_code'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        $captcha_code = post('captcha_code');

        //参数验证
        $compact = ['mobile', 'captcha_code'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['mobile', 'captcha_code'], 'required'],
                    [['mobile', 'captcha_code'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $member = Member::findOne(['mobile' => $mobile]);
            if (empty($member)) {
                $this->setApiResult(Err::ERR_DATA_NOT_EXIST, '用户不存在');
            }
            if ($member['status'] == Member::STATUS_DISABLE) {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, '账户已禁用');
            }
            if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code, 'login')) {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的短信验证码");
            }
            //没有淘宝pid 则绑定
            if (empty($member['tb_pid'])) {
                \Yii::$app->queue->pushOn(new MemberHandler(), ['event' => 'pid', 'data' => ['member_id' => $member['id']]], 'pid');
            }
            $token = $member->getToken();
            $data = [
                'nickname' => $member['nickname'],
                'avatar' => (empty($member['avatar'])) ? "" : UrlService::buildPicUrl('avatar', $member['avatar']),
                'invite_code' => $member['invite_code'],
                'mobile' => $member['mobile'],
                'score' => $member['score'],
                'created_time' => $member['created_time'],
                'token' => $token
            ];
            SmsCaptcha::flush($mobile, 'login');
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $data);
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 第三方登录
     */
    public function actionThreeLogin() {
        if (!verify_request_sign(['wechat'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $wechat = post('wechat');
        //参数验证
        $compact = ['wechat'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['wechat'], 'required'],
                    [['wechat'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $member = Member::findOne(['wechat' => $wechat]);
            if (empty($member)) {
                $this->setApiResult(Err::ERR_DATA_NOT_EXIST, '用户不存在,请注册');
            }
            if ($member['status'] == Member::STATUS_DISABLE) {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, '账户已禁用');
            }
            //没有淘宝pid 则绑定
            if (empty($member['tb_pid'])) {
                \Yii::$app->queue->pushOn(new MemberHandler(), ['event' => 'pid', 'data' => ['member_id' => $member['id']]], 'pid');
            }
            $token = $member->getToken();
            $data = [
                'nickname' => $member['nickname'],
                'avatar' => (empty($member['avatar'])) ? "" : UrlService::buildPicUrl('avatar', $member['avatar']),
                'invite_code' => $member['invite_code'],
                'mobile' => $member['mobile'],
                'score' => $member['score'],
                'created_time' => $member['created_time'],
                'token' => $token
            ];
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $data);
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 修改密码
     * @return mixed
     */
    public function actionUpdatePassword() {
        if (!verify_request_sign(['password', 'mobile', 'captcha_code'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }

        $mobile = trim(post("mobile"));
        $captcha_code = trim(post("captcha_code"));
        $password = trim(post('password'));

        if (mb_strlen($mobile, "utf-8") != 11 || !preg_match("/^[1-9]\d{10}$/", $mobile)) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的手机号码");
        }
        $member = Member::findOne(['mobile' => $mobile]);
        if (empty($member)) {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, '用户不存在');
        }
        if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code, 'change_password')) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的短信验证码");
        }

        if (mb_strlen($password, "utf-8") < 6) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "密码最少6位数");
        }

        $member->setSalt(32);
        $member->setPassword($password);
        $member->save();
        if (!empty($member->errors)) {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, array_reverse($member->errors));
        } else {
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
        }
    }

    /**
     * 绑定微信
     */
    public function actionBindWechat() {
        if (!verify_request_sign(['wechat', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $wechat = trim(post("wechat"));
        $check = Member::findOne(['wechat' => $wechat]);
        if ($check) {
            $this->setApiResult(Err::ERR_DATA_EXIST, '微信已占用，请更换');
        }
        $model_member = Member::findOne($this->member_id);
        $model_member->wechat = $wechat;
        $model_member->save(0);
        if (!empty($model_member->errors)) {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, array_reverse($model_member->errors));
        } else {
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
        }
    }

    /**
     * 微信解绑
     */
    public function actionUnbindWechat() {
        if (!verify_request_sign(['token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        Member::updateAll(['wechat' => NULL], ['id' => $this->member_id]);
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
    }

    /**
     * 用户设置
     */
    public function actionSet() {
        if (!verify_request_sign(['token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $info = Member::find()
                        ->where(['id' => $this->member_id])
                        ->select('avatar, nickname, mobile, (CASE WHEN alipay IS NULL THEN 0 ELSE 1 END) as is_bind_alipay, alipay, (CASE WHEN wechat IS NULL THEN 0 ELSE 1 END) as is_bind_wechat')
                        ->asArray()->one();
        if ($info) {
            $info['avatar'] = (empty($info['avatar'])) ? "" : UrlService::buildPicUrl('avatar', $info['avatar']);
            $info['alipay'] = (empty($info['alipay'])) ? "" : $info['alipay'];
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $info);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 头像上传
     */
    public function actionAvatar() {
        $model = new UploadForm();
        $model->file = UploadedFile::getInstanceByName('file');
        if ($model->file) {
            $file_name = 'm_' . $this->member_id . '.' . $model->file->extension;
            $url = \Yii::$app->basePath . '/web' . \Yii::$app->params['upload']['avatar'] . '/' . $file_name;  //图片保存路径
            if ($model->file->saveAs($url)) {
                Member::updateAll(['avatar' => $file_name], ['id' => $this->member_id]);
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['avatar' => UrlService::buildPicUrl('avatar', $file_name)]);
            } else {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, '保存失败');
            }
        } else {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, '上传失败');
        }
    }

    /**
     * 更新用户昵称
     */
    public function actionNickname() {
        if (!verify_request_sign(['nickname', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $nickname = trim(post("nickname"));
        if (mb_strlen($nickname, "utf-8") < 0 || empty($nickname)) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "请填写合法的用户昵称");
        }
        $member_model = Member::findOne($this->member_id);
        $member_model->nickname = $nickname;
        if ($member_model->save(0)) {
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
        } else {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, '更新失败');
        }
    }

    /**
     * 验证旧手机号码
     */
    public function actionValidateOldMobile() {
        if (!verify_request_sign(['mobile', 'captcha_code', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        $captcha_code = post('captcha_code');
        //参数验证
        $compact = ['mobile', 'captcha_code'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['mobile', 'captcha_code'], 'required'],
                    [['mobile', 'captcha_code'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $member = Member::findOne(['mobile' => $mobile, 'id' => $this->member_id]);
            if (empty($member)) {
                $this->setApiResult(Err::ERR_DATA_NOT_EXIST, '数据不存在');
            }

            if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code, 'validate_old_mobile')) {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的短信验证码");
            }
            SmsCaptcha::flush($mobile, 'validate_old_mobile');
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 验证新手机号码
     */
    public function actionValidateNewMobile() {
        if (!verify_request_sign(['mobile', 'captcha_code', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        $captcha_code = post('captcha_code');
        //参数验证
        $compact = ['mobile', 'captcha_code'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['mobile', 'captcha_code'], 'required'],
                    [['mobile', 'captcha_code'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $member = Member::findOne(['mobile' => $mobile]);
            if (!empty($member)) {
                $this->setApiResult(Err::ERR_DATA_EXIST, '手机号码已占用');
            }

            if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($mobile, $captcha_code, 'validate_new_mobile')) {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的短信验证码");
            }

            $model_member = Member::findOne($this->member_id);
            $model_member->mobile = $mobile;
            if ($model_member->save(0)) {
                SmsCaptcha::flush($mobile, 'validate_new_mobile');
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
            } else {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, $model_member::getModelError($model_member));
            }
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 支付宝绑定详情
     */
    public function actionAlipayInfo() {
        if (!verify_request_sign(['token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $alipay_info = Member::find()->where(['id' => $this->member_id])->select('truename, mobile, alipay, money')->asArray()->one();
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $alipay_info);
    }

    /**
     * 绑定支付宝
     */
    public function actionBindAlipay() {
        if (!verify_request_sign(['truename', 'alipay', 'captcha_code', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $truename = trim(post('truename'));
        $alipay = trim(post('alipay'));
        $captcha_code = trim(post('captcha_code'));
        //参数验证
        $compact = ['truename', 'captcha_code', 'alipay'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['truename', 'captcha_code', 'alipay'], 'required'],
                    [['truename', 'captcha_code', 'alipay'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $model_member = Member::findOne($this->member_id);
            if (mb_strlen($captcha_code, "utf-8") != 6 || !SmsCaptcha::checkCaptcha($model_member->mobile, $captcha_code, 'bind_alipay')) {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的短信验证码");
            }
            $model_member->truename = $truename;
            $model_member->alipay = $alipay;
            if ($model_member->save(0)) {
                SmsCaptcha::flush($model_member->mobile, 'bind_alipay');
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
            } else {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, $model_member::getModelError($model_member));
            }
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    public function actionWithdrawList() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $query = Withdraw::find();
        $query->andWhere(['member_id' => $this->member_id]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('order_sn, status, money, remark, created_time')
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)
                ->orderBy(['created_time' => SORT_DESC])
                ->asArray()
                ->all();
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
            'list' => $list,
            'pages' => [
                'total_count' => $total_res_count,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    /**
     * 支付宝提现
     */
    public function actionWithdraw() {
        if (!verify_request_sign(['token', 'money'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $money = floatval(post('money'));
        if (($money - 1) < 0) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "提现金额最低为1元");
        }
        $user_money = Member::find()->where(['id' => $this->member_id])->one();
        if (($user_money['money'] - $money) < 0) {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, "可提现金额不足");
        }
        $model = new Withdraw();
        $model->member_id = $this->member_id;
        $model->money = $money;
        $model->status = 0;
        if ($model->save(0)) {
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
        } else {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, $model::getModelError($model));
        }
    }

    /**
     * 我的地址列表
     */
    public function actionAddressList() {
        if (!verify_request_sign(['token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $list = Address::find()->where(['member_id' => $this->member_id])->select('id as address_id, consignee, consignee_tel, province, province_id, city_id, city, district_id, district, address, is_default')->asArray()->all();
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['list' => $list]);
    }

    /**
     * 地址设置
     */
    public function actionAddressSet() {
        if (!verify_request_sign(['consignee', 'consignee_tel', 'province_id', 'province', 'city_id', 'city', 'district_id', 'district', 'address', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $consignee = trim(post('consignee'));
        $consignee_tel = trim(post('consignee_tel'));
        $province_id = intval(post('province_id'));
        $province = trim(post('province'));
        $city_id = intval(post('city_id'));
        $city = trim(post('city'));
        $district_id = intval(post('district_id'));
        $district = trim(post('district'));
        $address = trim(post('address'));
        $address_id = intval(post('address_id'));
        //参数验证
        $compact = ['consignee', 'consignee_tel', 'province_id', 'province', 'city_id', 'city', 'district_id', 'district', 'address'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['consignee', 'consignee_tel', 'province_id', 'province', 'city_id', 'city', 'district_id', 'district', 'address'], 'required'],
                    [['consignee_tel', 'province', 'city', 'district', 'address'], 'string'],
                    [['province_id', 'city_id', 'district_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            if (mb_strlen($consignee_tel, "utf-8") != 11 || !preg_match("/^[1-9]\d{10}$/", $consignee_tel)) {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "请输入正确的手机号码");
            }

            if ($address_id) {
                $model_adress = Address::findOne($address_id);
            } else {
                $model_adress = new Address();
            }
            $model_adress->member_id = $this->member_id;
            $model_adress->consignee = $consignee;
            $model_adress->consignee_tel = $consignee_tel;
            $model_adress->province_id = $province_id;
            $model_adress->province = $province;
            $model_adress->city_id = $city_id;
            $model_adress->city = $city;
            $model_adress->district_id = $district_id;
            $model_adress->district = $district;
            $model_adress->address = $address;

            if ($model_adress->save(0)) {
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
            } else {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, $model_adress::getModelError($model_adress));
            }
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 设置/取消默认地址
     */
    public function actionAddressDefault() {
        if (!verify_request_sign(['address_id', 'is_default', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $address_id = intval(post('address_id'));
        $is_default = intval(post('is_default'));
        //参数验证
        $compact = ['address_id', 'is_default'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['address_id', 'is_default'], 'required'],
                    [['address_id', 'is_default'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        try {
            $model_adress = Address::findOne(['member_id' => $this->member_id, 'id' => $address_id]);
            if (!$model_adress) {
                $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
            }

            $model_adress->is_default = $is_default;
            if ($model_adress->save(0)) {
                if ($is_default == 1) {
                    Address::updateAll(['is_default' => 0], 'member_id=:member_id and id <> :id', [':member_id' => $this->member_id, ':id' => $address_id]);
                }
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
            } else {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, $model_adress::getModelError($model_adress));
            }
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 删除地址
     */
    public function actionAddressDel() {
        if (!verify_request_sign(['address_id', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $address_id = intval(post('address_id'));
        //参数验证
        $compact = ['address_id'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['address_id'], 'required'],
                    [['address_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        try {
            $model_adress = Address::findOne(['member_id' => $this->member_id, 'id' => $address_id]);
            if (!$model_adress) {
                $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
            }
            if ($model_adress->delete()) {
                $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
            } else {
                $this->setApiResult(Err::ERR_OPERATE_FAIL, $model_adress::getModelError($model_adress));
            }
        } catch (Exception $e) {
            $this->setApiResult($e->getCode(), $e->getMessage());
        }
    }

    /**
     * 获取默认地址记录
     */
    public function actionAddressDefaultInfo() {
        if (!verify_request_sign(['token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $info = Address::find()->where(['member_id' => $this->member_id, 'is_default' => 1])->select('id as address_id, consignee, consignee_tel, province_id, province, city_id, city, district_id, district, address')->asArray()->one();
        if ($info) {
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['info' => $info]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 我的粉丝
     */
    public function actionFans() {
        if (!verify_request_sign(['token', 'grade_id'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $grade_id = intval(post('grade_id'));
        if (!in_array($grade_id, ['-1', '0', '1'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $list = Member::getChild($this->member_id);
        if ($list) {
            $data = [];
            $child = [];
            $recommend = [];
            switch ($grade_id) {
                case 0:
                    foreach ($list as $key => $k) {
                        if ($k['grade_id'] == 0) {
                            $data[$key] = $k;
                            $data[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                            if ($k['parent'] == $this->member_id) {
                                $child[$key] = $k;
                                $child[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                            } else {
                                $recommend[$key] = $k;
                                $recommend[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                            }
                        }
                    }
                    unset($list);
                    break;

                case 1:
                    foreach ($list as $key => $k) {
                        if ($k['grade_id'] == 1) {
                            $data[$key] = $k;
                            $data[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                            if ($k['parent'] == $this->member_id) {
                                $child[$key] = $k;
                                $child[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                            } else {
                                $recommend[$key] = $k;
                                $recommend[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                            }
                        }
                    }
                    unset($list);
                    break;

                default :
                    foreach ($list as $key => $k) {
                        $data[$key] = $k;
                        $data[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                        if ($k['parent'] == $this->member_id) {
                            $child[$key] = $k;
                            $child[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                        } else {
                            $recommend[$key] = $k;
                            $recommend[$key]['avatar'] = (!empty($k['avatar'])) ? UrlService::buildPicUrl("avatar", $k['avatar']) : '';
                        }
                    }

                    unset($list);
                    break;
            }

            $all_grade_list = array_column($data, 'grade_id');
            $all_count_list = array_count_values($all_grade_list);
            $all_level_0_count = isset($all_count_list[0]) ? $all_count_list[0] : 0;
            $all_level_1_count = isset($all_count_list[1]) ? $all_count_list[1] : 0;

            $child_grade_list = array_column($child, 'grade_id');
            $child_count_list = array_count_values($child_grade_list);
            $child_level_0_count = isset($child_count_list[0]) ? $child_count_list[0] : 0;
            $child_level_1_count = isset($child_count_list[1]) ? $child_count_list[1] : 0;

            $recommend_grade_list = array_column($recommend, 'grade_id');
            $recommend_count_list = array_count_values($recommend_grade_list);
            $recommend_level_0_count = isset($recommend_count_list[0]) ? $recommend_count_list[0] : 0;
            $recommend_level_1_count = isset($recommend_count_list[1]) ? $recommend_count_list[1] : 0;


            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'all_list' => [
                    'list' => array_values($data),
                    'level_0_count' => $all_level_0_count,
                    'level_1_count' => $all_level_1_count,
                ],
                'child_list' => [
                    'list' => array_values($child),
                    'level_0_count' => $child_level_0_count,
                    'level_1_count' => $child_level_1_count,
                ],
                'recommend_list' => [
                    'list' => array_values($recommend),
                    'level_0_count' => $recommend_level_0_count,
                    'level_1_count' => $recommend_level_1_count,
                ],
                'total' => count($data),
            ]);
        }
    }

    /**
     * 邀请码
     */
    public function actionRecommend() {
        $member_info = Member::findOne($this->member_id);
        $avatar = \Yii::$app->basePath . '/web' . UrlService::getPicUrl('avatar', $member_info->avatar);
        $recommend_url = \Yii::$app->basePath . '/web' . UrlService::getPicUrl('recommend', 'm_' . $this->member_id . '.png');
        if (!@file_get_contents($recommend_url)) {
            QrCode::png(UrlService::buildApiUrl("/wechat/index", ['invite_code' => $member_info->invite_code]), $recommend_url, 3, 5, 2);
            if (@file_get_contents($avatar)) {
                $QR = imagecreatefromstring(file_get_contents($recommend_url));
                $avatar = imagecreatefromstring(file_get_contents($avatar));
                $QR_width = imagesx($QR); //二维码图片宽度 
                $QR_height = imagesy($QR); //二维码图片高度 
                $logo_width = imagesx($avatar); //logo图片宽度 
                $logo_height = imagesy($avatar); //logo图片高度 
                $logo_qr_width = $QR_width / 3;
                $scale = $logo_width / $logo_qr_width;
                $logo_qr_height = $logo_height / $scale;
                $from_width = ($QR_width - $logo_qr_width) / 2;
                //重新组合图片并调整大小 
                imagecopyresampled($QR, $avatar, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
                imagepng($QR, $recommend_url);
            }
        }
        $cover = [];
        $cover_list = Cover::find()->all();
        if ($cover_list) {
            foreach ($cover_list as $key => $k) {
                $cover[$key] = UrlService::buildPicUrl('cover', $k['image_key']);
            }
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['recommend_image' => UrlService::buildPicUrl('recommend', 'm_' . $this->member_id . '.png'), 'cover' => $cover]);
    }

    /**
     * 意见反馈
     */
    public function actionSuggest() {
        if (!verify_request_sign(['token', 'content'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $content = trim(post('content'));
        $model = new UploadForm();
        $model->file = UploadedFile::getInstancesByName('file');
        $res = $model->uploadMore("suggest");
        if (empty($res)) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, "上传图片不能为空");
        }
        $model_suggest = new Suggest();
        $model_suggest->member_id = $this->member_id;
        $model_suggest->content = $content;
        $model_suggest->image_keys = json_encode($res);
        if ($model_suggest->save(0)) {
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
        } else {
            $this->setApiResult(Err::ERR_OPERATE_FAIL, Err::getMessage(Err::ERR_OPERATE_FAIL));
        }
    }

    /**
     * 我的客服
     */
    public function actionCustomer() {
        $user_id = $this->member_id;
        $invite = MemberInvite::findOne(['member_id' => $user_id]);
        $list = Member::getFather($invite['invite_id']);
        foreach ($list as $key => $k) {
            if ($k['grade_id'] == MemberGrade::MAX_LEVEL) {
                $parent_id = $k['member_id'];
                break;
            }
        }
        $customer = CustomerSetting::find()->where(['member_id' => $parent_id])->select("customer_img, wechat, work_time")->asArray()->one();
        $customer['customer_img'] = empty($customer['customer_img']) ? UrlService::buildPicUrl('customer', "default.png") : UrlService::buildPicUrl('customer', $customer['customer_img']);
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $customer);
    }

    /**
     * 会员收益
     */
    public function actionBalance() {
        $last_start_day = date('Y-m-d 00:00:00', strtotime(date('Y-m-01') . ' -1 month'));
        $last_end_day = date('Y-m-d H:i:s', strtotime(date('Y-m-01') . ' -1 second'));
        //上个月结算
        $last_month_self_settlement = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['status' => 2])
                ->andWhere(['between', 'order_time', $last_start_day, $last_end_day])
                ->sum('self_commision');
        $last_month_child_settlement = TaobaoCommision::find()
                ->andWhere(['higher_id' => $this->member_id])
                ->andWhere(['status' => 2])
                ->andWhere(['between', 'order_time', $last_start_day, $last_end_day])
                ->sum('higher_commision');
        $last_month_settlement = $last_month_self_settlement + $last_month_child_settlement;
        //上个月预估
        $last_month_self_estimate = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $last_start_day, $last_end_day])
                ->sum('self_commision');
        $last_month_child_estimate = TaobaoCommision::find()
                ->where(['higher_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $last_start_day, $last_end_day])
                ->sum('higher_commision');
        $last_month_estimate = $last_month_self_estimate + $last_month_child_estimate;

        $this_start_day = date('Y-m-01 00:00:00', strtotime(date("Y-m-d")));
        $this_end_day = date('Y-m-d H:i:s', strtotime(date('Y-m-01') . " +1 month -1 second"));
        //本月预估
        $this_month_self_estimate = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $this_start_day, $this_end_day])
                ->sum('self_commision');
        $this_month_child_estimate = TaobaoCommision::find()
                ->where(['higher_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $this_start_day, $this_end_day])
                ->sum('higher_commision');
        $this_month_estimate = $this_month_self_estimate + $this_month_child_estimate;

        $today_start = date('Y-m-d 00:00:00');
        $today_end = date('Y-m-d H:i:s', strtotime(date('Y-m-d') . " +1 days -1 second"));
        //今天付款数
        $today_self_pay = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['status' => 1])
                ->andWhere(['between', 'order_time', $today_start, $today_end])
                ->count();
        $today_child_pay = TaobaoCommision::find()
                ->andWhere(['higher_id' => $this->member_id])
                ->andWhere(['status' => 1])
                ->andWhere(['between', 'order_time', $today_start, $today_end])
                ->count();
        $today_pay = $today_self_pay + $today_child_pay;
        //今天预估收入
        $today_self_estimate = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $today_start, $today_end])
                ->sum('self_commision');
        $today_child_estimate = TaobaoCommision::find()
                ->where(['higher_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $today_start, $today_end])
                ->sum('higher_commision');
        $today_estimate = $today_self_estimate + $today_child_estimate;

        $yesterday_start = date('Y-m-d 00:00:00', strtotime(date('Y-m-d') . " -1 days"));
        $yesterday_end = date('Y-m-d H:i:s', strtotime(date('Y-m-d') . " -1 second"));
        //今天付款数
        $yesterday_self_pay = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['status' => 1])
                ->andWhere(['between', 'order_time', $yesterday_start, $yesterday_end])
                ->count();
        $yesterday_child_pay = TaobaoCommision::find()
                ->andWhere(['higher_id' => $this->member_id])
                ->andWhere(['status' => 1])
                ->andWhere(['between', 'order_time', $yesterday_start, $yesterday_end])
                ->count();
        $yesterday_pay = $yesterday_self_pay + $yesterday_child_pay;
        //今天预估收入
        $yesterday_self_estimate = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $yesterday_start, $yesterday_end])
                ->sum('self_commision');
        $yesterday_child_estimate = TaobaoCommision::find()
                ->where(['higher_id' => $this->member_id])
                ->andWhere(['or', ['status' => 1], ['status' => 2], ['status' => 4]])
                ->andWhere(['between', 'order_time', $yesterday_start, $yesterday_end])
                ->sum('higher_commision');
        $yesterday_estimate = $yesterday_self_estimate + $yesterday_child_estimate;

        /**
         * 累计结算
         */
        $total_self_commision = TaobaoCommision::find()
                ->andWhere(['member_id' => $this->member_id])
                ->andWhere(['status' => 2])
                ->sum('self_commision');
        $total_child_commision = TaobaoCommision::find()
                ->andWhere(['higher_id' => $this->member_id])
                ->andWhere(['status' => 2])
                ->sum('higher_commision');
        $total_commision = $total_self_commision + $total_child_commision;
        $data = [
            'last_month_settlement' => $last_month_settlement,
            'last_month_estimate' => $last_month_estimate,
            'this_month_estimate' => $this_month_estimate,
            'today_pay' => $today_pay,
            'today_estimate' => $today_estimate,
            'yesterday_pay' => $yesterday_pay,
            'yesterday_estimate' => $yesterday_estimate,
            'total_commision' => $total_commision,
            'money' => $this->current_user['money']
        ];
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), $data);
    }

    /**
     * 收益详情
     */
    public function actionCommisionDetail() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $month = trim(post('month'));
        $commision_query = TaobaoCommision::find();
        $query = TaobaoCommision::find();
        $query->with(['order' => function($query) {
                $query->select('id, goods_name, balance_time, balance_amount');
            }]);
        $query->andWhere(['or', ['member_id' => $this->member_id], ['higher_id' => $this->member_id]]);
        $query->andWhere(['status' => 2]);
        if ($month) {
            $start_day = date('Y-m-d 00:00:00', strtotime($month . ''));
            $end_day = date('Y-m-d H:i:s', strtotime($month . " +1 month -1 second"));
            $query->andWhere(["between", "order_time", $start_day, $end_day]);
            $commision_query->andWhere(["between", "order_time", $start_day, $end_day]);
        }

        $commision_list = $commision_query->andWhere(['or', ['member_id' => $this->member_id], ['higher_id' => $this->member_id]])
                ->select(new Expression("SUM(CASE member_id WHEN :member_id THEN self_commision ELSE higher_commision END) AS commision, date_format(order_time, '%Y-%m') order_time", [':member_id' => $this->member_id]))
                ->groupBy(new Expression("date_format(order_time, '%Y-%m')"))
                ->asArray()
                ->all();
        $commisionList = array_column($commision_list, NULL, 'order_time');
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select("order_id, order_time, member_id, self_commision, higher_id, higher_commision, is_balance")->offset(($p - 1) * $this->page_size)->limit($this->page_size)->orderBy(['order_time' => SORT_DESC])->asArray()->all();
        $data = [];
        if ($list) {
            foreach ($list as &$_order) {
                $month = date("Y-m", strtotime($_order["order_time"]));
                $data[] = [
                    'goods_name' => $_order['order']["goods_name"],
                    'order_create_time' => $_order['order_time'],
                    'balance_time' => $_order['order']['balance_time'],
                    'order_amount' => $_order['order']['balance_amount'],
                    'commision' => (($_order['member_id'] == $this->member_id) ? $_order['self_commision'] : $_order['higher_commision']),
                    'month' => $month,
                    'total' => $commisionList[$month]['commision'],
                ];
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['list' => $data, 'month' => date('Y-m'), 'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
            ]]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 远程下载微信头像
     * @param type $url
     * @param type $uid
     * @return type
     */
    private static function downloadImage($url, $uid) {
        $position_url = '/web/uploads/avatar/';
        $path = \Yii::$app->basePath . $position_url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $file = curl_exec($ch);
        curl_close($ch);
        return self::saveAsImage($file, $path, $uid);
    }

    /**
     * 保存图片
     * @param type $file
     * @param type $path
     * @return type
     */
    private static function saveAsImage($file, $path, $uid) {
        $filename = 'm_' . $uid . '.jpg';
        $resource = fopen($path . $filename, 'a');
        fwrite($resource, $file);
        fclose($resource);
        return $filename;
    }

}
