<?php

namespace app\modules\api\controllers;

use app\common\components\ApiController;
use app\models\marketing\Hot;
use app\models\marketing\Propaganda;
use app\common\services\UrlService;
use app\common\helpers\Err;
use yii\base\DynamicModel;
use yii\base\Exception;

class MarketController extends ApiController {

    /**
     * 每日爆款
     */
    public function actionHot() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $query = Hot::find();
        $query->with(['image' => function($query) {
                $query->select('hot_id, goods_id, file_key');
            }]);
        $query->andWhere(['status' => 1]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('id,  name, summary, share_count, created_time')
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)->orderBy(['weight' => SORT_DESC, 'created_time' => SORT_DESC])
                ->asArray()
                ->all();

        if ($list) {
            foreach ($list as $key => $k) {
                if (!empty($k['image'])) {
                    foreach ($k['image'] as $m => $n) {
                        $k['image'][$m]['file_key'] = UrlService::buildPicUrl('hot', $n['file_key']);
                    }
                }
                $list[$key] = $k;
            }
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
            'list' => $list,
            'pages' => [
                'total_count' => $total_res_count,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

    /**
     * 宣传素材
     */
    public function actionPropaganda() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $query = Propaganda::find();
        $query->andWhere(['status' => 1]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('id,  name, summary, main_image, share_count, created_time')
                ->offset(($p - 1) * $this->page_size)
                ->limit($this->page_size)->orderBy(['weight' => SORT_DESC, 'created_time' => SORT_DESC])
                ->asArray()
                ->all();
        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = UrlService::buildPicUrl('propaganda', $k['main_image']);
            }
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
            'list' => $list,
            'pages' => [
                'total_count' => $total_res_count,
                'total_page' => $total_page,
                'p' => $p
            ]
        ]);
    }

}
