<?php

namespace app\modules\api\controllers;

use app\models\integral\IntegralCate;
use app\models\integral\IntegralGoods;
use app\models\integral\IntegralOrder;
use app\models\integral\IntegralOrderGoods;
use app\models\member\Member;
use app\models\order\ExpressOrder;
use app\common\components\ApiController;
use app\common\services\UrlService;
use app\common\services\integral\IntegralGoodsService;
use app\common\helpers\Err;
use yii\base\DynamicModel;
use yii\base\Exception;

class IntegralController extends ApiController {

    public function init() {
        $this->config['handle_login'] = ['detail', 'cate', 'list', 'info'];  //todo 小写 按yii路由请求规则 无需加前缀action
        parent::init();
    }

    /**
     * 分类列表
     */
    public function actionCate() {
        $list = IntegralCate::find()
                        ->where(['status' => 1])
                        ->select('id as c_id, name as c_name, main_image')
                        ->orderBy(['weight' => SORT_DESC])
                        ->asArray()->all();
        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = (!empty($k['main_image'])) ? UrlService::buildPicUrl("integral_cate", $k['main_image']) : '';
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['list' => $list]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 商品接口
     */
    public function actionList() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;

        $c_id = intval(post('c_id'));
        $query = IntegralGoods::find();
        if ($c_id) {
            $query->andWhere(['cate_id' => $c_id]);
        }

        $query->andWhere(['status' => 1]);

        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);

        $list = $query->select('id as g_id, goods_name, price, stock, main_image')->offset(($p - 1) * $this->page_size)
                        ->limit($this->page_size)->orderBy(['created_time' => SORT_DESC])->asArray()->all();
        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = (!empty($k['main_image'])) ? UrlService::buildPicUrl("integral", $k['main_image']) : '';
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
                'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
                ]
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 获取积分商品详情
     */
    public function actionInfo() {
        if (!verify_request_sign(['goods_id'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $goods_id = intval(post('goods_id'));
        //参数验证
        $compact = ['goods_id'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['goods_id'], 'required'],
                    [['goods_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        $info = IntegralGoods::find()->where(['status' => 1, 'id' => $goods_id])->select('id as goods_id, goods_name, price, pic, stock')->asArray()->one();
        if ($info) {
            $pic_list = json_decode($info['pic'], TRUE);
            foreach ($pic_list as $key => $k) {
                $pic_list[$key] = UrlService::buildPicUrl("integral", $k);
            }
            $info['pic'] = $pic_list;
            $info['url'] = UrlService::buildApiUrl("/integral/detail", ['id' => $info['goods_id']]);
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['info' => $info]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 商品详情
     * @return type
     */
    public function actionDetail() {
        $goods_id = intval(get('id'));
        $info = IntegralGoods::find()->where(['status' => 1, 'id' => $goods_id])->select('summary')->asArray()->one();
        return $this->renderPartial("detail", ["info" => $info]);
    }

    /**
     * 积分兑换
     */
    public function actionExchange() {
        if (!verify_request_sign(['token', 'goods_id', 'num', 'consignee', 'consignee_tel', 'province_id', 'province', 'city_id', 'city', 'district_id', 'district', 'address'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $goods_id = intval(post('goods_id'));
        $num = intval(post('num'));
        $consignee = trim(post('consignee'));
        $consignee_tel = trim(post('consignee_tel'));
        $province_id = intval(post('province_id'));
        $province = trim(post('province'));
        $city_id = intval(post('city_id'));
        $city = trim(post('city'));
        $district_id = intval(post('district_id'));
        $district = trim(post('district'));
        $address = trim(post('address'));
        //参数验证
        $compact = ['goods_id', 'num', 'consignee', 'consignee_tel', 'province_id', 'province', 'city_id', 'city', 'district_id', 'district', 'address'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['goods_id', 'num', 'consignee', 'consignee_tel', 'province_id', 'province', 'city_id', 'city', 'district_id', 'district', 'address'], 'required'],
                    [['goods_id', 'num', 'province_id', 'city_id', 'district_id'], 'integer'],
                    [['consignee', 'consignee_tel', 'province', 'city', 'district', 'address' ], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }

        $goods_info = IntegralGoods::find()->where(['status' => 1, 'id' => $goods_id])->one();
        if ($goods_info) {
            if (($goods_info['stock'] - $num) < 0) {
                $this->setApiResult(Err::ERR_STOCK_NOT_ENOUGH, Err::getMessage(Err::ERR_STOCK_NOT_ENOUGH));
            }

            $member_info = Member::findOne($this->member_id);
            if (($member_info['score'] - $num * $goods_info['price']) < 0) {
                $this->setApiResult(Err::ERR_ORDER_FAIL, "积分不足");
            }

            $connection = IntegralOrder::getDb();
            $transaction = $connection->beginTransaction();
            try {
                $total_price = $goods_info['price'] * $num;
                //创建主订单
                $order_model = new IntegralOrder();
                $order_model->member_id = $this->member_id;
                $order_model->total_price = $total_price;

                if (!$order_model->save(0)) {
                    $transaction->rollBack();
                    $this->setApiResult(Err::ERR_ORDER_FAIL, Err::getMessage(Err::ERR_ORDER_FAIL));
                }

                //创建订单商品记录
                $order_goods_model = new IntegralOrderGoods();
                $order_goods_model->order_id = $order_model->id;
                $order_goods_model->goods_id = $goods_info['id'];
                $order_goods_model->goods_name = $goods_info['goods_name'];
                $order_goods_model->price = $goods_info['price'];
                $order_goods_model->num = $num;

                if (!$order_goods_model->save(0)) {
                    $transaction->rollBack();
                    $this->setApiResult(Err::ERR_ORDER_FAIL, Err::getMessage(Err::ERR_ORDER_FAIL));
                }
                //库存变化
                if (!IntegralGoods::updateAll(['stock' => new \yii\db\Expression("`stock` - " . $num)], ['id' => $goods_info['id']])) {
                    $transaction->rollBack();
                    $this->setApiResult(Err::ERR_ORDER_FAIL, Err::getMessage(Err::ERR_ORDER_FAIL));
                }

                //积分商品库存变化
                IntegralGoodsService::setStockChangeLog($goods_info['id'], -$num, "在线兑换");
                
                $express_order_model = new ExpressOrder();
                $express_order_model->type = 2;
                $express_order_model->order_id = $order_model->id;
                $express_order_model->consignee = $consignee;
                $express_order_model->consignee_tel = $consignee_tel;
                $express_order_model->province_id = $province_id;
                $express_order_model->province = $province;
                $express_order_model->city_id = $city_id;
                $express_order_model->city = $city;
                $express_order_model->district_id = $district_id;
                $express_order_model->district = $district;
                $express_order_model->address = $address;
                $express_order_model->status = IntegralOrder::STATUS_WAIT_SEND;
                $express_order_model->save(0);

                if (!Member::updateAll(['score' => new \yii\db\Expression("`score` - " . $total_price)], ['id' => $this->member_id])) {
                    $transaction->rollBack();
                    $this->setApiResult(Err::ERR_ORDER_FAIL, "积分扣除失败");
                } else {
                    $transaction->commit();
                    $this->setApiResult(Err::SUCCESS, "兑换成功");
                }
            } catch (Exception $e) {
                $transaction->rollBack();
                $this->setApiResult(Err::ERR_ORDER_FAIL, Err::getMessage(Err::ERR_ORDER_FAIL));
            }
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

}
