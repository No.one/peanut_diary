<?php

namespace app\modules\api\controllers;

use Yii;
use app\common\services\UtilService;
use app\common\components\ApiController;
use app\common\helpers\Err;
use app\models\member\Member;
use app\models\member\ThreeLogin;
use app\models\member\MemberInvite;
use abei2017\wx\Application;

class WechatController extends ApiController {

    public function init() {
        $this->config['handle_login'] = ['index', 'oauth', 'test'];  //todo 小写 按yii路由请求规则 无需加前缀action
        parent::init();
    }

    public function actionIndex() {
        $this->checkOauth();
        $wechat_info = Yii::$app->session->get('wx_user');
        $check = Member::findOne(['wechat'=>$wechat_info['unionid']]);
        if ($check == false) {
            //生成新会员
            $invite_info = Member::checkInviteCode(Yii::$app->request->get('invite_code'));
            if ($invite_info) {
                $model_member = new Member;
                $model_member->grade_id = 0;
                $model_member->wechat = $wechat_info['unionid'];
                $model_member->insert();
                MemberInvite::createInvite($model_member->id, $invite_info->id);
                header('location:' . Yii::$app->params['app_download']);
                exit();
            } else {
                $this->setApiResult(Err::ERR_PARAMS_ERROR, "请填扫合法的二维码");
            }
        } else {
            header('location:' . Yii::$app->params['app_download']);
            exit();
        }
    }

    /**
     * 检查权限
     */
    private function checkOauth() {
        $conf = Yii::$app->params['wx']['mp'];
        $url = Yii::$app->request->hostInfo . Yii::$app->request->getUrl();
        $callback = Yii::$app->urlManager->createAbsoluteUrl(['/api/wechat/oauth', 'url' => urlencode($url)]);
        $conf['oauth']['callback'] = $callback;
        $app = new Application(['conf' => $conf]);
        $oauth = $app->driver('mp.oauth');
        $wxLoginUser = Yii::$app->session->get('wx_user');
        if ($wxLoginUser == null) {
            $oauth->send();
            die();
        }
    }

    /**
     * 权限跳转
     */
    public function actionOauth() {
        $url = Yii::$app->request->get('url');
        $conf = Yii::$app->params['wx']['mp'];
        $app = new Application(['conf' => $conf]);
        $oauth = $app->driver('mp.oauth');
        $user = $oauth->user();
        Yii::$app->session->set('wx_user', $user);
        header('location:' . urldecode($url));
        exit();
    }

}
