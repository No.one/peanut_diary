<?php
namespace app\modules\api\controllers;

use app\common\components\ApiController;
use app\common\helpers\Err;
use yii\base\DynamicModel;
use app\models\sms\SmsCaptcha;
use app\models\member\Member;


class SmsController extends ApiController{
    public function init()
    {
        $this->config['handle_login'] = ['register','login', 'change-password'];  //todo 小写 按yii路由请求规则 无需加前缀action
        parent::init();
    }
    
    /**
     * 注册短信发送
     */
    public function actionRegister(){
        if(!verify_request_sign(['mobile'])){
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
       
        //参数验证
        $compact   = ['mobile'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['mobile'], 'required'],
            [['mobile'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check = Member::findOne(['mobile'=>$mobile]);
        if($check){
            $this->setApiResult(Err::ERR_DATA_EXIST, "手机号码已存在，请直接登录");
        }
      
        $log = SmsCaptcha::find()
                ->where(['mobile'=>$mobile, 'event'=>'register'])->orderBy('id desc')->one();
        if($log){
            if(strtotime($log['expires_at']) >=time()){
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "短时间内请勿重复发送");
            }else{
                $log->delete();
                $sms = new SmsCaptcha();
                $result = $sms->Captcha($mobile, 'register');
                if($result){
                    $this->setApiResult(Err::SUCCESS, "发送成功");
                }else{
                    $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
                }
            }
        }else{
            $sms = new SmsCaptcha();
            $result = $sms->Captcha($mobile, 'register');
            if($result){
                $this->setApiResult(Err::SUCCESS, "发送成功");
            }else{
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
            }
        }
    }
    
    /**
     * 登录短信
     */
    public function actionLogin(){
        if(!verify_request_sign(['mobile'])){
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        //参数验证
        $compact   = ['mobile'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['mobile'], 'required'],
            [['mobile'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check = Member::findOne(['mobile'=>$mobile]);
        if(!$check){
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, "手机号码不存在，请注册");
        }
        $log = SmsCaptcha::find()
                ->where(['mobile'=>$mobile, 'event'=>'login'])->orderBy('id desc')->one();
        if($log){
//            if(strtotime($log['expires_at']) >=time()){
//                $this->setApiResult(Err::ERR_OPERATE_FAIL, "短时间内请勿重复发送");
//            }else{
                $log->delete();
                $sms = new SmsCaptcha();
                $result = $sms->Captcha($mobile, 'login');
                if($result){
                    $this->setApiResult(Err::SUCCESS, "发送成功");
                }else{
                    $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
                }
//            }
        }else{
            $sms = new SmsCaptcha();
            $result = $sms->Captcha($mobile, 'login');
            if($result){
                $this->setApiResult(Err::SUCCESS, "发送成功");
            }else{
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
            }
        }
    }
    
    /**
     * 修改短信/忘记密码
     */
    public function actionChangePassword(){
        if(!verify_request_sign(['mobile'])){
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        //参数验证
        $compact   = ['mobile'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['mobile'], 'required'],
            [['mobile'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check = Member::findOne(['mobile'=>$mobile]);
        if(!$check){
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, "手机号码不存在，请注册");
        }
        $log = SmsCaptcha::find()
                ->where(['mobile'=>$mobile, 'event'=>'change_password'])->orderBy('id desc')->one();
        if($log){
            if(strtotime($log['expires_at']) >=time()){
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "短时间内请勿重复发送");
            }else{
                $log->delete();
                $sms = new SmsCaptcha();
                $result = $sms->Captcha($mobile, 'change_password');
                if($result){
                    $this->setApiResult(Err::SUCCESS, "发送成功");
                }else{
                    $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
                }
            }
        }else{
            $sms = new SmsCaptcha();
            $result = $sms->Captcha($mobile, 'change_password');
            if($result){
                $this->setApiResult(Err::SUCCESS, "发送成功");
            }else{
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
            }
        }
    }
    
    /**
     * 验证旧手机号码
     */
    public function actionValidateOldMobile(){
        if(!verify_request_sign(['mobile', 'token'])){
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        //参数验证
        $compact   = ['mobile'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['mobile'], 'required'],
            [['mobile'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        
        $check = Member::findOne(['id'=>$this->member_id, 'mobile'=>$mobile]);
        if(!$check){
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, "手机号码不存在，请重试");
        }
        $log = SmsCaptcha::find()
                ->where(['mobile'=>$mobile, 'event'=>'validate_old_mobile'])->orderBy('id desc')->one();
        if($log){
            if(strtotime($log['expires_at']) >=time()){
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "短时间内请勿重复发送");
            }else{
                $log->delete();
                $sms = new SmsCaptcha();
                $result = $sms->Captcha($mobile, 'validate_old_mobile');
                if($result){
                    $this->setApiResult(Err::SUCCESS, "发送成功");
                }else{
                    $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
                }
            }
        }else{
            $sms = new SmsCaptcha();
            $result = $sms->Captcha($mobile, 'validate_old_mobile');
            if($result){
                $this->setApiResult(Err::SUCCESS, "发送成功");
            }else{
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
            }
        }
    }
    
    /**
     * 验证新手机
     */
    public function actionValidateNewMobile(){
        if(!verify_request_sign(['mobile', 'token'])){
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        //参数验证
        $compact   = ['mobile'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['mobile'], 'required'],
            [['mobile'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        
        $check = Member::findOne(['mobile'=>$mobile]);
        if($check){
            $this->setApiResult(Err::ERR_DATA_EXIST, "手机号码已占用，请更换新的号码");
        }
        $log = SmsCaptcha::find()
                ->where(['mobile'=>$mobile, 'event'=>'validate_new_mobile'])->orderBy('id desc')->one();
        if($log){
            if(strtotime($log['expires_at']) >=time()){
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "短时间内请勿重复发送");
            }else{
                $log->delete();
                $sms = new SmsCaptcha();
                $result = $sms->Captcha($mobile, 'validate_new_mobile');
                if($result){
                    $this->setApiResult(Err::SUCCESS, "发送成功");
                }else{
                    $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
                }
            }
        }else{
            $sms = new SmsCaptcha();
            $result = $sms->Captcha($mobile, 'validate_new_mobile');
            if($result){
                $this->setApiResult(Err::SUCCESS, "发送成功");
            }else{
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
            }
        }
    }
    
    /**
     * 绑定支付宝
     */
    public function actionBindAlipay(){
        if(!verify_request_sign(['mobile', 'token'])){
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $mobile = post('mobile');
        //参数验证
        $compact   = ['mobile'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['mobile'], 'required'],
            [['mobile'], 'string'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        
        $check = Member::findOne(['id'=>$this->member_id, 'mobile'=>$mobile]);
        if(!$check){
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, "手机号码不存在，请重试");
        }
        $log = SmsCaptcha::find()
                ->where(['mobile'=>$mobile, 'event'=>'bind_alipay'])->orderBy('id desc')->one();
        if($log){
            if(strtotime($log['expires_at']) >=time()){
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "短时间内请勿重复发送");
            }else{
                $log->delete();
                $sms = new SmsCaptcha();
                $result = $sms->Captcha($mobile, 'bind_alipay');
                if($result){
                    $this->setApiResult(Err::SUCCESS, "发送成功");
                }else{
                    $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
                }
            }
        }else{
            $sms = new SmsCaptcha();
            $result = $sms->Captcha($mobile, 'bind_alipay');
            if($result){
                $this->setApiResult(Err::SUCCESS, "发送成功");
            }else{
                $this->setApiResult(Err::ERR_OPERATE_FAIL, "发送失败");
            }
        }
    }
    
    
}

