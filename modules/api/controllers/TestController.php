<?php

namespace app\modules\api\controllers;

use Yii;
use app\common\services\UrlService;
use yii\web\Controller;
use TbkUatmFavoritesGetRequest;
use TbkUatmFavoritesItemGetRequest;
use TopClient;
use app\common\services\TaoBaoService;
use app\models\goods\Channels;
use app\models\goods\GoodsCate;
use app\models\goods\Goods;
use app\models\member\Pid;
use dosamigos\qrcode\QrCode;
use app\models\member\Member;
use app\common\handler\MemberHandler;
use app\common\handler\SendMailHandler;
use app\common\services\email\EmailService;
use ETaobao\Factory;

class TestController extends Controller {

    public $enableCsrfValidation = false;

    public function actionTest() {
        $logo = Yii::$app->basePath . '/web' . UrlService::getPicUrl('avatar', 'm_56.png');
        $QR = 'baidu.png'; //已经生成的原始二维码图 
        QrCode::png("http://www.baidu.com", $QR);
        if ($logo !== FALSE) {
            $QR = imagecreatefromstring(file_get_contents($QR));
            $logo = imagecreatefromstring(file_get_contents($logo));
            $QR_width = imagesx($QR); //二维码图片宽度 
            $QR_height = imagesy($QR); //二维码图片高度 
            $logo_width = imagesx($logo); //logo图片宽度 
            $logo_height = imagesy($logo); //logo图片高度 
            $logo_qr_width = $QR_width / 3;
            $scale = $logo_width / $logo_qr_width;
            $logo_qr_height = $logo_height / $scale;
            $from_width = ($QR_width - $logo_qr_width) / 2;
            //重新组合图片并调整大小 
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        }
        imagepng($QR, 'helloweba.png');

//         return $this->renderPartial("test", ['url'=>  UrlService::buildApiUrl('/test/qrcode')]);
    }

    public function actionIndex() {
       $cache = \Yii::$app->cache;
       $result = $cache->get('memberGrade');
       print_r($result);
    }

    public function actionChangeGoods() {
//        mm_132927835_46626979_15902600067
//        mm_132927835_46626979_16476750288
        $url = "http://193.112.121.99/xiaocao/hightapi.action";
        $post_data = [
            "token" => "70002100813678567ed6b7a5a17c1e943b0ee3c91434b5d5907ca6af3259d3b4d731fdf2260577283",
            "item_id" => "564459678367",
            "adzone_id" => "16476750288",
            "site_id" => "46626979",
            "qq" => "2116177952",
        ];
        $res = doPost($url, $post_data, NULL);
        echo "<pre>";
        print_r($res);
    }

    public function actionQueue() {
        \Yii::$app->queue->pushOn(new SendMailHandler(), ['event' => 'notice', 'data' => ['email' => '1252138037@qq.com', 'event'=>'notice', 'subject'=>'系统通知', 'msg'=>"淘宝推广pid不足10个，请尽快添加"]], 'email');
//        \Yii::$app->queue->pushOn(new MemberHandler(), ['event' => 'pid', 'data' => ['member_id' => 22]], 'pid');
    }

}
