<?php

namespace app\modules\api\controllers;

use app\common\components\ApiController;
use app\models\shop\ShopCate;
use app\models\shop\Shop;
use app\models\shop\ShopGoods;
use app\models\shop\ShopCollect;
use app\models\goods\Goods;
use app\common\services\UrlService;
use app\common\helpers\Err;
use yii\base\DynamicModel;
use yii\base\Exception;

class ShopController extends ApiController {

    public function init() {
        $this->config['handle_login'] = ['detail', 'cate', 'list'];  //todo 小写 按yii路由请求规则 无需加前缀action
        parent::init();
    }
    
    /**
     * 店铺分类
     */
    public function actionCate() {
        $list = ShopCate::find()
                        ->where(['status' => 1])
                        ->select('id as c_id, name as c_name, main_image')
                        ->orderBy(['weight' => SORT_DESC])
                        ->asArray()->all();
        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = (!empty($k['main_image'])) ? UrlService::buildPicUrl("shop_cate", $k['main_image']) : '';
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['list' => $list]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 店铺列表
     */
    public function actionList() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;

        $c_id = intval(post('c_id'));
        $query = Shop::find();
        if ($c_id) {
            $query->andWhere(['cate_id' => $c_id]);
        }

        $query->andWhere(['status' => 1]);
        $query->with(['goods'=>  function($query){
            $query->with(['goods'=>function($query){
                $query->select('id, name, cover, price, coupon_price');
            }])->select('shop_id, goods_id');
        }]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
      
        $list = $query->select('id, name, main_image')->offset(($p - 1) * $this->page_size)
                        ->limit($this->page_size)->orderBy(['weight'=> SORT_ASC ,'created_time' => SORT_DESC])->asArray()->all();
        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = (!empty($k['main_image'])) ? UrlService::buildPicUrl("shop_logo", $k['main_image']) : '';
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
                'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
                ]
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }
    
    /**
     * 店铺详情
     */
    public function actionInfo(){
        $shop_id = intval(post('shop_id'));
        $info = Shop::find()->andWhere(['status'=>1, 'id'=>$shop_id])->select('id, name, main_image, pic')->asArray()->one();
        if($info){
            $check_collect = [];
            if($this->member_id > 0){
                $check_collect = ShopCollect::findOne(['member_id' => $this->member_id, 'shop_id' => $info['id']]);
            }
            $info['main_image'] = UrlService::buildPicUrl('shop_logo', $info['main_image']);
            $info['pic'] = UrlService::buildPicUrl('shop_banner', json_decode($info['pic'], TRUE)[0]);
            $info['is_collect'] = (!empty($check_collect)) ? 1:0;
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['info' => $info]);
        }else{
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 店铺关联商品
     */
    public function actionGoods(){
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $shop_id = intval(post('shop_id'));
        $is_coupon = intval(post('is_coupon', 0));
        $orderBy = intval(post('order_by'));
        $sortBy = intval(post('sort_by', 1));
        $sort = $sortBy > 1 ? SORT_DESC : SORT_ASC;
        
        switch ($orderBy) {
            case 1;
                $order = [Goods::tableName().'.coupon_price' => $sort];
                break;

            case 2:
                $order = [Goods::tableName().'.volume' => $sort];
                break;

            case -1:
            default :
                $order = [Goods::tableName().'.created_time' => $sort];
                break;
        }
     
        $query = ShopGoods::find()->alias('sg');
        $query->joinWith(['goods'=>function($query) use($is_coupon,$order) {
            if ($is_coupon && $is_coupon > 0) {
                $query->andWhere(['coupon_status' => 1]);
                $query->andWhere(['<', 'coupon_start_time', date('Y-m-d H:i:s', time())]);
                $query->andWhere(['>', 'coupon_end_time', date('Y-m-d H:i:s', time())]);
            }
            $query->andWhere(['status'=>1])->select('id, original_id, name as goods_name, user_type, price, tk_rate, cover, volume, coupon_amount, coupon_price, coupon_info, coupon_start_time, coupon_end_time');
            $query->orderBy($order);
        }], TRUE, 'INNER JOIN');
        $query->andWhere(['shop_id'=>$shop_id]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('sg.id, sg.goods_id')
                        ->offset(($p - 1) * $this->page_size)
                        ->limit($this->page_size)
                        ->asArray()
                        ->all();
        if(!empty($this->current_user)){
            $grade_list = \Yii::$app->cache->get('memberGrade');
            $member_ratio = $grade_list[$this->current_user['grade_id']]['ratio']; 
        }else{
            $member_ratio = 0.5;
        }
        if ($list) {
            foreach ($list as $key => $k) {
                if ((strtotime($k['goods']['coupon_start_time']) < time()) && (strtotime($k['goods']['coupon_end_time']) > time())) {
                    if($member_ratio>0){
                        $list[$key]['goods']['commision'] = round((!empty($k['goods']['coupon_price']) ? $k['goods']['coupon_price'] : $k['goods']['price']) * $k['goods']['tk_rate'] * $member_ratio / 100, 2);                   
                    }else{
                        $list[$key]['goods']['commision'] = '';
                    }
                } else {
                    if($member_ratio>0){
                        $list[$key]['goods']['commision'] = round($k['goods']['price'] * $k['goods']['tk_rate'] * $member_ratio / 100, 2);
                    }else{
                        $list[$key]['goods']['commision'] = '';
                    }
                    $list[$key]['goods']['coupon_amount'] = 0;
                    $list[$key]['goods']['coupon_price'] = '';
                    $list[$key]['goods']['coupon_info'] = '';
                    $list[$key]['goods']['coupon_start_time'] = '';
                    $list[$key]['goods']['coupon_end_time'] = '';
                }
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
                'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
                ]
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }


    /**
     * 店铺收藏
     */
    public function actionCollect() {
        if (!verify_request_sign(['shop_id'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $shop_id = intval(post('shop_id'));
        //参数验证
        $compact = ['shop_id'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['shop_id'], 'required'],
            [['shop_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check_shop = Shop::findOne(['id' => $shop_id, 'status' => 1]);
        if (!$check_shop) {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
        $check_collect = ShopCollect::findOne(['member_id' => $this->member_id, 'shop_id' => $shop_id]);
        if (!$check_collect) {
            $model = new ShopCollect();
            $model->member_id = $this->member_id;
            $model->shop_id = $shop_id;
            $model->created_time = date('Y-m-d H:i:s');
            $model->save(0);
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
    }

    /**
     * 取消收藏
     */
    public function actionCannelCollect() {
        if (!verify_request_sign(['shop_id'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $shop_id = intval(post('shop_id'));
        //参数验证
        $compact = ['shop_id'];
        $validator = DynamicModel::validateData(compact($compact), [
            [['shop_id'], 'required'],
            [['shop_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check_shop = Shop::findOne(['id' => $shop_id, 'status' => 1]);
        if (!$check_shop) {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
        $check_collect = ShopCollect::findOne(['member_id' => $this->member_id, 'shop_id' => $shop_id]);
        if ($check_collect) {
            ShopCollect::deleteAll(['member_id' => $this->member_id, 'shop_id' => $shop_id]);
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
    }

    /**
     * 收藏店铺列表
     */
    public function actionCollectList() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $query = Shop::find();
        $query->alias('s')
            ->innerJoin(ShopCollect::tableName().' sc', 's.id=sc.shop_id');
        $query->andWhere(['s.status' => 1]);
        $query->andWhere(['sc.member_id' => $this->member_id]);
        $query->with(['goods'=>  function($query){
            $query->with(['goods'=>function($query){
                $query->select('id, name, cover, price, coupon_price');
            }])->select('shop_id, goods_id');
        }]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('s.id, s.name, s.main_image, s.pic')
            ->offset(($p - 1) * $this->page_size)
            ->limit($this->page_size)
            ->orderBy(["sc.created_time" => SORT_DESC])
            ->asArray()->all();

        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = (!empty($k['main_image'])) ? UrlService::buildPicUrl("shop_logo", $k['main_image']) : '';
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
                'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
                ]
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }
}
