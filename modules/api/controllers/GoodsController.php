<?php

namespace app\modules\api\controllers;

use app\models\goods\Goods;
use app\models\goods\GoodsCate;
use app\models\goods\GoodsCollect;
use app\models\member\MemberToken;
use app\common\services\TaoBaoService;
use app\common\components\ApiController;
use app\common\helpers\Err;
use app\common\services\UrlService;
use yii\base\DynamicModel;
use yii\base\Exception;

class GoodsController extends ApiController {

    public function init() {
        $this->config['handle_login'] = ['cate', 'list', 'info'];  //todo 小写 按yii路由请求规则 无需加前缀action
        parent::init();
    }

    /**
     * 分类列表
     */
    public function actionCate() {
        $list = GoodsCate::find()
                        ->where(['status' => 1])
                        ->select('id as c_id, name as c_name, main_image')
                        ->orderBy(['weight' => SORT_DESC])
                        ->asArray()->all();
        if ($list) {
            foreach ($list as $key => $k) {
                $list[$key]['main_image'] = (!empty($k['main_image'])) ? UrlService::buildPicUrl("goods_cate", $k['main_image']) : '';
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['list' => $list]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 商品接口
     */
    public function actionList() {
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $searchKey = trim(post("keywords", ""));
        $c_id = intval(post('c_id'));
        $is_coupon = intval(post('is_coupon'));
        $query = Goods::find();
        $orderBy = intval(post('order_by'));
        $sortBy = intval(post('sort_by', 1));
        $sort = $sortBy > 1 ? SORT_DESC : SORT_ASC;
        switch ($orderBy) {
            case 1;
                $order = ['coupon_price' => $sort];
                break;

            case 2:
                $order = ['volume' => $sort];
                break;

            case -1:
            default :
                $order = ['created_time' => $sort];
                break;
        }
        if ($c_id) {
            $query->andWhere(['category_id' => $c_id]);
        }
        if (!empty($searchKey)) {
            $query->andWhere(['like', 'name', $searchKey]);
        }

        if ($is_coupon && $is_coupon > 0) {
            $query->andWhere(['coupon_status' => 1]);
            $query->andWhere(['<', 'coupon_start_time', date('Y-m-d H:i:s', time())]);
            $query->andWhere(['>', 'coupon_end_time', date('Y-m-d H:i:s', time())]);
        }
        $query->andWhere(['status' => 1]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('id as g_id, original_id, name as goods_name, user_type, price, tk_rate, cover, volume, coupon_amount, coupon_price, coupon_info, coupon_start_time, coupon_end_time')
                        ->offset(($p - 1) * $this->page_size)
                        ->limit($this->page_size)->orderBy($order)
                        ->asArray()
                        ->all();
        if(!empty($this->current_user)){
            $grade_list = \Yii::$app->cache->get('memberGrade');
            $member_ratio = $grade_list[$this->current_user['grade_id']]['ratio']; 
        }else{
            $member_ratio = 0.5;
        }
        
        if ($list) {
            foreach ($list as $key => $k) {
                if ((strtotime($k['coupon_start_time']) < time()) && (strtotime($k['coupon_end_time']) > time())) {
                    if($member_ratio>0){
                        $list[$key]['commision'] = round((!empty($k['coupon_price']) ? $k['coupon_price'] : $k['price']) * $k['tk_rate'] * $member_ratio / 100, 2);
                    }else{
                        $list[$key]['commision'] = '';
                    }
                } else {
                    if($member_ratio>0){
                        $list[$key]['commision'] = round($k['price'] * $k['tk_rate'] * $member_ratio / 100, 2);
                    }else{
                        $list[$key]['commision'] = '';
                    }
                    $list[$key]['coupon_amount'] = 0;
                    $list[$key]['coupon_price'] = '';
                    $list[$key]['coupon_info'] = '';
                    $list[$key]['coupon_start_time'] = '';
                    $list[$key]['coupon_end_time'] = '';
                }
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
                'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
                ]
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 商品详情
     */
    public function actionInfo() {
        if (!verify_request_sign(['goods_id'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $goods_id = intval(post('goods_id'));
        $token = trim(post('token'));
        //参数验证
        $compact = ['goods_id'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['goods_id'], 'required'],
                    [['goods_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        if(!empty($this->current_user)){
            $grade_list = \Yii::$app->cache->get('memberGrade');
            $member_ratio = $grade_list[$this->current_user['grade_id']]['ratio']; 
        }else{
            $member_ratio = 0.50;
        }
        $info = Goods::find()
                        ->where(['status' => 1, 'id' => $goods_id])
                        ->select('id as goods_id, name, category_id, original_id, user_type, price, pictures, click_url, volume, tk_rate, coupon_amount, coupon_price, coupon_info, coupon_start_time, coupon_end_time')
                        ->asArray()->one();
        if ($info) {
            if ((strtotime($info['coupon_start_time']) < time()) && (strtotime($info['coupon_end_time']) > time())) {
                if($member_ratio>0){
                    $info['commision'] = round((!empty($info['coupon_price']) ? $info['coupon_price'] : $info['price']) * $info['tk_rate'] * $member_ratio / 100, 2);
                }else{
                    $info['commision'] = '';
                }
            } else {
                if($member_ratio>0){
                    $info['commision'] = round($info['price'] * $info['tk_rate'] * $member_ratio / 100, 2);
                }else{
                    $info['commision'] = ''; 
                }
                $info['coupon_amount'] = 0;
                $info['coupon_price'] = '';
                $info['coupon_info'] = '';
                $info['coupon_start_time'] = '';
                $info['coupon_end_time'] = '';
            }
            $taobao = new TaoBaoService();
            $info['pictures'] = json_decode($info['pictures'], TRUE);
            $info['desc_img'] = $taobao->getItemDetail($info['original_id']);
            $info['is_collect'] = 0;
            if ($token) {
                $check_collect = MemberToken::find()
                        ->alias('t')
                        ->innerJoin('{{%goods_collect}} gc', 't.member_id=gc.member_id')
                        ->where(['t.token' => $token])
                        ->asArray()
                        ->one();
                if ($check_collect) {
                    $info['is_collect'] = 1;
                }
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['info' => $info]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 淘宝商品收藏
     */
    public function actionCollect() {
        if (!verify_request_sign(['goods_id', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $goods_id = intval(post('goods_id'));
        //参数验证
        $compact = ['goods_id'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['goods_id'], 'required'],
                    [['goods_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check_goods = Goods::findOne(['id' => $goods_id, 'status' => 1]);
        if (!$check_goods) {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
        $check_collect = GoodsCollect::findOne(['member_id' => $this->member_id, 'goods_id' => $goods_id]);
        if (!$check_collect) {
            $model = new GoodsCollect();
            $model->member_id = $this->member_id;
            $model->goods_id = $goods_id;
            $model->created_time = date('Y-m-d H:i:s');
            $model->save(0);
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
    }

    /**
     * 取消收藏
     */
    public function actionCannelCollect() {
        if (!verify_request_sign(['goods_id', 'token'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $goods_id = intval(post('goods_id'));
        //参数验证
        $compact = ['goods_id'];
        $validator = DynamicModel::validateData(compact($compact), [
                    [['goods_id'], 'required'],
                    [['goods_id'], 'integer'],
        ]);
        if ($validator->hasErrors()) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, $this->getErrMessage($validator->getFirstErrors()));
        }
        $check_goods = Goods::findOne(['id' => $goods_id, 'status' => 1]);
        if (!$check_goods) {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
        $check_collect = GoodsCollect::findOne(['member_id' => $this->member_id, 'goods_id' => $goods_id]);
        if ($check_collect) {
            GoodsCollect::deleteAll(['member_id' => $this->member_id, 'goods_id' => $goods_id]);
        }
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS));
    }

    /**
     * 收藏商品列表
     */
    public function actionCollectList() {
        if (!verify_request_sign(['token', 'p'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $p = intval(post('p'));
        $p = ($p > 0) ? $p : 1;
        $query = Goods::find();
        $query->alias('g')
                ->innerJoin('{{%goods_collect}} gc', 'g.id=gc.goods_id');
        $query->andWhere(['g.status' => 1]);
        $query->andWhere(['gc.member_id' => $this->member_id]);
        $total_res_count = $query->count();
        $total_page = ceil($total_res_count / $this->page_size);
        $list = $query->select('g.id as g_id, g.original_id, g.name as goods_name, g.user_type, g.price, g.tk_rate, g.cover, g.volume, g.coupon_price, g.coupon_info, g.coupon_start_time, g.coupon_end_time')
                        ->offset(($p - 1) * $this->page_size)
                        ->limit($this->page_size)
                        ->orderBy(["gc.created_time" => SORT_DESC])
                        ->asArray()->all();
        if ($list) {
            foreach ($list as $key => $k) {
                if ((strtotime($k['coupon_start_time']) < time()) && (strtotime($k['coupon_end_time']) > time())) {
                    $list[$key]['rate'] = round($k['coupon_price'] * $k['tk_rate'] / 100, 2);
                } else {
                    $list[$key]['rate'] = round($k['price'] * $k['tk_rate'] / 100, 2);
                    $list[$key]['coupon_price'] = '';
                    $list[$key]['coupon_info'] = '';
                    $list[$key]['coupon_start_time'] = '';
                    $list[$key]['coupon_end_time'] = '';
                }
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
                'pages' => [
                    'total_count' => $total_res_count,
                    'total_page' => $total_page,
                    'p' => $p
                ]
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

    /**
     * 获取商品折扣
     */
    public function actionDiscount() {
        if (!verify_request_sign(['token', 'original_id'])) {
            $this->setApiResult(Err::ERR_PARAMS_ERROR, Err::getMessage(Err::ERR_PARAMS_ERROR));
        }
        $item_id = trim(post("original_id"));
        $tb_pid_array = explode('_', $this->current_user['tb_pid']);
        $taobao_service = new TaoBaoService();
        $data = $taobao_service->getItemDiscount($item_id, $tb_pid_array[3], $tb_pid_array[2]);
        if (!isset($data['coupon_info'])) {
            $e_arr = explode("e=", $data['coupon_click_url']);
            $res = $taobao_service->getItemUrl($e_arr[1]);
            $jump_url = $res['item']['clickUrl'];
        } else {
            $jump_url = $data['coupon_click_url'];
        }
        if (!checkUrl($jump_url)) {
            $jump_url = "https:" . $jump_url;
        }
        $goods = Goods::findOne(['original_id' => $item_id]);
        $tpwd = $taobao_service->tpwd($goods['name'], $jump_url);
        $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), ['jump_url' => $jump_url, 'tpwd' => $tpwd]);
    }

    /**
     * 猜你喜欢
     */
    public function actionLikeGuess() {
        $category_id = intval(post('category_id'));
        $field = "id as g_id, category_id, original_id, name as goods_name, user_type, price, tk_rate, cover, volume, coupon_amount, coupon_price, coupon_info, coupon_start_time, coupon_end_time";
        if ($category_id > 0) {
            $sql = "SELECT " . $field . " FROM m_goods WHERE id >= ("
                    . "(SELECT MAX(id) FROM m_goods WHERE category_id = $category_id)-(SELECT MIN(id) FROM m_goods  WHERE category_id = $category_id)) * RAND() + (SELECT MIN(id) FROM m_goods  WHERE category_id = $category_id) and category_id = $category_id limit 6";
        } else {
            $sql = "SELECT " . $field . " FROM m_goods WHERE id >= ((SELECT MAX(id) FROM m_goods)-(SELECT MIN(id) FROM m_goods)) * RAND() + (SELECT MIN(id) FROM m_goods) limit 6";
        }
        if(!empty($this->current_user)){
            $grade_list = \Yii::$app->cache->get('memberGrade');
            $member_ratio = $grade_list[$this->current_user['grade_id']]['ratio']; 
        }else{
            $member_ratio = 0.5;
        }
        $list = \Yii::$app->getDb()->createCommand($sql)->queryAll();
        if ($list) {
            foreach ($list as $key => $k) {
                if ((strtotime($k['coupon_start_time']) < time()) && (strtotime($k['coupon_end_time']) > time())) {
                    if($member_ratio>0){
                        $list[$key]['commision'] = round((!empty($k['coupon_price']) ? $k['coupon_price'] : $k['price']) * $k['tk_rate'] * $member_ratio / 100, 2);
                    }else{
                        $list[$key]['commision'] = '';
                    }
                } else {
                    if($member_ratio>0){
                        $list[$key]['commision'] = round($k['price'] * $k['tk_rate'] * $member_ratio / 100, 2);
                    }else{
                        $list[$key]['commision'] = '';
                    }
                    $list[$key]['coupon_amount'] = 0;
                    $list[$key]['coupon_price'] = '';
                    $list[$key]['coupon_info'] = '';
                    $list[$key]['coupon_start_time'] = '';
                    $list[$key]['coupon_end_time'] = '';
                }
            }
            $this->setApiResult(Err::SUCCESS, Err::getMessage(Err::SUCCESS), [
                'list' => $list,
            ]);
        } else {
            $this->setApiResult(Err::ERR_DATA_NOT_EXIST, Err::getMessage(Err::ERR_DATA_NOT_EXIST));
        }
    }

}
