<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,maximum-scale=1.0">
        <title>商品详情</title>
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            div.content img{
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <?= $info["summary"];?>
        </div>
    </body>
</html>
