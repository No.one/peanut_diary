<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_customer_setting".
 */
class CustomerSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_setting}}';
    }
}
