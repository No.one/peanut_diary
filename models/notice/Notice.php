<?php

namespace app\models\notice;

use Yii;

/**
 * This is the model class for table "notice".
 *
 * @property integer $id
 * @property integer $from_member_id
 * @property string $from_name
 * @property string $to_member_id
 * @property integer $type
 * @property integer $send_type
 * @property string $name
 * @property string $summary
 * @property string $status
 * @property string $created_time
 * @property string $updated_time
 */
class Notice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notice}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_member_id', 'type', 'send_type'], 'integer'],
            [['to_member_id', 'summary'], 'required'],
            [['to_member_id', 'summary'], 'string'],
            [['created_time', 'updated_time'], 'safe'],
            [['from_name', 'name'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_member_id' => 'From Member ID',
            'from_name' => 'From Name',
            'to_member_id' => 'To Member ID',
            'type' => 'Type',
            'send_type' => 'Send Type',
            'name' => 'Name',
            'summary' => 'Summary',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
