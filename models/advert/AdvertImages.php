<?php

namespace app\models\advert;

use Yii;

/**
 * This is the model class for table "advert_images".
 */
class AdvertImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%advert_images}}';
    }
    
     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weight', 'status', 'pid'], 'integer'],
            [['updated_time', 'created_time'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['main_image'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'name' => 'Name',
            'weight' => 'Weight',
            'status' => 'Status',
            'main_image' => 'Main Image',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
