<?php

namespace app\models\marketing;

use Yii;
use app\models\marketing\Hot;

/**
 * This is the model class for table "hot".
 */
class HotImage extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%hot_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['hot_id'], 'integer'],
            [['created_time'], 'safe'],
            [['file_key'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'hot_id' => 'Hot Id',
            'file_key' => 'File Key',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_time = date('Y-m-d H:i:s');
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function getHot() {
        return $this->hasOne(Hot::className(), ['id' => 'hot_id']);
    }
}
