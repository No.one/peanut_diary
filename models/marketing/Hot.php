<?php

namespace app\models\marketing;

use Yii;
use app\models\marketing\HotImage;

/**
 * This is the model class for table "hot".
 */
class Hot extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%hot}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['weight', 'share_count', 'status'], 'integer'],
            [['name', 'summary'], 'required'],
            [['summary'], 'string'],
            [['updated_time', 'created_time'], 'safe'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'summary' => 'Summary',
            'weight' => 'Weight',
            'share_count' => 'Share Count',
            'status' => 'Status',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 1;
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
    
     public function getImage() {
        return $this->hasMany(HotImage::className(), ['hot_id' => 'id']);
    }
}
