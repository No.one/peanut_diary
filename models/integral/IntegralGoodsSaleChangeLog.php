<?php
namespace app\models\integral;

use Yii;

/**
 * This is the model class for table "integral_goods_sale_change_log".
 *
 * @property string $id
 * @property integer $goods_id
 * @property integer $order_id
 * @property integer $quantity
 * @property string $price
 * @property integer $member_id
 * @property string $created_time
 * @property string $updated_time
 */
class IntegralGoodsSaleChangeLog extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%integral_goods_sale_change_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id', 'order_id', 'quantity', 'member_id'], 'integer'],
            [['order_id'], 'required'],
            [['price'], 'number'],
            [['created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => '商品ID',
            'order_id' => 'Order ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'member_id' => 'Member ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
