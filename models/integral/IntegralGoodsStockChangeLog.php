<?php
namespace app\models\integral;

use Yii;

/**
 * This is the model class for table "integral_goods_stock_change_log".
 *
 * @property integer $id
 * @property integer $goods_id
 * @property integer $unit
 * @property integer $total_stock
 * @property string $note
 * @property string $created_time
 */
class IntegralGoodsStockChangeLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%integral_goods_stock_change_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goods_id'], 'required'],
            [['goods_id', 'unit', 'total_stock'], 'integer'],
            [['created_time'], 'safe'],
            [['note'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => '商品ID',
            'unit' => 'Unit',
            'total_stock' => 'Total Stock',
            'note' => 'Note',
            'created_time' => 'Created Time',
        ];
    }
}
