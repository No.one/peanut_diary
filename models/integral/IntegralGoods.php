<?php

namespace app\models\integral;

use Yii;

/**
 * This is the model class for table "integra_goods".
 *
 * @property string $id
 * @property integer $cate_id
 * @property string $goods_name
 * @property string $price
 * @property string $summary
 * @property integer $stock
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class IntegralGoods extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%integral_goods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_id', 'stock', 'status'], 'integer'],
            [['price'], 'number'],
            [['updated_time', 'created_time'], 'safe'],
            [['goods_name'], 'string', 'max' => 100],
            [['summary'], 'string', 'max' => 2000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cate_id' => '分类ID',
            'goods_name' => '商品名称',
            'price' => '兑换积分',
            'summary' => '描述',
            'stock' => '库存',
            'status' => '状态',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
