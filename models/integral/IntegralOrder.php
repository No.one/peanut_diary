<?php

namespace app\models\integral;

use app\common\services\goods\GoodsService;
use app\models\integral\IntegralGoods;
use Yii;

/**
 * This is the model class for table "m_integral_order".
 *
 * @property integer $id
 * @property integer $member_id
 * @property string $order_number
 * @property integer $status
 * @property string $price
 * @property string $total_price
 * @property integer $goods_count
 * @property string $remark
 * @property string $created_time
 * @property string $updated_time
 */
class IntegralOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%integral_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'member_id', 'status', 'goods_count'], 'integer'],
            [['total_price'], 'number'],
            [['created_time', 'updated_time'], 'safe'],
            [['order_number'], 'string', 'max' => 50],
            [['remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'order_number' => 'Order Number',
            'status' => 'Status',
            'total_price' => 'Total Price',
            'goods_count' => 'Goods Count',
            'remark' => 'Remark',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    const STATUS_WAIT_SEND = 0;
    const STATUS_DISPATCH = 1;
    const STATUS_TRADE_SUCCESS = 2;
    public static $orderStatus = [
        self::STATUS_WAIT_SEND            => "待发货",
        self::STATUS_DISPATCH             => "配货中",
        self::STATUS_TRADE_SUCCESS        => "已完成",
    ];

    /**
     * 订单和商品 关联关系
     * @return \yii\db\ActiveQuery
     */
    public function getGoods() {
        return $this->hasOne(IntegralOrderGoods::className(), ['order_id' => 'id']);
    }

    /**
     * 订单和物流单 关联关系
     * @return \yii\db\ActiveQuery
     */
    public function getExpressOrder() {
        return $this->hasOne(ExpressOrder::className(), ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert)
            {
                $this->order_number = $this->createOrderSn();
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 0;
            }
            else
            {
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;

        }
        else
        {
            return false;
        }
    }

    /**
     * 创建订单号
     * @return string
     */
    private function createOrderSn(){
        @date_default_timezone_set("PRC");
        $order_id_main = date('YmdHis') . rand(10,99);
        //订单号码主体长度
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for($i=0; $i<$order_id_len; $i++){
            $order_id_sum += (int)(substr($order_id_main,$i,1));

        }
        return $order_id_main . str_pad((100 - $order_id_sum % 100) % 100,2,'0',STR_PAD_LEFT);
    }

    /**
     * 订单取消库存回滚
     * @return bool
     */
    public function rollbackStock() {
        $order_goods  = OrderGoods::findOne(['order_id' => $this->id]);
        $goods        = Goods::findOne($order_goods->goods_id);
        $member_stock = MemberStock::findOne(['member_id' => $this->member_id, 'goods_id' => $goods->id]);

        $transaction = Yii::$app->db->beginTransaction();

        $goods->stock        = new \yii\db\Expression("`stock` + ".$order_goods->num);
        $member_stock->stock = new \yii\db\Expression("`stock` + ".$order_goods->num);
        if($goods->save() && $member_stock->save()) {
            $transaction->commit();
            //库存变化记录
            GoodsService::setStockChangeLog($order_goods->goods_id, +$order_goods->num, "订单取消");
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }

        ////库存加回去
        //Goods::updateAll(['stock' => new \yii\db\Expression("`stock` + ".$order_goods->num)], ['id' => $goods->id]);
        //MemberStock::updateAll(['stock' => new \yii\db\Expression("`stock` + ".$order_goods->num)], ['member_id'=>$this->member_id, 'goods_id'=>$goods->id]);

    }


}
