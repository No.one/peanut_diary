<?php

namespace app\models\order;

use Yii;

/**
 * This is the model class for table "m_pay_log".
 */
class PayLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pay_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'order_id', 'status'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['order_sn', 'royal_order_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'order_sn' => 'Order Sn',
            'status' => 'Status',
            'royal_order_id' => 'Royal Order Id',
            'pay_time' => 'Pay Time',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert)
            {
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 0;
            }
            else
            {
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;

        }
        else
        {
            return false;
        }
    }

    /**
     * 创建流水号
     * @return string
     */
    public static function createSn(){
        @date_default_timezone_set("PRC");
        $order_id_main = date('YmdHis') . rand(10000000,99999999);
        //订单号码主体长度
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for($i=0; $i<$order_id_len; $i++){
            $order_id_sum += (int)(substr($order_id_main,$i,1));

        }
        return 'P'.$order_id_main . str_pad((100 - $order_id_sum % 100) % 100,2,'0',STR_PAD_LEFT);
    }


}
