<?php

namespace app\models\order;

use Yii;
use app\models\member\Member;
use app\models\goods\Goods;
use app\models\order\TaobaoCommision;

/**
 * This is the model class for table "m_taobao_order".
 */
class TaobaoOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taobao_order}}';
    }
    
    const STATUS_ORDER_PAY = 1;
    const STATUS_ORDER_BALANCE = 2;
    const STATUS_ORDER_INVALID = 3;
    const STATUS_TRADE_SUCCESS = 4;
    public static $orderStatus = [
        self::STATUS_ORDER_PAY       => "订单付款",
        self::STATUS_ORDER_BALANCE   => "订单结算",
        self::STATUS_ORDER_INVALID   => "订单失效",
        self::STATUS_TRADE_SUCCESS   => "交易成功",
    ];
    
    public function getMember() {
        return $this->hasOne(Member::className(), ['adzoneid' => 'adv_id']);
    }
    
    public function getGoods(){
        return $this->hasOne(Goods::className(), ['original_id'=>'goods_id']);
    }
    
    public function getCommision(){
        return $this->hasOne(TaobaoCommision::className(), ['order_id'=>'id']);
    }
}
