<?php

namespace app\models\order;

use Yii;
use app\models\member\Member;
use app\models\order\TaobaoOrder;

class TaobaoCommision extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%taobao_commision}}';
    }

    public function getOrder(){
        return $this->hasOne(TaobaoOrder::className(), ['id'=>'order_id']);
    }

    public function getSelfCommision() {
        return $this->hasOne(Member::className(), ['id' => 'member_id']);
    }

    public function getHigherCommision() {
        return $this->hasOne(Member::className(), ['id' => 'higher_id']);
    }

    public function getOperatorCommision() {
        return $this->hasOne(Member::className(), ['id' => 'operate_id']);
    }

   

}
