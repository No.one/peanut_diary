<?php

namespace app\models\order;

use app\common\services\goods\GoodsService;
use app\models\goods\Goods;
use app\models\member\Member;
use app\models\member\MemberStock;
use Yii;

/**
 * This is the model class for table "m_order".
 *
 * @property integer $id
 * @property integer $member_id
 * @property string $order_number
 * @property integer $status
 * @property string $price
 * @property string $total_price
 * @property integer $goods_count
 * @property string $remark
 * @property string $created_time
 * @property string $updated_time
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'member_id', 'status', 'goods_count'], 'integer'],
            [['total_price'], 'number'],
            [['created_time', 'updated_time'], 'safe'],
            [['order_number'], 'string', 'max' => 50],
            [['remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'order_number' => 'Order Number',
            'status' => 'Status',
            'total_price' => 'Total Price',
            'goods_count' => 'Goods Count',
            'remark' => 'Remark',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    const STATUS_WAIT_PAY = 1;
    const STATUS_TRADE_SUCCESS = 2;
    const STATUS_HAD_PAY_WAIT_CONFIRM = 3;
    const STATUS_WAIT_SEND = 4;
    const STATUS_PREPARE_GOODS = 5;
    const STATUS_CANCEL = 6;
    const STATUS_HAD_SEND = 7;
    const STATUS_RETURN_GOODS = 8;
    const STATUS_DISPATCH = 9;
    public static $orderStatus = [
        self::STATUS_WAIT_PAY             => "待支付",
        self::STATUS_TRADE_SUCCESS        => "交易成功",
        self::STATUS_HAD_PAY_WAIT_CONFIRM => "已支付待确认",
        self::STATUS_WAIT_SEND            => "已确认待发货",
        self::STATUS_PREPARE_GOODS        => "配货中",
        self::STATUS_CANCEL               => "订单取消",
        self::STATUS_HAD_SEND             => "已发货",
        self::STATUS_RETURN_GOODS         => "退货中",
        self::STATUS_DISPATCH             => "派送中",
    ];
    
}
