<?php

namespace app\models\order;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "m_apply_intercept".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $member_id
 * @property integer $status
 * @property string $remark
 * @property integer $operate_id
 * @property string $created_time
 * @property string $updated_time
 */
class ApplyIntercept extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_apply_intercept';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'member_id', 'operate_id', 'status'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['remark'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'member_id' => 'Member ID',
            'status' => 'Status',
            'remark' => 'Remark',
            'operate_id' => 'Operate ID',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => date("Y-m-d H:i:s"),
            ],
        ];
    }

    const STATUS_APPLY_ING = 0;
    const STATUS_APPLY_SUCCESS = 1;
    const STATUS_APPLY_FAIL = 2;

    /**
     * 创建model
     * @param $params
     * @return \app\models\order\ApplyIntercept
     * @throws \yii\base\Exception
     */
    private static function createNewModel($params) {
        $model = new self();
        $model->order_id   = $params['order_id'];
        $model->member_id  = $params['member_id'];
        $model->status     = self::STATUS_APPLY_ING;
        $model->remark     = isset($params['remark']) ? $params['remark'] : '';
        $model->operate_id = isset($params['operate_id']) ? $params['operate_id'] : '';
        if(!$model->save()) {
            throw new Exception("截单申请保存失败");
        }

        return $model;
    }

    /**
     * 添加申请单
     * @param $params
     * @return \app\models\order\ApplyIntercept
     */
    public static function addApply($params) {
        return self::createNewModel($params);
    }


}
