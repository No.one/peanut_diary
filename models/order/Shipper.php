<?php

namespace app\models\order;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "m_consignee".
 */
class Shipper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shipper}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_id'], 'integer'],
            [['created_time', 'updated_time', 'deleted_time'], 'safe'],
            [['shipper'], 'string', 'max' => 25],
            [['shipper_tel'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'shipper' => 'Shipper',
            'shipper_tel' => 'Shipper Tel',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'deleted_time' => 'Deleted Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($insert){
                $this->created_time = date('Y-m-d H:i:s');
            }else{
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 新建model并返回
     * @param $params
     * @return \app\models\order\Shipper
     * @throws \yii\base\Exception
     */
    private static function createNewModel($params) {
        $model = new Shipper();
        $model->member_id   = $params['member_id'] ?: 0;
        $model->shipper     = $params['shipper'];
        $model->shipper_tel = $params['shipper_tel'];

        if(!$model->save()) {
            throw new Exception('寄件人信息保存失败');
        }

        return $model;
    }

    /**
     * 新增寄件人
     * @param $params
     * @return \app\models\order\Shipper
     */
    public static function addShipper($params) {
        return self::createNewModel($params);
    }

    /**
     * 修改寄件人
     * @param $shipper_id
     * @param $member_id
     * @param $params
     * @return static
     * @throws Exception
     */
    public static function updateShipper($shipper_id, $member_id, $params){
        $model = Shipper::findOne(['id'=>$shipper_id, 'member_id'=>$member_id]);
        $model->shipper     = $params['shipper'];
        $model->shipper_tel = $params['shipper_tel'];
        if(!$model->save()) {
            throw new Exception('寄件人信息修改失败');
        }
        return $model;
    }

}
