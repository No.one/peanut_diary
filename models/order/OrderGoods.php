<?php

namespace app\models\order;

use Yii;

/**
 * This is the model class for table "m_order_goods".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $goods_id
 * @property integer $package_id
 * @property string $goods_name
 * @property string $alias_name
 * @property integer $num
 * @property string $price
 * @property string $shelf_life
 * @property integer $status
 * @property string $created_time
 * @property string $update_time
 */
class OrderGoods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_goods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'goods_id', 'package_id', 'num', 'status'], 'integer'],
            [['price'], 'number'],
            [['created_time', 'update_time'], 'safe'],
            [['goods_name', 'alias_name'], 'string', 'max' => 255],
            [['shelf_life'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'goods_id' => 'Goods ID',
            'package_id' => 'Package ID',
            'goods_name' => 'Goods Name',
            'alias_name' => 'Alias Name',
            'num' => 'Num',
            'price' => 'Price',
            'shelf_life' => 'Shelf Life',
            'status' => 'Status',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert)
            {
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 1;
            }
            else
            {
                $this->update_time =  date('Y-m-d H:i:s');
            }
            return true;

        }
        else
        {
            return false;
        }
    }
}
