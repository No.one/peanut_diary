<?php

namespace app\models\order;

use Yii;

/**
 * This is the model class for table "m_express_order".
 *
 * @property integer $id
 * @property string $express_number
 * @property integer $order_id
 * @property string $sender_name
 * @property string $sender_tel
 * @property string $receiver_name
 * @property string $receiver_tel
 * @property string $receive_address
 * @property string $remark
 * @property string $created_time
 * @property string $updated_time
 */
class ExpressOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%express_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['express_id', 'order_id', 'status', 'type'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['express_number'], 'string', 'max' => 32],
            [['sender_name', 'receiver_name'], 'string', 'max' => 25],
            [['sender_tel', 'receiver_tel'], 'string', 'max' => 50],
            [['receive_address'], 'string', 'max' => 100],
            [['remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'express_number' => 'Express Number',
            'order_id' => 'Order ID',
            'sender_name' => 'Sender Name',
            'sender_tel' => 'Sender Tel',
            'receiver_name' => 'Receiver Name',
            'receiver_tel' => 'Receiver Tel',
            'receive_address' => 'Receive Address',
            'status' => 'Status',
            'remark' => 'Remark',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
