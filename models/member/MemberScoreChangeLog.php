<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "member_score_change_log".
 */
class MemberScoreChangeLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_score_change_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id', 'unit', 'total_score'], 'integer'],
            [['created_time'], 'safe'],
            [['note'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户ID',
            'unit' => 'Unit',
            'total_score' => 'Total Score',
            'note' => 'Note',
            'created_time' => 'Created Time',
        ];
    }
}
