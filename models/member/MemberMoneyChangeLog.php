<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "member_money_change_log".
 */
class MemberMoneyChangeLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_money_change_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id', 'unit', 'total_money'], 'integer'],
            [['created_time'], 'safe'],
            [['note'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户ID',
            'unit' => 'Unit',
            'total_moeny' => 'Total Money',
            'note' => 'Note',
            'created_time' => 'Created Time',
        ];
    }
}
