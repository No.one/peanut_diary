<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "member_address".
 *
 * @property integer $id
 * @property string $name
 * @property double $ratio
 * @property integer $sort
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class MemberGrade extends \yii\db\ActiveRecord {

    const MAX_LEVEL = 3;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%member_grade}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['status', 'sort'], 'integer'],
            [['ratio'], 'double'],
            [['updated_time', 'created_time'], 'safe'],
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ratio' => 'Ratio',
            'status' => 'Nickname',
            'sort' => 'Sort',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_time = date('Y-m-d H:i:s');
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            $cache = Yii::$app->cache;
            $cache->delete('memberGrade');
            return true;
        } else {
            return false;
        }
    }

    /**
     * 保存后修改数据
     * @param type $insert
     * @param type $changedAttributes
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->createCache();
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->createCache();
    }

    /**
     * 缓存
     * @return type
     */
    private function createCache() {
        $cache = Yii::$app->cache;
        $cache->delete('memberGrade');
        $data = self::find()
                        ->where(['status' => 1])
                        ->select('id, name, ratio')
                        ->orderBy(['sort' => SORT_ASC])
                        ->asArray()->all();
        if ($data) {
            foreach ($data as $key => $k) {
                $result[$k['id']] = $k;
            }
        }
        $cache->set('memberGrade', $result);
        return $result;
    }

    /**
     * 获取等级
     * @return type
     */
    public function getGrade() {
        $cache = Yii::$app->cache;
        $data = $cache->get('memberGrade');
        if (!empty($data)) {
            return $data;
        } else {
            return $this->createCache();
        }
    }

}
