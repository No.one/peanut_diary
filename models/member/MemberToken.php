<?php

namespace app\models\member;

use app\common\helpers\Err;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "m_member_token".
 *
 * @property integer $id
 * @property string $token
 * @property integer $member_id
 * @property string $expire_time
 * @property string $created_time
 * @property string $updated_time
 */
class MemberToken extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_token}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_id'], 'integer'],
            [['expire_time', 'created_time', 'updated_time'], 'safe'],
            [['token'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'member_id' => 'Member ID',
            'expire_time' => 'Expire Time',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }

    /**
     * 默认操作时间
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => date("Y-m-d H:i:s"),
            ],
        ];
    }

    /**
     * @param $user_id
     * @return \app\models\member\MemberToken
     * @throws \yii\base\Exception
     */
    private static function createNewModel($user_id) {
        $model = new MemberToken();
        $model->member_id = $user_id;
        $model->token = self::generateToken();
        $model->expire_time = time()+ 3 * 24 * 3600;
        if(!$model->save()) {
            throw new Exception('Token添加失败', Err::ERR_OPERATE_FAIL);
        }
        return $model;
    }

    /**
     * @param $user_id
     * @return \app\models\member\MemberToken
     */
    public static function addToken($user_id) {
        return self::createNewModel($user_id);
    }

    /**
     * @return string
     */
    public static function generateToken() {
        $bytes = openssl_random_pseudo_bytes(40);
        $token = strtr(substr(base64_encode($bytes), 0, 40), '+/', '_-');
        return $token;
    }

}
