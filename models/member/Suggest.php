<?php

namespace app\models\member;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "m_suggest".
 */
class Suggest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%suggest}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_id', 'operate_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['content', 'remark', 'image_keys'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'operate_id' => 'Operate Id',
            'content' => 'content',
            'image_keys' => 'Image Keys',
            'remark' => 'remark',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'deleted_time' => 'Deleted Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($insert){
                $this->created_time = date('Y-m-d H:i:s');
            }else{
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;
        }else{
            return false;
        }
    }
}
