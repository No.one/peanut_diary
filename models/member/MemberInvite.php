<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "member_invite".
 *
 * @property string $id
 * @property integer $member_id
 * @property integer $book_id
 * @property string $type
 * @property string $created_time
 */
class MemberInvite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%member_invite}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'invite_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_id' => 'Member Id',
            'invite_id' => 'Invite ID',
        ];
    }
    
    /**
     * 创建邀请关系
     * @param type $uid
     * @param type $invite_uid
     */
    public static function createInvite($member_id,$invite_uid){
        $model = new MemberInvite;
        $model->member_id = $member_id;
        $model->invite_id = $invite_uid; 
        $model->save();
    }
}
