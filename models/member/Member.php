<?php

namespace app\models\member;

use app\common\helpers\Err;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use app\models\order\TaobaoOrder;
use app\models\order\TaobaoCommision;
use app\models\member\MemberInvite;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $nickname
 * @property string $mobile
 * @property integer $sex
 * @property string $avatar
 * @property string $salt
 * @property string $reg_ip
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class Member extends \yii\db\ActiveRecord {

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public function setPassword($password) {
        $this->password = $this->getSaltPassword($password);
    }

    public function setSalt($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
        $salt = '';
        for ($i = 0; $i < $length; $i++) {
            $salt .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        $this->salt = $salt;
    }

    public function getSaltPassword($password) {
        return md5($password . md5($this->salt));
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%member}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['status', 'grade_id'], 'integer'],
            [['updated_time', 'created_time', 'deleted_time'], 'safe'],
            [['truename'], 'string', 'max' => 100],
            [['mobile'], 'string', 'max' => 11],
            [['invite_code', 'reg_ip'], 'string', 'max' => 32],
            [['mobile'], 'checkMobile'],
            [['nickname'], 'checkNinkname']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'nickname' => '用户名',
            'truename' => 'Truename',
            'mobile' => '手机号码',
            'invite_code' => '邀请码',
            'status' => 'Status',
            'reg_ip' => 'Reg Ip',
            'grade_id' => '用户等级',
            'updated_time' => 'Updated Time',
            'deleted_time' => 'Deleted Time',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->invite_code = $this->createCode();
                $this->grade_id = 1;
                $this->created_time = date('Y-m-d H:i:s');
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            $cache = Yii::$app->cache;
            $cache->delete('memberList');
            return true;
        } else {
            return false;
        }
    }

    /**
     * 生成用户邀请码
     * @return string
     */
    public function createCode() {
        $code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $rand = $code[rand(0, 25)]
                . strtoupper(dechex(date('m')))
                . date('d') . substr(time(), -5)
                . substr(microtime(), 2, 5)
                . sprintf('%02d', rand(0, 99));
        for (
        $a = md5($rand, true),
        $s = '0123456789ABCDEFGHIJKLMNOPQRSTUV',
        $d = '',
        $f = 0; $f < 6; $g = ord($a[$f]),
                $d .= $s[( $g ^ ord($a[$f + 6]) ) - $g & 0x1F],
                $f++
        )
            ;
        if (mb_strlen($d) == 6) {
            $code_log = self::findOne(['invite_code' => $d]);
            if (!$code_log) {
                return $d;
            }
        }
        $d = $this->createCode();
        return $d;
    }

    /**
     * @param $password
     * @return bool
     */
    public function checkPassword($password) {
        return $this->getSaltPassword($password) == $this->password;
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function getToken() {
        try {
            $memberToken = MemberToken::findOne(['member_id' => $this->id]);
            if (empty($memberToken)) {
                $tokenObj = MemberToken::addToken($this->id);
                $token = $tokenObj->token;
            } else {
                $memberToken->token = MemberToken::generateToken();
                $memberToken->expire_time = time() + 3 * 24 * 3600;
                if (!$memberToken->save()) {
                    throw new Exception('Token更新失败', Err::ERR_OPERATE_FAIL);
                }

                $token = $memberToken->token;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
        return $token;
    }

    /**
     * 检查邀请码
     * @param type $invite_code
     * @return type
     */
    public static function checkInviteCode($invite_code) {
        $query = self::find()->andWhere(['invite_code' => $invite_code]);
        return $query->one();
    }

    /**
     * 获取子分类
     * @param type $id
     * @param type $lev
     * @param type $end_lev
     * @return type
     */
    public static function getChild($id, $lev = 1, $end_lev = '') {
        $arr = self::_getMemberList();
        $child = array();
        foreach ($arr as $val) {
            if ($val['parent'] == $id) {
                $val['lev'] = $lev;
                $child[] = $val;
                $child = array_merge($child, self::getChild($val['member_id'], $lev + 1, $end_lev));
            }
            if ($end_lev && ($lev > $end_lev)) {
                break;
            }
        }
        return $child;
    }

    /**
     * 查找指定用户的所有上级团队成员
     * @param type $id
     * @param type $lev
     * @param type $end_lev
     * @return type
     */
    public static function getFather($id, $lev = 1, $end_lev = '') {
        $arr = self::_getMemberList();
        $father = array();
        foreach ($arr as $val) {
            if ($end_lev && ($lev > $end_lev)) {
                break;
            }
            if ($val['member_id'] == $id) {
                $val['lev'] = $lev;
                $father[] = $val;
                $father = array_merge($father, self::getFather($val['parent'], $lev + 1, $end_lev));
            }
        }
        return $father;
    }

    /**
     * 获取所有用户基数
     * @return type
     */
    public static function _getMemberList() {
        $cache = Yii::$app->cache;
        $data = $cache->get('memberList');
        if (!empty($data)) {
            return $data;
        } else {
            $data = self::find()
                            ->alias('m')
                            ->innerJoin("{{%member_invite}} mi", "m.id=mi.member_id")
                            ->where(['status' => 1])->select('m.id as member_id, m.nickname, m.mobile, m.avatar, m.grade_id, m.adzoneid, m.created_time, mi.invite_id as parent')->asArray()->all();
            $cache->set('memberList', $data, 3600);
            return $data;
        }
    }

    public function getInvite() {
        return $this->hasOne(MemberInvite::className(), ['member_id' => 'id']);
    }

    public function getTaobaoOrders() {
        return $this->hasMany(TaobaoOrder::className(), ['adv_id' => 'adzoneid'])
                        ->orderBy('create_time');
    }

    /**
     * 检查昵称
     * @param type $attribute
     * @param type $params
     * @return boolean
     */
    public function checkNinkname($attribute, $params) {
        $query = Member::find()->andWhere(['nickname' => $this->$attribute]);
        if ($this->id > 0)
            $query->andWhere(['<>', 'id', $this->id]);
        $count = $query->count();
        if ($count == 0) {
            return true;
        } else {
            $this->addError('nickname', '用户名已经存在!');
        }
    }

    public function checkMobile($attribute, $params) {
        $query = Member::find()->andWhere(['mobile' => $this->$attribute]);
        if ($this->id > 0)
            $query->andWhere(['<>', 'id', $this->id]);
        $count = $query->count();
        if ($count == 0) {
            return true;
        } else {
            $this->addError('mobile', '手机号码已经占用!');
        }
    }

}
