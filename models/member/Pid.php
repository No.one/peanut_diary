<?php

namespace app\models\member;

use app\models\PLock;

/**
 * This is the model class for table "m_pid".
 */
class Pid extends PLock {

    // 重载定义悲观锁标识字段，如 locked_at
    public function pesstimisticLock() {
        return 'locked_at';
    }

    // 重载定义最大锁定时长，如1小时
    public function maxLockTime() {
        return 60;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%pid}}';
    }

}
