<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "cover".
 *
 * @property integer $id
 * @property string $image_key
 * @property string $created_time
 */
class Cover extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cover}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_time'], 'safe'],
            [['image_key'], 'string', 'max' => 200],
            [['image_key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_key' => 'Image Key',
            'created_time' => 'Created Time',
        ];
    }
}
