<?php

namespace app\models\member;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "m_address".
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'member_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['consignee'], 'string', 'max' => 25],
            [['consignee_tel'], 'string', 'max' => 50],
            [['consignee_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'consignee' => 'Consignee',
            'consignee_tel' => 'Consignee Tel',
            'consignee_address' => 'Consignee address',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
            'deleted_time' => 'Deleted Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($insert){
                $this->is_default = 0;
                $this->created_time = date('Y-m-d H:i:s');
            }else{
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * 新建model并返回
     * @param $params
     * @return \app\models\order\Consignee
     * @throws \yii\base\Exception
     */
    private static function createNewModel($params) {
        $model = new Consignee();
        $model->member_id         = $params['member_id'] ?: 0;
        $model->consignee         = $params['consignee'];
        $model->consignee_tel     = $params['consignee_tel'];
        $model->consignee_address = $params['consignee_address'];

        if(!$model->save()) {
            throw new Exception('收件人信息保存失败');
        }

        return $model;
    }

    /**
     * 新增寄件人
     * @param $params
     * @return \app\models\order\Shipper
     */
    public static function addConsignee($params) {
        return self::createNewModel($params);
    }

    /**
     * 修改寄件人
     * @param $consignee_id
     * @param $member_id
     * @param $params
     * @return static
     * @throws Exception
     */
    public static function updateConsignee($consignee_id, $member_id, $params){
        $model = Consignee::findOne(['id'=>$consignee_id, 'member_id'=>$member_id]);
        $model->consignee         = $params['consignee'];
        $model->consignee_tel     = $params['consignee_tel'];
        $model->consignee_address = $params['consignee_address'];
        if(!$model->save()) {
            throw new Exception('收件人信息修改失败');
        }
        return $model;
    }


}
