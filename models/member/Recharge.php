<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "recharge".
 *
 * @property integer $id
 * @property integer $goods_id
 * @property integer $unit
 * @property integer $total_stock
 * @property string $note
 * @property string $created_time
 */
class Recharge extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%recharge}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'type', 'uid'], 'integer'],
            [['updated_time', 'created_time'], 'safe'],
            [['certificate'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'uid'          => '会员ID',
            'type'         => '类型',
            'certificate'  => '凭证',
            'status'       => '状态',
            'created_time' => '添加时间',
            'updated_time' => '更新时间',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert)
            {
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 0;
            }
            else
            {
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;

        }
        else
        {
            return false;
        }
    }

    /**
     * 充值类型
     * @var array
     */
    public static $rechargeType = [
        1  => "$1000",
        2  => "$2000",
        3  => "$5000",
        4  => "$10000",
        5  => "$10000以上",
    ];

    /**
     * 充值类型 数组
     * @var array
     */
    public static $rechargeList = [
        ["key"=>1, "value"=>"$1000"],
        ["key"=>2, "value"=>"$2000"],
        ["key"=>3, "value"=>"$5000"],
        ["key"=>4, "value"=>"$10000"],
        ["key"=>5, "value"=>"$10000以上"],
    ];
}
