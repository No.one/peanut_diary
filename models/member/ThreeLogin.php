<?php
namespace app\models\member;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "member".
 *
 * @property integer $id
 * @property string $nickname
 * @property string $mobile
 * @property integer $sex
 * @property string $avatar
 * @property string $salt
 * @property string $reg_ip
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class ThreeLogin extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%three_login}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['member_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'member_id' => '用户ID'
        ];
    }

}
