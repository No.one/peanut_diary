<?php

namespace app\models\member;

use Yii;

/**
 * This is the model class for table "Withdraw.
 */
class Withdraw extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%withdraw}}';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->order_sn = $this->createOrderSn();
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 0;
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * 创建订单号
     * @return string
     */
    private function createOrderSn() {
        @date_default_timezone_set("PRC");
        $order_id_main = date('YmdHis') . rand(10, 99);
        //订单号码主体长度
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for ($i = 0; $i < $order_id_len; $i++) {
            $order_id_sum += (int) (substr($order_id_main, $i, 1));
        }
        return $order_id_main . str_pad((100 - $order_id_sum % 100) % 100, 2, '0', STR_PAD_LEFT);
    }

}
