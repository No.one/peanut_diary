<?php

namespace app\models\article;

use Yii;

/**
 * This is the model class for table "article_log".
 *
 * @property integer $id
 * @property string $cat_id
 * @property string $name
 * @property string $summary
 * @property integer $weight
 * @property integer $view_count
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class ArticleLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'a_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Member ID',
            'a_id' => 'Article ID',
        ];
    }
}
