<?php

namespace app\models\article;

use Yii;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $cat_id
 * @property string $name
 * @property string $summary
 * @property integer $weight
 * @property integer $view_count
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'weight', 'view_count', 'status'], 'integer'],
            [['name', 'summary'], 'required'],
            [['summary'], 'string'],
            [['updated_time', 'created_time'], 'safe'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'name' => 'Name',
            'summary' => 'Summary',
            'weight' => 'Weight',
            'view_count' => 'View Count',
            'status' => 'Status',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
