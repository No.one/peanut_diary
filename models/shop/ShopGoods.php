<?php

namespace app\models\shop;

use Yii;
use app\models\goods\Goods;

/**
 * This is the model class for table "shop_goods".
 */
class ShopGoods extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%shop_goods}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['shop_id', 'goods_id'], 'integer'],
            [['created_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'shop_id' => 'Shop Id',
            'goods_id' => 'goods Id',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_time = date('Y-m-d H:i:s');
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function getGoods() {
        return $this->hasOne(Goods::className(), ['id' => 'goods_id']);
    }
}
