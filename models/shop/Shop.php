<?php

namespace app\models\shop;

use Yii;
use app\models\shop\ShopCate;
use app\models\shop\ShopGoods;

/**
 * This is the model class for table "shop".
 */
class Shop extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_id', 'weight', 'status'], 'integer'],
            [['updated_time', 'created_time'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['main_image'], 'string', 'max' => 100],
            [['pic'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cate_id' => '分类ID',
            'name' => '店铺名称',
            'main_image' => '封面图',
            'pic' => '轮播图',
            'status' => '状态',
            'weight' => '权重',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_time = date('Y-m-d H:i:s');
                $this->status = 1;
            } else {
                $this->updated_time = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function getCate(){
        return $this->hasOne(ShopCate::className(), ['id'=>'cate_id']);
    }
    
    public function getGoods(){
        return $this->hasMany(ShopGoods::className(), ['shop_id'=>'id']);
    }
    
    
}
