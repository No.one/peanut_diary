<?php

namespace app\models\shop;

use Yii;

/**
 * This is the model class for table "shop_collect".
 */
class ShopCollect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%shop_collect}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id','shop_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_id' => '用户ID',
            'shop_id' => '店铺id',
        ];
    }
}
