<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "express".
 *
 * @property integer $id
 * @property string $e_name
 * @property string $e_state
 * @property string $e_code
 * @property string $e_letter
 * @property string $e_order
 * @property string $e_url
 * @property integer $e_zt_state
 */
class Express extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%express}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_name', 'e_code', 'e_letter', 'e_url'], 'required'],
            [['e_state', 'e_order'], 'string'],
            [['e_zt_state'], 'integer'],
            [['e_name', 'e_code'], 'string', 'max' => 50],
            [['e_letter'], 'string', 'max' => 1],
            [['e_url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'e_name' => 'E Name',
            'e_state' => 'E State',
            'e_code' => 'E Code',
            'e_letter' => 'E Letter',
            'e_order' => 'E Order',
            'e_url' => 'E Url',
            'e_zt_state' => 'E Zt State',
        ];
    }
}
