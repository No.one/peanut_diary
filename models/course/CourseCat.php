<?php

namespace app\models\course;

use Yii;

/**
 * This is the model class for table "course_cat".
 *
 * @property string $id
 * @property string $name
 * @property string $avatar
 * @property string $summary
 * @property integer $weight
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class CourseCat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_cat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avatar', 'summary'], 'required'],
            [['weight', 'status'], 'integer'],
            [['updated_time', 'created_time'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['avatar'], 'string', 'max' => 200],
            [['summary'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'avatar' => 'Avatar',
            'summary' => 'Summary',
            'weight' => 'Weight',
            'status' => 'Status',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
