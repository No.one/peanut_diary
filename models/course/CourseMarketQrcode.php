<?php

namespace app\models\course;

use Yii;

/**
 * This is the model class for table "course_market_qrcode".
 *
 * @property string $id
 * @property string $member_id
 * @property integer $order_id
 * @property integer $order_item_id
 * @property string $qrcode
 * @property string $qrcode_image
 * @property integer $status
 * @property integer $total_scan_count
 * @property string $created_time
 * @property string $updated_time
 */
class CourseMarketQrcode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course_market_qrcode}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'qrcode_image'], 'required'],
            [['order_id', 'order_item_id', 'status', 'total_scan_count'], 'integer'],
            [['qrcode'], 'string'],
            [['created_time', 'updated_time'], 'safe'],
            [['member_id'], 'string', 'max' => 20],
            [['qrcode_image'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'order_id' => 'Order ID',
            'order_item_id' => 'Order Item ID',
            'qrcode' => 'Qrcode',
            'qrcode_image' => 'Qrcode Image',
            'status' => 'Status',
            'total_scan_count' => 'Total Scan Count',
            'created_time' => 'Created Time',
            'updated_time' => 'Updated Time',
        ];
    }
}
