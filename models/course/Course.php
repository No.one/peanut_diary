<?php

namespace app\models\course;

use Yii;

/**
 * This is the model class for table "course".
 *
 * @property string $id
 * @property integer $cat_id
 * @property string $name
 * @property integer $is_free
 * @property string $price
 * @property integer $model
 * @property string $course_date
 * @property string $course_addr
 * @property string $contacts
 * @property string $main_image
 * @property string $course_pic
 * @property string $video_url
 * @property string $summary
 * @property string $tags
 * @property integer $status
 * @property integer $month_count
 * @property integer $total_count
 * @property integer $view_count
 * @property integer $comment_count
 * @property string $updated_time
 * @property string $created_time
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'is_free', 'model', 'status', 'month_count', 'total_count', 'view_count', 'comment_count'], 'integer'],
            [['is_free', 'model'], 'required'],
            [['price'], 'number'],
            [['updated_time', 'created_time'], 'safe'],
            [['name', 'course_date', 'main_image'], 'string', 'max' => 100],
            [['course_addr'], 'string', 'max' => 255],
            [['contacts'], 'string', 'max' => 500],
            [['course_pic', 'summary'], 'string', 'max' => 2000],
            [['video_url'], 'string', 'max' => 1000],
            [['tags'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'name' => 'Name',
            'is_free' => 'Is Free',
            'price' => 'Price',
            'model' => 'Model',
            'course_date' => 'Course Date',
            'course_addr' => 'Course Addr',
            'contacts' => 'Contacts',
            'main_image' => 'Main Image',
            'course_pic' => 'Course Pic',
            'video_url' => 'Video Url',
            'summary' => 'Summary',
            'tags' => 'Tags',
            'status' => 'Status',
            'month_count' => 'Month Count',
            'total_count' => 'Total Count',
            'view_count' => 'View Count',
            'comment_count' => 'Comment Count',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
