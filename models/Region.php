<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "region".
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }
    
    /**
     * 获取缓存
     * @return type
     */
    public static function getCache(){
        $cache = Yii::$app->cache;
        $list = $cache->get('region');
        if(empty($list)){
            $list = self::find()->where('region_type != 0')->select('region_id as id , parent_id as fid, region_name as name')->asArray()->all();
            $cache->set('region', $list);
        }
       return $list;
    }
    
}
