<?php

namespace app\models;

use yii\db\ActiveRecord;

// 悲观锁AR基类，需要使用悲观锁的AR可以由此派生
class PLock extends ActiveRecord {
   
    // 声明悲观锁使用的标记字段，作用类似于 optimisticLock() 方法
    public function pesstimisticLock() {
        return null;
    }

    // 定义锁定的最大时长，超过该时长后，自动解锁。
    public function maxLockTime() {
        return 0;
    }

    // 尝试加锁，加锁成功则返回true
    public function lock() {
        $lock = $this->pesstimisticLock();
        $now = time();
        $values = [$lock => $now];
        // 以下2句，更新条件为主键，且上次锁定时间距现在超过规定时长
        $condition = ['and', $this->getOldPrimaryKey(true), ['<', $lock, $now-$this->maxLockTime()]];
        $rows = $this->updateAll($values, $condition);
        // 加锁失败，返回 false
        if (!$rows) {
            return false;
        }
        return true;
    }

    // 重载updateInternal()
    protected function updateInternal($attributes = null) {
        // 这些与原来代码一样
        if (!$this->beforeSave(false)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        if (empty($values)) {
            $this->afterSave(false, $values);
            return 0;
        }
        $condition = $this->getOldPrimaryKey(true);
        // 改为获取悲观锁标识字段
        $lock = $this->pesstimisticLock();
        // 如果 $lock 为 null，那么，不启用悲观锁。
        if ($lock !== null) {
            // 等下保存时，要把标识字段置0
            $values[$lock] = 0;
            // 这里把原来的标识字段值作为更新的另一个条件
            $condition[$lock] = $this->$lock;
        }
        $rows = $this->updateAll($values, $condition);

        // 如果已经启用了悲观锁，但是却没有完成更新，或者更新的记录数为0；
        // 那就说明之前的加锁已经自动失效了，记录正在被修改，
        // 或者已经完成修改，于是抛出异常。
        if ($lock !== null && !$rows) {
            throw new StaleObjectException('The object being updated is outdated.');
        }
        $changedAttributes = [];
        foreach ($values as $name => $value) {
            $changedAttributes[$name] = isset($this->_oldAttributes[$name]) ? $this->_oldAttributes[$name] : null;
//            $this->_oldAttributes[$name] = $value;
        }
        $this->afterSave(false, $changedAttributes);
        return $rows;
    }

}
