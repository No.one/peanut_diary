<?php

namespace app\models\goods;

use Yii;

/**
 * This is the model class for table "goods_collect".
 */
class GoodsCollect extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods_collect}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id','goods_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'member_id' => '用户ID',
            'goods_id' => '商品id',
        ];
    }
}
