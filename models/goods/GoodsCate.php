<?php

namespace app\models\goods;

use Yii;

/**
 * This is the model class for table "goods_cate".
 */
class GoodsCate extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%goods_cate}}';
    }

    public static function getByName($name) {
        return self::findOne(['name' => $name]);
    }

    public static function getByParentIdAndName($parentId, $name) {
        return self::findOne(['name' => $name, 'parent_id' => $parentId]);
    }

}
