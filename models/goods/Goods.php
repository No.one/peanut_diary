<?php

namespace app\models\goods;

use Yii;

/**
 * This is the model class for table "goods".
 */
class Goods extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%goods}}';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert)
            {
                $this->created_time = date('Y-m-d H:i:s');
            }
            else
            {
                $this->updated_time =  date('Y-m-d H:i:s');
            }
            return true;

        }
        else
        {
            return false;
        }
    }
    
    public function isCoupon() {
        if (!empty($this->coupon_end_time)) {
            if (time() > strtotime($this->coupon_end_time . ' 23:59:59')) {
                $this->coupon_status = 0;
            }else{
                $this->coupon_status = 1;
            }
        } else {
            $this->coupon_status = 0;
        }
        return $this->coupon_status > 0 ? true : false;
    }

    public static function getByNumIid($numIid) {
        return self::findOne(['original_id' => $numIid]);
    }

}
