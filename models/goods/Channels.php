<?php

namespace app\models\goods;

use Yii;

/**
 * This is the model class for table "channels".
 */
class Channels extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%channels}}';
    }

    public static function getByName($name) {
        return self::findOne(['name'=>$name]);
    }

}
