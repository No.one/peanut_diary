<?php

namespace app\models\pay;

use app\models\course\CourseMarketQrcode;
use Yii;

/**
 * This is the model class for table "pay_order_item".
 *
 * @property string $id
 * @property integer $pay_order_id
 * @property string $member_id
 * @property integer $is_free
 * @property integer $model
 * @property string $name
 * @property string $main_image
 * @property integer $quantity
 * @property string $price
 * @property string $discount
 * @property integer $target_type
 * @property integer $target_id
 * @property string $note
 * @property integer $status
 * @property integer $comment_status
 * @property string $updated_time
 * @property string $created_time
 */
class PayOrderItem extends \yii\db\ActiveRecord
{
    /**
     * 多表查询
     * @return \yii\db\ActiveQuery
     */
    public function getCourseMarketQrcode()
    {
        return $this->hasOne(CourseMarketQrcode::className(), ['order_id' => 'pay_order_id','order_item_id'=> 'id']);
    }

    public function getQrocde(){
        return $this->hasOne(CourseMarketQrcode::className(), ['order_id' => 'pay_order_id', 'order_item_id'=>'id']);
    }

    public function getPay_order(){
        return $this->hasOne(PayOrder::className(), ['id'=>'pay_order_id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pay_order_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_order_id', 'member_id', 'is_free', 'model', 'quantity', 'target_type', 'target_id', 'status', 'comment_status'], 'integer'],
            [['main_image', 'note'], 'required'],
            [['price', 'discount'], 'number'],
            [['note'], 'string'],
            [['updated_time', 'created_time'], 'safe'],
            [['name', 'main_image'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pay_order_id' => 'Pay Order ID',
            'member_id' => 'Member ID',
            'is_free' => 'Is Free',
            'model' => 'Model',
            'name' => 'Name',
            'main_image' => 'Main Image',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'discount' => 'Discount',
            'target_type' => 'Target Type',
            'target_id' => 'Target ID',
            'note' => 'Note',
            'status' => 'Status',
            'comment_status' => 'Comment Status',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
