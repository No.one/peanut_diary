<?php

namespace app\models\sms;

use app\common\services\UtilService;
use app\common\services\sms\SmsService;
use Yii;

/**
 * This is the model class for table "sms_captcha".
 *
 * @property integer $id
 * @property string $mobile
 * @property string $captcha
 * @property string $ip
 * @property string $expires_at
 * @property integer $status
 * @property string $created_time
 */
class SmsCaptcha extends \yii\db\ActiveRecord {

    public static function checkCaptcha($mobile, $captcha, $event) {
        $info = self::find()->where([ 'mobile' => $mobile, 'captcha' => $captcha, 'event'=>$event])->orderBy('id desc')->one();
        if ($info){
            return true;
        }
        return false;
    }
    
    public static function flush($mobile, $event){
        self::deleteAll(['mobile'=>$mobile, 'event'=>$event]);
        return true;
    }

    public function Captcha($mobile, $event) {
        $this->event = $event;
        $this->mobile = $mobile;
        $this->ip = UtilService::getIP();
        $this->captcha = rand(100000, 999999);
        $this->msg = '您的短信验证码是：' . $this->captcha;
        $this->expires_at = date("Y-m-d H:i:s", time() + 60 * 10);
        $this->created_time = date("Y-m-d H:i:s");
        $this->status = 0;

        $data = [
            'mobile' => $mobile,
            'msg' => $this->msg
        ];
        if (SmsService::send($data)) {
            $this->save(0);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%sms_captcha}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['expires_at', 'created_time'], 'safe'],
            [['status'], 'required'],
            [['status'], 'integer'],
            [['msg', 'event'], 'string', 'max' => 255],
            [['mobile', 'ip'], 'string', 'max' => 20],
            [['captcha'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'event' => 'Event',
            'mobile' => 'Mobile',
            'captcha' => 'Captcha',
            'msg' => 'Msg',
            'ip' => 'Ip',
            'expires_at' => 'Expires At',
            'status' => 'Status',
            'created_time' => 'Created Time',
        ];
    }

}
