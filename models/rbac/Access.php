<?php

namespace app\models\rbac;

use Yii;

/**
 * This is the model class for table "access".
 *
 * @property string $id
 * @property integer $fid
 * @property string $url
 * @property string $title
 * @property string $urls
 * @property string $icon
 * @property integer $weigh
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class Access extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%access}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fid', 'weigh', 'status'], 'integer'],
            [['url'], 'required'],
            [['updated_time', 'created_time'], 'safe'],
            [['url'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 50],
            [['urls'], 'string', 'max' => 1000],
            [['icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fid' => 'Fid',
            'url' => 'Url',
            'title' => 'Title',
            'urls' => 'Urls',
            'icon' => 'Icon',
            'weigh' => 'Weigh',
            'status' => 'Status',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
