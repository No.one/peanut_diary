<?php

namespace app\models\question;

use Yii;

/**
 * This is the model class for table "user_question".
 *
 * @property integer $id
 * @property integer $p_id
 * @property integer $member_id
 * @property string $member_name
 * @property integer $role
 * @property string $name
 * @property string $summary
 * @property integer $view_count
 * @property integer $status
 * @property string $updated_time
 * @property string $created_time
 */
class UserQuestion extends \yii\db\ActiveRecord
{
    public function getImage()
    {
        return $this->hasMany(QuestionImages::className(), ['question_id' => 'id']);
    }

    public function getMessage()
    {
        return $this->hasMany(UserQuestion::className(), ['p_id' => 'id'])->from(UserQuestion::tableName() . ' cate');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_question}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['p_id', 'member_id', 'role', 'view_count', 'status'], 'integer'],
            [['member_id', 'role', 'name', 'summary'], 'required'],
            [['summary'], 'string'],
            [['updated_time', 'created_time'], 'safe'],
            [['member_name'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'p_id' => 'P ID',
            'member_id' => 'Member ID',
            'member_name' => 'Member Name',
            'role' => 'Role',
            'name' => 'Name',
            'summary' => 'Summary',
            'view_count' => 'View Count',
            'status' => 'Status',
            'updated_time' => 'Updated Time',
            'created_time' => 'Created Time',
        ];
    }
}
