<?php

namespace app\models\question;

use Yii;

/**
 * This is the model class for table "question_images".
 *
 * @property string $id
 * @property integer $question_id
 * @property string $image_key
 * @property string $created_time
 */
class QuestionImages extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%question_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id'], 'required'],
            [['question_id'], 'integer'],
            [['created_time'], 'safe'],
            [['image_key'], 'string', 'max' => 500],
            [['image_key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'image_key' => 'Image Key',
            'created_time' => 'Created Time',
        ];
    }
}
