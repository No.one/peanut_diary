<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\common\services\UtilService;

class UploadForm extends Model {

    /**
     * @var UploadedFile[]
     */
    public $file;

    public function rules() {
        return [
            [['file'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ];
    }

    public function attributeLabels() {
        return [
            'file' => '多文件上传'
        ];
    }

    public function uploadMore($bucket = '') {
        $file_list = [];
        $upload_config = \Yii::$app->params['upload'];
        if ($this->file && $this->validate()) {
            foreach ($this->file as $file) {
                $file_name = mt_rand(1100, 9900) . time() . $file->baseName . '.' . $file->extension;
                $file->saveAs(UtilService::getRootPath() . "/web" .$upload_config[$bucket] . '/' . $file_name);
                $file_list[] = $file_name;
            }
            return $file_list;
        }else{
            return FALSE;
        }
    }

}
