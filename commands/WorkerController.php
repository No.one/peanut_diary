<?php
namespace app\commands;

use shmilyzxt\queue\Worker;

class WorkerController extends \yii\console\Controller
{

    /**
     * 队列监听进程
     * @param string $queueName 队列名称
     * @param int $attempt 任务尝试次数
     * @param int $memeory 允许使用最大内存
     * @param int $sleep 每次尝试从队列中获取任务的间隔时间
     * @param int $delay 代表把任务重新加入队列时是否延时（0代表不延时）
     * @throws \Exception
     */
    public function actionListen($queueName='default',$attempt=10,$memeory=128,$sleep=3 ,$delay=0){
        Worker::listen(\Yii::$app->queue,$queueName,$attempt,$memeory,$sleep,$delay);
    }
}