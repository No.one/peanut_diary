<?php

namespace app\commands;

use app\commands\BaseController;
use app\models\order\TaobaoOrder;
use app\models\member\Member;
use app\models\member\MemberGrade;
use app\models\order\TaobaoCommision;
use app\common\services\member\MemberService;
use \Exception;

class OrderController extends BaseController {

    /**
     * 定时计算订单收益 php yii commision
     */
    public function actionCommision() {
        $order_status = array_flip(TaobaoOrder::$orderStatus);
        $grade_model = new MemberGrade;
        $grade = $grade_model->getGrade();
        $list = TaobaoOrder::find()->with([
                    'member' => function($query) {
                        $query->with('invite')->where(['status' => 1])->select('id, grade_id, adzoneid');
                    }])->with(['commision' => function($query) {
                                $query->where(['is_balance'=>0])->select('order_id, status');
                            }])->andWhere(['OR', ['order_status' => "订单结算"], ['order_status' => "订单付款"], ['order_status' => "交易成功"]])->select('id, adv_id, balance_amount, income_ratio, estimated_revenue, create_time, order_status')
                        ->limit(500)
                        ->asArray()
                        ->all();
                foreach ($list as &$_order) {
                    if ($grade[$_order['member']['grade_id']]['ratio'] != '0.00') {
                        $father_list = Member::getFather($_order['member']['invite']['invite_id']);
                        if (!empty($_order['commision'])) {
                            $model = TaobaoCommision::find()->where(['is_balance' => 0, 'order_id' => $_order['id'], 'member_id' => $_order['member']['id']])->one();
                        } else {
                            $model = new TaobaoCommision();
                            $model->order_time = $_order['create_time'];
                            $model->member_id = $_order['member']['id'];
                            $model->order_id = $_order['id'];
                            $model->self_commision = $_order['estimated_revenue'] * $grade[$_order['member']['grade_id']]['ratio'];
                            if (!empty($father_list)) {
                                $higher_id = 0;
                                $operate_id = 0;
                                foreach ($father_list as &$_user) {
                                    if ($_user['grade_id'] == 2) {
                                        $higher_id = $_user['member_id'];
                                        break;
                                    }
                                }
                                foreach ($father_list as &$_user) {
                                    if ($_user['grade_id'] == 3) {
                                        $operate_id = $_user['member_id'];
                                        break;
                                    }
                                }
                                $higher_rate = 0;
                                if ($higher_id > 0) {
                                    $model->higher_id = $higher_id;
                                    $model->higher_commision = $_order['estimated_revenue'] * 0.1;
                                    $higher_rate = 0.1;
                                }
                                if ($operate_id > 0) {
                                    $model->operate_id = $operate_id;
                                    $model->operator_commision = $_order['estimated_revenue'] * (1 - $grade[$_order['member']['grade_id']]['ratio'] - $higher_rate - 0.18);
                                }
                            }
                        }
                        $model->status = $order_status[$_order['order_status']];
                        $model->save(0);
                    }
                }
                $this->echoLog("it's over ");
            }

        /**
         * 账户结算 每个月25号凌晨开始计算
         */
        public function actionBalance() {
            $last_start_day = date('Y-m-d 00:00:00', strtotime(date('Y-m-01') . ' -1 month'));
            $last_end_day = date('Y-m-d H:i:s', strtotime(date('Y-m-01') . ' -1 second'));
            $query = TaobaoCommision::find();
            $list = $query->with(['selfCommision' => function($query) {
                                    $query->where(['status'=>1])->select('id, nickname');
                                }])->with(['higherCommision' => function($query) {
                                    $query->where(['status'=>1])->select('id, nickname');
                                }])->with(['operatorCommision' => function($query) {
                                    $query->where(['status'=>1])->select('id, nickname');
                                }])->andWhere(['status' => 2])
                            ->andWhere(['between', 'order_time', $last_start_day, $last_end_day])
                            ->select('id, order_id, member_id, self_commision, higher_id, higher_commision, operate_id, operator_commision')
                            ->limit(500)
                            ->asArray()->all();
            foreach ($list as &$_order) {
                $this->userMoneyChange($_order);
            }
            $this->echoLog("it's over ");
        }

        private function userMoneyChange($_order) {
            $connection = Member::getDb();
            $transaction = $connection->beginTransaction();
            try {
                if (Member::updateAll(['money' => new \yii\db\Expression("`money` + " . $_order['self_commision'])], ['id' => $_order['selfCommision']['id']])) {
                    MemberService::setMoneyChangeLog($_order['selfCommision']['id'], +($_order['self_commision']), "订单佣金-" . $_order['order_id'],  1);
                }else{
                    $transaction->rollBack();
                    throw new Exception('结算失败');
                }
                
                if(!empty($_order['higherCommision'])){
                    if (Member::updateAll(['money' => new \yii\db\Expression("`money` + " . $_order['higher_commision'])], ['id' => $_order['higherCommision']['id']])) {
                        MemberService::setMoneyChangeLog($_order['higherCommision']['id'], +($_order['higher_commision']), "订单佣金-" . $_order['order_id'],  1);
                    }else{
                        $transaction->rollBack();
                        throw new Exception('结算失败');
                    }
                }
                
                if(!empty($_order['operatorCommision'])){
                    if (Member::updateAll(['money' => new \yii\db\Expression("`money` + " . $_order['operator_commision'])], ['id' => $_order['operatorCommision']['id']])) {
                        MemberService::setMoneyChangeLog($_order['operatorCommision']['id'], +($_order['operator_commision']), "订单佣金-" . $_order['order_id'],  1);
                    }else{
                        $transaction->rollBack();
                        throw new Exception('结算失败');
                    }
                }
                TaobaoCommision::updateAll(['is_balance'=>1], ['id'=>$_order['id']]);
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }

    }
        