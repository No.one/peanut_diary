<?php
namespace app\common\helpers;
/**
 * Created by PhpStorm.
 * User: kang
 * Date: 2018/5/23
 * Time: 13:53
 */
class Util {

    /**
     * 获取数组里的值
     * @param $array
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    public function array_get($array, $key, $default = null) {
        if (is_null($key)) {
            return $array;
        }
        if (isset($array[$key])) {
            return $array[$key];
        }
        foreach (explode('.', $key) as $segment) {
            if (!is_array($array) || !array_key_exists($segment, $array)) {
                return $default instanceof \Closure ? $default() : $default;
            }

            $array = $array[$segment];
        }
        return $array;
    }

}