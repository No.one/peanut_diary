<?php

namespace app\common\helpers;

use yii\base\Exception;

/**
 * Class Error 定义错误码、错误信息
 */
class Err
{
    const SUCCESS                 = 200; //请求ok
    const ERR_OPERATE_FAIL        = 201; //请求操作失败
    const ERR_PARAMS_ERROR        = 202; //参数错误
    const ERR_DATA_EXIST          = 203; //数据存在
    const ERR_DATA_NOT_EXIST      = 204; //数据不存在
    const ERR_QUICK_LOGIN         = 300; //第三方登录失败
    const ERR_TOKEN_ERROR         = 403; //token有误
    const ERR_SERVER_ERROR        = 500; //服务器内部错误
    const ERR_STOCK_NOT_ENOUGH    = 600; //库存不足
    const ERR_ORDER_FAIL          = 601; //下单失败请重新下单


    const ERR_MEMBER_PWD_ERROR    = 1001;

    private static $message = [
        self::SUCCESS                => "success",
        self::ERR_OPERATE_FAIL       => "系统繁忙",
        self::ERR_PARAMS_ERROR       => "参数有误",
        self::ERR_TOKEN_ERROR        => "登录信息有误",
        self::ERR_SERVER_ERROR       => "服务器内部有误",
        self::ERR_MEMBER_PWD_ERROR   => "账号密码不匹配",
        self::ERR_DATA_NOT_EXIST     => "数据不存在",
        self::ERR_STOCK_NOT_ENOUGH   => "库存不足",
        self::ERR_ORDER_FAIL         => "下单失败请重新下单",
    ];

    /**
     * 翻译 Error
     *
     * @param integer $code
     * @return string
     */
    public static function getMessage($code)
    {
        if (isset(static::$message[$code])) {
            return static::$message[$code];
        }

        // 不能识别的全部按内部错误处理
        return static::$message[self::ERR_SERVER_ERROR];
    }

    /**
     * 格式化 Exception
     *
     * @param \Exception $e
     * @param bool $detailed
     * @return string
     */
    public static function formatException($e, $detailed = true)
    {
        if (! $detailed) {
            return sprintf("%s(%d, %s)", get_class($e), $e->getCode(), $e->getMessage());
        } else {
            return sprintf("%s(%d, %s):\n%s", get_class($e), $e->getCode(), $e->getMessage(), $e->getTraceAsString());
        }
    }

    /**
     * 判断一个异常是否是数据库键重复异常
     *
     * @param Exception $e
     * @return bool
     */
    public static function isDuplicatedException($e)
    {
        return 23000 === $e->getCode() && false !== preg_match('/Duplicate/', $e->getMessage());
    }

    /**
     * 判断一个异常是否是网络连接超时异常
     *
     * @param Exception $e
     * @return bool
     */
    public static function isTimeoutException($e)
    {
        return 0 === $e->getCode() && false !== preg_match('/Connection timed out/', $e->getMessage());
    }
}
