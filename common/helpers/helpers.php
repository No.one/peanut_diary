<?php

/**
 * 检查网址是否带http/https
 * @param type $url
 * @return boolean
 */
function checkUrl($url) {
    if (preg_match("/^(http:\/\/|https:\/\/).*$/", $url)) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * @param string $url
 * @return mixed
 */
function doGet($url) {
    //初始化
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // 执行后不直接打印出来
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    // 跳过证书检查
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // 不从证书中检查SSL加密算法是否存在
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //执行并获取HTML文档内容
    $output = curl_exec($ch);
    //释放curl句柄
    curl_close($ch);
    $output = json_decode($output, TRUE);
    return $output;
}

/**
 * @param string $url
 * @param array $post_data
 * @param array | boolean $header
 * @return mixed
 */
function doPost($url, $post_data, $header) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    // 执行后不直接打印出来
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // 设置请求方式为post
    curl_setopt($ch, CURLOPT_POST, true);
    // post的变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    // 请求头，可以传数组
    curl_setopt($ch, CURLOPT_HEADER, $header);
    // 跳过证书检查
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // 不从证书中检查SSL加密算法是否存在
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    $output = curl_exec($ch);
    curl_close($ch);
    $output = json_decode($output, TRUE);
    return $output;
}

function object_array($array) {
    if (is_object($array)) {
        $array = (array) $array;
    } if (is_array($array)) {
        foreach ($array as $key => $value) {
            $array[$key] = object_array($value);
        }
    }
    return $array;
}

function post($key, $default = "") {
    return \Yii::$app->request->post($key, $default);
}

function get($key, $default = "") {
    return \Yii::$app->request->get($key, $default);
}

/**
 * 对 API 入参进行签名验证（所有参数按键名升序排列后，求 SHA1 值：SHA1( key=value&key2=value2...$transfer_key ) ）
 * @param  array $expectParams 需要参与签名验证的参数键名
 * @return void
 */
function verify_request_sign($expectParams) {
    if (post('verify')) {
        return true;
    } else {
        $transfer_key = \Yii::$app->params['api']['DATA_TRANSFER_KEY'];
        $reqSign = post('sign');
        $expSign = array();
        sort($expectParams);
        foreach ($expectParams as $expParam) {
            $expSign[] = $expParam . '=' . post($expParam);
        }
        $text = implode('&', $expSign) . $transfer_key;
        $expSign = sha1($text);
        if ($reqSign == $expSign) {
            return true;
        } else {
            \Yii::warning('Bad request sign: ' . $reqSign . '  Expecting: ' . $expSign);
            \Yii::warning('Text before signing: ' . $text);
            \Yii::warning('Original request: ' . serialize($_REQUEST));
        }
        return false;
    }
}
