<?php

namespace app\common\services;

use Yii;
use app\models\goods\Goods;
use TbkDgItemCouponGetRequest;
use TbkItemInfoGetRequest;
use TbkItemRecommendGetRequest;
use TbkTpwdCreateRequest;
use TbkUatmFavoritesItemGetRequest;
use TopClient;
use ETaobao\Factory;

class TaoBaoService {

    private $client;
    private $error = '商品不存在';
    private $total;
    private $pageNo;
    private $pages;
    private $page_size = 20;
    private $three_url = "http://193.112.121.99/xiaocao";
    private $three_qq = "1079516751";
    private $appkey = "107951675120180822";

    /**
     * TaoBao constructor.
     */
    public function __construct() {
        $this->client = new TopClient(Yii::$app->params['tbk']['appkey'], Yii::$app->params['tbk']['secretKey']);
        $this->client->format = 'json';

        $config = [
            'appkey' => Yii::$app->params['tbk']['appkey'],
            'secretKey' => Yii::$app->params['tbk']['secretKey'],
            'format' => 'json',
            'sandbox' => false,
        ];
        $this->app = Factory::Tbk($config);
    }

    /**
     * @return mixed
     */
    public function getTotal() {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total) {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getPageNo() {
        return $this->pageNo;
    }

    /**
     * @param mixed $pageNo
     */
    public function setPageNo($pageNo) {
        $this->pageNo = $pageNo;
    }

    /**
     * @return mixed
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * @param mixed $pages
     */
    public function setPages($pages) {
        $this->pages = $pages;
    }

    /**
     * @return string
     */
    public function getError() {
        return string($this->error);
    }

    /**
     * @param string $error
     */
    public function setError(string $error) {
        $this->error = $error;
    }

    public function item($numIid) {
        if (empty($numIid)) {
            $this->error = '非法的num_iid';
            return false;
        }
        $req = new TbkItemInfoGetRequest();
        $req->setFields("num_iid,title,pict_url,small_images,zk_final_price,item_url,volume");
        $req->setNumIids($numIid);
        $resp = $this->client->execute($req);
        if (!empty($resp->results->n_tbk_item)) {
            $items = $resp->results->n_tbk_item;
            foreach ($items as $row) {
                return $this->itemToModel($row);
            }
            return null;
        } else {
            if (isset($resp->code)) {
                $this->error = $resp->code;
                return false;
            }
            return null;
        }
    }

    public function tpwd($text, $url) {
        $req = new TbkTpwdCreateRequest;
        $req->setText($text);
        $req->setUrl($url);
        $req->setExt("{}");
        $resp = $this->client->execute($req);
        if (isset($resp->code)) {
            return '';
        }
        return $resp->data->model;
    }

    public function recommend($numIid) {
        $req = new TbkItemRecommendGetRequest;
        $req->setFields("num_iid,title,pict_url,small_images,reserve_price,zk_final_price,user_type,provcity,item_url,volume");
        $req->setNumIid($numIid);
        $req->setCount("20");
        $req->setPlatform("1");
        $resp = $this->client->execute($req);
        if (!empty($resp->results->n_tbk_item)) {
            $items = $resp->results->n_tbk_item;
            $list = new Collection();
            foreach ($items as $row) {
                $goods = $this->itemToModel($row);
                $list->add($goods);
            }
            return $list;
        } else {
            if (isset($resp->code)) {
                $this->error = $resp->code;
                return false;
            }
            return null;
        }
    }

    public function likeGuess($os, $ip, $ua, $net) {
        $param = [
            'adzone_id' => (string) Yii::$app->params['tbk']['zoneId'],
            'os' => $os,
            'ip' => $ip,
            'ua' => $ua,
            'net' => $net,
        ];
        $resp = $this->app->item->likeGuess($param);
        return $resp;
    }

    /**
     * 获取淘宝联盟选品库列表
     */
    public function getFavorites() {
        $param = [
            'fields' => 'favorites_title,favorites_id,type',
            'page_no' => '1',
            'page_size' => '100',
            'type' => '-1'
        ];
        $resp = $this->app->uatm->getFavorites($param);
        if ($resp) {
            foreach ($resp->results->tbk_favorites as $row) {
                $favourites[$row->favorites_id] = $row->favorites_title;
            }
            Yii::$app->session->set("favorites", $favourites);
        }
    }

    /**
     * 获取选品库里商品
     * @param type $favoriteId
     * @param type $pageNo
     * @return boolean
     */
    public function getItemFavorites($favoriteId, $pageNo) {
        $param = [
            "platform" => 1,
            "page_size" => $this->page_size,
            "adzone_id" => Yii::$app->params['tbk']['zoneId'],
            "favorites_id" => $favoriteId,
            "page_no" => $pageNo,
            "fields" => "num_iid,title,user_type,pict_url,small_images,reserve_price,zk_final_price,coupon_click_url,coupon_start_time,item_url,coupon_end_time,coupon_remain_count,coupon_info,click_url,status,volume,tk_rate"
        ];
        $resp = $this->app->uatm->getItemFavorites($param);
        if ($resp) {
            $total = $resp->total_results;
            $pageTotal = $total / $this->page_size;
            $this->total = $total;
            $this->pages = ceil($pageTotal);
            $this->pageNo = $pageNo;
            $items = $resp->results->uatm_tbk_item;
            $list = [];
            
            foreach ($items as $row) {
                $row->zk_final_price = floatval($row->zk_final_price);
                if (isset($row->coupon_info)) {
                    preg_match_all('/\d+/', $row->coupon_info, $matches);
                    if ($matches) {
                        $row->coupon_amount = $matches[0][1];
                    } else {
                        $row->coupon_amount = 0;
                    }
                    $row->coupon_price = floatval($row->zk_final_price - $row->coupon_amount);
                }
                $goods = $this->itemToModel($row);
                $list[] = $goods;
            }
            return $list;
        } else {
            if (isset($resp->code)) {
                $this->error = $resp->code;
            }
            return false;
        }
    }

    /**
     * 构造商品对象
     * @param type $item
     * @return Goods
     */
    protected function itemToModel($item) {
        $goods = Goods::getByNumIid($item->num_iid);
        if (!empty($goods)) {
            $goods_model = $goods;
        } else {
            $goods_model = new Goods();
            $goods_model->id = 0;
        }
        $goods_model->name = $item->title;
        $goods_model->user_type = $item->user_type;
        $goods_model->pictures = isset($item->small_images->string) ? json_encode($item->small_images->string) : '';
        $goods_model->tk_rate = $item->tk_rate;
        $goods_model->price = $item->zk_final_price;
        $goods_model->original_id = $item->num_iid;
        $goods_model->item_url = $item->item_url;
        $goods_model->cover = $item->pict_url;
        $goods_model->volume = $item->volume;
        $goods_model->status = 1;
        if (isset($item->coupon_info)) {
            $goods_model->coupon_info = $item->coupon_info;
        }
        if (isset($item->coupon_end_time)) {
            $goods_model->coupon_end_time = $item->coupon_end_time;
        }
        if (isset($item->coupon_amount)) {
            $goods_model->coupon_amount = $item->coupon_amount;
        }
        if (isset($item->coupon_start_time)) {
            $goods_model->coupon_start_time = $item->coupon_start_time;
        }
        if (isset($item->coupon_price)) {
            $goods_model->coupon_price = $item->coupon_price;
        }
        if (isset($item->coupon_status)) {
            $goods_model->coupon_status = $item->coupon_status;
        }
        if (isset($item->coupon_click_url)) {
            $goods_model->coupon_click_url = $item->coupon_click_url;
        }
        if (isset($item->coupon_remain_count)) {
            $goods_model->coupon_remain_count = $item->coupon_remain_count;
        }
        if (isset($item->coupon_start_fee)) {
            $goods_model->coupon_start_fee = $item->coupon_start_fee;
        }
        if (isset($item->click_url)) {
            $goods_model->click_url = $item->click_url;
        }
        if ($goods_model->isCoupon()) {
            $goods_model->coupon_status = 1;
        }
        return $goods_model;
    }

    /**
     * @param $keywords
     * @param int $perPageSize
     * @return bool|LengthAwarePaginator
     */
    public function searchCoupon($keywords, $perPageSize = 10) {
        $page = request()->page;
        $req = new TbkDgItemCouponGetRequest();
        $req->setQ($keywords);
        $req->setAdzoneId(Yii::$app->params['tbk']['zoneId']);
        $req->setPlatform('1');
        $req->setPageSize('16');
        $req->setPageNo($page);
        $resp = $this->client->execute($req);
        $result = [];
        if (empty($resp->code)) {
            if ($resp->total_results > 0) {
                if (isset($resp->results)) {
                    $list = new Collection();
                    $data = $resp->results->tbk_coupon;
                    if ($data) {
                        foreach ($data as $k => $v) {
                            preg_match_all('/\d+/', $v->coupon_info, $matches);
                            if ($matches) {
                                $v->coupon_amount = $matches[0][1];
                            } else {
                                $v->coupon_amount = 0;
                            }
                            $v->zk_final_price = floatval($v->zk_final_price);
                            $v->coupon_price = floatval($v->zk_final_price - $v->coupon_amount);
                            $v->coupon_status = 1;
                            $list->add($this->itemToModel($v));
                        }
                        $paginator = new LengthAwarePaginator($list, $resp->total_results, 16, $req->getPageNo());
                        $paginator->appends(['keywords' => $keywords]);
                        $paginator->setPath('/search/coupon');
                        return $paginator;
                    }
                }
            }
        } else {
            $this->error = $resp->msg;
        }
        return new LengthAwarePaginator('', 0, 16, $req->getPageNo());
    }

    public function getItemDetail($item_num_id) {
        $data = json_encode(['item_num_id' => $item_num_id]);
        $url = "http://hws.m.taobao.com/cache/mtop.wdetail.getItemDescx/4.1/?&data=$data&type=json";
        $result = doGet($url);
        return $result['data']['images'];
    }
    
    /**
     * 获取商品折扣
     */
    public function getItemDiscount($item_id, $adzone_id, $site_id){
        $url = $this->three_url . "/hightapi.action";
        $post_data = [
            "token" => Yii::$app->params['tbk']['token'],
            "item_id" => $item_id,
            "adzone_id" => $adzone_id,
            "site_id" => $site_id,
            "qq" => $this->three_qq,
        ];
        $res = doPost($url, $post_data, NULL);
        return $res['result']['data'];
    }
    
    /**
     * 无券商品转换
     */
    public function getItemUrl($e){
        $url = $this->three_url . "/wuquan.action";
        $post_data = [
            "e" => $e,
            "appkey" => $this->appkey,
            "qq" => $this->three_qq,
        ];
        $res = doPost($url, $post_data, NULL);
        return $res['iteminfo']['data']['result'];
    }
   
}
