<?php
namespace app\common\services\course;


use app\common\services\BaseService;
use app\common\services\UtilService;
use app\models\course\CourseMarketQrcode;
use app\models\pay\PayOrder;
use app\models\pay\PayOrderItem;
use dosamigos\qrcode\lib\Enum;
use dosamigos\qrcode\QrCode;

class CourseService extends BaseService
{
    /**
     * 生成线下课程二维码
     * @param $_item
     */
    public static function createCourseQrcode($order_info, $order_item)
    {
        $date_now = date("Y-m-d H:i:s");
        $course_market_qrcode_info = CourseMarketQrcode::find()
            ->where(['order_id' => $order_info['id']])
            ->andWhere(['member_id' => $order_info['member_id']])->one();
        if (!$course_market_qrcode_info) {
            $course_market_qrcode_info = new CourseMarketQrcode();
        }

        $bucket = 'qrcode';
        $upload_config = \Yii::$app->params['upload'];
        $upload_dir_path = UtilService::getRootPath() . "/web" . $upload_config[$bucket] . "/";
        $folder_name = date('Ymd', strtotime($order_info['created_time']));
        $upload_dir = $upload_dir_path . $folder_name;

        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777);
            chmod($upload_dir, 0777);
        }

        $upload_file_name = md5($order_info['order_sn'] . "#" . $order_item['id']) . ".png";
        //课程线下扫码链接
        $qr_code_url = \Yii::$app->params['domain']['m']
            . "/course/sign?member_id=" . $order_info['member_id']
            . "&order_id=" . $order_info['id']
            . "&order_sn=" . $order_info['order_sn']
            . "&order_item_id=" . $order_item['id'];
        $filename = $upload_dir . '/' . "$upload_file_name";

        QrCode::png($qr_code_url, $filename, Enum::QR_ECLEVEL_H, 5, 0);

        $course_market_qrcode_info->member_id = $order_info['member_id'];
        $course_market_qrcode_info->order_id = $order_info['id'];
        $course_market_qrcode_info->order_item_id = $order_item['id'];
        $course_market_qrcode_info->qrcode = $order_info['note'];
        $course_market_qrcode_info->qrcode_image = $folder_name . "/" . $upload_file_name;
        $course_market_qrcode_info->status = 0;
        $course_market_qrcode_info->total_scan_count = 0;
        $course_market_qrcode_info->updated_time = $date_now;
        $course_market_qrcode_info->created_time = $date_now;
        return $course_market_qrcode_info->save(0);
    }

    /**
     * 查看用户是否购买过这个商品
     * 1：查看视频 2购买  3报名  4显示二维码
     */
    public static function course_has_buyed($info, $current_user)
    {
        $data['qrcode_image'] = '';
        switch ($info['model']) {
            case 0://线下
                $pay_order_info = PayOrderItem::find()->joinWith('qrocde')
                    ->where([PayOrderItem::tableName() . '.member_id' => $current_user['id']])
                    ->andWhere([PayOrderItem::tableName() . '.target_id' => $info['id']])
                    ->andWhere([CourseMarketQrcode::tableName() . '.status' => 0])->one();
                if ($info['is_free'] == 1) {//免费
                    if ($pay_order_info){
                        $data['qrcode_image'] = $pay_order_info['qrocde']['qrcode_image'];
                        $data['state'] = 4;
                    } else {
                        $data['state'] = 3;
                    }
                } else {//收费
                    if ($pay_order_info){
                        $data['qrcode_image'] = $pay_order_info['qrocde']['qrcode_image'];
                        $data['state'] = 4;
                    } else {
                        $data['state'] = 2;
                    }
                }
                break;
            case 1://线上
                //免费
                if ($info['is_free'] == 1) {//免费
                    $data['state'] = 1;
                } else {//收费
                    $pay_order_info = PayOrderItem::find()->joinWith('pay_order')->where([PayOrderItem::tableName().'.member_id' => $current_user['id']])
                        ->andWhere(['target_id' => $info['id']])->andWhere([PayOrder::tableName().'.status'=>1])->asArray()->one();
                    if ($pay_order_info){
                        if ($pay_order_info['pay_order']['status'] == 1 && $pay_order_info['pay_order']['express_status'] != 0) {
                            $data['state'] = 1;
                        } else {
                            $data['state'] = 2;
                        }
                    } else {
                        $data['state'] = 2;
                    }
                }
                break;
        }
        return $data;
    }
}