<?php
namespace app\common\services;


class ConstantMapService
{

    public static $client_type_wechat = 1;

    public static $default_avatar = 'default_avatar';
    public static $default_password = '******';
    public static $default_time_stamps = '0000-00-00 00:00:00';
    public static $default_syserror = '系统繁忙，请稍后再试';
    public static $default_nodata = '暂无数据';
    public static $default_no_auth = "暂无权限";

    public static $status_default = -1;

    public static $status_mapping = [
        1 => '正常',
        0 => '已删除'
    ];

    public static $goods_type = [
        1 => '3罐装',
        2 => '6罐装'
    ];

    public static $status_grade = [
        1 => '正常',
        0 => '禁止'
    ];

    public static $status_goods = [
        1 => '正常',
        0 => '下架'
    ];
    
    public static $status_marketing = [
        1 => '正常',
        0 => '禁止'
    ];

    public static $course_free_mapping = [
        1 => '<label class="tag-free">免费</label>',
        0 => '<label class="tag-charge">收费</label>',
    ];

    public static $course_model_mapping = [
        1 => '<label class="tag-online">线上</label>',
        0 => '<label class="tag-line">线下</label>',
    ];

    public static $bind_state = [
        0 => '未绑定',
        1 => '已绑定'
    ];

    // 1卖家负责 （免费）  2买家负责（收费）
    public static $product_who_pay_ship = [
        1 => '快递包邮',
        2 => '快递：'
    ];

    public static $commodity_status = [
        1 => "商品",
        2 => "课程"
    ];

    public static $course_qrcode_status = [
        0 => "未使用",
        1 => "已使用",
    ];

    public static $sex_mapping = [
        1 => '男',
        2 => '女',
        0 => '未填写'
    ];

    public static $pay_status_mapping = [
        1 => '<i class="status-2">已支付</i>',
        -8 => '<i class="status-1">待支付</i>',
        0 => '<i class="status-3">已关闭</i>'
    ];
    
    public static $integral_status_mapping = [
        0 => '<i class="status-1">待发货</i>',
        1 => '<i class="status-2">配送中</i>',
        2 => '<i class="status-3">已完成</i>'
    ];


    public static $express_status_mapping = [
        0 => '待发货',
        1 => '发货中',
        2 => '已完成',
        
    ];
//<!--状态class区分 status-1 待付款 status-2 运输中 status-3 已完成-->
    public static $express_status_mapping_for_member = [
        1 => '已签收',
        -6 => '已发货',
        -7 => '待发货',
        -8 => '待支付',
        0 => '已关闭'
    ];
    /**
     * 快捷登录方
     * @var array
     */
    public static $client_type_mapping = [
        1 => 'weixin',
        2 => 'qq',
        3 => 'weibo',
    ];

}