<?php

namespace app\common\services;

class QueueListService extends BaseService {

    /**
     * 队列
     * @param $obj 调用方法对象
     * @param array $data 数据
     * @param $queue_name 队列名称
     * @return mixed
     * yii worker/listen email 10 128 3 0
     */
    public static function addQueue($dealy = 0, $obj, $data = [], $queue_name) {
        if ($dealy > 0) {
            return \Yii::$app->queue->laterOn($dealy, $obj, $data, $queue_name);
        } else {
            return \Yii::$app->queue->pushOn($obj, $data, $queue_name);
        }
    }

}
