<?php

namespace app\common\services\member;

use app\common\services\BaseService;
use app\models\member\Member;
use app\models\member\MemberMoneyChangeLog;
use app\models\member\MemberScoreChangeLog;

class MemberService extends BaseService {

    public static function setMoneyChangeLog($member_id = 0, $unit = 0, $note = '', $type) {
        if (!$member_id || !$unit) {
            return false;
        }
        $info = Member::find()->where([ 'id' => $member_id])->one();
        if (!$info) {
            return false;
        }
        $model_moeny = new MemberMoneyChangeLog();
        $model_moeny->member_id = $member_id;
        $model_moeny->type = $type;
        $model_moeny->unit = $unit;
        $model_moeny->total_money = $info['money'];
        $model_moeny->note = $note;
        $model_moeny->created_time = date("Y-m-d H:i;s");
        return $model_moeny->save(0);
    }
    
    public static function setScoreChangeLog($member_id = 0, $unit = 0, $note = '') {
        if (!$member_id || !$unit) {
            return false;
        }
        $info = Member::find()->where([ 'id' => $member_id])->one();
        if (!$info) {
            return false;
        }
        $model_moeny = new MemberScoreChangeLog();
        $model_moeny->member_id = $member_id;
        $model_moeny->unit = $unit;
        $model_moeny->total_score = $info['score'];
        $model_moeny->note = $note;
        $model_moeny->created_time = date("Y-m-d H:i;s");
        return $model_moeny->save(0);
    }

}
