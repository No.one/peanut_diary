<?php
namespace app\common\services\email;

use app\common\services\BaseService;

class EmailService extends BaseService
{
    /**
     * 短信发送
     * @param type $data
     * @return boolean
     */
    public static function send($data){
        $mailer= \Yii::$app->mailer->compose($data['event'],$data['data']);
        $mailer->setTo($data['email']);
        $mailer->setSubject($data['subject']);
        return $mailer->send();
    }

}