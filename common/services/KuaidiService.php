<?php
namespace app\common\services;

use app\common\services\BaseService;
use Yii;

class KuaidiService extends BaseService {
    
    public static function getExpress($com, $num){
        $post_data = array();
        $post_data["customer"] = Yii::$app->params['kuaidi100']['customer'];
        $key= Yii::$app->params['kuaidi100']['key'];
        $post_data["param"] = json_encode(['com'=>$com, 'num'=>$num]);

        $url= Yii::$app->params['kuaidi100']['url'];
        $post_data["sign"] = md5($post_data["param"].$key.$post_data["customer"]);
        $post_data["sign"] = strtoupper($post_data["sign"]);
        $o="";
        foreach ($post_data as $k=>$v)
        {
            $o.= "$k=".urlencode($v)."&";		//默认UTF-8编码格式
        }
        $post_data=substr($o,0,-1);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $data = str_replace("\"",'"',$result );
        curl_close($ch);
        return json_decode($data,true);
    }
    
}
