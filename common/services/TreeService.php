<?php
namespace app\common\services;

use app\common\components\HttpClient;

use Yii;

class TreeService extends BaseService
{
    /**
     * 无限分类树形图
     * @param $list
     * @param int $id
     * @param int $lev
     * @return array
     */
    public static function subtree($list, $id = 0, $lev = 1)
    {
        $subs = array(); // 子孙数组
        foreach ($list as $v) {
            if ($v['fid'] == $id) {
                $v['lev'] = $lev;
                $subs[] = $v;
                $subs = array_merge($subs, self::subtree($list, $v['id'], $lev + 1));
            }
        }
        return $subs;
    }

    public static function subtreechild($list, $id = 0, $lev = 1)
    {
        $subs = array(); // 子孙数组
        foreach ($list as $k=>$v) {
            if ($v['fid'] == $id) {
                $v['lev'] = $lev;
                $v['class'] = "";
                $class = explode("/",$v['url']);
                $v['class'] = $class[0];
                $subs[$k] = $v;
                $subs[$k]['child'] = self::subtreechild($list, $v['id'], $lev + 1);
            }
        }
        return $subs;
    }
}