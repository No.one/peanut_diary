<?php
namespace app\common\services\sms;


use app\common\components\HttpClient;
use app\common\services\BaseService;

class SmsService extends BaseService
{
    /**
     * 短信发送
     * @param type $data
     * @return boolean
     */
    public static function send($data){
        $sms_account = \Yii::$app->params['sms']['common'];
        $post_data = array();
        $post_data['account'] = $sms_account['account'];
        $post_data['pswd'] = $sms_account['pswd'];
        $post_data['mobile'] = $data['mobile'];
        $post_data['msg']= $data['msg']; 
        $post_data['needstatus'] ="true";
        $post_data['resptype'] = 'json';
        $o = "";
        foreach ($post_data as $k => $v) {
            $o.= "$k=" . urlencode($v) . "&";
        }
        $postData = substr($o, 0, -1);
        $result = json_decode(HttpClient::post($sms_account['url'], $postData), TRUE);
        if($result['result'] == 0){
            return true;
        }else{
            return false;
        }
    }

}