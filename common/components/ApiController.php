<?php

namespace app\common\components;

use app\models\member\Member;
use app\models\member\MemberToken;
use app\common\helpers\Err;
use yii\helpers\Json;
use yii\web\Controller;

class ApiController extends Controller {

    protected $page_size = 10;
    public $enableCsrfValidation = false;
    public $member_id = 0;
    public $config = [];
    private $token = '';
    private $params = [];
    private $outputData = [];
    protected  $current_user = null;

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action) {
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Methods:OPTIONS, GET, POST'); // 允许option，get，post请求
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Token"); // 允许x-requested-with请求头
        header("Content-Type: application/json");
        if(strtoupper($_SERVER['REQUEST_METHOD'])=='OPTIONS'){
            exit();
        }
        $this->token = !empty(post('token')) ? post('token') : '';
        if ($this->checkLogin()) {  //即需要登录的方法
            //token验证
            if (!$this->token || !$this->checkToken($this->token)) {
                $this->setApiResult(Err::ERR_TOKEN_ERROR, Err::getMessage(Err::ERR_TOKEN_ERROR));
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * 验证token
     * @param $token
     * @return bool
     */
    private function checkToken($token) {
        $userToken = MemberToken::find()
                ->alias('t')
                ->innerJoin('{{%member}} m', 't.member_id=m.id')
                ->where(['t.token' => $token])
                ->select('m.id as member_id, m.grade_id, m.money, m.score, t.expire_time, m.tb_pid, m.adzoneid')
                ->asArray()
                ->one();
        $status = !empty($userToken) && $userToken['expire_time'] >= time();
        if ($status) {
            $this->current_user = $userToken;
            $this->member_id = $userToken['member_id'];
        }
        return $status;
    }

    /**
     * 验证是否需要登录
     * @return bool
     */
    private function checkLogin() {
        $actions = [];
        if (isset($this->config['handle_login']) && !empty($this->config['handle_login'])) {
            $actions = implode(',', $this->config['handle_login']);
            $actions = strtolower($actions);
            $actions = explode(',', $actions);
        }

        return !in_array(strtolower(\Yii::$app->controller->action->id), $actions);
    }

    /**
     * API返回结果数组拼装
     * @param $code int  错误码
     * @param $message  string  错误信息
     * @param array $data  array  返回参数
     */
    public function setApiResult($code, $message, $data = []) {
        $this->outputData = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
            'current_time' => date("Y-m-d H:i:s"),
        ];
        echo json_encode($this->outputData);
        exit();
    }

    /**
     * 获取请求参数,有默认给默认值
     * @param $name
     * @param null $defaultValue
     * @return mixed|null
     */
    public function getParam($name, $defaultValue = null) {
        return isset($this->params[$name]) && !empty($this->params[$name]) ? $this->params[$name] : $defaultValue;
    }

    /**
     * 获取DynamicModel错误信息
     * @param $error array
     * @return mixed|string
     */
    public function getErrMessage($error) {
        $msg = '';
        if (is_array($error)) {
            foreach ($error as $err_msg) {
                $msg = $err_msg;
            }
        }
        return $msg;
    }

    

}
