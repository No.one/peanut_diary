<?php
/**
 * yii worker/listen email 10 128 3 0
 */

namespace app\common\handler;
use shmilyzxt\queue\base\JobHandler;
use app\common\services\email\EmailService;

class SendMailHandler extends JobHandler {

    public function handle($job, $data) {
        if ($job->getAttempts() > 3) {
            $this->failed($job);
        }

        $payload = $job->getPayload();
        echo date('Y-m-d H:i:s');
        switch ($data['event']) {
            case 'notice':
                $this->_notice($data['data']);
                break;
            default:
                break;
        }
    }

    public function failed($job, $data) {
        die("发了3次都失败了，算了");
    }

    public function _notice($data) {
        $post_data['email'] = $data['email'];
        $post_data['event'] = $data['notice'];
        $post_data['subject'] = $data['subject'];
        $post_data['data'] = ['msg' => $data['msg']];
        EmailService::send($post_data);
    }

}
