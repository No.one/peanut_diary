<?php

/**
 * yii worker/listen xxx 10 128 3 0
 */
namespace app\common\handler;

use shmilyzxt\queue\base\JobHandler;
use app\models\member\Member;
use app\models\member\Pid;
use app\common\handler\SendMailHandler;

class MemberHandler extends JobHandler {

    public function handle($job, $data) {
        if ($job->getAttempts() > 3) {
            $this->failed($job);
        }
        $payload = $job->getPayload();
        echo date('Y-m-d H:i:s');
        switch ($data['event']) {
            case 'pid':
                $this->_pid($data['data']);
                break;
            default:
                break;
        }
    }

    public function failed($job, $data) {
        die("发了3次都失败了，算了");
    }

    public function _pid($data) {
        $member_id = $data['member_id'];
        $pid = new Pid();
        $pid_model = Pid::find()
                        ->andWhere(['status' => 0])
                        ->andWhere(['<', $pid->pesstimisticLock(), time() - $pid->maxLockTime()])
                        ->orderBy(['sysid' => SORT_ASC])->one();
        if ($pid_model) {
            if ($pid_model->lock()) {
                $model = Pid::findOne(['sysid' => $pid_model['sysid']]);
                $member = Member::findOne($member_id);
                $member->adzoneid = $model->adzoneid;
                $member->tb_pid = $model->adzonePid;
                if ($member->save(0)) {
                    $model->status = 1;
                    $model->save(0);
                    $pid_count = Pid::find()->where(['status'=>0])->count();
                    if($pid_count < 10){
                        \Yii::$app->queue->pushOn(new SendMailHandler(), [
                            'event' => 'notice', 
                            'data' => [
                                'email' => '1252138037@qq.com', 
                                'event'=>'notice', 
                                'subject'=>'系统通知', 
                                'msg'=>"淘宝推广pid不足10个，请尽快添加"
                                ]
                            ], 'email');
                    }
                }
            }
        }
    }

}
